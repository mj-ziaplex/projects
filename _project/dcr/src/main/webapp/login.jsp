<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
       
    <base href="">
    <title><fmt:message key="login.signin.title"/></title>
    <link rel='stylesheet' href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/Wcm_CP.css" type='text/css'>
    <script>
       var baseURL = "${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}";
    </script>
    <script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/Wcm.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name='BuildName' content=''>
    <meta name='BuildDate' content=''>
    <meta name='Copyright' content='� Copyright IBM Corp. 2002, 2009.  All Rights Reserved.'>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
    </head>

    <body class="wcmBody" bgcolor="white">

    <!-- banner table -->
    <table width='100%' cellspacing='0' cellpadding='0' border='0'>
         <tr>
            <td valign="top" width="25">
                <img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/p8MiddleLogOnBar.jpg" width="25" height="140" border="0" />
            </td>
            <td valign="top" align="left" width="304">
                <img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/p8LeftLogOnBar.jpg" width="304" height="140" border="0" />
            </td>
            <td valign="top" width="100%">
                <img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/p8MiddleLogOnBar.jpg" width="110%" height="140" border="0" />
            </td>
            <td valign="top" align="right">
                <img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/p8RightLogOnBar.jpg" width="354" height="140" border="0" />
            </td>
        </tr>
    </table>

<!-- form begins -->
<form onkeypress="return checkEnter(event)" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/login/login.html" method="POST" name="loginForm" id="loginForm">
<input type='image' src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/Spacer.gif" border='0' height='0' width='0' style='height: 0; width: 0; '/>
<br><br><br>

<!-- sign-in box -->
<table align="center" cellspacing="1" cellpadding="0" border="0" class="wcmSignInBorder">

<!-- header table -->
<tr>
    <td>
        <table width="100%" cellspacing="1" class="wcmSignInBorderBackground">
            <tr>
                <td width='1%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/Spacer.gif" width="8" height="24"></td>

                <td width='99%'><fmt:message key="login.signin.dcr"/></td>

            </tr>
        </table>
    </td>
</tr>

<tr>
    <td>
        <table border="0" class="wcmSignInFormBackground" cellspacing="0" cellpadding="10" align="center">

            <tr>
                <td valign="top"><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/Spacer.gif" height="1" width="35"/></td>
                <td valign="top" align="right">
                    <br>
                    <table border="0" cellpadding="2" cellspacing="0" valign="top" >
                        <tr>
                            <td class="wcmFormText" align="right"><fmt:message key="login.signin.input.name"/>&nbsp;</td>
                            <td><input onkeydown='' class='wcmFormInput' type='text' id='userId' name='userId' value='' size='30'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="wcmFormText" align="right"><fmt:message key="login.signin.input.password"/>&nbsp;</td>
                            <td><input onkeydown='' class='wcmFormInput' type='password' id='password' name='password' size='30' autocomplete='off'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="wcmFormText" align="right"><fmt:message key="login.signin.input.sitecode"/>&nbsp;</td>
                            <td><input onkeydown='' class='wcmFormInput' type='text' id='siteCode' name='siteCode' value='' size='30'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="wcmFormText" align="right"><fmt:message key="login.signin.input.role"/>&nbsp;</td>
                            <td>
                            	<select name='selectedRole' class='wcmFormInput'>
                            		<option value="cashier" selected>Cashier</option>
									<option value="manager">Manager</option>
									<option value="ppa">CCQA</option>
									<option value="guest">Finance</option>
                            	</select>
                            </td>
                        </tr>                        
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left" class="wcmFormText">
                                <br>
                                <a class='wcmLink' href='javascript:confirmSubmit()'><fmt:message key="login.signin.input.signin"/></a>
                                | <a class='wcmLink' onClick="resetForm()" href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/login/index.html'><fmt:message key="login.signin.input.rest"/></a>

                                | <a class='wcmLink' href='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/login/index.html'><fmt:message key="login.signin.input.help"/></a>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" class="wcmErrorText">
                            	<br/>
                            	${errorMsg}
                            </td>
                        </tr>                        
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/web/common/Spacer.gif" width="1" height="3">
                </td>
            </tr>
        </table>

    </td>
</tr>
</table>

</form>

<script>
function checkEnter(e){
	 e = e || event;
	 var isEnterPressed = !((e.keyCode || event.which || event.charCode || 0) !== 13);
	 
 	if (isEnterPressed) {
 		confirmSubmit();
 		return false;
 	}
 	
}

function resetForm(){
	   document.getElementById("loginForm").reset();
} 

function trim(str) {
    return str.replace(/^\s+|\s+$/g,"");
}

<!--
if (document.getElementById)
{
    var e = document.getElementById("userId")
    if (e != null)
        e.focus();
}
//-->

function confirmSubmit() {
  var userId = document.getElementById('userId').value;
  var password = document.getElementById('password').value;

  if (userId.length == 0 || password.length == 0) {
	  alert("Name, Password and Site Code can't be blank");
	  return;
  }
  
  var siteNames = ([
          			<c:forEach items="${ccNames}" var="ccName" varStatus="rowCnt">
    		    		'${ccName}'
	    				<c:if test="${not rowCnt.last}">
	    		        	,
	    				</c:if>
    				</c:forEach>
                      ]);

  var siteCode = trim(document.getElementById('siteCode').value);
  var siteName = '';
  var isSiteCodeValid = false;
  
  if (siteCode.length == 3) {
  	for (var i = 0; i < siteNames.length; i++) {
  		if (siteNames[i].toLowerCase().length >= siteCode.length && 
  				siteNames[i].toLowerCase().substring(0, siteCode.toLowerCase().length) == siteCode.toLowerCase()) {
  			siteName = siteNames[i];
  			isSiteCodeValid = true;
  		}
	}
  	
	var confirmationMessage = "";
  	if (isSiteCodeValid) {
  	  	confirmationMessage = "Are you sure you want to access Collections for " + siteName + "?";
  	  	
  	  	if (confirm(confirmationMessage)) {
   		 	document.loginForm.submit();
   	  	}
  	} else {
  		confirmationMessage = siteCode + " is not a valid Site Code";
  		alert(confirmationMessage);
	}
  } else {
	if (siteCode.length == 0) {
		alert("Site Code can't be blank");
	} else {
		alert(siteCode + " is not a valid Site Code");
	}
  }
  
}

</script>

    </body>
</html>










    


    





