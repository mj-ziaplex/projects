<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/slocpi.css" media="screen">
<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/custom-table.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/common.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/slocpi.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/sessionTotalRecon.js"></script>

<script>
	//for session total recon
	var pesoTradVulTotalCollectionVal = 0;
	var pesoExemptionsVal = 0;
	var pesoSubTotalAVal = 0;
	var pesoNonPostedTotalVal = 0;
	var pesoManualPostingVal = 0;
	var pesoSubTotalBVal = 0;
	var pesoPostedSuccessfullyVal = 0;
	var pesoSessionTotalAtEODVal = 0;
	var pesoTotalPostedNonPolicyVal = 0;
	
	var dollarTradVulTotalCollectionVal = 0;
	var dollarExemptionsVal = 0;
	var dollarSubTotalAVal = 0;
	var dollarNonPostedTotalVal = 0;
	var dollarManualPostingVal = 0;
	var dollarSubTotalBVal = 0;
	var dollarPostedSuccessfullyVal = 0;
	var dollarSessionTotalAtEODVal = 0;
	var dollarTotalPostedNonPolicyVal = 0;
	
	var doesPesoSessionTotalTally = false;
	var doesDollarSessionTotalTally = false;
</script>
<c:forEach items="${productsToDisplay}" var="column" varStatus="rowCnt">
	<script>	
				var tableLayout = slocpiTableLayout;
				if('${company}'=='SLGFI'){
					tableLayout = slgfiTableLayout;
				}
				
 				//var headerName = "${column.productType.productName}" + "(" + "${column.productType.productCode}" + ")";
 				var slocpiHeaderName = "${column.productType.productName}";
 				var slocpiHeaderField = "${column.productType.productCode}";
 				
 				<c:choose>
	 				<c:when test="${column.productType.productName eq 'TRAD/VUL Peso'}">
	 					pesoSessionTotalAtEODVal = ${column.sessionTotal};
	 					pesoTotalPostedNonPolicyVal = ${column.totalPostedNonPolicy}
	 				</c:when>
	 				<c:when test="${column.productType.productName eq 'TRAD/VUL Dollar'}">
	 					dollarSessionTotalAtEODVal = ${column.sessionTotal};
	 					dollarTotalPostedNonPolicyVal = ${column.totalPostedNonPolicy}
	 				</c:when>
 				</c:choose>
 				
 				var slocpiColumn = [{name: slocpiHeaderName, field: slocpiHeaderField, width: "10%"}];
   				appendJsons([tableLayout,slocpiColumn]);//append columns to layout, loop bec. multiple products
   				
   				var tempTotalCollectionSummary = 0;//part of A subtotal
   				var tempTotalNonPostedFromDTR = 0;//part of B subtotal
   				var tempTotalForManualPosting = 0;//part of B subtotal
   				var tempTotalExemptions = 0;//part of A subtotal

   				blankRow.${column.productType.productCode} = " ";//add new prop/val pair to the variable
   				
   				collectionSummaryHeader.${column.productType.productCode} = " ";
	   				totalCashCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCashCounter}").toFixed(2));
	   				totalCashEpsBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCashEpsBpi}").toFixed(2));
	   				totalCashNonCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCashNonCounter}").toFixed(2));
	   				totalCheckOnUs.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckOnUs}").toFixed(2));
	   				totalCheckLocal.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckLocal}").toFixed(2));
	   				totalCheckRegional.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckRegional}").toFixed(2));
	   				totalCheckNonCounter.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckNonCounter}").toFixed(2));
	   				totalCheckOT.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCheckOT}").toFixed(2));
	   				totalPmo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPmo}").toFixed(2));
	   				totalCreditMemo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalCreditMemo}").toFixed(2));
	   				totalUsCheckInManila.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalUsCheckInManila}").toFixed(2));
	   				totalUsCheckOutPh.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalUsCheckOutPh}").toFixed(2));
	   				totalDollarCheque.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalDollarCheque}").toFixed(2));
	   				
	   				cardTypes.${column.productType.productCode} = " ";
		   				totalPosBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosBpi}").toFixed(2));
		   				totalPosCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosCtb}").toFixed(2));
		   				totalPosHsbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosHsbc}").toFixed(2));
		   				totalPosScb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosScb}").toFixed(2));
		   				totalPosRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosRcbc}").toFixed(2));
		   				totalPosBdo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalPosBdo}").toFixed(2));
		   				totalMdsBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsBpi}").toFixed(2));
		   				totalMdsCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsCtb}").toFixed(2));
		   				totalMdsHsbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsHsbc}").toFixed(2));
		   				totalMdsSbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsSbc}").toFixed(2));
		   				totalMdsRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsRcbc}").toFixed(2));
		   				totalMdsBdo.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalMdsBdo}").toFixed(2));
		   				
		   				totalAutoCtb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoCtb}").toFixed(2));
		   				totalAutoScb.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoScb}").toFixed(2));
		   				totalAutoRcbc.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalAutoRcbc}").toFixed(2));
		   				totalSunlinkOnline.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalSunlinkOnline}").toFixed(2));
		   				totalSunlinkHsbcDollar.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalSunlinkHsbcDollar}").toFixed(2));
		   				totalSunlinkHsbcPeso.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalSunlinkHsbcPeso}").toFixed(2));
		   				totalEpsBpi.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalEpsBpi}").toFixed(2));
		   				
	   				totalNonCash.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNonCash}").toFixed(2));
   					tempTotalCollectionSummary = parseFloat("${column.totalCashCounter}") +
   							parseFloat("${column.totalCashEpsBpi}") +
   							parseFloat("${column.totalCashNonCounter}") +
   							parseFloat("${column.totalCheckOnUs}") +
   							parseFloat("${column.totalCheckLocal}") +
   							parseFloat("${column.totalCheckRegional}") +
   							parseFloat("${column.totalCheckNonCounter}") +
   							parseFloat("${column.totalCheckOT}") +
   							parseFloat("${column.totalPmo}") +
   							parseFloat("${column.totalCreditMemo}") +
   							parseFloat("${column.totalUsCheckInManila}") +
   							parseFloat("${column.totalUsCheckOutPh}") +
   							parseFloat("${column.totalDollarCheque}") +
   							
   							parseFloat("${column.totalPosBpi}") +
   							parseFloat("${column.totalPosCtb}") +
   							parseFloat("${column.totalPosHsbc}") +
   							parseFloat("${column.totalPosScb}") +
   							parseFloat("${column.totalPosRcbc}") +
   							parseFloat("${column.totalPosBdo}") +
   							parseFloat("${column.totalMdsBpi}") +
   							parseFloat("${column.totalMdsCtb}") +
   							parseFloat("${column.totalMdsHsbc}") +
   							parseFloat("${column.totalMdsSbc}") +
   							parseFloat("${column.totalMdsRcbc}") +
   							parseFloat("${column.totalMdsBdo}") +
   							
   							parseFloat("${column.totalAutoCtb}") +
   							parseFloat("${column.totalAutoScb}") +
   							parseFloat("${column.totalAutoRcbc}") +
   							parseFloat("${column.totalSunlinkOnline}") +
   							parseFloat("${column.totalSunlinkHsbcDollar}") +
   							parseFloat("${column.totalSunlinkHsbcPeso}") +
   							parseFloat("${column.totalEpsBpi}") +
   							
   							parseFloat("${column.totalNonCash}");
   					totalCollectionSummary.${column.productType.productCode} = numberWithCommas(tempTotalCollectionSummary.toFixed(2));
   					
   				nonPostedFromDTRHeader.${column.productType.productCode} = " ";
	   				totalNewBusinessIndividual.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNewBusinessIndividual}").toFixed(2));
	   				totalNewBusinessVariable.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNewBusinessVariable}").toFixed(2));
	   				totalVariableRenewal.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalVariableRenewal}").toFixed(2));
	   				totalIndividualRenewal.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalIndividualRenewal}").toFixed(2));
	   				totalNonCashFromDTR.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNonCashFromDTR}").toFixed(2));
	   				tempTotalNonPostedFromDTR = parseFloat("${column.totalNewBusinessIndividual}") +
						parseFloat("${column.totalNewBusinessVariable}") +
							parseFloat("${column.totalVariableRenewal}") +
							parseFloat("${column.totalIndividualRenewal}") +
							parseFloat("${column.totalNonCashFromDTR}");
	   				totalNonPostedFromDTR.${column.productType.productCode} = numberWithCommas(tempTotalNonPostedFromDTR.toFixed(2));
	   				
   				forManualPostingHeader.${column.productType.productCode} = " ";
   					totalNonPolicy.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalNonPolicy}").toFixed(2));
   					totalWorksiteNonPosted.${column.productType.productCode} = numberWithCommas(parseFloat("${column.totalWorksiteNonPosted}").toFixed(2));
   					tempTotalForManualPosting = parseFloat("${column.totalNonPolicy}") + parseFloat("${column.totalWorksiteNonPosted}");
   					totalForManualPosting.${column.productType.productCode} = numberWithCommas(tempTotalForManualPosting.toFixed(2));
   				
   				exemptionsHeader.${column.productType.productCode} = " ";
   					exemptionsReversal.${column.productType.productCode} = 
   						<c:choose>
   							<c:when test="${isConfirmed}">
   								alignRightCell(numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2)));
   							</c:when>
   							<c:otherwise>   						
		   						<c:choose>
		   							<c:when test="${column.totalReversalAmt == 0.0}">
		   								centerCell("<a class=\"blue\" href=\"javascript:showAddEditReversalForm('${column.id}','${column.productType.currency.id}','${column.productType.productName}','add','${column.productType.company.name}')\"><img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/UndoButton.png\"></a>");
		   							</c:when>
		   							<c:otherwise>
		   								alignRightCell("<a href=\"javascript:showReversalsTable('${column.productType.productCode}','${column.productType.company.name}')\">" + numberWithCommas(parseFloat("${column.totalReversalAmt}").toFixed(2)) + "</a>");
		   							</c:otherwise>
		   						</c:choose>
		   					</c:otherwise>
		   				</c:choose>
		   				
		   			exemptionsWorksite.${column.productType.productCode} = 

				   				<c:choose>
				   					<c:when test="${column.productType.id ne '12' && column.productType.id ne '13' && column.productType.id ne '16' && column.productType.id ne '17'}">
				   						" ";
			   						</c:when>
									<c:when test="${isConfirmed}">
										alignRightCell(numberWithCommas(parseFloat("${column.worksiteAmount}").toFixed(2)));
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${company eq 'SLOCPI'}">
												centerCell("<input type='text' class='rightAlignDecimalSlocpi' id='tempWorksiteAmount${rowCnt.index}' name='tempWorksiteAmount${rowCnt.index}' value='"
														+ numberWithCommas(parseFloat('${column.worksiteAmount}').toFixed(2)) + "' onchange='formatTxtFldValDecimal(this)' />"
														+ "<input type='hidden' id='tempId${rowCnt.index}' name='tempId${rowCnt.index}' value='${column.id}'/>");
											</c:when>
											<c:otherwise>
												centerCell("<input type='text' class='rightAlignDecimalSlgfi' id='tempWorksiteAmount${rowCnt.index}' name='tempWorksiteAmount${rowCnt.index}' value='"
													+ numberWithCommas(parseFloat('${column.worksiteAmount}').toFixed(2)) + "' onchange='formatTxtFldValDecimal(this)' />"
													+ "<input type='hidden' id='tempId${rowCnt.index}' name='tempId${rowCnt.index}' value='${column.id}'/>");

											</c:otherwise>
										</c:choose>
									</c:otherwise>
					   			</c:choose>

   					tempTotalExemptions = parseFloat("${column.totalReversalAmt}") + parseFloat("${column.worksiteAmount}");
   					totalExemptions.${column.productType.productCode} = alignRightCell(boldCell(numberWithCommas(tempTotalExemptions.toFixed(2))));
   				
   	   			if('${column.productType.productCode}'=='IL_VL_PESO' || '${column.productType.productCode}'=='RL_RV_PESO'){
   	   				pesoTradVulTotalCollectionVal = tempTotalCollectionSummary;
   	   				pesoExemptionsVal = tempTotalExemptions;
   	   				pesoNonPostedTotalVal = tempTotalNonPostedFromDTR;
   	   				pesoManualPostingVal = tempTotalForManualPosting;
   	   			}else if('${column.productType.productCode}'=='IL_VL_DOLLAR' || '${column.productType.productCode}'=='RL_RV_DOLLAR'){
   	   				dollarTradVulTotalCollectionVal = tempTotalCollectionSummary;
   	   				dollarExemptionsVal = tempTotalExemptions;
   	   				dollarNonPostedTotalVal = tempTotalNonPostedFromDTR;
   	   				dollarManualPostingVal = tempTotalForManualPosting;
   	   			}
   	</script>
</c:forEach><!--break 4 c:forEach loops for slocpi/collections, nonPostedFromDTR, forManualPosting & exemptions, each with diff BO-->
<script>
	slocpiTableBlankRow.push(blankRow);
	
	slocpiCollectionSummaryArea.push(collectionSummaryHeader);
	var collectionSummaryHeaderFlag = false;
	if(checkForValue(totalCashCounter)){slocpiCollectionSummaryArea.push(totalCashCounter); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCashEpsBpi)){slocpiCollectionSummaryArea.push(totalCashEpsBpi); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCashNonCounter)){slocpiCollectionSummaryArea.push(totalCashNonCounter); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCheckOnUs)){slocpiCollectionSummaryArea.push(totalCheckOnUs); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCheckLocal)){slocpiCollectionSummaryArea.push(totalCheckLocal); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCheckRegional)){slocpiCollectionSummaryArea.push(totalCheckRegional); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCheckNonCounter)){slocpiCollectionSummaryArea.push(totalCheckNonCounter); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCheckOT)){slocpiCollectionSummaryArea.push(totalCheckOT); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPmo)){slocpiCollectionSummaryArea.push(totalPmo); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCreditMemo)){slocpiCollectionSummaryArea.push(totalCreditMemo); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalUsCheckInManila)){slocpiCollectionSummaryArea.push(totalUsCheckInManila); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalUsCheckOutPh)){slocpiCollectionSummaryArea.push(totalUsCheckOutPh); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalDollarCheque)){slocpiCollectionSummaryArea.push(totalDollarCheque); collectionSummaryHeaderFlag = true;};
	
	slocpiCollectionSummaryArea.push(cardTypes);
	var cardTypesSubHeaderFlag = false;
	if(checkForValue(totalPosBpi)){slocpiCollectionSummaryArea.push(totalPosBpi); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPosCtb)){slocpiCollectionSummaryArea.push(totalPosCtb); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPosHsbc)){slocpiCollectionSummaryArea.push(totalPosHsbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPosScb)){slocpiCollectionSummaryArea.push(totalPosScb); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPosRcbc)){slocpiCollectionSummaryArea.push(totalPosRcbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalPosBdo)){slocpiCollectionSummaryArea.push(totalPosBdo); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsBpi)){slocpiCollectionSummaryArea.push(totalMdsBpi); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsCtb)){slocpiCollectionSummaryArea.push(totalMdsCtb); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsHsbc)){slocpiCollectionSummaryArea.push(totalMdsHsbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsSbc)){slocpiCollectionSummaryArea.push(totalMdsSbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsRcbc)){slocpiCollectionSummaryArea.push(totalMdsRcbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalMdsBdo)){slocpiCollectionSummaryArea.push(totalMdsBdo); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	
	if(checkForValue(totalAutoCtb)){slocpiCollectionSummaryArea.push(totalAutoCtb); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalAutoScb)){slocpiCollectionSummaryArea.push(totalAutoScb); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalAutoRcbc)){slocpiCollectionSummaryArea.push(totalAutoRcbc); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalSunlinkOnline)){slocpiCollectionSummaryArea.push(totalSunlinkOnline); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalSunlinkHsbcDollar)){slocpiCollectionSummaryArea.push(totalSunlinkHsbcDollar); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalSunlinkHsbcPeso)){slocpiCollectionSummaryArea.push(totalSunlinkHsbcPeso); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalEpsBpi)){slocpiCollectionSummaryArea.push(totalEpsBpi); cardTypesSubHeaderFlag=true; collectionSummaryHeaderFlag = true;};
	
	if(!cardTypesSubHeaderFlag){
		for(a in slocpiCollectionSummaryArea){
			if(slocpiCollectionSummaryArea[a] == cardTypes){
				delete slocpiCollectionSummaryArea[a];
			}
		}
	}
	if(checkForValue(totalNonCash)){slocpiCollectionSummaryArea.push(totalNonCash); collectionSummaryHeaderFlag = true;};
	if(checkForValue(totalCollectionSummary)){slocpiCollectionSummaryArea.push(totalCollectionSummary); collectionSummaryHeaderFlag = true;};
	if(!collectionSummaryHeaderFlag){
		for(a in slocpiCollectionSummaryArea){
			if(slocpiCollectionSummaryArea[a] == collectionSummaryHeader){
				delete slocpiCollectionSummaryArea[a];
			}
		}
	}
	
	slocpiNonPostedFromDTRArea.push(nonPostedFromDTRHeader);
	var nonPostedFromDTRHeaderFlag = false;
	if(checkForValue(totalNewBusinessIndividual)){slocpiNonPostedFromDTRArea.push(totalNewBusinessIndividual); nonPostedFromDTRHeaderFlag=true;};
	if(checkForValue(totalNewBusinessVariable)){slocpiNonPostedFromDTRArea.push(totalNewBusinessVariable); nonPostedFromDTRHeaderFlag=true;};
	if(checkForValue(totalVariableRenewal)){slocpiNonPostedFromDTRArea.push(totalVariableRenewal); nonPostedFromDTRHeaderFlag=true;};
	if(checkForValue(totalIndividualRenewal)){slocpiNonPostedFromDTRArea.push(totalIndividualRenewal); nonPostedFromDTRHeaderFlag=true;};
	if(checkForValue(totalNonCashFromDTR)){slocpiNonPostedFromDTRArea.push(totalNonCashFromDTR); nonPostedFromDTRHeaderFlag=true;};
	if(checkForValue(totalNonPostedFromDTR)){slocpiNonPostedFromDTRArea.push(totalNonPostedFromDTR); nonPostedFromDTRHeaderFlag=true;};
	if(!nonPostedFromDTRHeaderFlag){
		for(a in slocpiNonPostedFromDTRArea){
			if(slocpiNonPostedFromDTRArea[a] == nonPostedFromDTRHeader){
				delete slocpiNonPostedFromDTRArea[a];
			}
		}
	}
	
	slocpiForManualPostingArea.push(forManualPostingHeader);
	var forManualPostingHeaderFlag = false;
	if(checkForValue(totalNonPolicy)){slocpiForManualPostingArea.push(totalNonPolicy); forManualPostingHeaderFlag = true;};
	if(checkForValue(totalWorksiteNonPosted)){slocpiForManualPostingArea.push(totalWorksiteNonPosted); forManualPostingHeaderFlag = true;};
	if(checkForValue(totalForManualPosting)){slocpiForManualPostingArea.push(totalForManualPosting); forManualPostingHeaderFlag = true;};
	if(!forManualPostingHeaderFlag){
		for(a in slocpiForManualPostingArea){
			if(slocpiForManualPostingArea[a] == forManualPostingHeader){
				delete slocpiForManualPostingArea[a];
			}
		}
	}
	
	/*slocpiExemptionsManualInputArea.push(exemptionsHeader);
	var exemptionsHeaderFlag = false;
	if(checkForValue(exemptionsReversal)){slocpiExemptionsManualInputArea.push(exemptionsReversal); exemptionsHeaderFlag = true;};
	if(checkForValue(exemptionsWorksite)){slocpiExemptionsManualInputArea.push(exemptionsWorksite); exemptionsHeaderFlag = true;};
	if(checkForValue(totalExemptions)){slocpiExemptionsManualInputArea.push(totalExemptions); exemptionsHeaderFlag = true;};
	if(!exemptionsHeaderFlag){
		for(a in slocpiExemptionsManualInputArea){
			if(slocpiExemptionsManualInputArea[a] == exemptionsHeader){
				delete slocpiExemptionsManualInputArea[a];
			}
		}
	}*/
	
	/*slocpiCollectionSummaryArea.push(collectionSummaryHeader);
	slocpiCollectionSummaryArea.push(totalCashCounter);
	slocpiCollectionSummaryArea.push(totalCashNonCounter);
	slocpiCollectionSummaryArea.push(totalCheckOnUs);
	slocpiCollectionSummaryArea.push(totalCheckLocal);
	slocpiCollectionSummaryArea.push(totalCheckRegional);
	slocpiCollectionSummaryArea.push(totalCheckNonCounter);
	slocpiCollectionSummaryArea.push(totalCheckOT);
	slocpiCollectionSummaryArea.push(totalCreditMemo);
	slocpiCollectionSummaryArea.push(totalUsCheckInManila);
	slocpiCollectionSummaryArea.push(totalUsCheckOutPh);
	slocpiCollectionSummaryArea.push(totalDollarCheque);
	slocpiCollectionSummaryArea.push(totalBankOTCCheckPayment);
	slocpiCollectionSummaryArea.push(cardTypes);
	slocpiCollectionSummaryArea.push(totalPosBpi);
	slocpiCollectionSummaryArea.push(totalPosCtb);
	slocpiCollectionSummaryArea.push(totalPosHsbc);
	slocpiCollectionSummaryArea.push(totalPosScb);
	slocpiCollectionSummaryArea.push(totalPosRcbc);
	slocpiCollectionSummaryArea.push(totalPosBdo);
	slocpiCollectionSummaryArea.push(totalNonCash);
	slocpiCollectionSummaryArea.push(totalCollectionSummary);
	
	slocpiNonPostedFromDTRArea.push(nonPostedFromDTRHeader);
	slocpiNonPostedFromDTRArea.push(totalNewBusinessIndividual);
	slocpiNonPostedFromDTRArea.push(totalNewBusinessVariable);
	slocpiNonPostedFromDTRArea.push(totalVariableRenewal);
	slocpiNonPostedFromDTRArea.push(totalIndividualRenewal);
	slocpiNonPostedFromDTRArea.push(totalNonCashFromDTR);
	slocpiNonPostedFromDTRArea.push(totalNonPostedFromDTR);
	
	slocpiForManualPostingArea.push(forManualPostingHeader);
	slocpiForManualPostingArea.push(totalNonPolicy);
	slocpiForManualPostingArea.push(totalWorksiteNonPosted);
	slocpiForManualPostingArea.push(totalForManualPosting);*/
	
	var showSaveButton = false;
	//show reversal/worksite if there at least some rows above	
	
	var hasSessionTotalFlag = false;
	
	if(pesoSessionTotalAtEODVal > 0 || dollarSessionTotalAtEODVal > 0){
		hasSessionTotalFlag = true;
	}
	if(collectionSummaryHeaderFlag || nonPostedFromDTRHeaderFlag || forManualPostingHeaderFlag || hasSessionTotalFlag){
		slocpiExemptionsManualInputArea.push(exemptionsHeader);
		slocpiExemptionsManualInputArea.push(exemptionsReversal);
		slocpiExemptionsManualInputArea.push(exemptionsWorksite);
		slocpiExemptionsManualInputArea.push(totalExemptions);
		showSaveButton = true;
	}
</script>
<script>
	var reconHeaderName = " ";
	var reconHeaderField = "reconVal";
	var reconValColumnStyle = "text-align:right;";
	var reconValColumn = [{name: reconHeaderName, field: reconHeaderField, width: "25%", styles: reconValColumnStyle}];
	appendJsons([sessionTotalReconTableLayout,reconValColumn]);

	blankRowSTR.reconVal = " ";
	pesoHeader.reconVal = " ";
	
	pesoTradVulTotalCollection.reconVal = numberWithCommas(pesoTradVulTotalCollectionVal.toFixed(2));
	pesoExemptions.reconVal = numberWithCommas(pesoExemptionsVal.toFixed(2));
	pesoSubTotalAVal = pesoTradVulTotalCollectionVal + pesoExemptionsVal;
	pesoSubTotalA.reconVal = boldCell(numberWithCommas(pesoSubTotalAVal.toFixed(2)));
	
	pesoNonPostedTotal.reconVal = numberWithCommas(pesoNonPostedTotalVal.toFixed(2));
	pesoManualPosting.reconVal = numberWithCommas(pesoManualPostingVal.toFixed(2));
	pesoSubTotalBVal = pesoNonPostedTotalVal + pesoManualPostingVal;
	pesoSubTotalB.reconVal = boldCell(numberWithCommas(pesoSubTotalBVal.toFixed(2)));
	
	pesoPostedSuccessfullyVal = pesoSubTotalAVal - pesoSubTotalBVal;
	pesoPostedSuccessfullyVal = Math.abs(pesoPostedSuccessfullyVal);
	pesoPostedSuccessfully.reconVal = numberWithCommas(pesoPostedSuccessfullyVal.toFixed(2));
	
	//TODO: sync DCRBalToolProd for 4 new ${column.variables} then use here
	
	//but get EOD and Reconciled values from ${obj.prop}(make obj first!)
	// pesoSessionTotalAtEODVal = pesoPostedSuccessfullyVal;//for now, put 'A-B' value in 'AtEOD'
	pesoSessionTotalAtEOD.reconVal = boldCell(numberWithCommas(pesoSessionTotalAtEODVal.toFixed(2)));
	pesoSessionTotalAtEOD2.reconVal = boldCell(numberWithCommas(pesoSessionTotalAtEODVal.toFixed(2)));
	pesoTotalPostedNonPolicy.reconVal = boldCell(numberWithCommas(pesoTotalPostedNonPolicyVal.toFixed(2)));
	
	var pesoEqual = (Math.abs(pesoSessionTotalAtEODVal + pesoTotalPostedNonPolicyVal - pesoPostedSuccessfullyVal) < 0.000001);
	//alert('pesoEqual: '+pesoEqual);
	//ifels ba, green check! ifels imba, red X! (red bold underlined value)
		 if(pesoEqual){
			 doesPesoSessionTotalTally = true;
			 pesoSessionTotalReconciled.reconVal = centerCell("<img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/CheckIcon.png\">");
		 }else{
			 pesoSessionTotalReconciled.reconVal = centerCell("<img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/XIcon.png\">");
		 }

	dollarHeader.reconVal = " ";
	
	dollarTradVulTotalCollection.reconVal = numberWithCommas(dollarTradVulTotalCollectionVal.toFixed(2));
	dollarExemptions.reconVal = numberWithCommas(dollarExemptionsVal.toFixed(2));
	dollarSubTotalAVal = dollarTradVulTotalCollectionVal + dollarExemptionsVal;
	dollarSubTotalA.reconVal = boldCell(numberWithCommas(dollarSubTotalAVal.toFixed(2)));
	
	dollarNonPostedTotal.reconVal = numberWithCommas(dollarNonPostedTotalVal.toFixed(2));
	dollarManualPosting.reconVal = numberWithCommas(dollarManualPostingVal.toFixed(2));
	dollarSubTotalBVal = dollarNonPostedTotalVal + dollarManualPostingVal;
	dollarSubTotalB.reconVal = boldCell(numberWithCommas(dollarSubTotalBVal.toFixed(2)));
	
	dollarPostedSuccessfullyVal = dollarSubTotalAVal - dollarSubTotalBVal;
	dollarPostedSuccessfullyVal = Math.abs(dollarPostedSuccessfullyVal);
	dollarPostedSuccessfully.reconVal = numberWithCommas(dollarPostedSuccessfullyVal.toFixed(2));
	
	//but get EOD and Reconciled values from ${obj.prop}(make obj first!)
	// dollarSessionTotalAtEODVal = dollarPostedSuccessfullyVal;//for now, put 'A-B' value in 'AtEOD'
	dollarSessionTotalAtEOD.reconVal = boldCell(numberWithCommas(dollarSessionTotalAtEODVal.toFixed(2)));
	dollarSessionTotalAtEOD2.reconVal = boldCell(numberWithCommas(dollarSessionTotalAtEODVal.toFixed(2)));
	dollarTotalPostedNonPolicy.reconVal = boldCell(numberWithCommas(dollarTotalPostedNonPolicyVal.toFixed(2)));
	
	var dollarEqual = (Math.abs(dollarSessionTotalAtEODVal + dollarTotalPostedNonPolicyVal - dollarPostedSuccessfullyVal) < 0.000001);
	//alert('dollarEqual: '+dollarEqual);
		 if(dollarEqual){
			 doesDollarSessionTotalTally = true;
			 dollarSessionTotalReconciled.reconVal = centerCell("<img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/CheckIcon.png\">");
		 }else{
			 dollarSessionTotalReconciled.reconVal = centerCell("<img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/XIcon.png\">");
		 }
</script>
<script>
	sessionTotalReconTableBlankRow.push(blankRowSTR);
	
	sessionTotalReconPesoArea.push(pesoHeader);
	sessionTotalReconPesoArea.push(pesoTradVulTotalCollection);
	sessionTotalReconPesoArea.push(pesoExemptions);
	sessionTotalReconPesoArea.push(pesoSubTotalA);
	sessionTotalReconPesoArea.push(blankRowSTR);//
	sessionTotalReconPesoArea.push(pesoNonPostedTotal);
	sessionTotalReconPesoArea.push(pesoManualPosting);
	sessionTotalReconPesoArea.push(pesoSubTotalB);
	sessionTotalReconPesoArea.push(blankRowSTR);//
	sessionTotalReconPesoArea.push(pesoPostedSuccessfully);
	sessionTotalReconPesoArea.push(blankRowSTR);//
	<c:choose>
		<c:when test="${isDayOne}">
			sessionTotalReconPesoArea.push(pesoSessionTotalAtEOD);
		</c:when>
		<c:otherwise>
			sessionTotalReconPesoArea.push(pesoSessionTotalAtEOD2);
		</c:otherwise>
	</c:choose>
	sessionTotalReconPesoArea.push(pesoTotalPostedNonPolicy);
	sessionTotalReconPesoArea.push(blankRowSTR);//
	sessionTotalReconPesoArea.push(pesoSessionTotalReconciled);
	
	sessionTotalReconDollarArea.push(dollarHeader);
	sessionTotalReconDollarArea.push(dollarTradVulTotalCollection);
	sessionTotalReconDollarArea.push(dollarExemptions);
	sessionTotalReconDollarArea.push(dollarSubTotalA);
	sessionTotalReconDollarArea.push(blankRowSTR);//
	sessionTotalReconDollarArea.push(dollarNonPostedTotal);
	sessionTotalReconDollarArea.push(dollarManualPosting);
	sessionTotalReconDollarArea.push(dollarSubTotalB);
	sessionTotalReconDollarArea.push(blankRowSTR);//
	sessionTotalReconDollarArea.push(dollarPostedSuccessfully);
	sessionTotalReconDollarArea.push(blankRowSTR);//
	<c:choose>
		<c:when test="${isDayOne}">
			sessionTotalReconDollarArea.push(dollarSessionTotalAtEOD);
		</c:when>
		<c:otherwise>
			sessionTotalReconDollarArea.push(dollarSessionTotalAtEOD2);
		</c:otherwise>
	</c:choose>
	sessionTotalReconDollarArea.push(dollarTotalPostedNonPolicy);
	sessionTotalReconDollarArea.push(blankRowSTR);//
	sessionTotalReconDollarArea.push(dollarSessionTotalReconciled);
</script>

<div class="overflow" id="slocpiTableDiv">
	<div id="slocpiTableNode">		
	</div>
	<c:choose>
		<c:when test="${company == 'SLOCPI'}">
			<form id="slocpiForm" name="slocpiForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/slocpi/confirm.wms" method="post">
			<div id="confirmButtonDiv">
			</div>	
			<input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
			<input type="hidden" id="id0" name="id0" value=""/>
			<input type="hidden" id="worksiteAmount0" name="worksiteAmount0" value=""/>
			<input type="hidden" id="id1" name="id1" value=""/>
			<input type="hidden" id="worksiteAmount1" name="worksiteAmount1" value=""/>
			<input type="hidden" id="htmlCode" name="htmlCode" value=""/>
			<!--input type="hidden" id="id2" name="id2" value=""/>
			<input type="hidden" id="worksiteAmount2" name="worksiteAmount2" value=""/-->
			</form>
		</c:when>
		<c:otherwise>
			<form id="slgfiForm" name="slgfiForm" action="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/slgfi/confirm.wms" method="post">
			<div id="confirmButtonDiv">
			</div>	
			<input type="hidden" name="dcrCashierId" value="${dcrCashierBO.id}"/>
			<input type="hidden" id="id0" name="id0" value=""/>
			<input type="hidden" id="worksiteAmount0" name="worksiteAmount0" value=""/>
			<input type="hidden" id="id1" name="id1" value=""/>
			<input type="hidden" id="worksiteAmount1" name="worksiteAmount1" value=""/>
			<input type="hidden" id="htmlCode" name="htmlCode" value=""/>
			<!--input type="hidden" id="id2" name="id2" value=""/>
			<input type="hidden" id="worksiteAmount2" name="worksiteAmount2" value=""/>
			<input type="hidden" id="id3" name="id3" value=""/>
			<input type="hidden" id="worksiteAmount3" name="worksiteAmount3" value=""/-->
			</form>
		</c:otherwise>
	</c:choose>
</div>
<div class="overflow" id="sessionTotalReconTableDiv">
	<div id="sessionTotalReconTableNode">		
	</div>
</div>

<script>
loadSlocpiTableNodeData('${company}',showSaveButton,'${isConfirmed}');
// alert();
loadSessionTotalReconTableNodeData();
if(${isConfirmed}){
	$(document).ready(function(){
		var confirmButton = document.getElementById("confirmButton");
		confirmButton.disabled = true;
	});
}

if(!doesPesoSessionTotalTally || !doesDollarSessionTotalTally){
	$(document).ready(function(){
		var confirmButton = document.getElementById("confirmButton");
		confirmButton.disabled = true;
	});
}
</script>