<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/reversal.css" media="screen">
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/reversal.js"></script>

<script>
<c:forEach items="${productsToDisplay}" var="product" varStatus="productRowCnt">
	<c:choose>
		<c:when test="${product.totalReversalAmt == 0.0}">
		</c:when>
		<c:otherwise>
			var dcrReversalData=[
	 				<c:forEach items="${product.dcrReversals}" var="dcrReversal" varStatus="dcrReversalRowCnt">
	 					    {dateTime: "${dcrReversal.dateTimeStr}", 
	 					    remarks: alignLeftCell("${dcrReversal.remarks}"), 
	 					    amount: alignRightCell(numberWithCommas(parseFloat("${dcrReversal.amount}").toFixed(2))),
	 					    actions: centerCell(
	 					    		"<a class=\"blue\" href=\"${pageContext.request.contextPath}/<c:choose><c:when test="${company eq 'SLAMCIP'}">slamcip</c:when><c:when test="${company eq 'SLAMCID'}">slamcid</c:when><c:when test="${company eq 'SLOCPI'}">slocpi</c:when><c:when test="${company eq 'SLGFI'}">slgfi</c:when><c:when test="${company eq 'SLFPI'}">slfpi</c:when></c:choose>/deletereversal.html?dcrCashierId=${dcrCashierBO.id}&dcrReversalId=${dcrReversal.id}\"><img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/XIcon.png\"></a>"
	 					    		+
	 					    		"<a class=\"blue\" href=\"javascript:showEditReversalFormWithData('${product.id}','${product.productType.currency.id}','${product.productType.productName}',parseFloat('${dcrReversal.amount}').toFixed(2),'${dcrReversal.remarks}','${dcrReversal.id}','edit','${product.productType.company.name}')\"><img src=\"${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/images/EditIcon.png\"></a>"
	 					    		)
	 					    }
	 					    <c:if test="${not dcrReversalRowCnt.last}">
	 					    	,
 						    </c:if>	
	 				</c:forEach>,
		 					{dateTime: "&nbsp;", 
	 					    remarks: alignRightCell(boldCell("TOTAL:")), 
	 					    amount: alignRightCell(boldCell(numberWithCommas(parseFloat("${product.totalReversalAmt}").toFixed(2)))),
	 					    actions: "&nbsp;"
	 					    }
	 		];
			initReversaTablePopUp(dcrReversalData, '${product.productType.productCode}',
					'${product.id}','${product.productType.currency.id}','${product.productType.productName}','${product.productType.company.name}');			
		</c:otherwise>
	</c:choose>
</c:forEach>
</script>

<!-- below will be the reversal divs, hidden in plain sight -->
<div id="reversalDivsByProductId" >
	<!--div id="outerNodeId">
		<div id="innerNodeId">
		</div>
	</div-->
</div>

<div id="addEditReversalDiv">
	<form action="${pageContext.request.contextPath}/<c:choose><c:when test="${company eq 'SLAMCIP'}">slamcip</c:when><c:when test="${company eq 'SLAMCID'}">slamcid</c:when><c:when test="${company eq 'SLOCPI'}">slocpi</c:when><c:when test="${company eq 'SLGFI'}">slgfi</c:when><c:when test="${company eq 'SLFPI'}">slfpi</c:when></c:choose>/addreversal.html?dcrCashierId=${dcrCashierBO.id}" 
	method="POST" name="addEditReversalForm" id="addEditReversalForm">
		<!-- Reversal Option ang window title -->
		<span id="productName"></span>
		<br/>
		<span id="amountLabel">Amount</span>
		<input type="text" name="amount" id="amount" size="45"/>
		<span id="reasonLabel">Reason</span>
		<textarea rows="5" cols="45" name="remarks" id="reversalRemarks"></textarea>
		<div class="divSubmitClearBtns">
			<input id="submitBtnId" class="submitBtn" type="submit" value="Submit"/>
			<input id="clearBtnId" class="clearBtn" type="button" value="Clear" onclick="resetAddEditReversalForm()"/>
		</div>
		<input type="hidden" id="dcrBalancingToolProductId" name="dcrBalancingToolProductId" value=""/>
		<input type="hidden" id="currencyId" name="currencyId" value=""/>
		<input type="hidden" id="dcrReversalId" name="dcrReversalId" value=""/>
	</form>
</div>