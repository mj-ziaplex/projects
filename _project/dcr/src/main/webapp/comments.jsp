<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/css/comment.css" media="screen">

<link rel="stylesheet" href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/themes/base/jquery-ui.css" />
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-1.8.2.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/jquery/jquery-ui-1.9.1/ui/jquery-ui.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/extraFunctions.js"></script>
<script src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/js/comment.js"></script>

<script>
var dcrCommentsMaximum=[
                    	<c:choose>
                		<c:when test="${not empty dcrComments}">
                			<c:forEach items="${dcrComments}" var="bo" varStatus="rowCnt">
                		    		{user: "${bo.acf2id}", 
                			         datePosted: "${bo.datePostedFormatted}", 
                					 remark: alignLeftCell("${bo.remarks}")}
                					<c:if test="${not rowCnt.last}">
                			        	,
                					</c:if>
                			</c:forEach>
                		</c:when>
                	    <c:otherwise>
                	    	{user: "&nbsp;", 
                	         datePosted: "&nbsp;", 
                			 remark: "No Comments"}
                		</c:otherwise>
                	</c:choose>
];
setDataForPopUp(dcrCommentsMaximum);

var dcrCommentsMinimum=[
                    	<c:choose>
                    		<c:when test="${not empty dcrComments}">
                    			<c:forEach items="${dcrComments}" var="bo" varStatus="rowCnt" begin="0" end="1">
                    		    		{user: "${bo.acf2id}", 
                    			         datePosted: "${bo.datePostedFormatted}", 
                    					 remark: alignLeftCell("${bo.remarks}")}
                    					<c:if test="${not rowCnt.last}">
                    			        	,
                    					</c:if>
                    			</c:forEach>
                    		</c:when>
                    	    <c:otherwise>
                    	    	{user: "&nbsp;", 
                    	         datePosted: "&nbsp;", 
                    			 remark: "No Comments"}
                    		</c:otherwise>
                    	</c:choose>
                    ];
setDCRComments(dcrCommentsMinimum,${company});
</script>

<div class="overflow" id="commentDiv" >
	<div id="commentTableNode">
		<div id="commentLabelNode">
		</div>	
		<div id="commentNode">
		</div>
	</div>
	<div id="addCommentNode"><!-- area for View and Add Buttons -->	
	</div>		
	<div id="navigationNode"><!-- area for Go back and Continue Buttons -->
	</div>
</div>

<div id="allCommentsPopUpDiv">
	<div id="allCommentsPopUpTableNode">
		<div id="allCommentsPopUpNode">
		</div>
	</div>
</div>

<div id="addCommentDiv">
	<form action="${pageContext.request.contextPath}/<c:choose><c:when test="${company eq 'SLAMCIP'}">slamcip</c:when><c:when test="${company eq 'SLAMCID'}">slamcid</c:when><c:when test="${company eq 'SLOCPI'}">slocpi</c:when><c:when test="${company eq 'SLGFI'}">slgfi</c:when><c:when test="${company eq 'SLFPI'}">slfpi</c:when><c:otherwise>other</c:otherwise></c:choose>/addcomment.html?dcrCashierId=${dcrCashierBO.id}" 
	method="POST" name="addCommentForm" id="addCommentForm">
		Remarks:
		<br/><textarea rows="7" cols="50" name="remarks" id="commentRemarks" maxlength="350"></textarea>
		<span class="charsRemaining"></span>
		<br/><input id="addCommentBtn" type="submit" value="Add"/>	
	</form>
</div>