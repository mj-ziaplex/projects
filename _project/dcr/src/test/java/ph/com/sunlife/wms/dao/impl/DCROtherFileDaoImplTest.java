package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCROtherFile;
import ph.com.sunlife.wms.util.WMSDateUtil;

import com.ibatis.sqlmap.client.SqlMapClient;

public class DCROtherFileDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private SqlMapClient sqlMapClient;

	private DCRDao dcrDao;

	private DCRCashierDao dcrCashierDao;

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public DCROtherFileDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		try {
			DCR dcr = new DCR();
			dcr.setCcId("XX");
			dcr.setDcrDate(WMSDateUtil.toDate("31OCT2011"));
			dcr.setRequiredCompletionDate(WMSDateUtil.toDate("03NOV2011"));
			dcr.setStatus("NEW");
			dcr.setWorkObjectNumber("dnasjkdnasjk");
			dcr = dcrDao.save(dcr);

			DCRCashier dc = new DCRCashier();
			dc.setCashier("WC01");
			dc.setDcr(dcr);
			dc.setStatus("NEW");
			dc.setLastUpdateDate(new Date());
			dc = dcrCashierDao.save(dc);

			DCROtherFileDaoImpl dao = new DCROtherFileDaoImpl();
			dao.setSqlMapClient(sqlMapClient);

			DCROtherFile file1 = new DCROtherFile();
			file1.setDcrCashier(dc);
			file1.setDcrFileDescription("some description");
			file1.setDcrFileType("pdf");
			file1.setDcrFileUploadDate(new Date());
			file1.setVersionId("{5434121241}");
			file1.setDcrFileSize(1024L);
			Long file1Id = dao.saveOtherFile(file1);
			assertNotNull(file1Id);
			file1.setId(file1Id);

			DCROtherFile file2 = new DCROtherFile();
			file2.setDcrCashier(dc);
			file2.setDcrFileDescription("some description again");
			file2.setDcrFileType("excel2007");
			file2.setDcrFileUploadDate(new Date());
			file2.setVersionId("{54341212dsadsa41}");
			file2.setDcrFileSize(1024L);
			Long file2Id = dao.saveOtherFile(file2);
			assertNotNull(file2Id);
			file2.setId(file2Id);

			List<DCROtherFile> list = dao.getOtherFilesByDcrCashierId(dc
					.getId());
			assertEquals(2, list.size());
			assertTrue(list.contains(file1));
			assertTrue(list.contains(file2));

			list = dao.getOtherFilesByDcrId(dcr.getId());
			assertEquals(2, list.size());
			assertTrue(list.contains(file1));
			assertTrue(list.contains(file2));

		} finally {
			endTransaction();
		}

	}

}
