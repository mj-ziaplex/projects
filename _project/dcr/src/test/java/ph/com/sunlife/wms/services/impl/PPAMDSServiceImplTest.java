package ph.com.sunlife.wms.services.impl;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

public class PPAMDSServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

//	private IpacDao ipacDao;
//
//	private DCRPPAMDSDao dcrPPAMDSDao;

	public PPAMDSServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

//	public void setIpacDao(IpacDao ipacDao) {
//		this.ipacDao = ipacDao;
//	}
//
//	public void setDcrPPAMDSDao(DCRPPAMDSDao dcrPPAMDSDao) {
//		this.dcrPPAMDSDao = dcrPPAMDSDao;
//	}
	
	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml",
				"ph/com/sunlife/wms/service/spring/service-config.xml" };
	}

	public void testInsert() throws Exception {
//		endTransaction();
//		startNewTransaction();
//		
//		Date date = WMSDateUtil.toDate("05DEC2012");
//		Date tomorrow = DateUtils.addDays(date, 1);
//		
//		Set<String> salesSlipNumbers = new HashSet<String>();
//		for (Company company : Company.values()) {
//			List<PPAMDS> pesoList = ipacDao.getMDSTransactions(company, "C4", date, tomorrow, Currency.PHP);
//			List<PPAMDS> dollarList = ipacDao.getMDSTransactions(company, "C4", date, tomorrow, Currency.USD);
//			
//			if (CollectionUtils.isNotEmpty(pesoList)) {
//				for (PPAMDS mds : pesoList) {
//					String salesSlipNumber = mds.getSalesSlipNumber();
//					if (StringUtils.isNotEmpty(salesSlipNumber)) {
//						salesSlipNumbers.add(salesSlipNumber);
//					}
//				}
//			}
//			
//			if (CollectionUtils.isNotEmpty(dollarList)) {
//				for (PPAMDS mds : dollarList) {
//					String salesSlipNumber = mds.getSalesSlipNumber();
//					if (StringUtils.isNotEmpty(salesSlipNumber)) {
//						salesSlipNumbers.add(salesSlipNumber);
//					}
//				}
//			}
//		}
//		
//		if (CollectionUtils.isNotEmpty(salesSlipNumbers)) {
//			for (String salesSlipNumber : salesSlipNumbers) {
//				DCRPPAMDS record = new DCRPPAMDS();
//				record.setAccountNumber(salesSlipNumber);
//				record.setDcrId(66L);
//				record.setApprovalNumber(salesSlipNumber);
//				record.setReconciled("Y");
//				record.setSalesSlipNumber(salesSlipNumber);
//				
//				dcrPPAMDSDao.insertDCRPPAMDS(record);
//			}
//		}
//		
//		setComplete();
//		endTransaction();
	}

//	public void testIsMdsReconciled() throws Exception {
//		PPAMDSServiceImpl service = new PPAMDSServiceImpl();
//		service.setIpacDao(ipacDao);
//		service.setDcrPPAMDSDao(dcrPPAMDSDao);
//		
//		Date date = WMSDateUtil.toDate("05DEC2012");
//		assertTrue(service.isMdsReconciled("C4", date, DateUtils.addDays(date, 1)));
//	}
}
