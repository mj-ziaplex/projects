package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;

public class DCRChequeDepositSlipDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	// private static final int TEST_DAYS_AGO = -366;

	private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;

	public DCRChequeDepositSlipDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrChequeDepositSlipDao(
			DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
		this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testSave() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRChequeDepositSlip dcds = new DCRChequeDepositSlip();
		
		dcds.setAccountNumber("acc1");
		dcds.setCurrency(Currency.USD);
		dcds.setDcrCashier(Long.MAX_VALUE);
		dcds.setCheckType(1L);
		dcds.setCompanyId(Company.SLAMCIP.getId());
		dcds.setDepositoryBank("some bank");
		dcds.setRequiredTotalAmount(190.0);
		dcds.setTotalChequeAmount(185.0);
		dcds.setDeposlipFile("Some bytes".getBytes());
		Date today = new Date();
		dcds.setDepositDate(today);
		dcds.setProdCode("GAF");
		

		dcds = dcrChequeDepositSlipDao.save(dcds);

		Long id = dcds.getId();
		assertNotNull(id);

		DCRChequeDepositSlip dcds2 = new DCRChequeDepositSlip();
		dcds2.setCompanyId(Company.SLAMCIP.getId());
		dcds2.setCurrency(Currency.USD);
		dcds2.setDcrCashier(Long.MAX_VALUE);
		dcds2.setCheckType(CheckType.ON_US);
		dcds2.setProdCode("GAF");
		DCRChequeDepositSlip retrievedFromDB = dcrChequeDepositSlipDao.getChequeDepositSlip(dcds2);
		assertNotNull(retrievedFromDB);
		assertEquals(190.0, retrievedFromDB.getRequiredTotalAmount());
		assertEquals(185.0, retrievedFromDB.getTotalChequeAmount());

		endTransaction();
	}
	
	public void testGetAll() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRChequeDepositSlip dcds = new DCRChequeDepositSlip();
		
		dcds.setAccountNumber("acc1");
		dcds.setCurrency(Currency.USD);
		dcds.setDcrCashier(Long.MAX_VALUE);
		dcds.setCheckType(1L);
		dcds.setCompanyId(Company.SLAMCIP.getId());
		dcds.setDepositoryBank("some bank");
		dcds.setRequiredTotalAmount(190.0);
		dcds.setTotalChequeAmount(185.0);
		dcds.setDeposlipFile("Some bytes".getBytes());
		Date today = new Date();
		dcds.setDepositDate(today);
		dcds.setProdCode("GAF");
		

		dcds = dcrChequeDepositSlipDao.save(dcds);

		Long id = dcds.getId();
		assertNotNull(id);

		DCRChequeDepositSlip dcds2 = new DCRChequeDepositSlip();
		dcds2.setCompanyId(Company.SLAMCIP.getId());
		dcds2.setCurrency(Currency.USD);
		dcds2.setDcrCashier(Long.MAX_VALUE);
		dcds2.setCheckType(CheckType.ON_US);
		dcds2.setProdCode("GAF");
		DCRChequeDepositSlip retrievedFromDB = dcrChequeDepositSlipDao.getChequeDepositSlip(dcds2);
		assertNotNull(retrievedFromDB);
		assertEquals(190.0, retrievedFromDB.getRequiredTotalAmount());
		assertEquals(185.0, retrievedFromDB.getTotalChequeAmount());
		
		
		List<DCRChequeDepositSlip> list =dcrChequeDepositSlipDao.getAllChequeDepositSlipsByDcrCashierId(Long.MAX_VALUE);
		assertNotNull(list);
		System.out.println(list.size());
//		assertEquals(190.0, list.get(0).getRequiredTotalAmount());

		endTransaction();
	}

}
