package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRMixedChequePayeeDao;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.services.bo.CompanyBO;
import ph.com.sunlife.wms.services.bo.MixedChequePayeeBO;

public class DCRMixedChequePayeeServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRMixedChequePayeeDao dcrMixedChequePayeeDao;

	private DCRMixedChequePayeeServiceImpl dcrMixedChequePayeeService;

	public DCRMixedChequePayeeServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrMixedChequePayeeDao(
			DCRMixedChequePayeeDao dcrMixedChequePayeeDao) {
		this.dcrMixedChequePayeeDao = dcrMixedChequePayeeDao;
	}

	@Override
	protected void onSetUp() throws Exception {
		super.onSetUp();

		dcrMixedChequePayeeService = new DCRMixedChequePayeeServiceImpl();
		dcrMixedChequePayeeService
				.setDcrMixedChequePayeeDao(dcrMixedChequePayeeDao);
	}

	public void testCreateNewMixedChequePayee() throws Exception {
		endTransaction();
		startNewTransaction();

		MixedChequePayeeBO check1 = new MixedChequePayeeBO();
		check1.setDcrCashier(Long.MAX_VALUE);
		check1.setBank("Lorem Ipsum Bank");
		check1.setCheckAmount(1000.0);
		check1.setCheckNumber("102-120-1029");
		check1.setCheckType(CheckType.LOCAL);
		check1.setCurrency(Currency.PHP);
		check1.setPayee(CompanyBO.SLFPI);
		check1.setOthers(500.0);
		check1.setSlamci(100.0);
		check1.setSlocpi(100.0);
		check1.setSlfpi(200.0);
		check1.setSlgfi(100.0);

		check1 = dcrMixedChequePayeeService.createNewMixedChequePayee(check1);
		assertNotNull(check1.getId());

		endTransaction();
	}

	public void testDeleteMixedChequePayee() throws Exception {
		endTransaction();
		startNewTransaction();

		MixedChequePayeeBO check1 = new MixedChequePayeeBO();
		check1.setDcrCashier(Long.MAX_VALUE);
		check1.setBank("Lorem Ipsum Bank");
		check1.setCheckAmount(1000.0);
		check1.setCheckNumber("102-120-1029");
		check1.setCheckType(CheckType.LOCAL);
		check1.setCurrency(Currency.PHP);
		check1.setPayee(CompanyBO.SLFPI);
		check1.setOthers(500.0);
		check1.setSlamci(100.0);
		check1.setSlocpi(100.0);
		check1.setSlfpi(200.0);
		check1.setSlgfi(100.0);
		check1.setCheckDate(new Date());
		check1 = dcrMixedChequePayeeService.createNewMixedChequePayee(check1);
		assertNotNull(check1.getId());

		MixedChequePayeeBO check2 = new MixedChequePayeeBO();
		check2.setDcrCashier(Long.MAX_VALUE);
		check2.setBank("Lorem Ipsum Bank");
		check2.setCheckAmount(1000.0);
		check2.setCheckNumber("102-120-1029");
		check2.setCheckType(CheckType.LOCAL);
		check2.setCurrency(Currency.PHP);
		check2.setPayee(CompanyBO.SLFPI);
		check2.setOthers(500.0);
		check2.setSlamci(100.0);
		check2.setSlocpi(100.0);
		check2.setSlfpi(200.0);
		check2.setSlgfi(100.0);
		check2.setCheckDate(new Date());

		check2 = dcrMixedChequePayeeService.createNewMixedChequePayee(check2);
		Long mixedChequePayeeToDeleteId = check2.getId();
		assertNotNull(mixedChequePayeeToDeleteId);

		List<MixedChequePayeeBO> list = dcrMixedChequePayeeService
				.getAllMixedChequePayees(Long.MAX_VALUE);
		assertNotNull(list);
		assertEquals(2, list.size());

		assertTrue(dcrMixedChequePayeeService
				.deleteMixedChequePayee(mixedChequePayeeToDeleteId));

		list = dcrMixedChequePayeeService
				.getAllMixedChequePayees(Long.MAX_VALUE);
		assertNotNull(list);
		assertEquals(1, list.size());

		endTransaction();
	}

	public void testUpdateMixedChequePayee() throws Exception {
		endTransaction();
		startNewTransaction();

		MixedChequePayeeBO check1 = new MixedChequePayeeBO();
		check1.setDcrCashier(Long.MAX_VALUE);
		check1.setBank("Lorem Ipsum Bank");
		check1.setCheckAmount(1000.0);
		check1.setCheckNumber("102-120-1029");
		check1.setCheckType(CheckType.LOCAL);
		check1.setCurrency(Currency.PHP);
		check1.setPayee(CompanyBO.SLFPI);
		check1.setOthers(500.0);
		check1.setSlamci(100.0);
		check1.setSlocpi(100.0);
		check1.setSlfpi(200.0);
		check1.setSlgfi(100.0);
		check1.setCheckDate(new Date());

		check1 = dcrMixedChequePayeeService.createNewMixedChequePayee(check1);
		assertNotNull(check1.getId());

		MixedChequePayeeBO check2 = new MixedChequePayeeBO();
		check2.setDcrCashier(Long.MAX_VALUE);
		check2.setBank("Lorem Ipsum Bank");
		check2.setCheckAmount(1000.0);
		check2.setCheckNumber("102-120-1029");
		check2.setCheckType(CheckType.LOCAL);
		check2.setCurrency(Currency.PHP);
		check2.setPayee(CompanyBO.SLFPI);
		check2.setOthers(500.0);
		check2.setSlamci(100.0);
		check2.setSlocpi(100.0);
		check2.setSlfpi(200.0);
		check2.setSlgfi(100.0);
		check2.setCheckDate(new Date());
		check2 = dcrMixedChequePayeeService.createNewMixedChequePayee(check2);
		Long mixedChequePayeeToDeleteId = check2.getId();
		assertNotNull(mixedChequePayeeToDeleteId);

		List<MixedChequePayeeBO> list = dcrMixedChequePayeeService
				.getAllMixedChequePayees(Long.MAX_VALUE);
		assertNotNull(list);
		assertEquals(2, list.size());

		assertTrue(dcrMixedChequePayeeService
				.deleteMixedChequePayee(mixedChequePayeeToDeleteId));

		check1.setBank("Lorem Ipsum Bank");
		check1.setCheckAmount(2000.0);
		check1.setSlfpi(1200.0);

		assertTrue(dcrMixedChequePayeeService.updateMixedChequePayee(check1));

		list = dcrMixedChequePayeeService
				.getAllMixedChequePayees(Long.MAX_VALUE);
		assertNotNull(list);
		assertEquals(1, list.size());

		MixedChequePayeeBO retrievedMCP = list.get(0);
		assertEquals(check1, retrievedMCP);
		assertEquals(check1.getCheckAmount(), retrievedMCP.getCheckAmount());
		assertEquals(check1.getSlfpi(), retrievedMCP.getSlfpi());

		endTransaction();
	}

}
