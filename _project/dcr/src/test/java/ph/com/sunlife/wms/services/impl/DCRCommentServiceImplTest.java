package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCommentDao;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.util.WMSConstants;

public class DCRCommentServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRCommentDao dcrCommentDao;

	public DCRCommentServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrCommentDao(DCRCommentDao dcrCommentDao) {
		this.dcrCommentDao = dcrCommentDao;
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		long dcrCashierId = Long.MAX_VALUE;

		DCRCommentServiceImpl service = new DCRCommentServiceImpl();
		service.setDcrCommentDao(dcrCommentDao);

		DCRCommentBO dcrCommentBO = new DCRCommentBO();
		dcrCommentBO.setAcf2id("pu98");
		dcrCommentBO.setDcrCashier(dcrCashierId);
		dcrCommentBO.setDatePosted(new Date());
		dcrCommentBO.setRemarks("lorem ipsum dolor"
				+ WMSConstants.LINE_SEPARATOR + "lorem ipsum dolor 2");

		dcrCommentBO = service.save(dcrCommentBO);
		assertNotNull(dcrCommentBO.getId());

		List<DCRCommentBO> list = service.getAllComments(dcrCashierId);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(1, list.size());

		DCRCommentBO result = list.get(0);

		assertEquals(dcrCommentBO, result);
		assertEquals("lorem ipsum dolor lorem ipsum dolor 2",
				result.getRemarks());
		assertEquals("pu98", result.getAcf2id());

		endTransaction();
	}
}
