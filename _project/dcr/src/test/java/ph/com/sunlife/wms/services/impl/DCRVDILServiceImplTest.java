package ph.com.sunlife.wms.services.impl;

import java.util.Date;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.services.bo.DCRDollarCoinCollectionBO;
import ph.com.sunlife.wms.services.bo.DCRVDILBO;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRVDILServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRVDILDao dcrVdilDao;

	private DCRCashierDao dcrCashierDao;

	private DCRDao dcrDao;

	private IpacDao ipacDao;
	
	private CashierDao cashierDao;
	
//	private DCRFilenetIntegrationService dcrFilenetIntegrationService;
	
	public void setDcrFilenetIntegrationService(
			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
//		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
	}
	
	public void setCashierDao(CashierDao cashierDao) {
		this.cashierDao = cashierDao;
	}



	public DCRVDILServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setDcrVdilDao(DCRVDILDao dcrVdilDao) {
		this.dcrVdilDao = dcrVdilDao;
	}

	public void setIpacDao(IpacDao ipacDao) {
		this.ipacDao = ipacDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/service/spring/spring-config.xml" };
	}

	public void testCreateNewVdil() throws Exception {
		endTransaction();
		startNewTransaction();
		
		DCR dcr1 = new DCR();
		dcr1.setCcId("A2");
		dcr1.setCcName("Ace");
		dcr1.setDcrDate(WMSDateUtil.toDate("16DEC2011"));
		dcr1.setRequiredCompletionDate(WMSDateUtil.toDate("20DEC2011"));
		dcr1.setStatus("PENDING");
		dcr1 = dcrDao.save(dcr1);
		
		DCR dcr2 = new DCR();
		dcr2.setCcId("A2");
		dcr2.setCcName("Ace");
		dcr2.setDcrDate(WMSDateUtil.toDate("19DEC2011"));
		dcr2.setRequiredCompletionDate(WMSDateUtil.toDate("26DEC2011"));
		dcr2.setStatus("PENDING");
		dcr2 = dcrDao.save(dcr2);
		
		DCRCashier dcrCashier1 = new DCRCashier();
		dcrCashier1.setCashier("wc01");
		dcrCashier1.setDcr(dcr1);
		dcrCashier1.setLastUpdateDate(WMSDateUtil.toDate("16DEC2011"));
		dcrCashier1 = dcrCashierDao.save(dcrCashier1);
		
		// dcrCashier1 = dcrCashierDao.save(dcrCashier1);
		
		DCRVDIL dv1 = new DCRVDIL();
		dv1.setDcrCashier(dcrCashier1);
		dv1 = dcrVdilDao.save(dv1);
		
		DCRDollarCoinCollection coll1 = new DCRDollarCoinCollection();
		coll1.setDateCreated(WMSDateUtil.toDate("18DEC2011"));
		coll1.setDcrvdil(dv1);
		coll1.setDenom1(10);
		coll1.setDenom50c(15);
		coll1.setDenom25c(20);
		coll1.setDenom10c(25);
		coll1.setDenom5c(30);
		coll1.setDenom1c(35);

		coll1 = dcrVdilDao.saveDCRDollarCoinCollection(coll1);

		DCR dcr = new DCR();
		dcr.setCcId("A2");
		dcr.setDcrDate(WMSDateUtil.toDate("20DEC2011"));
		dcr.setStatus("PENDING");
		dcr = dcrDao.save(dcr);

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setDcr(dcr);
		dcrCashier.setCashier("WC01");
		dcrCashier.setLastUpdateDate(new Date());
		dcrCashier.setStatus("PENDING");
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRVDILServiceImpl service = new DCRVDILServiceImpl();
		service.setDcrVdilDao(dcrVdilDao);
		service.setDcrCashierDao(dcrCashierDao);
		service.setIpacDao(ipacDao);
		service.setDcrDao(dcrDao);
		service.setCashierDao(cashierDao);

		DCRVDILBO vdil = service.createNewVdil(dcrCashier.getId());

		assertNotNull(vdil);
		assertNotNull(vdil.getId());
		assertNotNull(vdil.getPrSeriesNumbers());
		assertEquals(4, vdil.getPrSeriesNumbers().size());
		assertEquals(4, vdil.getIsoCollections().size());
		
		DCRDollarCoinCollectionBO previousCoinCollection = vdil.getPreviousCoinCollection();
		assertEquals(10,previousCoinCollection.getDenom1());
		assertEquals(15,previousCoinCollection.getDenom50c());
		assertEquals(20,previousCoinCollection.getDenom25c());
		assertEquals(25,previousCoinCollection.getDenom10c());
		assertEquals(30,previousCoinCollection.getDenom5c());
		assertEquals(35,previousCoinCollection.getDenom1c());
		endTransaction();
	}

	public void testGetVdil() throws Exception {
		endTransaction();
		startNewTransaction();

		DCR dcr = new DCR();
		dcr.setCcId("A2");
		dcr.setDcrDate(WMSDateUtil.toDate("17OCT2012"));
		dcr.setStatus("PENDING");
		dcr = dcrDao.save(dcr);

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setDcr(dcr);
		dcrCashier.setCashier("WC01");
		dcrCashier.setLastUpdateDate(new Date());
		dcrCashier.setStatus("PENDING");
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRVDILServiceImpl service = new DCRVDILServiceImpl();
		service.setDcrVdilDao(dcrVdilDao);
		service.setDcrCashierDao(dcrCashierDao);
		service.setIpacDao(ipacDao);
		service.setDcrDao(dcrDao);
		service.setCashierDao(cashierDao);

		DCRVDILBO businessObject = service.createNewVdil(dcrCashier.getId());
		assertNotNull(businessObject);
		assertNotNull(businessObject.getId());

		DCRVDILBO newlyInsertedBusinessObject = service.getVdil(dcrCashier.getId(), "wc01");
		assertNotNull(newlyInsertedBusinessObject);
		assertNotNull(newlyInsertedBusinessObject.getId());

		DCRVDILBO nullBusinessObject = service.getVdil(Long.MIN_VALUE, "wc03");
		assertNotSame(businessObject, nullBusinessObject);

		assertEquals(businessObject, newlyInsertedBusinessObject);
		endTransaction();
	}

//	public void testUpdateVdil() throws Exception {
//		endTransaction();
//		startNewTransaction();
//
//		DCR dcr = new DCR();
//		dcr.setCcId("A2");
//		dcr.setDcrDate(WMSDateUtil.toDate("17OCT2012"));
//		dcr.setStatus("PENDING");
//		dcr = dcrDao.save(dcr);
//
//		DCRCashier dcrCashier = new DCRCashier();
//		dcrCashier.setDcr(dcr);
//		dcrCashier.setCashier("WC01");
//		dcrCashier.setLastUpdateDate(new Date());
//		dcrCashier.setStatus("PENDING");
//		dcrCashier = dcrCashierDao.save(dcrCashier);
//
//		DCRVDILServiceImpl service = new DCRVDILServiceImpl();
//		service.setDcrVdilDao(dcrVdilDao);
//		service.setDcrCashierDao(dcrCashierDao);
//		service.setIpacDao(ipacDao);
//		service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
//
//		// final long dcrCashierId = Long.MAX_VALUE;
//
//		// DCRCashier dc = new DCRCashier();
//		// dc.setCashier("wc01");
//		// dc.setDcr(Long.MAX_VALUE);
//		// dc.setStatus("PENDING");
//		// dc = dcrCashierDao.save(dc);
//
//		final Long dcrCashierId = dcrCashier.getId();
//
//		DCRVDILBO businessObject = service.createNewVdil(dcrCashierId);
//		assertNotNull(businessObject);
//		assertNotNull(businessObject.getId());
//
//		DCRVDILBO newlyInsertedBusinessObject = service.getVdil(dcrCashierId);
//		assertNotNull(newlyInsertedBusinessObject);
//		assertNotNull(newlyInsertedBusinessObject.getId());
//
//		DCRVDILBO nullBusinessObject = service.getVdil(Long.MIN_VALUE);
//		assertNull(nullBusinessObject);
//
//		assertEquals(businessObject, newlyInsertedBusinessObject);
//
//		Date startOfToday = WMSDateUtil.startOfTheDay(new Date());
//		updateValuesOfBusinessObject(newlyInsertedBusinessObject, startOfToday);
//
//		File physicalFile = new File("C:\\jsard.pdf");
//		InputStream is = new FileInputStream(physicalFile);
//		byte[] file = IOUtils.toByteArray(is);
//		newlyInsertedBusinessObject.setVdilFile(file);
//		newlyInsertedBusinessObject.setCustomerCenter("A3");
//		newlyInsertedBusinessObject.setUserId("PW12");
//		newlyInsertedBusinessObject.setDcrDate(WMSDateUtil.toDate("31OCT2012"));
//		newlyInsertedBusinessObject = service
//				.updateVdil(newlyInsertedBusinessObject);
//		assertNotNull(newlyInsertedBusinessObject);
//
//		DCRVDILBO updatedBusinessObject = service.getVdil(dcrCashierId);
//		assertEquals(2, updatedBusinessObject.getAfterCutoffCard());
//		assertEquals(2000.0, updatedBusinessObject.getAfterCutoffCashDollar());
//		assertEquals(2000.0, updatedBusinessObject.getAfterCutoffCashPeso());
//		assertEquals(2, updatedBusinessObject.getAfterCutoffCheck());
//		assertEquals("lorem ipsum", updatedBusinessObject.getForSafeKeeping());
//		assertEquals(2000.0, updatedBusinessObject.getOverages());
//		assertEquals(2000.0, updatedBusinessObject.getPettyCash());
//		assertEquals(2000.0, updatedBusinessObject.getPhotocopy());
//		assertEquals(2, updatedBusinessObject.getPnPDCPendingWarehouse());
//		assertEquals(2000.0, updatedBusinessObject.getTotalDollarCoins());
//		assertEquals(2000.0, updatedBusinessObject.getUnclaimedChange());
//		assertEquals(2, updatedBusinessObject.getVultradPDCRPendingWarehouse());
//		assertEquals(2000.0, updatedBusinessObject.getWorkingFund());
//
//		for (DCRVDILISOCollectionBO subBusinessObject : updatedBusinessObject
//				.getIsoCollections()) {
//			assertEquals(2000.0, subBusinessObject.getAmount());
//			assertEquals(Long.valueOf(1L), subBusinessObject.getTypeId());
//			assertTrue(DateUtils.isSameDay(startOfToday,
//					subBusinessObject.getDcrDate()));
//			assertTrue(DateUtils.isSameDay(startOfToday,
//					subBusinessObject.getPickupDate()));
//		}
//
//		DCRDollarCoinCollectionBO coinCollection = updatedBusinessObject
//				.getCurrentCoinCollection();
//		if (coinCollection != null) {
//			assertEquals(20, coinCollection.getDenom1());
//			assertEquals(20, coinCollection.getDenom50c());
//			assertEquals(20, coinCollection.getDenom25c());
//			assertEquals(20, coinCollection.getDenom10c());
//			assertEquals(20, coinCollection.getDenom5c());
//			assertEquals(20, coinCollection.getDenom1c());
//		}
//
//		DCRDollarCoinExchangeBO exchange = updatedBusinessObject
//				.getDollarCoinExchange();
//		if (exchange != null) {
//			assertEquals(10, exchange.getIncomingDenom1());
//			assertEquals(10, exchange.getIncomingDenom50c());
//			assertEquals(10, exchange.getIncomingDenom25c());
//			assertEquals(10, exchange.getIncomingDenom10c());
//			assertEquals(10, exchange.getIncomingDenom5c());
//			assertEquals(10, exchange.getIncomingDenom1c());
//
//			assertEquals(5, exchange.getOutgoingDenom1());
//			assertEquals(5, exchange.getOutgoingDenom50c());
//			assertEquals(5, exchange.getOutgoingDenom25c());
//			assertEquals(5, exchange.getOutgoingDenom10c());
//			assertEquals(5, exchange.getOutgoingDenom5c());
//			assertEquals(5, exchange.getOutgoingDenom1c());
//		}
//
//		endTransaction();
//	}

//	private void updateValuesOfBusinessObject(
//			DCRVDILBO newlyInsertedBusinessObject, Date startOfToday) {
//		newlyInsertedBusinessObject.setAfterCutoffCard(2);
//		newlyInsertedBusinessObject.setAfterCutoffCashDollar(2000.0);
//		newlyInsertedBusinessObject.setAfterCutoffCashPeso(2000.0);
//		newlyInsertedBusinessObject.setAfterCutoffCheck(2);
//		newlyInsertedBusinessObject.setForSafeKeeping("lorem ipsum");
//		newlyInsertedBusinessObject.setOverages(2000.0);
//		newlyInsertedBusinessObject.setPettyCash(2000.0);
//		newlyInsertedBusinessObject.setPhotocopy(2000.0);
//		newlyInsertedBusinessObject.setPnPDCPendingWarehouse(2);
//		newlyInsertedBusinessObject.setTotalDollarCoins(2000.0);
//		newlyInsertedBusinessObject.setUnclaimedChange(2000.0);
//		newlyInsertedBusinessObject.setVultradPDCRPendingWarehouse(2);
//		newlyInsertedBusinessObject.setWorkingFund(2000.0);
//
//		for (DCRVDILISOCollectionBO subBusinessObject : newlyInsertedBusinessObject
//				.getIsoCollections()) {
//			subBusinessObject.setAmount(2000.0);
//			subBusinessObject.setCurrencyId(1L);
//			subBusinessObject.setDcrDate(startOfToday);
//			subBusinessObject.setPickupDate(startOfToday);
//			subBusinessObject.setTypeId(1L);
//		}
//
//		for (DCRVDILPRSeriesNumberBO subBusinessObject : newlyInsertedBusinessObject
//				.getPrSeriesNumbers()) {
//			subBusinessObject.setCompanyId(1L);
//			subBusinessObject.setFrom(2);
//			subBusinessObject.setTo(2);
//			subBusinessObject.setReason("lorem ipsum");
//		}
//
//		DCRDollarCoinExchangeBO exchange = newlyInsertedBusinessObject
//				.getDollarCoinExchange();
//		if (exchange != null) {
//			exchange.setIncomingDenom1(10);
//			exchange.setIncomingDenom50c(10);
//			exchange.setIncomingDenom25c(10);
//			exchange.setIncomingDenom10c(10);
//			exchange.setIncomingDenom5c(10);
//			exchange.setIncomingDenom1c(10);
//
//			exchange.setOutgoingDenom1(5);
//			exchange.setOutgoingDenom50c(5);
//			exchange.setOutgoingDenom25c(5);
//			exchange.setOutgoingDenom10c(5);
//			exchange.setOutgoingDenom5c(5);
//			exchange.setOutgoingDenom1c(5);
//			newlyInsertedBusinessObject.setDollarCoinExchange(exchange);
//		}
//
//		DCRDollarCoinCollectionBO current = newlyInsertedBusinessObject
//				.getCurrentCoinCollection();
//		if (current != null) {
//			current.setDenom1(20);
//			current.setDenom50c(20);
//			current.setDenom25c(20);
//			current.setDenom10c(20);
//			current.setDenom5c(20);
//			current.setDenom1c(20);
//			newlyInsertedBusinessObject.setCurrentCoinCollection(current);
//		}
//
//	}
}
