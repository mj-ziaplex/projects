package ph.com.sunlife.wms.web.filter;

import static org.junit.Assert.assertTrue;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

public class AggressiveCashingStopperFilterTest {

	@Test
	public void testDoFilter() throws Exception {
		MockHttpServletRequest request = new MockHttpServletRequest("GET",
				"/handler");
		request.setParameter("myField", "0");
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockFilterChain chain = new MockFilterChain();

		AggressiveCachingStopperFilter filter = new AggressiveCachingStopperFilter();

		filter.doFilter(request, response, chain);

		assertTrue(StringUtils.contains(
				((HttpServletRequest) chain.getRequest()).getQueryString(),
				"tm"));

	}
}
