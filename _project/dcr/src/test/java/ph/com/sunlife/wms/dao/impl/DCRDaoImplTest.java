package ph.com.sunlife.wms.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.AbstractWMSSqlMapClientDaoSupport;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRAuditLog;
import ph.com.sunlife.wms.dao.domain.DCRNonCashierFile;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorNote;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorReconciled;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private static final int TEST_DAYS_AGO = -732;
    private DCRDao dcrDao;

    public DCRDaoImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
            "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }

    public void testSave() throws Exception {
        endTransaction();
        startNewTransaction();

        DCR dcr = new DCR();
        dcr.setCcId("A0");

        Date dayLastWeek = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        dayLastWeek = WMSDateUtil.startOfTheDay(dayLastWeek);
        dcr.setDcrDate(dayLastWeek);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dayLastWeek));
        dcr.setStatus(WMSConstants.STATUS_NEW);
        dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");

        DCR savedDCR = dcrDao.save(dcr);
        assertNotNull(savedDCR.getId());

        endTransaction();
    }

    public void testGetById() throws Exception {
        endTransaction();
        startNewTransaction();

        DCR dcr = new DCR();
        dcr.setCcId("A0");

        Date dayLastWeek = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        dayLastWeek = WMSDateUtil.startOfTheDay(dayLastWeek);
        dcr.setDcrDate(dayLastWeek);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dayLastWeek));
        dcr.setStatus(WMSConstants.STATUS_NEW);
        dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");

        DCR savedDCR = dcrDao.save(dcr);
        Long id = savedDCR.getId();

        DCR retrievedDCR = dcrDao.getById(id);
        assertEquals(savedDCR, retrievedDCR);
        assertEquals("Makati Financial Store", retrievedDCR.getCcName());
        assertEquals("CCH002", retrievedDCR.getHubId());

        endTransaction();
    }

    public void testGetAllCenterCodes() throws Exception {
        List<String> centerCodes = dcrDao.getAllCenterIds();

        assertEquals(63, centerCodes.size());
    }

    public void testgetCustomerCenterId() throws Exception {
        String ccId = dcrDao.getCustomerCenterId("BDO");
        assertEquals("A6", ccId);
    }

    public void testGetDCRWorkItemsForCCCashier() throws Exception {
        endTransaction();
        startNewTransaction();

        List<String> centerCodes = dcrDao.getAllCenterIds();

        Date dayLastWeek = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        dayLastWeek = WMSDateUtil.startOfTheDay(dayLastWeek);
        Date requiredCompletionDate = WMSDateUtil.getRCD(dayLastWeek);

        for (String centerCode : centerCodes) {
            DCR dcr = new DCR();
            dcr.setCcId(centerCode);
            dcr.setDcrDate(dayLastWeek);
            dcr.setRequiredCompletionDate(requiredCompletionDate);
            dcr.setStatus("NEW");

            dcrDao.save(dcr);

            DCR dcrYesterday = new DCR();
            dcrYesterday.setCcId(centerCode);
            dcrYesterday.setDcrDate(DateUtils.addDays(dayLastWeek, -1));
            dcrYesterday.setRequiredCompletionDate(DateUtils.addDays(
                    requiredCompletionDate, 1));
            dcrYesterday.setStatus("PENDING");

            dcrDao.save(dcrYesterday);
        }

        String ccId = "F3";
        List<DCR> dcrList = dcrDao.getDCRWorkItemsForCCCashier(ccId);

        final Date finalDayLastWeek = dayLastWeek;
        CollectionUtils.filter(dcrList, new Predicate() {
            @Override
            public boolean evaluate(Object object) {
                DCR dcr = (DCR) object;
                Date dcrDate = dcr.getDcrDate();
                return (dcrDate.before(finalDayLastWeek) || DateUtils
                        .isSameDay(finalDayLastWeek, dcrDate));
            }
        });

        assertEquals(0, dcrList.size());

        endTransaction();
    }

    public void testGetAllDCRForTheDay() throws Exception {
        endTransaction();
        startNewTransaction();

        List<String> centerCodes = dcrDao.getAllCenterIds();

        Date dayLastWeek = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        dayLastWeek = WMSDateUtil.startOfTheDay(dayLastWeek);
        Date requiredCompletionDate = WMSDateUtil.getRCD(dayLastWeek);

        for (String centerCode : centerCodes) {
            DCR dcr = new DCR();
            dcr.setCcId(centerCode);
            dcr.setDcrDate(dayLastWeek);
            dcr.setRequiredCompletionDate(requiredCompletionDate);
            dcr.setStatus("NEW");

            dcrDao.save(dcr);

            DCR dcrYesterday = new DCR();
            dcrYesterday.setCcId(centerCode);
            dcrYesterday.setDcrDate(DateUtils.addDays(dayLastWeek, -1));
            dcrYesterday.setRequiredCompletionDate(DateUtils.addDays(
                    requiredCompletionDate, 1));
            dcrYesterday.setStatus("PENDING");

            dcrDao.save(dcrYesterday);
        }

        List<DCR> listOfDCRForToday = dcrDao.getAllDCRForTheDay(dayLastWeek);
        assertEquals(44, listOfDCRForToday.size());

        endTransaction();
    }

    @SuppressWarnings("unchecked")
    public void testGetAllPendingDCRForCC() throws Exception {
        endTransaction();
        startNewTransaction();

        Date lastYear = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        lastYear = WMSDateUtil.startOfTheDay(lastYear);
        Date requiredCompletionDate = WMSDateUtil.getRCD(lastYear);
        String ccId = "E6";

        DCR dcr = new DCR();
        dcr.setCcId(ccId);
        dcr.setDcrDate(lastYear);
        dcr.setRequiredCompletionDate(requiredCompletionDate);
        dcr.setStatus("NEW");

        dcr = ((AbstractWMSSqlMapClientDaoSupport<DCR>) dcrDao).save(dcr);

        DCR dcrTheDayBefore = new DCR();
        dcrTheDayBefore.setCcId(ccId);
        dcrTheDayBefore.setDcrDate(DateUtils.addDays(lastYear, -1));
        dcrTheDayBefore.setRequiredCompletionDate(DateUtils.addDays(
                requiredCompletionDate, -1));
        dcrTheDayBefore.setStatus("PENDING");

        dcrTheDayBefore = ((AbstractWMSSqlMapClientDaoSupport<DCR>) dcrDao)
                .save(dcrTheDayBefore);

        DCR dcrTheWeekBefore = new DCR();
        dcrTheWeekBefore.setCcId(ccId);
        dcrTheWeekBefore.setDcrDate(DateUtils.addDays(lastYear, -7));
        dcrTheWeekBefore.setRequiredCompletionDate(DateUtils.addDays(
                requiredCompletionDate, -7));
        dcrTheWeekBefore.setStatus("PENDING");

        dcrTheWeekBefore = ((AbstractWMSSqlMapClientDaoSupport<DCR>) dcrDao)
                .save(dcrTheWeekBefore);

        List<DCR> listOfDCR = dcrDao.getAllPendingDCRForCC(ccId);
        // assertEquals(3, listOfDCR.size());
        assertTrue(listOfDCR.contains(dcr));
        assertTrue(listOfDCR.contains(dcrTheDayBefore));
        assertTrue(listOfDCR.contains(dcrTheWeekBefore));

        endTransaction();
    }

    public void testGetCustomerCenters() throws Exception {
        List<String> ccIds = new ArrayList<String>();
        ccIds.add("A0");
        ccIds.add("A2");
        ccIds.add("A3");

        List<CustomerCenter> customerCenters = dcrDao.getCustomerCenters(ccIds);

        for (CustomerCenter center : customerCenters) {
            if ("A0".equals(center.getCcId())) {
                assertEquals("Makati Financial Store", center.getCcName());
                assertEquals("MCC", center.getCcCode());
            }
        }

        assertEquals(3, customerCenters.size());
    }

    public void testSaveNote() throws Exception {
        endTransaction();
        startNewTransaction();

        try {
            Long dcrId = 10L;

            DCRStepProcessorNote note = new DCRStepProcessorNote();
            note.setAcf2id("PV70");
            note.setNoteContent("some content");
            note.setNoteType("PPA");
            note.setRowNum(15);
            note.setDcrId(dcrId);

            Long noteId = dcrDao.saveNote(note);

            assertNotNull(noteId);
        } finally {
            endTransaction();
        }
    }

    public void testGetAllNotesForDcr() throws Exception {
        endTransaction();
        startNewTransaction();

        try {
            Long dcrId = Long.MAX_VALUE - 1;

            DCRStepProcessorNote note1 = new DCRStepProcessorNote();
            note1.setAcf2id("PV70");
            note1.setNoteContent("some content");
            note1.setNoteType("PPA");
            note1.setRowNum(15);
            note1.setDcrId(dcrId);

            Long noteId1 = dcrDao.saveNote(note1);
            assertNotNull(noteId1);
            note1.setId(noteId1);

            DCRStepProcessorNote note2 = new DCRStepProcessorNote();
            note2.setAcf2id("PK58");
            note2.setNoteContent("some content");
            note2.setNoteType("CCM");
            note2.setRowNum(39);
            note2.setDcrId(dcrId);

            Long noteId2 = dcrDao.saveNote(note2);
            assertNotNull(noteId2);
            note2.setId(noteId2);

            DCRStepProcessorNote note3 = new DCRStepProcessorNote();
            note3.setAcf2id("PK58");
            note3.setNoteContent("some content");
            note3.setNoteType("CCM");
            note3.setRowNum(39);
            note3.setDcrId(Long.MAX_VALUE);

            Long noteId3 = dcrDao.saveNote(note3);
            assertNotNull(note3);
            note3.setId(noteId3);

            List<DCRStepProcessorNote> notes = dcrDao.getAllNotesForDcr(dcrId);
            assertEquals(2, notes.size());

            assertTrue(notes.contains(note1));
            assertTrue(notes.contains(note2));
            assertFalse(notes.contains(note3));

            DCRStepProcessorNote retrievedNote = dcrDao.getNoteById(note3
                    .getId());
            assertEquals(note3, retrievedNote);

        } finally {
            endTransaction();
        }
    }

//	public void testSomething() throws Exception {
//		List<DCRStepProcessorNote> notes = dcrDao.getAllNotesForDcr(13061L);
//
//		String separator = System.getProperty("line.separator");
//		for (DCRStepProcessorNote note : notes) {
//			String content = note.getNoteContent();
//
//			System.out.println(content.contains("\n"));
//			if (StringUtils.contains(content, separator)) {
//				content = StringUtils.replace(content, "\n", "\\n");
//				content = StringUtils.replace(content, "\r", "\\r");
//			}
//			System.out.println(content);
//			note.setNoteContent(content);
//		}
//		
//		System.out.println(notes);
//	}
    public void testUpdateWorkObjectNumber() throws Exception {
        endTransaction();
        startNewTransaction();

        DCR dcr = new DCR();

        Date dayLastWeek = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        dayLastWeek = WMSDateUtil.startOfTheDay(dayLastWeek);
        dcr.setDcrDate(dayLastWeek);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dayLastWeek));
        dcr.setStatus(WMSConstants.STATUS_NEW);
        dcr.setCcId("A2");
        // dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");

        DCR savedDCR = dcrDao.save(dcr);
        assertNotNull(savedDCR.getId());

        savedDCR.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");
        boolean isUpdateSuccessful = dcrDao.updateWorkObjectNumber(savedDCR);
        assertTrue(isUpdateSuccessful);
//        DCR updatedDCR = dcrDao.getById(savedDCR.getId());
//        assertEquals(savedDCR.getWorkObjectNumber(), updatedDCR.getWorkObjectNumber());

        endTransaction();
    }

    public void testDCRStepProcessorReconciledMethods() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRStepProcessorReconciled entity = new DCRStepProcessorReconciled();
        entity.setDcrId(Long.MAX_VALUE);

        entity = dcrDao.createDCRStepProcessorReconciled(entity);
        Long id = entity.getId();
        assertNotNull(id);

        List<DCRStepProcessorReconciled> list = dcrDao
                .getDCRStepProcessorReconciled(Long.MAX_VALUE);
        DCRStepProcessorReconciled retrievedEntity = list.get(0);
        assertEquals(entity, retrievedEntity);

        entity.setReconciledRowNums("1, 2, 3, 4, 5, 6");
        dcrDao.createDCRStepProcessorReconciled(entity);

        list = dcrDao.getDCRStepProcessorReconciled(Long.MAX_VALUE);
        assertEquals(2, list.size());

        endTransaction();
    }

    public void testUpdateDcrStatus() throws Exception {
        endTransaction();
        startNewTransaction();

        DCR entity = new DCR();
        entity.setCcId("A2");
        entity.setDcrDate(WMSDateUtil.toDate("10JAN2011"));
        entity.setRequiredCompletionDate(WMSDateUtil.toDate("12JAN2011"));
        entity.setStatus("Pending");
        entity = dcrDao.save(entity);

        entity.setStatus("Approved for Recon");
        assertTrue(dcrDao.updateDcrStatus(entity));

//        Long id = entity.getId();

//        DCR retrievedDCR = dcrDao.getById(id);
//        assertEquals(entity.getStatus(), retrievedDCR.getStatus());

        endTransaction();
    }

    public void testSaveNonCashierFile() throws Exception {
        endTransaction();
        startNewTransaction();

        try {
            final DCRNonCashierFile f1 = new DCRNonCashierFile();
            f1.setDcrFileDescription("dat desc");
            f1.setDcrFileType("pdf");
            f1.setDcrFileUploadDate(new Date());
            f1.setDcrId(Long.MAX_VALUE);
            f1.setUploaderId("wc01");
            f1.setVersionId("{SD7878AS89-7878SAD8790}");
            f1.setFileSize(1024L);

            DCRNonCashierFile f2 = new DCRNonCashierFile();
            f2.setDcrFileDescription("dat desc2");
            f2.setDcrFileType("pdf");
            f2.setDcrFileUploadDate(new Date());
            f2.setDcrId(Long.MAX_VALUE);
            f2.setUploaderId("wc01");
            f2.setVersionId("{SD78662S89-7878SAD8790}");
            f2.setFileSize(2048L);

            f1.setId(dcrDao.saveNonCashierFile(f1));
            f2.setId(dcrDao.saveNonCashierFile(f2));

            List<DCRNonCashierFile> files = dcrDao
                    .getAllNonCashierFile(Long.MAX_VALUE);
            assertEquals(2, files.size());
            assertTrue(files.contains(f1));
            assertTrue(files.contains(f2));

            DCRNonCashierFile f3 = (DCRNonCashierFile) CollectionUtils.find(
                    files, new Predicate() {
                @Override
                public boolean evaluate(Object object) {
                    return f1.equals(object);
                }
            });
            assertNotNull(f3);

            assertEquals(f1.getDcrFileDescription(), f3.getDcrFileDescription());
            assertEquals(f1.getDcrFileType(), f3.getDcrFileType());
            assertEquals(f1.getUploaderId(), f3.getUploaderId());
            assertEquals(f1.getVersionId(), f3.getVersionId());
            assertTrue(DateUtils.isSameDay(f1.getDcrFileUploadDate(),
                    f3.getDcrFileUploadDate()));
            assertEquals(f1.getDcrId(), f3.getDcrId());
            assertEquals(f1, f3);

        } finally {
            endTransaction();
        }
    }

    public void testCheckIfHoliday() throws Exception {
        Date date = WMSDateUtil.toDate("01NOV2011");

        boolean isHoliday = dcrDao.checkIfHoliday(date);
        assertTrue(isHoliday);
    }

    public void testAuditLogMethods() throws Exception {
        Date logDate = new Date();
        Date logDate2 = DateUtils.addDays(logDate, 1);

        DCRAuditLog log = new DCRAuditLog();
        log.setActionDetails("SET TO: Approved for Recon");
        log.setDcrId(Long.MAX_VALUE);
        log.setLogDate(logDate);
        log.setProcessStatus("Approved for Recon");
        log.setUserId("pv70");

        Long id = dcrDao.createLog(log);
        assertNotNull(id);
        log.setId(id);

        DCRAuditLog log2 = new DCRAuditLog();
        log2.setActionDetails("SET TO: For Verification");
        log2.setDcrId(Long.MAX_VALUE);
        log2.setLogDate(logDate2);
        log2.setProcessStatus("For Verification");
        log2.setUserId("pv70");

        Long id2 = dcrDao.createLog(log2);
        assertNotNull(id2);
        log2.setId(id2);

        List<DCRAuditLog> logs = dcrDao.getAuditLogsByDcrId(Long.MAX_VALUE);
        assertEquals(2, logs.size());

        assertTrue(logs.contains(log));
        assertTrue(logs.contains(log2));
    }

    public void testGetDays() throws Exception {
        Date fiveDaysAgo = dcrDao.getDateFiveBusinessDaysAgo();
        System.out.println("5 Business Days ago was: " + fiveDaysAgo);

        Date threeDaysAgo = dcrDao.getDateThreeBusinessDaysAgo();
        System.out.println("3 Business Days ago was: " + threeDaysAgo);

        Date fourDaysAgo = dcrDao.getPreviousBusinessDate(4);
        System.out.println("4 Business Days ago was: " + fourDaysAgo);

        Date twoDaysAgo = dcrDao.getPreviousBusinessDate(2);
        System.out.println("2 Business Days ago was: " + twoDaysAgo);
    }

    public void testGetLapsedDCRSetForVerification() throws Exception {
        endTransaction();
        startNewTransaction();
        DCR dcr = new DCR();
        dcr.setCcId("A2");

        Date today = new Date();

        Date dcrDate = DateUtils.addDays(today, TEST_DAYS_AGO);
        dcrDate = WMSDateUtil.startOfTheDay(dcrDate);
        dcr.setDcrDate(dcrDate);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dcrDate));
        dcr.setStatus(WMSConstants.STATUS_FOR_VERIFICATION);
        dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");
        dcr.setUpdateDate(DateUtils.addDays(dcrDate, 1));

        Date updateDate = DateUtils.addDays(today, -729);

        List<DCR> list = dcrDao.getLapsedDCRSetForVerification(updateDate);
        assertEquals(0, list.size());

        endTransaction();
    }

    public void testGetLapsedDCRSetForAwaitingReqts() throws Exception {
        endTransaction();
        startNewTransaction();
        DCR dcr = new DCR();
        dcr.setCcId("A2");

        Date today = new Date();

        Date dcrDate = DateUtils.addDays(today, TEST_DAYS_AGO);
        dcrDate = WMSDateUtil.startOfTheDay(dcrDate);

        dcr.setDcrDate(dcrDate);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dcrDate));
        dcr.setStatus(WMSConstants.STATUS_AWAITING_REQUIREMENTS);
        dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");
        dcr.setUpdateDate(DateUtils.addDays(dcrDate, 1));
        DCR savedDCR = dcrDao.save(dcr);
        Long dcrId = savedDCR.getId();

        DCRNonCashierFile f1 = new DCRNonCashierFile();
        f1.setDcrFileDescription("bree bree");
        f1.setDcrFileType("html");
        f1.setDcrFileUploadDate(dcrDate);
        f1.setDcrId(dcrId);
        f1.setUploaderId("bree");
        f1.setFileSize(1024L);
        f1.setVersionId("bree bree bree 1234");
        dcrDao.saveNonCashierFile(f1);

        DCRNonCashierFile f2 = new DCRNonCashierFile();
        f2.setDcrFileDescription("bree bree 2");
        f2.setDcrFileType("html");
        f2.setDcrId(dcrId);
//		f2.setDcrFileUploadDate(DateUtils.addDays(today, -728));
        f2.setUploaderId("bree");
        f2.setFileSize(1024L);
        f2.setVersionId("bree bree bree 4321");
        dcrDao.saveNonCashierFile(f2);

        Date updateDate = DateUtils.addDays(today, -729);
        List<DCR> list = dcrDao.getLapsedDCRSetForAwaitingReqts(updateDate);
        assertEquals(1, list.size());
//        assertTrue(list.contains(savedDCR));

        DCRNonCashierFile param = new DCRNonCashierFile();
        param.setDcrId(dcrId);
        param.setDcrFileUploadDate(dcr.getUpdateDate());

        int count = dcrDao.countSubmittedNonCashierFileAfterGivenDate(param);
        assertEquals(0, count);

        endTransaction();
    }

    public void testGetDCRManagerByCC() throws Exception {
        endTransaction();
        startNewTransaction();

        DCR dcr = new DCR();
        dcr.setCcId("A2");

        Date today = new Date();

        Date dcrDate = DateUtils.addDays(today, TEST_DAYS_AGO);
        dcrDate = WMSDateUtil.startOfTheDay(dcrDate);

        dcr.setDcrDate(dcrDate);
        dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dcrDate));
        dcr.setStatus(WMSConstants.STATUS_AWAITING_REQUIREMENTS);
        dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");
        dcr.setUpdateDate(DateUtils.addDays(dcrDate, 1));
        DCR savedDCR = dcrDao.save(dcr);
        // Long dcrId = savedDCR.getId();

        List<String> ccIds = new ArrayList<String>();
        ccIds.add("A2");

        List<DCR> dcrList = dcrDao.getDCRManagerByCC(ccIds);
        assertTrue(dcrList.contains(savedDCR));

        endTransaction();
    }

    public void testCountHolidays() throws Exception {
        Date date = WMSDateUtil.toDate("25MAR2013");

        int count = dcrDao.countHolidays(date);
        assertEquals(2, count);
    }
    // public void testUpdateDcrToPending() throws Exception {
    // endTransaction();
    // startNewTransaction();
    //
    // DCR dcr = new DCR();
    // dcr.setCcId("A2");
    //
    // Date today = new Date();
    //
    // Date dcrDate = DateUtils.addDays(today, TEST_DAYS_AGO);
    // dcrDate = WMSDateUtil.startOfTheDay(dcrDate);
    //
    // dcr.setDcrDate(dcrDate);
    // dcr.setRequiredCompletionDate(WMSDateUtil.getRCD(dcrDate));
    // dcr.setStatus("NEW");
    // dcr.setWorkObjectNumber("FFD7587791096C4FB70AD50D65889C49");
    // dcr.setUpdateDate(DateUtils.addDays(dcrDate, 1));
    // DCR savedDCR = dcrDao.save(dcr);
    //
    // dcrDao.update
    // endTransaction();
    // }
}
