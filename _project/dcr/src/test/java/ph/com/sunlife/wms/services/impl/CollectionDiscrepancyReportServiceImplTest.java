package ph.com.sunlife.wms.services.impl;

import java.util.Date;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.CollectionDiscrepancyReportDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class CollectionDiscrepancyReportServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRDao dcrDao;

	private CollectionDiscrepancyReportDao collectionDiscrepancyReportDao;

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setCollectionDiscrepancyReportDao(
			CollectionDiscrepancyReportDao collectionDiscrepancyReportDao) {
		this.collectionDiscrepancyReportDao = collectionDiscrepancyReportDao;
	}

	public CollectionDiscrepancyReportServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testGenerateDiscrepancyReport() throws Exception {
		CollectionDiscrepancyReportServiceImpl service = new CollectionDiscrepancyReportServiceImpl();
		service.setCollectionDiscrepancyReportDao(collectionDiscrepancyReportDao);
		service.setDcrDao(dcrDao);

		Date start = WMSDateUtil.toDate("01OCT2012");
		Date end = WMSDateUtil.toDate("31OCT2012");
		
		service.generateDiscrepancyReport(null, start, end);
		
//		InputStream input = getClass().getClassLoader().getResourceAsStream(
//				"discrepancy_report_template.xlsx");
//
//		OutputStream output = null;
//		try {
//			byte[] template = IOUtils.toByteArray(input);
//			assertNotNull(template);
//
//			Date start = WMSDateUtil.toDate("01MAR2012");
//			Date end = WMSDateUtil.toDate("01APR2012");
//
//			byte[] excelFile = service.generateDiscrepancyReport(template,
//					start, end);
//
//			output = new FileOutputStream(
//					"C:\\DiscrepancyReports\\DCR_Discrepancy_Report_March-2012.xlsx");
//			IOUtils.write(excelFile, output);
//
//			output.flush();
//		} finally {
//			IOUtils.closeQuietly(input);
//			IOUtils.closeQuietly(output);
//		}

	}

	public void testGenerateDiscrepancyReportNow() throws Exception {
		// CollectionDiscrepancyReportServiceImpl service = new
		// CollectionDiscrepancyReportServiceImpl();
		// service.setCollectionDiscrepancyReportDao(collectionDiscrepancyReportDao);
		// service.setDcrDao(dcrDao);
		//
		// service.generateDiscrepancyReportNow();
		// //
		// // InputStream input =
		// getClass().getClassLoader().getResourceAsStream(
		// // "discrepancy_report_template.xlsx");
		// //
		// OutputStream output = null;
		// try {
		// // byte[] template = IOUtils.toByteArray(input);
		// // assertNotNull(template);
		// //
		// // Date start = WMSDateUtil.toDate("01OCT2012");
		// // Date end = WMSDateUtil.toDate("01NOV2012");
		// //
		// byte[] excelFile = service.generateDiscrepancyReportNow();
		// //
		// output = new FileOutputStream("C:\\excel.xlsx");
		// IOUtils.write(excelFile, output);
		//
		// output.flush();
		// } finally {
		// IOUtils.closeQuietly(input);
		// IOUtils.closeQuietly(output);
		// }

	}

}
