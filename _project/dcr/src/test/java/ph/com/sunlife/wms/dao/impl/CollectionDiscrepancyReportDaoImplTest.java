package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.domain.DiscrepancyReportEntry;
import ph.com.sunlife.wms.dao.domain.StartEndMonth;
import ph.com.sunlife.wms.util.WMSDateUtil;

import com.ibatis.sqlmap.client.SqlMapClient;

public class CollectionDiscrepancyReportDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private SqlMapClient sqlMapClient;

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	public CollectionDiscrepancyReportDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testGetTimelinessDiscrepancyReports() throws Exception {
		StartEndMonth param = new StartEndMonth();

		Date startOfMonth = WMSDateUtil.startOfMonth("01OCT2012");
		Date endOfMonth = WMSDateUtil.startOfMonth("01NOV2012");

		param.setStartOfMonth(startOfMonth);
		param.setEndOfMonth(endOfMonth);

		CollectionDiscrepancyReportDaoImpl dao = new CollectionDiscrepancyReportDaoImpl();
		dao.setSqlMapClient(sqlMapClient);

		List<DiscrepancyReportEntry> entries = dao
				.getTimelinessDiscrepancyReports(param);
		
		assertEquals(0, entries.size());
	}
	
	public void testGetAccuracyDiscrepancyReports() throws Exception {
		StartEndMonth param = new StartEndMonth();

		Date startOfMonth = WMSDateUtil.startOfMonth("01OCT2012");
		Date endOfMonth = WMSDateUtil.startOfMonth("01NOV2012");

		param.setStartOfMonth(startOfMonth);
		param.setEndOfMonth(endOfMonth);

		CollectionDiscrepancyReportDaoImpl dao = new CollectionDiscrepancyReportDaoImpl();
		dao.setSqlMapClient(sqlMapClient);

		List<DiscrepancyReportEntry> entries = dao
				.getAccuracyDiscrepancyReports(param);
		
		assertEquals(0, entries.size());
	}

}
