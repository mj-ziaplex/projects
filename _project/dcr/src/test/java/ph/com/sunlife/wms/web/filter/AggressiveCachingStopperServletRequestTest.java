package ph.com.sunlife.wms.web.filter;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

public class AggressiveCachingStopperServletRequestTest {

	@Test
	public void testGetQueryString() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET",
				"/handler");
		request.setParameter("myField", "0");

		AggressiveCachingStopperServletRequest requestWrapper = new AggressiveCachingStopperServletRequest(
				request);

		String queryString = requestWrapper.getQueryString();
		System.out.println(queryString);

		StringBuilder url = new StringBuilder();
		url.append(requestWrapper.getRequestURL() + "?"
				+ requestWrapper.getQueryString());

		assertTrue(StringUtils.contains(url.toString(), "tm="));
	}
}
