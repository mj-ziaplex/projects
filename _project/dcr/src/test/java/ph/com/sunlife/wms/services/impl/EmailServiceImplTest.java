package ph.com.sunlife.wms.services.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.services.EmailService;
import ph.com.sunlife.wms.services.bo.ContactBO;

public class EmailServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private EmailService emailServiceTarget;
	
//	private JavaMailSender mailSender;
//
//	private DCRFilenetIntegrationService dcrFilenetIntegrationService;

	public EmailServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

//	public void setMailSender(JavaMailSender mailSender) {
//		this.mailSender = mailSender;
//	}
//
//	public void setDcrFilenetIntegrationService(
//			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
//		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
//	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/service/spring/spring-config.xml" };
	}

	public void testSendEmail() throws Exception {
		// EmailServiceImpl service = new EmailServiceImpl();
		// service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
		// service.setMailSender(mailSender);
		//
		// WMSEmail email = new WMSEmail();
		// email.setDcrDate(WMSDateUtil.toDate("31OCT2012"));
		// email.setDocCapsiteId("A2");
		// email.setTo("crmen@sunlife.com;ecuna@sunlife.com:zmlim@sunlife.com");
		// email.setCc("jsard@sunlife.com;crmen@sunlife.com;ecuna@sunlife.com:zmlim@sunlife.com");
		// email.setFrom("zmlim@sunlife.com");
		// email.setSubject("test");
		// email.setText("some text");
		//
		// service.sendEmail(email);
	}

	public void testGetAddressBook() throws Exception {
		List<ContactBO> list = null;
		list = emailServiceTarget.getAddressBook();
		assertNotNull(list);

		final ContactBO bo = new ContactBO();

		bo.setUserId("PM26");
		bo.setFullName("Albano, Jonathan");
		bo.setShortName("ALBAJ");
		bo.setEmailAddress("PM26@Sunlife.com");

		ContactBO foundContactBO = (ContactBO) CollectionUtils.find(list,
				new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						ContactBO thisContact = (ContactBO) object;
						return thisContact.equals(bo);
					}
				});

		assertNotNull(foundContactBO);
	}


	public void setEmailServiceTarget(EmailService emailServiceTarget) {
		this.emailServiceTarget = emailServiceTarget;
	}

}
