package ph.com.sunlife.wms.services.impl;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;

public class DCRAddingMachingServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRAddingMachineDao dcrAddingMachineDao;

	private DCRAddingMachineServiceImpl dcrAddingMachingService;

	private DCRFilenetIntegrationService dcrFilenetIntegrationService;

	private DCRCashierDao dcrCashierDao;

	public void setDcrFilenetIntegrationService(
			DCRFilenetIntegrationService dcrFilenetIntegrationService) {
		this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public DCRAddingMachingServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected void onSetUp() throws Exception {
		super.onSetUp();
		dcrAddingMachingService = new DCRAddingMachineServiceImpl();
		dcrAddingMachingService.setDcrAddingMachineDao(dcrAddingMachineDao);
		dcrAddingMachingService.setDcrCashierDao(dcrCashierDao);
		dcrAddingMachingService
				.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/service/spring/spring-config.xml" };
	}

	public void setDcrAddingMachineDao(DCRAddingMachineDao dcrAddingMachineDao) {
		this.dcrAddingMachineDao = dcrAddingMachineDao;
	}

	// public void testCheckIfFileIsValid() throws IOException {
	// File file = new File("src/test/resources/IPAC_VALUE_mapping.xlsx");
	// assertTrue(file.exists());
	//
	// byte[] fileData = FileUtils.readFileToByteArray(file);
	//
	// boolean isFileValid = dcrAddingMachingService
	// .checkFileType(fileData);
	// assertTrue(isFileValid);
	//
	// file = new File("src/test/resources/random.txt");
	// assertTrue(file.exists());
	//
	// fileData = FileUtils.readFileToByteArray(file);
	// isFileValid = dcrAddingMachingService.checkFileType(fileData);
	// assertFalse(isFileValid);
	// }

	public void testUploadDCRAddingMachine() throws Exception {
//		File file = new File("src/test/resources/IPAC_VALUE_mapping.xlsx");
//		assertTrue(file.exists());
//
//		byte[] fileData = FileUtils.readFileToByteArray(file);
//
//		DCRAddingMachineBO businessObject = dcrAddingMachingService
//				.uploadDCRAddingMachine(2092L, fileData);
//
//		assertNotNull(businessObject.getId());
	}

}
