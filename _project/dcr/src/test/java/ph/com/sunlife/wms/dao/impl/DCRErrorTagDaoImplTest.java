package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRErrorTagDao;
import ph.com.sunlife.wms.dao.domain.DCRError;

public class DCRErrorTagDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRErrorTagDao dcrErrorTagDao;

	public DCRErrorTagDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}
	
	public void setDcrErrorTagDao(DCRErrorTagDao dcrErrorTagDao) {
		this.dcrErrorTagDao = dcrErrorTagDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}
	
	public void testGetManagerByDcrId() throws Exception{
		endTransaction();
		startNewTransaction();
		
		Long dcrId = 20L;
		String managerId = dcrErrorTagDao.getManagerByDcrId(dcrId);
		
		assertEquals("pl13",managerId);
		
		endTransaction();
	}
	
	public void testGetStepIdByStatus() throws Exception {
		endTransaction();
		startNewTransaction();
		
		String dcrStatusCCM = "For Review and Approval";
		String dcrStatusPPA = "Approved for Recon";
		
		String ccmStepId = dcrErrorTagDao.getStepIdByStatus(dcrStatusCCM);
		assertNotNull(ccmStepId);
		assertEquals("STPCCM01", ccmStepId);
		
		String ppaStepId = dcrErrorTagDao.getStepIdByStatus(dcrStatusPPA);
		assertNotNull(ppaStepId);
		assertEquals("STPPPA01", ppaStepId);
		
		endTransaction();
	}
	
	public void testGetDCRErrorByStatus() throws Exception {
		endTransaction();
		startNewTransaction();
		
		String dcrStatusCCM = "For Review and Approval";
		String dcrStatusPPA = "Approved for Recon";
		
		List<DCRError> ccmErrList = dcrErrorTagDao.getDCRErrorByStatus(dcrStatusCCM);
		assertNotNull(ccmErrList);
		assertEquals(13, ccmErrList.size());
		
		List<DCRError> ppaErrList = dcrErrorTagDao.getDCRErrorByStatus(dcrStatusPPA);
		assertNotNull(ppaErrList);
		assertEquals(20, ppaErrList.size());
		
		endTransaction();
	}
	
	//PASSED
//	public void testGetDCRErrorTagLookup() throws Exception {
//		endTransaction();
//		startNewTransaction();
//		
//		long dcrId = 115L;
//		
//		List<DCRErrorTagLookupDisplay> list = dcrErrorTagDao.getDCRErrorTagLookupDisplay(dcrId);
		
		// assertTrue(CollectionUtils.isNotEmpty(list));
		// assertEquals(33, list.size());
		
		//dcrErrorTagDao.getDCRErrorTagLookup
//		endTransaction();
//	}
	
//	public void testAddDCRErrorTagLog() throws Exception {
//		endTransaction();
//		startNewTransaction();
//		
//		DCRErrorTagLog  errorTagLog1 = new DCRErrorTagLog();
//		errorTagLog1.setDatePosted(new Date());
//		errorTagLog1.setDateUpdated(new Date());
//		
//		long dcrId = Long.MAX_VALUE;
//		errorTagLog1.setDcrId(dcrId);
//		
//		DCRCompletedStep dcrCompletedStep = new DCRCompletedStep();
//		
//		dcrCompletedStep.setUserName("wc01");
//		
//		DCRStep dcrStep = new DCRStep();
//		dcrStep.setStepId("STPCCM01");
//		dcrStep.setStepName("For Review");
//		dcrCompletedStep.setDcrStep(dcrStep);
//		
//		List<DCRError> dcrErrors = new ArrayList<DCRError>();
//		DCRError dcrError1 = new DCRError();
//		dcrError1.setErrorId("ERRCCM07");
//		dcrError1.setErrorName("Pickup date not indicated in VDIL");
//		dcrErrors.add(dcrError1);
//		DCRError dcrError2 = new DCRError();
//		dcrError2.setErrorId("ERRCCM08");
//		dcrError2.setErrorName("Incorrect file uploaded");
//		dcrErrors.add(dcrError2);
//		dcrCompletedStep.setDcrErrors(dcrErrors);
//		
//		errorTagLog1.setDcrCompletedSteps(dcrCompletedStep);
//		
//		errorTagLog1.setReason("basta mali 2");
//		errorTagLog1.setPostedUpdatedById("pw12");
//		//errorTagLog1.setReasonDeleted("");
//		
//		errorTagLog1 = dcrErrorTagDao.addDCRErrorTagLog(errorTagLog1);
//		
//		Long errorTagLogId = errorTagLog1.getId();
//		assertNotNull(errorTagLogId);
//		
//		//dcrErrorTagDao.addDCRErrorTagLog
//		endTransaction();
//	}	
	
//	public void testAddDCRErrorTagLoglookup() throws Exception {
//		endTransaction();
//		startNewTransaction();
		
		//NOTE: sa svc, make a DCRErrorTagLogLookup object for each List<DCRError> dcrErrors of DCRCompletedStep of DCRErrorTagLog
//		DCRErrorTagLogLookup entity = new DCRErrorTagLogLookup();
		//KAYA PALA PASADO EH
//		entity.setDcrId(Long.MAX_VALUE);
//		entity.setStepId("STPCCM01");
//		entity.setErrId("ERRCCM02");
//		entity = dcrErrorTagDao.addDCRErrorTagLoglookup(entity);
		
//		DCRErrorTagLog  errorTagLog2 = new DCRErrorTagLog();
//		long dcrId = Long.MAX_VALUE;
//		
//		errorTagLog2.setDcrId(dcrId);
//		
//		DCRCompletedStep dcrCompletedStep = new DCRCompletedStep();
//		
//		dcrCompletedStep.setStepCompletorUserId("wc01");
//		
//		DCRStep dcrStep = new DCRStep();
//		dcrStep.setStepId("STPCCM01");
//		dcrStep.setStepName("For Review");
//		dcrCompletedStep.setDcrStep(dcrStep);
//		
//		List<DCRError> dcrErrors = new ArrayList<DCRError>();
//		DCRError dcrError1 = new DCRError();
//		dcrError1.setErrorId("ERRCCM07");
//		dcrError1.setErrorName("Pickup date not indicated in VDIL");
//		dcrErrors.add(dcrError1);
//		DCRError dcrError2 = new DCRError();
//		dcrError2.setErrorId("ERRCCM08");
//		dcrError2.setErrorName("Incorrect file uploaded");
//		dcrErrors.add(dcrError2);
//		dcrCompletedStep.setDcrErrors(dcrErrors);
//		
//		errorTagLog2.setDcrCompletedStep(dcrCompletedStep);
//		
//		errorTagLog2 = dcrErrorTagDao.addDCRErrorTagLoglookup(errorTagLog2);
//		
//		Long errorTagLogId = errorTagLog2.getId();
//		assertNotNull(errorTagLogId);
		
//		Long entityId = entity.getId();
//		assertNotNull(entityId);		
		
		//dcrErrorTagDao.addDCRErrorTagLoglookup
//		endTransaction();
//	}
	
	//PASSED
//	public void testGetDCRErrorTagLogDisplay() throws Exception {
//		endTransaction();
//		startNewTransaction();
		
		// long dcrId = 6L;
		// List<DCRErrorTagLogDisplay> list = dcrErrorTagDao.getDCRErrorTagLogDisplay(dcrId);
		
		// assertTrue(CollectionUtils.isNotEmpty(list));
		//assertEquals(10, list.size());
		
		//dcrErrorTagDao.getDCRErrorTagLogDisplay
//		endTransaction();
//	}
	

}
