package ph.com.sunlife.wms.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.mockito.Mockito;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRReversal;
import ph.com.sunlife.wms.dao.domain.NonPostedTransaction;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.integration.client.UserCollectionsTotalUtil;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRCreationServiceImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private static final int TEST_DAYS_AGO = -720;
    private DCRDao dcrDao;
    private DCRCashierDao dcrCashierDao;
    private DCRReversalDao dcrReversalDao;
    private DCRBalancingToolDao dcrBalancingToolDao;
    private UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil;
    private UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil;
    private DCRFilenetIntegrationService dcrFilenetIntegrationService;
    private IpacDao ipacDao;
    private DCRIPACDao dcrIpacDao;
    private CachingService cachingService;

    public DCRCreationServiceImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }

    public void setSlgfiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slgfiUserCollectionsTotalUtil) {
        this.slgfiUserCollectionsTotalUtil = slgfiUserCollectionsTotalUtil;
    }

    public void setSlocpiUserCollectionsTotalUtil(
            UserCollectionsTotalUtil slocpiUserCollectionsTotalUtil) {
        this.slocpiUserCollectionsTotalUtil = slocpiUserCollectionsTotalUtil;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    public void setIpacDao(IpacDao ipacDao) {
        this.ipacDao = ipacDao;
    }

    public void setDCRIpacDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }

    public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
        this.dcrReversalDao = dcrReversalDao;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
        this.dcrCashierDao = dcrCashierDao;
    }

    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
        this.dcrBalancingToolDao = dcrBalancingToolDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/service/spring/spring-config.xml",
                    "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }
//    private static final boolean WILL_CREATE_DCR = false;

    public void testCreateAndLoadDcrWorkItems() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrCashierDao(dcrCashierDao);
        service.setDcrDao(dcrDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
        service.setCachingService(cachingService);

        ipacDao = mock(IpacDao.class);
        service.setIpacDao(ipacDao);

        final Date mockDateYearAgo = DateUtils.addDays(new Date(), TEST_DAYS_AGO);
        Date today = WMSDateUtil.startOfTheDay(mockDateYearAgo);
        Date yesterday = DateUtils.addDays(today, -1);
        Date theDayBeforeYesterday = DateUtils.addDays(today, -2);

        final String acf2id = "PK53";
        final String ccId = "A3";

        DCR dcr1 = new DCR();
        dcr1.setCcId(ccId);
        dcr1.setCcName("ACE");
        dcr1.setCenterCode("ACE");
        dcr1.setDcrDate(today);
        dcr1.setHubId("CCH001");
        dcr1.setStatus("NEW");
        dcr1.setRequiredCompletionDate(WMSDateUtil.getRCD(today));

        DCR dcr2 = new DCR();
        dcr2.setCcId(ccId);
        dcr2.setCcName("ACE");
        dcr2.setCenterCode("ACE");
        dcr2.setDcrDate(yesterday);
        dcr2.setHubId("CCH001");
        dcr2.setStatus("NEW");
        dcr2.setRequiredCompletionDate(WMSDateUtil.getRCD(yesterday));

        DCR dcr3 = new DCR();
        dcr3.setCcId(ccId);
        dcr3.setCcName("ACE");
        dcr3.setCenterCode("ACE");
        dcr3.setDcrDate(theDayBeforeYesterday);
        dcr3.setHubId("CCH001");
        dcr3.setStatus("NEW");
        dcr3.setRequiredCompletionDate(WMSDateUtil.getRCD(theDayBeforeYesterday));

        dcrDao.save(dcr1);
        dcrDao.save(dcr2);
        dcrDao.save(dcr3);

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(new Date());
        dcrCashier.setStatus("NEW");
        dcrCashier.setDcr(dcr1);

        dcrCashierDao.save(dcrCashier);

        when(ipacDao.hasCollections(ccId, today, acf2id)).thenReturn(true);
        when(ipacDao.hasCollections(ccId, yesterday, acf2id)).thenReturn(true);
        when(ipacDao.hasCollections(ccId, theDayBeforeYesterday, acf2id)).thenReturn(false);

        List<DCRCashierBO> cashierWorkItems = service.createAndLoadDcrWorkItems(ccId, acf2id);
        assertEquals(2, cashierWorkItems.size());
        endTransaction();
    }

    public void testInitiateDCRWorkItems() throws Exception {
//		endTransaction();
//		startNewTransaction();
//
//		DCRCreationServiceImpl service = new DCRCreationServiceImpl();
//		service.setDcrCashierDao(dcrCashierDao);
//		service.setDcrDao(dcrDao);
//		service.setDcrReversalDao(dcrReversalDao);
//		service.setIpacDao(ipacDao);
//		service.setCachingService(cachingService);
//
//		// DCRFilenetIntegrationServiceImpl service;
//
//		// dcrFilenetIntegrationService
//		// .setTest(!WILL_CREATE_DCR);
//		// service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
//
//		Date date = new Date();
//
//		if (!WILL_CREATE_DCR) {
//			// date = DateUtils.addDays(date, TEST_DAYS_AGO);
//			date = DateUtils.addDays(date, 3);
//		}
//
//		// date = WMSDateUtil.toDate("31OCT2012");
//
//		List<DCRBO> list = service.initiateDCRWorkItems(date);
//		assertEquals(50, list.size());
//
//		try {
//			list = service.initiateDCRWorkItems(date);
//			fail("Exception is expected");
//		} catch (Exception e) {
//			// expected
//		}
//
//		if (WILL_CREATE_DCR) {
//			setComplete();
//		}
//
//		System.out.println(WILL_CREATE_DCR);
//		endTransaction();
    }

    // @Ignore
    // public void testLoadDCRCashierWorkItems() throws Exception {
    // endTransaction();
    // startNewTransaction();
    //
    // DCRCreationServiceImpl service = new DCRCreationServiceImpl();
    // service.setDcrCashierDao(dcrCashierDao);
    // service.setDcrDao(dcrDao);
    //
    // service.initiateDCRWorkItems(new Date());
    //
    // List<DCRCashierBO> busObjs = service.loadDCRCashierWorkItems("G8",
    // "PK53");
    //
    // assertEquals(1, busObjs.size());
    //
    // List<DCRBO> parentObjs = service.extractDCRFromDCRCashierList(busObjs);
    // assertEquals(2, parentObjs.size());
    // endTransaction();
    // }
    public void testCreateDCRBalancingTools() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrBalancingToolDao(dcrBalancingToolDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setIpacDao(ipacDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);

        Date today = new Date();
        Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
        Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

        DCR dcr = new DCR();
        dcr.setCcId("G8");
        dcr.setDcrDate(startOfLastWeek);
        dcr.setStatus(WMSConstants.STATUS_NEW);

        String acf2id = "PK53";

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(startOfLastWeek);
        dcrCashier.setStatus(WMSConstants.STATUS_NEW);

        dcr = dcrDao.save(dcr);

        dcrCashier.setDcr(dcr);
        dcrCashier = dcrCashierDao.save(dcrCashier);

        Long dcrCashierId = dcrCashier.getId();

        DCRCashierBO bo = new DCRCashierBO();
        bo.setId(dcrCashierId);

        List<DCRBalancingToolBO> list = service.createAndDisplayDCRBalancingTools(bo);
        assertEquals(6, list.size());

        endTransaction();
    }

    public void testConfirmDCRBalancingTool() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrBalancingToolDao(dcrBalancingToolDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setDcrCashierDao(dcrCashierDao);
        service.setIpacDao(ipacDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);

        Date today = new Date();
        Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
        Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

        DCR dcr = new DCR();
        dcr.setCcId("G8");
        dcr.setDcrDate(startOfLastWeek);
        dcr.setStatus(WMSConstants.STATUS_NEW);

        String acf2id = "PK53";

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(startOfLastWeek);
        dcrCashier.setStatus(WMSConstants.STATUS_NEW);

        dcr = dcrDao.save(dcr);

        dcrCashier.setDcr(dcr);
        dcrCashier = dcrCashierDao.save(dcrCashier);

        Long dcrCashierId = dcrCashier.getId();

        DCRCashierBO bo = new DCRCashierBO();
        bo.setId(dcrCashierId);

        List<DCRBalancingToolBO> list = service.createAndDisplayDCRBalancingTools(bo);
        assertEquals(6, list.size());

        for (DCRBalancingToolBO busObj : list) {
            Date dateConfirmed = DateUtils.addDays(lastWeek, 1);
            busObj.setDateConfirmed(dateConfirmed);
            service.confirmDCRBalancingTool(busObj, true);
        }

        list = service.createAndDisplayDCRBalancingTools(bo);

        for (DCRBalancingToolBO busObj : list) {
            assertNotNull(busObj.getDateConfirmed());
        }

        DCRCashier rdc = dcrCashierDao.getById(dcrCashierId);
        assertEquals(WMSConstants.STATUS_PENDING, rdc.getStatus());

        endTransaction();
    }

    public void testGetDCRCashierBO() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrBalancingToolDao(dcrBalancingToolDao);
        service.setDcrCashierDao(dcrCashierDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setIpacDao(ipacDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);

        Date today = new Date();
        Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
        Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
        String acf2id = "PK53";

        DCR dcr = new DCR();
        dcr.setCcId("G8");
        dcr.setDcrDate(startOfLastWeek);
        dcr.setStatus("NEW");

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(startOfLastWeek);
        dcrCashier.setStatus("NEW");

        dcr = dcrDao.save(dcr);

        dcrCashier.setDcr(dcr);
        dcrCashier = dcrCashierDao.save(dcrCashier);

        Long dcrCashierId = dcrCashier.getId();

        DCRCashierBO bo = service.getDCRCashierBO(dcrCashierId, acf2id);

        assertNotNull(bo);
        assertEquals(dcrCashierId, bo.getId());
        assertEquals(acf2id, bo.getCashier().getAcf2id());
        assertEquals("NEW", bo.getStatus());

        endTransaction();
    }

    public void testBuildIpacValues() throws Exception {
        endTransaction();
        startNewTransaction();
        
        dcrIpacDao = mock(DCRIPACDao.class);

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrBalancingToolDao(dcrBalancingToolDao);
        service.setDcrCashierDao(dcrCashierDao);
        service.setDcrDao(dcrDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setIpacDao(ipacDao);
        service.setDcrIpacDao(dcrIpacDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);

        Date today = new Date();
        Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
        Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
        String acf2id = "PK53";

        DCR dcr = new DCR();
        dcr.setCcId("G8");
        dcr.setDcrDate(startOfLastWeek);
        dcr.setStatus("NEW");

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(startOfLastWeek);
        dcrCashier.setStatus("NEW");

        dcr = dcrDao.save(dcr);

        dcrCashier.setDcr(dcr);
        dcrCashier = dcrCashierDao.save(dcrCashier);

        Long dcrCashierId = dcrCashier.getId();

        DCRCashierBO bo = service.getDCRCashierBO(dcrCashierId, acf2id);

        List<DCRIpacValueBO> values = null;

        values = service.buildIpacValues(values, bo);
        assertEquals(0, values.size());

        endTransaction();
    }

    @SuppressWarnings("unchecked")
    public void testBuildBalancingToolProducts() throws Exception {
        endTransaction();
        startNewTransaction();

        DCRCreationServiceImpl service = new DCRCreationServiceImpl();
        service.setDcrBalancingToolDao(dcrBalancingToolDao);
        service.setDcrCashierDao(dcrCashierDao);
        service.setDcrDao(dcrDao);
        service.setDcrReversalDao(dcrReversalDao);
        service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
        service.setSlocpiUserCollectionsTotalUtil(slocpiUserCollectionsTotalUtil);
        service.setSlgfiUserCollectionsTotalUtil(slgfiUserCollectionsTotalUtil);

        Date lastYear = DateUtils.addDays(new Date(), -365);

        NonPostedTransaction tr1 = new NonPostedTransaction();
        tr1.setAmount(1000.0);
        tr1.setCompanyCode("SLOCP");
        tr1.setPaymentCurrencyStr("PHP");
        tr1.setProductCode("IL");
        tr1.setPaymentPostStatus(0);
        tr1.setTransactionType("I");
        tr1.setUserId("PK53");

        NonPostedTransaction tr2 = new NonPostedTransaction();
        tr2.setAmount(2000.0);
        tr2.setCompanyCode("SLOCP");
        tr2.setPaymentCurrencyStr("PHP");
        tr2.setProductCode("IL");
        tr2.setPaymentPostStatus(0);
        tr2.setTransactionType("R");
        tr2.setUserId("PK53");

        NonPostedTransaction tr3 = new NonPostedTransaction();
        tr3.setAmount(3000.0);
        tr3.setCompanyCode("SLOCP");
        tr3.setPaymentCurrencyStr("PHP");
        tr3.setProductCode("VL");
        tr3.setPaymentPostStatus(0);
        tr3.setTransactionType("VI");
        tr3.setUserId("PK53");

        NonPostedTransaction tr4 = new NonPostedTransaction();
        tr4.setAmount(4000.0);
        tr4.setCompanyCode("SLOCP");
        tr4.setPaymentCurrencyStr("PHP");
        tr4.setProductCode("VL");
        tr4.setPaymentPostStatus(0);
        tr4.setTransactionType("VR");
        tr4.setUserId("PK53");

        NonPostedTransaction tr5 = new NonPostedTransaction();
        tr5.setAmount(5000.0);
        tr5.setCompanyCode("SLOCP");
        tr5.setPaymentCurrencyStr("PHP");
        tr5.setProductCode("VL");
        tr5.setPaymentPostStatus(0);
        tr5.setTransactionType("W");
        tr5.setUserId("PK53");

        NonPostedTransaction tr6 = new NonPostedTransaction();
        tr6.setAmount(6000.0);
        tr6.setCompanyCode("SLOCP");
        tr6.setPaymentCurrencyStr("PHP");
        tr6.setProductCode("IL");
        tr6.setPaymentPostStatus(0);
        tr6.setTransactionType("N");
        tr6.setUserId("PK53");

        List<NonPostedTransaction> dtrList = new ArrayList<NonPostedTransaction>();
        dtrList.add(tr1);
        dtrList.add(tr2);
        dtrList.add(tr3);
        dtrList.add(tr4);
        dtrList.add(tr5);
        dtrList.add(tr6);

        dcrIpacDao = mock(DCRIPACDao.class);
        service.setDcrIpacDao(dcrIpacDao);
        Mockito.when(dcrIpacDao.getDCRIPACDTR("G8", lastYear, "PK53")).thenReturn(dtrList);
        Mockito.when(dcrIpacDao.getDCRIPACDTR("G8", lastYear)).thenReturn(dtrList);

        service.setIpacDao(ipacDao);

        Date today = new Date();
        Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
        Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
        String acf2id = "PK53";

        DCR dcr = new DCR();
        dcr.setCcId("G8");
        dcr.setDcrDate(startOfLastWeek);
        dcr.setStatus("NEW");

        DCRCashier dcrCashier = new DCRCashier();
        dcrCashier.setCashier(acf2id);
        dcrCashier.setLastUpdateDate(startOfLastWeek);
        dcrCashier.setStatus("NEW");

        dcr = dcrDao.save(dcr);

        dcrCashier.setDcr(dcr);
        dcrCashier = dcrCashierDao.save(dcrCashier);

        Long dcrCashierId = dcrCashier.getId();

        DCRCashierBO bo = service.getDCRCashierBO(dcrCashierId, acf2id);

        List<DCRIpacValueBO> values = null;

        values = service.buildIpacValues(values, bo);
        assertEquals(0, values.size());

        values = service.buildIpacValues(values, bo);
        assertEquals(0, values.size());

        List<DCRBalancingToolBO> balancingTools = service.createAndDisplayDCRBalancingTools(bo);

        List<DCRBalancingToolProductBO> selectedProducts = null;
        DCRBalancingToolProductBO selectedProduct = null;

        for (DCRBalancingToolBO btb : balancingTools) {
            Company thisCompany = Company.getCompany(btb.getCompany().getId());
            if (Company.SLOCPI.equals(thisCompany)) {

                List<DCRBalancingToolProductBO> products = btb.getProducts();

                values = service.buildIpacValues(values, bo);
                products = service.buildBalancingToolProducts("G8", lastYear, "PK53", values, products);

                int numberOfIpacValues = countIpacValues(products);
                assertEquals(0, numberOfIpacValues);
                btb.setProducts(products);

                selectedProduct = products.get(0);
                this.createDCRReversals(selectedProduct);

                selectedProducts = products;
            } else if (Company.OTHER.equals(thisCompany)) {
                List<DCRBalancingToolProductBO> products = btb.getProducts();

                values = service.buildIpacValues(values, bo);
                products = service.buildBalancingToolProducts("G8", lastYear, "PK53", values, products);
                btb.setProducts(products);

                for (DCRBalancingToolProductBO product : products) {
                    assertNotNull(product.getDcrOther());
                }
            }
        }

        service.buildBalancingToolProducts(null, null, null, Collections.EMPTY_LIST, selectedProducts);

        for (DCRBalancingToolProductBO product : selectedProducts) {
            if (product.equals(selectedProduct)) {
                assertEquals(2, selectedProduct.getDcrReversals().size());
            }

            if (ProductType.SLOCPI_P_TRADVUL.equals(product.getProductType())) {
                assertEquals(1000.0, product.getTotalNewBusinessIndividual());
                assertEquals(2000.0, product.getTotalIndividualRenewal());
                assertEquals(3000.0, product.getTotalNewBusinessVariable());
                assertEquals(4000.0, product.getTotalVariableRenewal());
                assertEquals(5000.0, product.getTotalWorksiteNonPosted());
                assertEquals(6000.0, product.getTotalNonPolicy());
            }
        }
        endTransaction();
    }

    private void createDCRReversals(DCRBalancingToolProductBO product)
            throws Exception {
        Date today = new Date();

        DCRReversal r1 = new DCRReversal();
        r1.setAmount(1000.0);
        r1.setRemarks("Lorem Ipsum Dolor");
        r1.setCurrency(Currency.PHP);
        r1.setDcrBalancingToolProductId(product.getId());
        r1.setDateTime(today);

        DCRReversal r2 = new DCRReversal();
        r2.setAmount(1000.0);
        r2.setRemarks("Lorem Ipsum Dolor");
        r2.setCurrency(Currency.PHP);
        r2.setDcrBalancingToolProductId(product.getId());
        r2.setDateTime(today);

        dcrReversalDao.save(r1);
        dcrReversalDao.save(r2);
    }

    private int countIpacValues(List<DCRBalancingToolProductBO> products) {
        int count = 0;

        for (DCRBalancingToolProductBO product : products) {
            count += product.getDcrIpacValues().size();
        }

        return count;
    }
}
