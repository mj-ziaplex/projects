package ph.com.sunlife.wms.util;

import junit.framework.TestCase;

public class MathUtilTest extends TestCase {

	public void testRound() throws Exception {
		double value = 999999999999999999998.4899999;
		double roundedValue = MathUtil.round(value);
		
		double expectedValue = 999999999999999999998.49;
		assertEquals(expectedValue, roundedValue);
	}
}
