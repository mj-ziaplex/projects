package ph.com.sunlife.wms.services.bo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ph.com.sunlife.wms.util.WMSDateUtil;
import junit.framework.TestCase;

public class DiscrepancyReportEntryBOTest extends TestCase {

	public void testSort() {
		DiscrepancyReportEntryBO b1 = new DiscrepancyReportEntryBO();
		b1.setCcName("Makati");
		b1.setDcrDate(WMSDateUtil.toDate("01MAR2013"));

		DiscrepancyReportEntryBO b2 = new DiscrepancyReportEntryBO();
		b2.setCcName("Alabang");
		b2.setDcrDate(WMSDateUtil.toDate("01MAR2013"));

		DiscrepancyReportEntryBO b3 = new DiscrepancyReportEntryBO();
		b3.setCcName("Makati");
		b3.setDcrDate(WMSDateUtil.toDate("28FEB2013"));

		List<DiscrepancyReportEntryBO> list = new ArrayList<DiscrepancyReportEntryBO>();
		list.add(b1);
		list.add(b2);
		list.add(b3);
		
		Collections.sort(list);
		
		assertEquals(b3, list.get(0));
		assertEquals(b2, list.get(1));
		assertEquals(b1, list.get(2));
	}
}
