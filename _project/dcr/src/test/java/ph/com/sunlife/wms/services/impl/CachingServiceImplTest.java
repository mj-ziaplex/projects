package ph.com.sunlife.wms.services.impl;

import java.util.Date;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.services.CachingService;

public class CachingServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private CachingService cachingService;

	private CachingServiceImpl cachingServiceTarget;
	
	public CachingServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setCachingService(CachingService cachingService) {
		this.cachingService = cachingService;
	}
	
	public void setCachingServiceTarget(CachingServiceImpl cachingServiceTarget) {
		this.cachingServiceTarget = cachingServiceTarget;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/service/spring/spring-config.xml" };
	}

	public void testGetCachedValue() throws Exception {
		String value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		value = cachingService.getCachedValue("ingenium.key.location");
		assertEquals("C:/key/the.key", value);
		
		Date date = cachingService.getWmsDcrSystemDate();
		System.out.println(date);
		
		cachingService.resetCachedValues();
		
		//System.out.println("Syempre si " + cachingService.getCachedValue("SinoPogi") + " ang pogi");
		
	}
	
	public void testIncrementWmsDcrSystemDate() throws Exception {
		endTransaction();
		startNewTransaction();
		assertTrue(cachingService.incrementWmsDcrSystemDate());
		endTransaction();
	}
	
	@Override
	protected void onSetUp() throws Exception {
		super.onSetUp();
		cachingServiceTarget.setUseSystemDate(false);
	}

	@Override
	protected void onTearDown() throws Exception {
		super.onTearDown();
		cachingServiceTarget.setUseSystemDate(true);
	}

	public void testSystemDate() {
		Date date = cachingServiceTarget.getWmsDcrSystemDate();
		
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(new Date());
//		
//		int hour = cal.get(Calendar.HOUR_OF_DAY);
//		int minute = cal.get(Calendar.MINUTE);
//		int second = cal.get(Calendar.SECOND);
//		
//		date = DateUtils.addHours(date, hour);
//		date = DateUtils.addMinutes(date, minute);
//		date = DateUtils.addSeconds(date, second);
		
		System.out.println("WMS System Date is " + date);
	}
}
