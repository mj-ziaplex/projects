package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.dao.domain.JobData;
import ph.com.sunlife.wms.dao.domain.JobDataErrorLog;
import ph.com.sunlife.wms.util.ExceptionUtil;

public class JobDataDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private JobDataDao jobDataDao;

	public JobDataDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setJobDataDao(JobDataDao jobDataDao) {
		this.jobDataDao = jobDataDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();
		Exception ex = new RuntimeException("Something went wrong");
		String stackTrace = ExceptionUtil.getStackTrace(ex);
		String trimmedStackTrace = ExceptionUtil.getFirstNCharacters(
				stackTrace, 1000);

		JobDataErrorLog log = new JobDataErrorLog();
		Date today = new Date();
		log.setDateTime(today);
		log.setJobId(1L);
		log.setErrorMessage(trimmedStackTrace);

		Long id = jobDataDao.createErrorLog(log);
		assertNotNull(id);
		log.setId(id);

		List<JobDataErrorLog> logs = jobDataDao.getJobDataErrorLog(1L);
		assertTrue(logs.contains(log));

		List<JobData> jobs = jobDataDao.getAllJobData();
		assertEquals(5, jobs.size());

		JobData selectedJobData = jobs.get(1);
		selectedJobData.setLastRun(today);
		selectedJobData.setSuccessful(false);
		boolean isUpdated = jobDataDao.updateJobData(selectedJobData);
		assertTrue(isUpdated);

		jobs = jobDataDao.getAllJobData();
		assertEquals(5, jobs.size());

		JobData updatedJobData = jobs.get(1);
		assertFalse(updatedJobData.isSuccessful());
		assertTrue(DateUtils.isSameDay(today, selectedJobData.getLastRun()));

		endTransaction();

	}
}
