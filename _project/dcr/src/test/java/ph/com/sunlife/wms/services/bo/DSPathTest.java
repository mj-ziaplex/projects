package ph.com.sunlife.wms.services.bo;

import junit.framework.TestCase;

public class DSPathTest extends TestCase {

	public void testGetDSPath() throws Exception {
		DSPath thisPath = DSPath.getDSPath(1L, 1L, "Cash");
		assertEquals(DSPath.SLAMCIP_CASH, thisPath);

		thisPath = DSPath.getDSPath(2L, 2L, "Cash");
		assertEquals(DSPath.SLAMCID_CASH, thisPath);
		
		thisPath = DSPath.getDSPath(3L, 2L, "Cash");
		assertEquals(DSPath.SLOCPID_CASH, thisPath);
		
		thisPath = DSPath.getDSPath(3L, 1L, "Cash");
		assertEquals(DSPath.SLOCPIP_CASH, thisPath);
		
		thisPath = DSPath.getDSPath(3L, 1L, "Cheque");
		assertEquals(DSPath.SLOCPIP_CHEQUE, thisPath);
		
		thisPath = DSPath.getDSPath(3L, 2L, "Cheque");
		assertEquals(DSPath.SLOCPID_CHEQUE, thisPath);
		
		thisPath = DSPath.getDSPath(4L, 1L, "Cheque");
		assertEquals(DSPath.PESO_CHEQUE, thisPath);

		thisPath = DSPath.getDSPath(2L, 1L, "Cheque");
		assertNull(thisPath);
	}
}
