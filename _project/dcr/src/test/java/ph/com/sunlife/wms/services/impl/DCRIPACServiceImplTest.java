/**
 * 
 */
package ph.com.sunlife.wms.services.impl;

import org.junit.Ignore;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.JobDataDao;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.exception.ServiceException;

/**
 * @author i176
 *
 */
@Ignore
public class DCRIPACServiceImplTest extends AbstractTransactionalDataSourceSpringContextTests {

//    private IpacDao ipacDao;
    private DCRDao dcrDao;
    private DCRIPACDao dcrIpacDao;
    private JobDataDao jobDataDao;
    private CachingService cachingService;

    public JobDataDao getJobDataDao() {
        return jobDataDao;
    }

    public void setJobDataDao(JobDataDao jobDataDao) {
        this.jobDataDao = jobDataDao;
    }

    public CachingService getCachingService() {
        return cachingService;
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }

//    public IpacDao getIpacDao() {
//        return ipacDao;
//    }

//    public void setIpacDao(IpacDao ipacDao) {
//        this.ipacDao = ipacDao;
//    }

    public DCRDao getDcrDao() {
        return dcrDao;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    public DCRIPACDao getDcrIPACDao() {
        return dcrIpacDao;
    }

    public void setDcrIPACDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }

    public DCRIPACServiceImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
        //this.setDefaultRollback(false);
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
                    "ph/com/sunlife/wms/service/spring/properties-config.xml",
                    "ph/com/sunlife/wms/service/spring/service-config.xml"};
    }

    public void testRunIPACToDCRJob() throws ServiceException {
        DCRIPACServiceImpl service = new DCRIPACServiceImpl();
        service.setDcrDao(dcrDao);
        service.setDcrIPACDao(dcrIpacDao);
//        service.setIpacDao(ipacDao);
        service.setJobDataDao(jobDataDao);
        service.setCachingService(cachingService);
        assertTrue(service.runIPACToDCRJob());
    }
}
