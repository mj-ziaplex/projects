package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.ContactDao;
import ph.com.sunlife.wms.dao.domain.Contact;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

public class ContactDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private ContactDao contactDao;

	public void setContactDao(ContactDao contactDao) {
		this.contactDao = contactDao;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public ContactDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testGetAllWMSUsers() throws WMSDaoException {

		List<Contact> list = null;
		list = contactDao.getAllWMSUsers();
		assertNotNull(list);

		final Contact contact = new Contact();

		contact.setUserId("PM26");
		contact.setFullName("Albano, Jonathan");
		contact.setShortName("ALBAJ");
		Contact foundContact = (Contact) CollectionUtils.find(list,
				new Predicate() {

					@Override
					public boolean evaluate(Object object) {
						Contact thisContact = (Contact) object;
						return thisContact.equals(contact);
					}
				});
	
		assertNotNull(foundContact);
	}

}
