package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;

public class DCRReversalServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRReversalDao dcrReversalDao;

	private DCRBalancingToolDao dcrBalancingToolDao;
	
	public DCRReversalServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
		this.dcrReversalDao = dcrReversalDao;
	}
	
	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
		this.dcrBalancingToolDao = dcrBalancingToolDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testSaveAndGetAllByBalancingToolProduct() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRReversalServiceImpl service = new DCRReversalServiceImpl();
		service.setDcrReversalDao(dcrReversalDao);
		service.setDcrBalancingToolDao(dcrBalancingToolDao);

		DCRReversalBO businessObject = new DCRReversalBO();
		businessObject.setAmount(1000.0);
		businessObject.setCurrency(Currency.PHP);
		businessObject.setDateTime(new Date());
		businessObject.setDcrBalancingToolProductId(Long.MAX_VALUE);
		businessObject.setRemarks("Lorem Ipsum Dolor");

		businessObject = service.save(businessObject);
		assertNotNull(businessObject.getId());

		DCRReversalBO businessObject2 = new DCRReversalBO();
		businessObject2.setAmount(2000.0);
		businessObject2.setCurrency(Currency.PHP);
		businessObject2.setDateTime(new Date());
		businessObject2.setDcrBalancingToolProductId(Long.MAX_VALUE);
		businessObject2.setRemarks("Lorem Ipsum Dolor 2");

		businessObject2 = service.save(businessObject2);
		assertNotNull(businessObject2.getId());

		List<DCRReversalBO> list = service
				.getAllByBalancingToolProduct(Long.MAX_VALUE);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(2, list.size());

		endTransaction();
	}

	public void testUpdateAndDelete() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRReversalServiceImpl service = new DCRReversalServiceImpl();
		service.setDcrReversalDao(dcrReversalDao);
		service.setDcrBalancingToolDao(dcrBalancingToolDao);

		DCRReversalBO businessObject = new DCRReversalBO();
		businessObject.setAmount(1000.0);
		businessObject.setCurrency(Currency.PHP);
		businessObject.setDateTime(new Date());
		businessObject.setDcrBalancingToolProductId(Long.MAX_VALUE);
		businessObject.setRemarks("Lorem Ipsum Dolor");

		businessObject = service.save(businessObject);
		assertNotNull(businessObject.getId());

		DCRReversalBO businessObject2 = new DCRReversalBO();
		businessObject2.setAmount(2000.0);
		businessObject2.setCurrency(Currency.PHP);
		businessObject2.setDateTime(new Date());
		businessObject2.setDcrBalancingToolProductId(Long.MAX_VALUE);
		businessObject2.setRemarks("Lorem Ipsum Dolor 2");

		businessObject2 = service.save(businessObject2);
		assertNotNull(businessObject2.getId());

		businessObject.setAmount(1500.0);
		businessObject.setRemarks("New Reversal");

		service.updateReversal(businessObject);
		assertTrue(service.deleteById(businessObject2.getId()));

		List<DCRReversalBO> list = service
				.getAllByBalancingToolProduct(Long.MAX_VALUE);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(1, list.size());

		DCRReversalBO actual = list.get(0);
		assertEquals(businessObject, actual);
		assertEquals("New Reversal", actual.getRemarks());
		assertEquals(1500.0, actual.getAmount());

		endTransaction();
	}
}
