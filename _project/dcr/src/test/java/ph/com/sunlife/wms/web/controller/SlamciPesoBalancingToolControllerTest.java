package ph.com.sunlife.wms.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Ignore;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRReversalDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRCommentService;
import ph.com.sunlife.wms.services.DCRLogInService;
import ph.com.sunlife.wms.services.bo.CashierBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolBO;
import ph.com.sunlife.wms.services.bo.DCRBalancingToolProductBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRCommentBO;
import ph.com.sunlife.wms.services.bo.DCRIpacValueBO;
import ph.com.sunlife.wms.services.bo.DCRReversalBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.services.impl.DCRCreationServiceImpl;
import ph.com.sunlife.wms.services.impl.DCRReversalServiceImpl;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.util.WMSDateUtil;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
@Ignore
public class SlamciPesoBalancingToolControllerTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private static final int DAYS_AGO = -730;

	private DCRDao dcrDao;

	private DCRCashierDao dcrCashierDao;

	private DCRBalancingToolDao dcrBalancingToolDao;

	private PropertiesMethodNameResolver slamciPesoBalancingToolMethodResolver;

	private DCRCommentService dcrCommentService;

	private DCRReversalDao dcrReversalDao;
	
	private DCRLogInService dcrLogInService;
	
	private IpacDao ipacDao;
	
	private CachingService cachingServiceTarget;
	
	private AttachmentService attachmentService;
	
	public void setCachingServiceTarget(CachingService cachingServiceTarget) {
		this.cachingServiceTarget = cachingServiceTarget;
	}
	
	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public void setDcrLogInService(DCRLogInService dcrLogInService) {
		this.dcrLogInService = dcrLogInService;
	}

	public void setSlamciPesoBalancingToolMethodResolver(
			PropertiesMethodNameResolver slamciPesoBalancingToolMethodResolver) {
		this.slamciPesoBalancingToolMethodResolver = slamciPesoBalancingToolMethodResolver;
	}

	public void setDcrCommentService(DCRCommentService dcrCommentService) {
		this.dcrCommentService = dcrCommentService;
	}

	public SlamciPesoBalancingToolControllerTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}
	
	public void setIpacDao(IpacDao ipacDao) {
		this.ipacDao = ipacDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
		this.dcrBalancingToolDao = dcrBalancingToolDao;
	}

	public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
		this.dcrReversalDao = dcrReversalDao;
	}
	
	

	@Override
	protected String[] getConfigLocations() {
		return new String[] {
				"ph/com/sunlife/wms/commons/spring/commons-config.xml",
				"ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/service-config.xml",
				"ph/com/sunlife/wms/web/spring/web-config.xml",
				"ph/com/sunlife/wms/web/spring/properties-config.xml",};
	}

	@SuppressWarnings("unchecked")
	public void testShowBalancingToolPage() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCreationServiceImpl service = new DCRCreationServiceImpl();
		service.setDcrBalancingToolDao(dcrBalancingToolDao);
		service.setDcrCashierDao(dcrCashierDao);
		service.setDcrDao(dcrDao);
		service.setDcrReversalDao(dcrReversalDao);
		service.setIpacDao(ipacDao);
		service.setCachingService(cachingServiceTarget);

		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
		String acf2id = "pu98";

		DCR dcr = new DCR();
		dcr.setCcId("A4");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier(acf2id);
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		Long dcrCashierId = dcrCashier.getId();

		CashierWorkItemForm form = new CashierWorkItemForm();
		form.setDcrCashierId(dcrCashierId);

		SlamciPesoBalancingToolController controller = new SlamciPesoBalancingToolController();
		controller.setDcrCreationService(service);
		controller.setMethodNameResolver(slamciPesoBalancingToolMethodResolver);
		controller.setDisplayViewName("slamciBalancingToolView");
		controller.setDcrCommentService(dcrCommentService);
		controller.setDcrLogInService(dcrLogInService);
		controller.setCachingService(cachingServiceTarget);
		controller.setAttachmentService(attachmentService);

		MockHttpServletRequest req = new MockHttpServletRequest("POST", "/slamcip/index.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));

		MockHttpServletResponse resp = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();

		UserSession userSession = new UserSession();
		userSession.setUserId(acf2id);
		userSession.setCashierBO(new CashierBO(acf2id));
		userSession.setSiteCode("A4");
		userSession.addRole(Role.CASHIER);
		session.setAttribute(WMSConstants.USER_SESSION, userSession);
		req.setSession(session);

		ModelAndView mv = controller.handleRequest(req, resp);

		Map<String, Object> model = mv.getModel();
		DCRCashierBO bo = (DCRCashierBO) model.get("dcrCashierBO");
		DCRBalancingToolBO balancingToolBO = (DCRBalancingToolBO) model.get("balancingTool");
		List<DCRBalancingToolProductBO> productsToDisplay = (List<DCRBalancingToolProductBO>) model.get("productsToDisplay");

		assertNotNull(bo);
		assertNotNull(balancingToolBO);
		assertTrue(CollectionUtils.isNotEmpty(productsToDisplay));

		assertEquals(Company.SLAMCIP.getId(), balancingToolBO.getCompany()
				.getId());
		assertEquals(8, productsToDisplay.size());

		List<DCRIpacValueBO> ipacValues = controller.getIpacValues(req, bo);
		assertEquals(0, ipacValues.size());

		endTransaction();
	}

	@SuppressWarnings("unchecked")
	public void testConfirmBalancingToolPage() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCreationServiceImpl service = new DCRCreationServiceImpl();
		service.setDcrBalancingToolDao(dcrBalancingToolDao);
		service.setDcrCashierDao(dcrCashierDao);
		service.setDcrDao(dcrDao);
		service.setDcrReversalDao(dcrReversalDao);
		service.setIpacDao(ipacDao);
		service.setCachingService(cachingServiceTarget);
		
		Date today = new Date();
		Date lastYear = DateUtils.addDays(today, DAYS_AGO);
		Date startOfLastYear = WMSDateUtil.startOfTheDay(lastYear);
		String acf2id = "pu98";

		DCR dcr = new DCR();
		dcr.setCcId("A4");
		dcr.setDcrDate(startOfLastYear);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier(acf2id);
		dcrCashier.setLastUpdateDate(startOfLastYear);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		Long dcrCashierId = dcrCashier.getId();

		DCRCommentBO comment1 = new DCRCommentBO();
		comment1.setAcf2id(acf2id);
		comment1.setDatePosted(lastYear);
		comment1.setDcrCashier(dcrCashierId);
		comment1.setRemarks("Lorem Ipsum Dolor");
		comment1 = dcrCommentService.save(comment1);

		CashierWorkItemForm form = new CashierWorkItemForm();
		form.setDcrCashierId(dcrCashierId);

		SlamciPesoBalancingToolController controller = new SlamciPesoBalancingToolController();
		controller.setDcrCreationService(service);
		controller.setMethodNameResolver(slamciPesoBalancingToolMethodResolver);
		controller.setDisplayViewName("slamciBalancingToolView");
		controller.setDcrCommentService(dcrCommentService);
		controller.setDcrLogInService(dcrLogInService);
		controller.setCachingService(cachingServiceTarget);
		controller.setAttachmentService(attachmentService);

		MockHttpServletRequest req = new MockHttpServletRequest("POST",
				"/slamcip/confirm.wms");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));

		MockHttpServletResponse resp = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();

		UserSession userSession = new UserSession();
		userSession.setUserId(acf2id);
		userSession.setCashierBO(new CashierBO(acf2id));
		userSession.setSiteCode("A4");
		userSession.addRole(Role.CASHIER);
		session.setAttribute(WMSConstants.USER_SESSION, userSession);
		req.setSession(session);

		ModelAndView mv = controller.handleRequest(req, resp);

		Map<String, Object> model = mv.getModel();
		DCRCashierBO bo = (DCRCashierBO) model.get("dcrCashierBO");
		DCRBalancingToolBO balancingToolBO = (DCRBalancingToolBO) model
				.get("balancingTool");
		List<DCRBalancingToolProductBO> productsToDisplay = (List<DCRBalancingToolProductBO>) model
				.get("productsToDisplay");

		assertNotNull(bo);
		assertNotNull(balancingToolBO);
		assertTrue(CollectionUtils.isNotEmpty(productsToDisplay));

		assertEquals(Company.SLAMCIP.getId(), balancingToolBO.getCompany()
				.getId());
		assertEquals(8, productsToDisplay.size());

		List<DCRIpacValueBO> ipacValues = controller.getIpacValues(req, bo);
		assertEquals(0, ipacValues.size());

		List<DCRCommentBO> comments = (List<DCRCommentBO>) model
				.get("dcrComments");
		assertEquals(1, comments.size());
		assertEquals(comment1, comments.get(0));

		boolean isConfirmed = (Boolean) model.get("isConfirmed");
		assertTrue(isConfirmed);

		endTransaction();
	}

	@SuppressWarnings("unchecked")
	public void testAllReversalFunctions() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCreationServiceImpl service = new DCRCreationServiceImpl();
		service.setDcrBalancingToolDao(dcrBalancingToolDao);
		service.setDcrCashierDao(dcrCashierDao);
		service.setDcrDao(dcrDao);
		service.setDcrReversalDao(dcrReversalDao);
		service.setIpacDao(ipacDao);
		service.setCachingService(cachingServiceTarget);

		DCRReversalServiceImpl service2 = new DCRReversalServiceImpl();
		service2.setDcrReversalDao(dcrReversalDao);

		Date today = new Date();
		Date lastYear = DateUtils.addDays(today, DAYS_AGO);
		Date startOfLastYear = WMSDateUtil.startOfTheDay(lastYear);
		String acf2id = "pu98";

		DCR dcr = new DCR();
		dcr.setCcId("A4");
		dcr.setDcrDate(startOfLastYear);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier(acf2id);
		dcrCashier.setLastUpdateDate(startOfLastYear);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		Long dcrCashierId = dcrCashier.getId();

		DCRCommentBO comment1 = new DCRCommentBO();
		comment1.setAcf2id(acf2id);
		comment1.setDatePosted(lastYear);
		comment1.setDcrCashier(dcrCashierId);
		comment1.setRemarks("Lorem Ipsum Dolor");
		comment1 = dcrCommentService.save(comment1);

		CashierWorkItemForm form = new CashierWorkItemForm();
		form.setDcrCashierId(dcrCashierId);

		SlamciPesoBalancingToolController controller = new SlamciPesoBalancingToolController();
		controller.setDcrCreationService(service);
		controller.setMethodNameResolver(slamciPesoBalancingToolMethodResolver);
		controller.setDisplayViewName("slamciBalancingToolView");
		controller.setDcrCommentService(dcrCommentService);
		controller.setDcrReversalService(service2);
		controller.setDcrLogInService(dcrLogInService);
		controller.setCachingService(cachingServiceTarget);
		controller.setAttachmentService(attachmentService);

		MockHttpServletRequest req = new MockHttpServletRequest("POST",
				"/slamcip/index.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));
		// req.setParameter("amount", "1000.0");
		// req.setParameter("currencyId", "1");

		MockHttpServletResponse resp = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();

		UserSession userSession = new UserSession();
		userSession.setUserId(acf2id);
		userSession.setCashierBO(new CashierBO(acf2id));
		userSession.setSiteCode("A4");
		userSession.addRole(Role.CASHIER);
		session.setAttribute(WMSConstants.USER_SESSION, userSession);
		req.setSession(session);

		ModelAndView firstMv = controller.handleRequest(req, resp);

		List<DCRBalancingToolProductBO> productsToDisplay = (List<DCRBalancingToolProductBO>) firstMv
				.getModel().get("productsToDisplay");

		DCRBalancingToolProductBO firstProduct = productsToDisplay.get(0);
		assertEquals(0.0, firstProduct.getTotalReversalAmt());

		req = new MockHttpServletRequest("POST", "/slamcip/addreversal.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));
		req.setParameter("amount", "1000.0");
		req.setParameter("currencyId", "1");
		req.setParameter("remarks", "Lorem Ipsum");
		req.setParameter("dcrBalancingToolProductId",
				String.valueOf(firstProduct.getId()));
		req.setSession(session);

		resp = new MockHttpServletResponse();

		ModelAndView secondMv = controller.handleRequest(req, resp);
		productsToDisplay = (List<DCRBalancingToolProductBO>) secondMv
				.getModel().get("productsToDisplay");

		firstProduct = productsToDisplay.get(0);
		assertEquals(1000.0, firstProduct.getTotalReversalAmt());

		req = new MockHttpServletRequest("POST", "/slamcip/addreversal.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));
		req.setParameter("amount", "1000.0");
		req.setParameter("currencyId", "1");
		req.setParameter("remarks", "Lorem Ipsum");
		req.setParameter("dcrBalancingToolProductId",
				String.valueOf(firstProduct.getId()));
		req.setSession(session);

		resp = new MockHttpServletResponse();

		ModelAndView thirdMv = controller.handleRequest(req, resp);
		productsToDisplay = (List<DCRBalancingToolProductBO>) thirdMv
				.getModel().get("productsToDisplay");

		firstProduct = productsToDisplay.get(0);
		assertEquals(2000.0, firstProduct.getTotalReversalAmt());
		List<DCRReversalBO> dcrReversals = firstProduct.getDcrReversals();
		assertEquals(2, dcrReversals.size());

		DCRReversalBO firstReversal = dcrReversals.get(0);

		req = new MockHttpServletRequest("POST", "/slamcip/deletereversal.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));
		req.setParameter("dcrReversalId", String.valueOf(firstReversal.getId()));
		req.setSession(session);

		resp = new MockHttpServletResponse();

		ModelAndView fourthMv = controller.handleRequest(req, resp);
		productsToDisplay = (List<DCRBalancingToolProductBO>) fourthMv
				.getModel().get("productsToDisplay");

		firstProduct = productsToDisplay.get(0);
		assertEquals(1000.0, firstProduct.getTotalReversalAmt());
		assertEquals(1, firstProduct.getDcrReversals().size());

		dcrReversals = firstProduct.getDcrReversals();

		DCRReversalBO reversalToBeUpdated = dcrReversals.get(0);

		req = new MockHttpServletRequest("POST", "/slamcip/updatereversal.html");
		req.setParameter("dcrCashierId", String.valueOf(dcrCashierId));
		req.setParameter("amount", "5000.0");
		req.setParameter("remarks", "Lorem Ipsum");
		Long dcrReversalId = reversalToBeUpdated.getId();
		req.setParameter("dcrReversalId", String.valueOf(dcrReversalId));
		req.setSession(session);

		resp = new MockHttpServletResponse();

		ModelAndView fifthMv = controller.handleRequest(req, resp);
		productsToDisplay = (List<DCRBalancingToolProductBO>) fifthMv
				.getModel().get("productsToDisplay");

		firstProduct = productsToDisplay.get(0);

		assertEquals(5000.0, firstProduct.getTotalReversalAmt());
		assertEquals(1, firstProduct.getDcrReversals().size());

		endTransaction();
	}
}
