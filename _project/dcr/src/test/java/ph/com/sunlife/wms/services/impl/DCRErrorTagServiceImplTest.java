package ph.com.sunlife.wms.services.impl;

import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRErrorTagDao;
import ph.com.sunlife.wms.services.bo.DCRErrorBO;

public class DCRErrorTagServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRErrorTagDao dcrErrorTagDao;

	public void setDcrErrorTagDao(DCRErrorTagDao dcrErrorTagDao) {
		this.dcrErrorTagDao = dcrErrorTagDao;
	}

	public DCRErrorTagServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testEditDCRErrorTagLog() throws Exception {
		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
		service.setDcrErrorTagDao(dcrErrorTagDao);
		// service.
	}

	public void testGetErrorLookup() throws Exception {
		endTransaction();
		startNewTransaction();

//		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
//		service.setDcrErrorTagDao(dcrErrorTagDao);
//		// test getLookup*->addLog->getLog
//
//		DCRErrorTagLookupBO bo = service.getDCRErrorTagLookupDisplay(6L);
//		List<DCRCompletedStepBO> steps = bo.getDcrCompletedSteps();
//
//		assertNotNull(bo);
//		assertNotNull(bo.getDcrId());
//		// System.out.println("from test: "+bo.getDcrId());
//		assertNotNull(steps);
//		assertEquals(2, steps.size());
//		assertEquals(13, steps.get(0).getDcrErrors().size());
//		assertEquals(20, steps.get(1).getDcrErrors().size());

		endTransaction();
	}

	public void testAddErrorLog() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
		service.setDcrErrorTagDao(dcrErrorTagDao);
		// test getLookup->addLog*->getLog

		// DCRErrorTagLogBO bo = new DCRErrorTagLogBO();
		// bo = service.addDCRErrorTagLog(bo);
		// assertNotNull(bo);

		endTransaction();
	}

	public void testGetErrorLog() throws Exception {
		endTransaction();
		startNewTransaction();

//		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
//		service.setDcrErrorTagDao(dcrErrorTagDao);
//		// test getLookup->addLog->getLog*
//
//		List<DCRErrorTagLogConsolidatedDisplayBO> bo = service
//				.getDCRErrorTagLogDisplay(6L);
//		assertNotNull(bo);
//		assertEquals(1, bo.size());
		// assertEquals(10,bo.get(0).getErrors().size());

		endTransaction();
	}
	
	public void testGetDCRErrorByStatus() throws Exception{
		endTransaction();
		startNewTransaction();
		
		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
		service.setDcrErrorTagDao(dcrErrorTagDao);
		
		String dcrStatusCCM = "For Review and Approval";
		String dcrStatusPPA = "Approved for Recon";
		
		List<DCRErrorBO> ccmErrList = service.getDCRErrorByStatus(dcrStatusCCM);
		assertNotNull(ccmErrList);
		assertEquals(13, ccmErrList.size());
		
		List<DCRErrorBO> ppaErrList = service.getDCRErrorByStatus(dcrStatusPPA);
		assertNotNull(ppaErrList);
		assertEquals(20, ppaErrList.size());
		
		endTransaction();
	}
	
	public void testGetStepIdByStatus() throws Exception{
		endTransaction();
		startNewTransaction();
		
		DCRErrorTagServiceImpl service = new DCRErrorTagServiceImpl();
		service.setDcrErrorTagDao(dcrErrorTagDao);
		
		String dcrStatusCCM = "For Review and Approval";
		String dcrStatusPPA = "Approved for Recon";
		
		String ccmStepId = service.getStepIdByStatus(dcrStatusCCM);
		assertNotNull(ccmStepId);
		assertEquals("STPCCM01", ccmStepId);
		
		String ppaStepId = service.getStepIdByStatus(dcrStatusPPA);
		assertNotNull(ppaStepId);
		assertEquals("STPPPA01", ppaStepId);
		
		endTransaction();
	}
}
