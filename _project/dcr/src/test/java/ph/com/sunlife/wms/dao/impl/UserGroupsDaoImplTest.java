package ph.com.sunlife.wms.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.dao.domain.UserGroupHub;

public class UserGroupsDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private UserGroupsDao userGroupsDao;

	public UserGroupsDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setUserGroupsDao(UserGroupsDao userGroupsDao) {
		this.userGroupsDao = userGroupsDao;
	}

	public void testGetUserGroupHubList() throws Exception {
		String acf2id = "P683";

		List<UserGroupHub> list = userGroupsDao.getUserGroupHubList(acf2id);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(13, list.size());
		
		list = userGroupsDao.getUserGroupHubList("wc01");
		
		System.out.println(list.size());
	}
	
	public void testGroupNames() throws Exception {
		List<String> groupIds = new ArrayList<String>();
		groupIds.add("G176");
		
		List<String> groupNames = userGroupsDao.getGroupNames(groupIds);
		assertTrue(groupNames.contains("WMSPRCSCNonCashDisbApproverL5"));
	}
}
