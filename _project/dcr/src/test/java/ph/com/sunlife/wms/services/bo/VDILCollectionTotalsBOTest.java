package ph.com.sunlife.wms.services.bo;

import ph.com.sunlife.wms.util.ParameterMap;
import junit.framework.TestCase;

public class VDILCollectionTotalsBOTest extends TestCase {

	public void testParameterMap() throws Exception {
		VDILCollectionTotalsBO bo = new VDILCollectionTotalsBO();
		bo.setCashPesoSlamc(200.50);

		ParameterMap map = new ParameterMap(bo);
		assertEquals(200.50, (Double) map.get("cashPesoSlamc"));
		assertEquals(0.0, (Double) map.get("cashPesoGroupLife"));
		
		DCRBO joshBO = new DCRBO();
		joshBO.setCcId("Heartbeat");
		
		ParameterMap map2 = new ParameterMap(joshBO);
		map.putAll(map2);
		
		assertEquals("Heartbeat", (String) map.get("ccId"));
	}
}
