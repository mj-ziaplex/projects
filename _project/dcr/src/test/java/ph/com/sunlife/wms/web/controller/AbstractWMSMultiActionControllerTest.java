package ph.com.sunlife.wms.web.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MethodNameResolver;

import ph.com.sunlife.wms.util.ParameterMap;
import ph.com.sunlife.wms.web.controller.binder.BinderAware;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;

public class AbstractWMSMultiActionControllerTest extends TestCase {

	public void testAfterBindIfCannotValidate() throws Exception {
		AbstractWMSMultiActionController controller = new TestController();
		controller.setFailureViewName("wrongview");

		MockHttpServletRequest request = new MockHttpServletRequest("GET",
				"/handler");
		request.setParameter("myField", "UGLY non-integer value");
		MockHttpServletResponse response = new MockHttpServletResponse();

		MethodNameResolver resolver = mock(MethodNameResolver.class);
		when(resolver.getHandlerMethodName(request)).thenReturn("doSomething");
		controller.setMethodNameResolver(resolver);

		ModelAndView mv = controller.handleRequest(request, response);

		final String resultKey = "org.springframework.validation.BindingResult.command";
		Errors errors = (Errors) mv.getModel().get(resultKey);

		assertEquals("wrongview", mv.getViewName());
		assertTrue(errors.hasErrors());
		assertEquals(2, errors.getErrorCount());
	}

	public void testUsingInvalidId() throws Exception {
		AbstractWMSMultiActionController controller = new TestController();
		controller.setFailureViewName("wrongview");

		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/handler");
		request.setParameter("myField", "0");

		MethodNameResolver resolver = mock(MethodNameResolver.class);
		when(resolver.getHandlerMethodName(request)).thenReturn("doSomething");

		controller.setMethodNameResolver(resolver);
		MockHttpServletResponse response = new MockHttpServletResponse();

		ModelAndView mv = controller.handleRequest(request, response);

		final String resultKey = "org.springframework.validation.BindingResult.command";
		Errors errors = (Errors) mv.getModel().get(resultKey);

		assertEquals("wrongview", mv.getViewName());
		assertTrue(errors.hasErrors());
		assertEquals(1, errors.getErrorCount());
	}

	public void testUsingValidId() throws Exception {
		AbstractWMSMultiActionController controller = new TestController();

		Validator validator = new MyValidator(1);
		controller.setAdditionalValidators(new Validator[] { validator });

		MockHttpServletRequest request = new MockHttpServletRequest("GET",
				"/handler");
		request.setParameter("myField", "1");

		MethodNameResolver resolver = mock(MethodNameResolver.class);
		when(resolver.getHandlerMethodName(request)).thenReturn("doSomething");

		controller.setMethodNameResolver(resolver);
		MockHttpServletResponse response = new MockHttpServletResponse();

		ModelAndView mv = controller.handleRequest(request, response);
		assertNotNull(mv);
	}

	public void testUsingCurrencyFormatter() throws Exception {
		AbstractWMSMultiActionController controller = new TestController();

		Validator validator = new MyValidator(1);
		controller.setAdditionalValidators(new Validator[] { validator });

		MockHttpServletRequest request = new MockHttpServletRequest("GET",
				"/handler");
		request.setParameter("myField", "1");
		request.setParameter("myValue", "1,500.00");

		MethodNameResolver resolver = mock(MethodNameResolver.class);
		when(resolver.getHandlerMethodName(request)).thenReturn("doSomething");

		controller.setMethodNameResolver(resolver);
		MockHttpServletResponse response = new MockHttpServletResponse();

		ModelAndView mv = controller.handleRequest(request, response);
		assertNotNull(mv);

		double myValue = (Double) mv.getModel().get("myValue");
		assertEquals(1500.0, myValue);
	}

	public static class TestController extends AbstractWMSMultiActionController {

		public ModelAndView doSomething(HttpServletRequest request,
				HttpServletResponse response, MyForm command) {
			System.out.println("do something is being executed");

			ModelAndView modelAndView = new ModelAndView();
			modelAndView.addObject("myValue", command.getMyValue());
			modelAndView.addObject("myField", command.getMyField());
			return modelAndView;
		}

		@Override
		protected void initBinder(HttpServletRequest request,
				ServletRequestDataBinder binder) throws Exception {
			super.initBinder(request, binder);

			binder.registerCustomEditor(Double.class, "myValue",
					createMoneyPropertyEditor());
		}

	}

	public static class MyForm extends BinderAware {

		int myField;

		double myValue;

		public double getMyValue() {
			return myValue;
		}

		public void setMyValue(double myValue) {
			this.myValue = myValue;
		}

		@Override
		public boolean validate(Errors errors) {
			if (myField < 1) {
				errors.rejectValue("myField", "some.error.code");
			}

			return !errors.hasErrors();
		}

		@Override
		public void afterBind() {
		}

		public int getMyField() {
			return myField;
		}

		public void setMyField(int myField) {
			this.myField = myField;
		}

		public String[] getAllowedFields() {
			return super.getAllowedFields();
		}

		@Override
		public String[] getRequiredFields() {
			return new String[] { "myField" };
		}

		@Override
		public String[] getUncheckedFields() {
			// TODO Auto-generated method stub
			return null;
		}

	}

	public static class MyValidator implements Validator {

		private final long id;

		private MyValidator(long newId) {
			this.id = newId;
		}

		public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
			return MyForm.class.isAssignableFrom(clazz);
		}

		public void validate(Object target, Errors errors) {
			MyForm command = (MyForm) target;
			int commandId = command.getMyField();

			if (commandId < 1) {
				errors.rejectValue("myField", "errors.myfield",
						new Object[] {}, "");
			}
		}

		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof MyValidator)) {
				return false;
			}

			MyValidator that = (MyValidator) o;

			if (id != that.id) {
				return false;
			}

			return true;
		}

		public int hashCode() {
			return (int) (id ^ (id >>> 32));
		}
	}
	
	public void testParameterMap() {
		CashierWorkItemForm form = new CashierWorkItemForm();
		MockHttpSession session = new MockHttpSession();
		form.setSession(session);
		form.setDcrCashierId(1L);
		form.setSecretKey("secret");
		Map<String, Object> map = new ParameterMap(form);
		
		assertEquals(Long.valueOf(1L), map.get("dcrCashierId"));
		assertEquals("secret", map.get("secretKey"));
	}
}
