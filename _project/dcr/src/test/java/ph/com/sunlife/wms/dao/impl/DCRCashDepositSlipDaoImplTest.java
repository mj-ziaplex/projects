package ph.com.sunlife.wms.dao.impl;

import java.util.Date;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;

public class DCRCashDepositSlipDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	// private static final int TEST_DAYS_AGO = -366;

	private DCRCashDepositSlipDao dcrCashDepositSlipDao;

	public DCRCashDepositSlipDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrCashDepositSlipDao(
			DCRCashDepositSlipDao dcrCashDepositSlipDao) {
		this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] {"ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml"};
	}

	public void testSave() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashDepositSlip dcds = new DCRCashDepositSlip();
		dcds.setAccountNumber("acc1");
		dcds.setCurrency(Currency.USD);
		dcds.setDcrCashier(Long.MAX_VALUE);
		dcds.setDcrDepoSlipVersionSerialId("asdf1000");
		dcds.setDepositoryBank("some bank");
		dcds.setRequiredTotalAmount(190.0);
		dcds.setTotalCashAmount(190.0);
		dcds.setDeposlipFile("Some bytes".getBytes());
		Date today = new Date();
		dcds.setDepositDate(today);
		dcds.setDenom1(1);
		dcds.setDenom2(2);
		dcds.setDenom5(1);
		dcds.setDenom10(1);
		dcds.setDenom20(1);
		dcds.setDenom50(1);
		dcds.setDenom100(1);
		dcds.setDenom200(1);
		dcds.setDenom500(1);
		dcds.setDenom1000(1);
		dcds.setCompany(Company.SLAMCIP);
		dcds.setCustomerCenter("test");
		dcds.setDcrDate(today);
		dcds.setProdCode("Cash");

		dcds = dcrCashDepositSlipDao.save(dcds);

		Long id = dcds.getId();
		assertNotNull(id);

		DCRCashDepositSlip retrievedFromDB = dcrCashDepositSlipDao.getById(id);
		assertNotNull(retrievedFromDB);
		assertEquals(190.0, retrievedFromDB.getTotalCashAmount());
		assertEquals("test",retrievedFromDB.getCustomerCenter());
		assertEquals("Cash", retrievedFromDB.getProdCode());

		endTransaction();
	}

}
