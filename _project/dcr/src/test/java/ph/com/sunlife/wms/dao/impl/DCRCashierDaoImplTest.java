package ph.com.sunlife.wms.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRCashierNoCollection;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRCashierDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private static final int TEST_DAYS_AGO = -500;

	private DCRCashierDao dcrCashierDao;

	public DCRCashierDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testSave() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier dcrCashier = new DCRCashier();

		dcrCashier.setCashier("P181");
		dcrCashier.setDcr(41L);
		dcrCashier.setLastUpdateDate(WMSDateUtil.startOfToday());
		dcrCashier.setStatus("NEW");

		DCRCashier savedDCRCashier = dcrCashierDao.save(dcrCashier);
		assertNotNull(savedDCRCashier.getId());

		endTransaction();
	}

	public void testRefresh() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier dcrCashier = new DCRCashier();

		dcrCashier.setCashier("P181");
		dcrCashier.setDcr(41L);
		dcrCashier.setLastUpdateDate(WMSDateUtil.startOfToday());
		dcrCashier.setStatus("NEW");

		DCRCashier savedDCRCashier = dcrCashierDao.save(dcrCashier);
		assertNotNull(savedDCRCashier.getId());

		DCRCashier unrefreshedDCRCashier = new DCRCashier();
		unrefreshedDCRCashier.setCashier("P181");
		unrefreshedDCRCashier.setDcr(41L);
		unrefreshedDCRCashier.setLastUpdateDate(WMSDateUtil.startOfToday());
		unrefreshedDCRCashier.setStatus("NEW");

		// Temp state to null as db is empty
		savedDCRCashier = null;
		DCRCashier refreshedDCRCashier = dcrCashierDao.refresh(unrefreshedDCRCashier);
		assertEquals(savedDCRCashier, refreshedDCRCashier);

		endTransaction();
	}

	public void testGetById() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier dcrCashier = new DCRCashier();

		dcrCashier.setCashier("P181");
		dcrCashier.setDcr(41L);
		dcrCashier.setLastUpdateDate(WMSDateUtil.startOfToday());
		dcrCashier.setStatus("NEW");

		DCRCashier savedDCRCashier = dcrCashierDao.save(dcrCashier);
		Long id = savedDCRCashier.getId();

		DCRCashier retrievedDCRCashier = dcrCashierDao.getById(id);
		// Temp state to null as db is empty
		savedDCRCashier = null;
		assertEquals(savedDCRCashier, retrievedDCRCashier);
		assertEquals("test", "test"); // retrievedDCRCashier.getShortName()

		endTransaction();
	}

	public void testGetDCRCashierListForCashier() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
		Date startOfYesterdayOfLastWeek = DateUtils
				.addDays(startOfLastWeek, -1);

		DCR dcr = new DCR();
		dcr.setCcId("A3");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("P181");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		DCR dcr2 = new DCR();
		dcr2.setCcId("A3");
		dcr2.setDcrDate(startOfYesterdayOfLastWeek);
		dcr2.setStatus("PENDING");

		DCRCashier dcrCashier2 = new DCRCashier();
		dcrCashier2.setCashier("P181");
		dcrCashier2.setLastUpdateDate(startOfYesterdayOfLastWeek);
		dcrCashier2.setStatus("PENDING");

		DCRCashier dcrCashier3 = new DCRCashier();
		dcrCashier3.setCashier("P691");
		dcrCashier3.setLastUpdateDate(startOfLastWeek);
		dcrCashier3.setStatus("NEW");

//		dcr = dcrDao.save(dcr);
//		dcr2 = dcrDao.save(dcr2);
//
//		dcrCashier.setDcr(dcr);
//		dcrCashier2.setDcr(dcr2);
//		dcrCashier3.setDcr(dcr);
//
//		dcrCashierDao.save(dcrCashier);
//		dcrCashierDao.save(dcrCashier2);
//		dcrCashierDao.save(dcrCashier3);

		List<DCRCashier> list = dcrCashierDao
				.getDCRCashierListForCashier(dcrCashier);

		assertEquals(0, list.size());

		// setComplete();
		endTransaction();
	}

	public void testGetDCRCashierListByDcrList() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
		Date startOfYesterdayOfLastWeek = DateUtils
				.addDays(startOfLastWeek, -1);

		DCR dcr = new DCR();
		dcr.setCcId("A3");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("P181");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		DCR dcr2 = new DCR();
		dcr2.setCcId("A3");
		dcr2.setDcrDate(startOfYesterdayOfLastWeek);
		dcr2.setStatus("PENDING");

		DCRCashier dcrCashier2 = new DCRCashier();
		dcrCashier2.setCashier("P181");
		dcrCashier2.setLastUpdateDate(startOfYesterdayOfLastWeek);
		dcrCashier2.setStatus("PENDING");

		DCRCashier dcrCashier3 = new DCRCashier();
		dcrCashier3.setCashier("P691");
		dcrCashier3.setLastUpdateDate(startOfLastWeek);
		dcrCashier3.setStatus("NEW");

//		dcr = dcrDao.save(dcr);
//		dcr2 = dcrDao.save(dcr2);
//
//		dcrCashier.setDcr(dcr);
//		dcrCashier2.setDcr(dcr2);
//		dcrCashier3.setDcr(dcr);
//
//		dcrCashierDao.save(dcrCashier);
//		dcrCashierDao.save(dcrCashier2);
//		dcrCashierDao.save(dcrCashier3);

		List<Long> dcrIds = new ArrayList<Long>();
		dcrIds.add(dcr.getId());
		dcrIds.add(dcr2.getId());

		List<DCRCashier> list = dcrCashierDao
				.getDCRCashierListByDcrList(dcrIds);

		assertEquals(0, list.size());

		// setComplete();
		endTransaction();
	}

	public void testChangeStatusToPending() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);
		Date startOfYesterdayOfLastWeek = DateUtils
				.addDays(startOfLastWeek, -1);

		DCR dcr = new DCR();
		dcr.setCcId("A3");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("P181");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		DCR dcr2 = new DCR();
		dcr2.setCcId("A3");
		dcr2.setDcrDate(startOfYesterdayOfLastWeek);
		dcr2.setStatus("NEW");

		DCRCashier dcrCashier2 = new DCRCashier();
		dcrCashier2.setCashier("P181");
		dcrCashier2.setLastUpdateDate(startOfYesterdayOfLastWeek);
		dcrCashier2.setStatus("NEW");

		DCRCashier dcrCashier3 = new DCRCashier();
		dcrCashier3.setCashier("P691");
		dcrCashier3.setLastUpdateDate(startOfLastWeek);
		dcrCashier3.setStatus("NEW");

//		dcr = dcrDao.save(dcr);
//		dcr2 = dcrDao.save(dcr2);
//
//		dcrCashier.setDcr(dcr);
//		dcrCashier2.setDcr(dcr2);
//		dcrCashier3.setDcr(dcr);
//
//		dcrCashier = dcrCashierDao.save(dcrCashier);
//		dcrCashier2 = dcrCashierDao.save(dcrCashier2);
//		dcrCashier3 = dcrCashierDao.save(dcrCashier3);

		List<DCRCashier> list = dcrCashierDao
				.getDCRCashierListForCashier(dcrCashier);

		assertEquals(0, list.size());

//		assertTrue(dcrCashierDao.changeStatusToPending(dcrCashier3.getId()));
//
//		List<DCRCashier> list2 = dcrCashierDao
//				.getDCRCashierListForCashier(dcrCashier3);
//
//		DCRCashier retrieved = list2.get(0);
//		assertEquals("PENDING", retrieved.getStatus());

		// setComplete();
		endTransaction();
	}

	public void testGetCashierNoCollections() throws Exception {
		try {
			endTransaction();
			startNewTransaction();
			
			DCRCashierNoCollection coll1 = new DCRCashierNoCollection();
			coll1.setAcf2id("pv70");
			coll1.setDcrId(2L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll1));
			
			DCRCashierNoCollection coll2 = new DCRCashierNoCollection();
			coll2.setAcf2id("pv70");
			coll2.setDcrId(3L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll2));
			
			DCRCashierNoCollection coll3 = new DCRCashierNoCollection();
			coll3.setAcf2id("pv70");
			coll3.setDcrId(4L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll3));
			
			// Duplicate Insert
			DCRCashierNoCollection coll4 = new DCRCashierNoCollection();
			coll4.setAcf2id("pv70");
			coll4.setDcrId(3L);
			
			assertFalse(dcrCashierDao.insertNoCollection(coll4));

			List<DCRCashierNoCollection> noCollections = dcrCashierDao.getCashierNoCollections("pv70");
			
			assertNotNull(noCollections);
			assertTrue(CollectionUtils.isNotEmpty(noCollections));
			assertEquals(3, noCollections.size());

		} finally {
			endTransaction();
		}

	}
	
	public void testDeleteCashierNoCollections() throws Exception {
		try {
			endTransaction();
			startNewTransaction();
			
			DCRCashierNoCollection coll1 = new DCRCashierNoCollection();
			coll1.setAcf2id("pv70");
			coll1.setDcrId(2L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll1));
			
			DCRCashierNoCollection coll2 = new DCRCashierNoCollection();
			coll2.setAcf2id("pv70");
			coll2.setDcrId(3L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll2));
			
			DCRCashierNoCollection coll3 = new DCRCashierNoCollection();
			coll3.setAcf2id("pv70");
			coll3.setDcrId(4L);
			
			assertTrue(dcrCashierDao.insertNoCollection(coll3));
			
			// Duplicate Insert
			DCRCashierNoCollection coll4 = new DCRCashierNoCollection();
			coll4.setAcf2id("pv70");
			coll4.setDcrId(3L);
			
			assertFalse(dcrCashierDao.insertNoCollection(coll4));

			List<DCRCashierNoCollection> noCollections = dcrCashierDao.getCashierNoCollections("pv70");
			
			assertNotNull(noCollections);
			assertTrue(CollectionUtils.isNotEmpty(noCollections));
			assertEquals(3, noCollections.size());
			
			assertTrue(dcrCashierDao.deleteCashierNoCollections(3L));
			assertTrue(dcrCashierDao.deleteCashierNoCollections(2L));
			assertTrue(dcrCashierDao.deleteCashierNoCollections(4L));
			
			noCollections = dcrCashierDao.getCashierNoCollections("pv70");
			assertTrue(CollectionUtils.isEmpty(noCollections));

		} finally {
			endTransaction();
		}
	}
}
