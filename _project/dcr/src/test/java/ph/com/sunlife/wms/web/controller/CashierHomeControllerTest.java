package ph.com.sunlife.wms.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.servlet.ModelAndView;

import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.DCRBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.form.CashierHomePageForm;

public class CashierHomeControllerTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testDoShowHomePage() throws Exception {
		MockHttpServletRequest req = new MockHttpServletRequest();
		MockHttpServletResponse res = new MockHttpServletResponse();

		DCRCreationService service = mock(DCRCreationService.class);

		String acf2id = "pv70";
		String ccId = "a1";

		when(service.createAndLoadDcrWorkItems(ccId, acf2id)).thenReturn(
				Collections.EMPTY_LIST);

		when(service.extractDCRFromDCRCashierList(Collections.EMPTY_LIST))
				.thenReturn(Collections.EMPTY_LIST);

		UserSession userSession = new UserSession();
		userSession.setSiteCode("A7");
		userSession.setUserId("pv70");
		userSession.addRole(Role.CASHIER);

		MockHttpSession session = new MockHttpSession();
		session.setAttribute(WMSConstants.USER_SESSION, userSession);
		req.setSession(session);

		CashierHomeController controller = new CashierHomeController();
		controller.setDcrCreationService(service);
		ModelAndView mv = controller.doShowHomePage(req, res,
				new CashierHomePageForm());
		assertEquals("homeViewPage", mv.getViewName());

	}

	public void testRefreshCashierWorkItems() throws Exception {
		CashierHomeController chc = new CashierHomeController();

		List<DCRBO> dcrWorkItems = new ArrayList<DCRBO>();

		DCRBO dcr1 = new DCRBO();
		dcr1.setId(1L);

		DCRBO dcr2 = new DCRBO();
		dcr1.setId(2L);

		// Not included
		DCRBO dcr3 = new DCRBO();
		dcr1.setId(3L);

		dcrWorkItems.add(dcr1);
		dcrWorkItems.add(dcr2);
		// dcrWorkItems.add(dcr3);

		List<DCRCashierBO> cashierWorkItems = new ArrayList<DCRCashierBO>();

		DCRCashierBO dc1 = new DCRCashierBO();
		dc1.setDcr(dcr1);
		dc1.setId(1L);

		DCRCashierBO dc2 = new DCRCashierBO();
		dc2.setDcr(dcr2);
		dc2.setId(2L);

		DCRCashierBO dc3 = new DCRCashierBO();
		dc3.setDcr(dcr3);
		dc3.setId(3L);

		cashierWorkItems.add(dc1);
		cashierWorkItems.add(dc2);
		cashierWorkItems.add(dc3);

		chc.refreshCashierWOrkItems(cashierWorkItems, dcrWorkItems);
		assertTrue(cashierWorkItems.contains(dc1));
		assertTrue(cashierWorkItems.contains(dc2));
		assertFalse(cashierWorkItems.contains(dc3));

		chc.refreshCashierWOrkItems(cashierWorkItems, null);
		assertTrue(CollectionUtils.isEmpty(cashierWorkItems));
	}
}
