//// COMMENTED AS CLASS WAS NOT BEING USED
//package ph.com.sunlife.wms.logging.aop;
//
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.verify;
//import junit.framework.TestCase;
//
//import org.apache.log4j.Logger;
//
///**
// * The WarnLog4JProfiler Unit Test Class. 
// */
//public class WarnLog4JProfilerTest extends TestCase {
//
//	private WarnLog4JProfiler profiler;
//	
//	private Logger logger;
//
//	@Override
//	protected void setUp() throws Exception {
//		logger = mock(Logger.class);
//
//		profiler = new WarnLog4JProfiler();
//		profiler.setLogger(logger);
//	}
//
//	public void testLog() {
//		String message = "some message";
//		profiler.log(message);
//		verify(logger).warn(message);
//		
//		Throwable t = new Exception();
//		profiler.log(message, t);
//		verify(logger).warn(message, t);
//		
//		assertEquals(logger, profiler.getLogger());
//	}
//	
//	
//}