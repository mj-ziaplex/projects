package ph.com.sunlife.wms.dao.domain;

import junit.framework.TestCase;

public class DCRPPAMDSNoteTest extends TestCase {

	public void testEquals() throws Exception {
		DCRPPAMDSNote n1 = new DCRPPAMDSNote();
		n1.setApprovalNumber("2041002");
		n1.setAccountNumber("2041001");
		n1.setSalesSlipNumber("2041010");
		
		final DCRPPAMDSNote n2 = new DCRPPAMDSNote();
		n2.setApprovalNumber("2041002");
		n2.setAccountNumber("2041001");
		n2.setSalesSlipNumber("2041010");
		
		DCRPPAMDSNote n3 = new DCRPPAMDSNote();
		n3.setApprovalNumber("213");
		n3.setAccountNumber("312");
		n3.setSalesSlipNumber("123");

		assertTrue(n1.equals(n2));
		assertFalse(n1.equals(n3));
	}
}
