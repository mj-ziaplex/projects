/**
 * 
 */
package ph.com.sunlife.wms.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRIPACDao;
import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.DCRIpacValue;
import ph.com.sunlife.wms.dao.domain.PPAMDS;
import ph.com.sunlife.wms.dao.exceptions.WMSDaoException;

/**
 * @author i176
 *
 */
@Ignore
public class DCRIPACDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private DCRIPACDao dcrIpacDao;
    
    public DCRIPACDaoImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setDcrDao(DCRDao dcrDao) {
    }

    public void setDcrIPACDao(DCRIPACDao dcrIpacDao) {
        this.dcrIpacDao = dcrIpacDao;
    }

    public void setIpacDao(IpacDao ipacDao) {
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
                    "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }

    @Test
    private List<PPAMDS> assembleAllMDSTransactions(
            String customerCenter,
            Date transactionDate) throws WMSDaoException {
        List<PPAMDS> grandList = new ArrayList<PPAMDS>();

//        Company[] mdsCompanies = new Company[]{Company.SLFPI, Company.SLGFI, Company.SLOCPI};
//        for (Company company : mdsCompanies) {

            List<PPAMDS> allMdsTransactions =
                    dcrIpacDao.getDCRIPACMDSTransaction(customerCenter, transactionDate);

            if (CollectionUtils.isNotEmpty(allMdsTransactions)) {
                grandList.addAll(allMdsTransactions);
            }
//        }
        return grandList;
    }
    
    @Test
    private List<DCRIpacValue> getDCRIPACDailyCollectionSummary(
            final String center_code,
            final Date process_date,
            final String acf2id) throws WMSDaoException {

    	System.out.println("JDBC: " + getJdbcTemplate().getDataSource());

        List<DCRIpacValue> list = dcrIpacDao.getDCRIPACDailyCollectionSummary("G8", new Date());
        
        assertEquals(0, list.size());

        return list;
    }
}
