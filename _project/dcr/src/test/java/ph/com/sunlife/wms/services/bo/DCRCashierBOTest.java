package ph.com.sunlife.wms.services.bo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

public class DCRCashierBOTest {

	@Test
	public void testSort() {
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, -7);
		Date lastMonth = DateUtils.addMonths(today, -1);
		Date lastYear = DateUtils.addYears(today, -1);
		
		DCRBO dcr1 = new DCRBO();
		dcr1.setDcrDate(today);
		
		DCRBO dcr2 = new DCRBO();
		dcr2.setDcrDate(lastYear);
		
		DCRBO dcr3 = new DCRBO();
		dcr3.setDcrDate(lastWeek);
		
		DCRBO dcr4 = new DCRBO();
		dcr4.setDcrDate(lastMonth);
		
		DCRCashierBO bo1 = new DCRCashierBO();
		bo1.setDcr(dcr1);
		
		DCRCashierBO bo2 = new DCRCashierBO();
		bo2.setDcr(dcr2);
		
		DCRCashierBO bo3 = new DCRCashierBO();
		bo3.setDcr(dcr3);
		
		DCRCashierBO bo4 = new DCRCashierBO();
		bo4.setDcr(dcr4);
		
		List<DCRCashierBO> list = new ArrayList<DCRCashierBO>();
		list.add(bo1);
		list.add(bo2);
		list.add(bo3);
		list.add(bo4);
		
		Collections.sort(list);
		
		assertEquals(bo1, list.get(0));
		assertEquals(bo3, list.get(1));
		assertEquals(bo4, list.get(2));
		assertEquals(bo2, list.get(3));
	}
}
