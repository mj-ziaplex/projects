package ph.com.sunlife.wms.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

public class WMSDateUtilTest {

	@Test
	public void testStartOfToday() {
		Date date = new Date();
		date = DateUtils.setHours(date, 0);
		date = DateUtils.setMinutes(date, 0);
		date = DateUtils.setSeconds(date, 0);
		date = DateUtils.setMilliseconds(date, 0);
		long timestamp = date.getTime();
		
		Date startOfToday = WMSDateUtil.startOfToday();
		
		assertEquals(Long.valueOf(timestamp), Long.valueOf(startOfToday.getTime()));
	}
	
	@Test
	public void testDaysBetween() {
		Date start = WMSDateUtil.toDate("28FEB2012");
		Date end = WMSDateUtil.toDate("03MAR2012");
		
		int days = WMSDateUtil.daysBetween(start, end);
		assertEquals(4, days);
	}

	@Test
	public void testStartOfTheDay() {
		Date date = new Date();
		date = DateUtils.setHours(date, 0);
		date = DateUtils.setMinutes(date, 0);
		date = DateUtils.setSeconds(date, 0);
		date = DateUtils.setMilliseconds(date, 0);
		long timestamp = date.getTime();
		
		Date startOfToday = WMSDateUtil.startOfTheDay(date);
		
		assertEquals(Long.valueOf(timestamp), Long.valueOf(startOfToday.getTime()));
	}
	
	public void testIntersection() {
		List<String> list1 = new ArrayList<String>();
		list1.add("abc");
		list1.add("123");
		list1.add("xyz");
		
		List<String> list2 = new ArrayList<String>();
		list2.add("doreme");
		list2.add("fasolati");
		list2.add("123");
		list2.add("someText");
		
		@SuppressWarnings("unchecked")
		Collection<String> intersection = CollectionUtils.intersection(list1, list2);
		assertEquals(1, intersection.size());
	}

}
