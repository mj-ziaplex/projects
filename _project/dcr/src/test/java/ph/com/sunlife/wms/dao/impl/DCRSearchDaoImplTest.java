package ph.com.sunlife.wms.dao.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRSearchDao;
import ph.com.sunlife.wms.dao.domain.CustomerCenter;

public class DCRSearchDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRSearchDao dcrSearchDao;

	public DCRSearchDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrSearchDao(DCRSearchDao dcrSearchDao) {
		this.dcrSearchDao = dcrSearchDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testGetCCByHubUserCC() throws Exception {
		List<CustomerCenter> list = dcrSearchDao.getCCByHubUserCC("A6");
		Set<CustomerCenter> set = new HashSet<CustomerCenter>(list);
		assertEquals(11, set.size());
		
		list = dcrSearchDao.getCCByHubUserCC("A5");
		set = new HashSet<CustomerCenter>(list);
		assertEquals(12, set.size());
	}
}
