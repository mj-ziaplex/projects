package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCROtherFileDao;
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.integration.DCRFilenetIntegrationService;
import ph.com.sunlife.wms.services.AttachmentService;
import ph.com.sunlife.wms.services.bo.AttachmentBO;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class AttachmentServiceImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private DCRCashDepositSlipDao dcrCashDepositSlipDao;
    private DCRBalancingToolDao dcrBalancingToolDao;
    private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;
    private DCRCashierDao dcrCashierDao;
    private DCROtherFileDao dcrOtherFileDao;
    private DCRAddingMachineDao dcrAddingMachineDao;
    private AttachmentService attachmentServiceTarget;
    private DCRVDILDao dcrVdilDao;
    private DCRDao dcrDao;
    private DCRFilenetIntegrationService dcrFilenetIntegrationService;

    public void setDcrVdilDao(DCRVDILDao dcrVdilDao) {
        this.dcrVdilDao = dcrVdilDao;
    }

    public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
        this.dcrBalancingToolDao = dcrBalancingToolDao;
    }

    public void setAttachmentServiceTarget(AttachmentService attachmentService) {
        this.attachmentServiceTarget = attachmentService;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    public void setDcrFilenetIntegrationService(
            DCRFilenetIntegrationService dcrFilenetIntegrationService) {
        this.dcrFilenetIntegrationService = dcrFilenetIntegrationService;
    }

    public void setDcrAddingMachineDao(DCRAddingMachineDao dcrAddingMachineDao) {
        this.dcrAddingMachineDao = dcrAddingMachineDao;
    }

    public void setDcrOtherFileDao(DCROtherFileDao dcrOtherFileDao) {
        this.dcrOtherFileDao = dcrOtherFileDao;
    }

    public AttachmentServiceImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
        this.dcrCashierDao = dcrCashierDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/service/spring/spring-config.xml"};
    }

    public void setDcrCashDepositSlipDao(
            DCRCashDepositSlipDao dcrCashDepositSlipDao) {
        this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
    }

    public void setDcrChequeDepositSlipDao(
            DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
        this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
    }

    public void testGetAttachmentsByDcrCashierId() throws Exception {
        endTransaction();
        startNewTransaction();

        try {
            DCR dcr = new DCR();
            dcr.setCcId("A2");
            dcr.setDcrDate(WMSDateUtil.toDate("19OCT2011"));
            dcr.setStatus("NEW");
            dcr = dcrDao.save(dcr);

            DCRCashier dc = new DCRCashier();
            dc.setCashier("wc01");
            dc.setDcr(dcr.getId());
            dc = dcrCashierDao.save(dc);
            Long dcrCashierId = dc.getId();

            DCRCashDepositSlip ds = new DCRCashDepositSlip();
            ds.setAccountNumber("9012901290");
            ds.setAcf2id("wc01");
            ds.setCompany(Company.SLOCPI);
            ds.setCurrency(Currency.PHP);
            ds.setCustomerCenter("A2");
            ds.setDcrCashier(dcrCashierId);
            ds.setDepositDate(new Date());
            ds.setProdCode("TRAD/VUL");
            ds.setDcrDepoSlipVersionSerialId("{48239489-342189321-2819}");
            ds = dcrCashDepositSlipDao.save(ds);


            AttachmentServiceImpl service = new AttachmentServiceImpl();
            service.setDcrCashDepositSlipDao(dcrCashDepositSlipDao);
            service.setDcrCashierDao(dcrCashierDao);
            service.setDcrOtherFileDao(dcrOtherFileDao);
            service.setDcrAddingMachineDao(dcrAddingMachineDao);
            service.setDcrFilenetIntegrationService(dcrFilenetIntegrationService);
            service.setDcrChequeDepositSlipDao(dcrChequeDepositSlipDao);
            service.setDcrVdilDao(dcrVdilDao);
            service.setDcrBalancingToolDao(dcrBalancingToolDao);

            List<AttachmentBO> list = service.getAttachmentsByDcrCashierId(dcrCashierId);

            assertTrue(CollectionUtils.isNotEmpty(list));

            final String expectedFilename = "SLOCPI_PHP_TRAD/VUL_CashDepositSlip_WC01.pdf";
            final String expectedFilenetId = "{48239489-342189321-2819}";
            final String expectedUploderId = "wc01";
            final String expectedFileUrl = "viewfile/view.html?fileId={48239489-342189321-2819}";

            AttachmentBO result = (AttachmentBO) CollectionUtils.find(list,
                    new Predicate() {

                        @Override
                        public boolean evaluate(Object object) {
                            AttachmentBO bo = (AttachmentBO) object;

                            return expectedFilename.equals(bo.getFilename())
                                    && expectedFilenetId.equals(bo.getFilenetId())
                                    && expectedUploderId.equals(bo.getUploaderId())
                                    && expectedFileUrl.equals(bo.getFileUrl());
                        }
                    });

            assertNotNull(result);

        } finally {
            endTransaction();

        }

    }

    public void testUploadHtml() throws Exception {

        // String htmlCode = "<html>";
        String htmlCode = "<div class=\"ibm-container-body ibm-two-column\">";
        htmlCode += "<div class=\"ibm-column ibm-first\">";
        htmlCode += "<div class=\"author\"><a title=\"\" class=\"dwauthor\" rel=\"#authortip1\" href=\"#author1\">Robert Perron</a>, "
                + "Documentation Architect, Lotus</div>"
                + "<div style=\"display: none;\" id=\"authortip1\" class=\"dwauthor-onload-state ibm-no-print\">Robert Perron is a documentation architect with Lotus in Westford, Massachusetts. "
                + "He has developed documentation for Lotus Notes and Domino since the early 1990's with a primary concentration on programmability. "
                + "He developed the documentation for the LotusScript and Java Notes classes and coauthored the book "
                + "<i>60 Minute Guide to LotusScript 3 - Programming for Notes 4</i>. He has authored several <i>LDD Today</i> articles . "
                + "Last year he authored \"A Comprehensive Tour of Programming Enhancements in Notes/Domino 6\" for <i>The View</i>.</div>"
                + "<div class=\"author\"><a class=\"dwauthor\" rel=\"#authortip2\" href=\"#author2\">Steve Nikopoulos</a>, Steve Nikopoulos, Lotus</div>"
                + "<div style=\"display: none;\" id=\"authortip2\" class=\"dwauthor-onload-state ibm-no-print\">"
                + "Steve Nikopoulos is a Senior Software Engineer with the Domino Server Programmability team. His most recent work includes the remoted Domino Objects, Domino/Websphere integration, and single sign-on. "
                + "In his spare time, he enjoys bike riding with his family.</div><div class=\"author\"><a class=\"dwauthor\" rel=\"#authortip3\" href=\"#author3\">Kevin Smith</a>, "
                + "Steve Nikopoulos, Lotus</div><div style=\"display: none;\" id=\"authortip3\" class=\"dwauthor-onload-state ibm-no-print\">Kevin Smith is an Advisory Software Engineer in the IBM Messaging &amp; "
                + "Collaboration Development organization in Westford, currently responsible for programmability features of the Domino server. He joined Lotus in 1990 as a technical support engineer for Lotus 1-2-3/M, "
                + "the famous PC spreadsheet ported to the famous IBM mainframe, one of the earliest IBM-Lotus engineering ventures.</div>";
        //htmlCode += "</html>";

        if (!StringUtils.contains(htmlCode, "<html>")) {
            htmlCode = "<html>" + htmlCode + "</html>";
        }

        AttachmentServiceImpl service = (AttachmentServiceImpl) attachmentServiceTarget;
        String contentType = service.getDcrContentType(htmlCode.getBytes());

        assertEquals("text/html", contentType);
    }

    public void testValidateContentType() {
//		AttachmentServiceImpl service = new AttachmentServiceImpl();
//		File rootFolder = new File("C:\\testme");
//
//		if (rootFolder.isDirectory()) {
//			for (File f : rootFolder.listFiles()) {
//				if (!f.isDirectory()) {
//					InputStream is = null;
//					// BufferedInputStream bis = null;
//
//					try {
//						is = new FileInputStream(f);
//						byte[] arr = IOUtils.toByteArray(is);
//						String mimeType = ((AttachmentServiceImpl) attachmentServiceTarget)
//								.getDcrContentType(arr);
//						System.out.println(f.getName() + " " + mimeType);
//					} catch (InvalidFileTypeException ex) {
//						System.out.println(f.getName() + " (" + ex.getMessage()
//								+ ") ");
//					} catch (IOException ex) {
//						System.out.println(f.getName()
//								+ " (exception encountered)");
//					} catch (Exception ex) {
//						System.out.println(f.getName()
//								+ " (exception encountered)");
//					} finally {
//						IOUtils.closeQuietly(is);
//					}
//				}
//			}
//		}
    }
}
