package ph.com.sunlife.wms.dao.impl;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.domain.Cashier;

public class CashierDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private CashierDao cashierDao;

    public CashierDaoImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
                    "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }

    public void testGetCashier() throws Exception {
        String acf2id = "p181";
        Cashier cashier = cashierDao.getCashier(acf2id);

        assertNotNull(cashier);
        assertEquals("Sagaoinit", cashier.getLastname());
        assertEquals("Menchu", cashier.getFirstname());
        assertEquals(" @sunlife.com", cashier.getEmailaddress());

        acf2id = "dfnmasjkdnsak";
        cashier = cashierDao.getCashier(acf2id);
        assertNull(cashier);
    }

    public void testGetMultipleCashier() throws Exception {
        final String thisCashierName = "lep1";

        List<String> cashierUserIds = new ArrayList<String>();
        cashierUserIds.add("i174");
        cashierUserIds.add("i547");
        cashierUserIds.add("i571");
        cashierUserIds.add(thisCashierName);
        List<Cashier> allCashierInfoList = cashierDao.getMultipleCashierInfo(cashierUserIds);

        assertNotNull(allCashierInfoList);
        assertTrue(CollectionUtils.isNotEmpty(allCashierInfoList));
        Cashier thisCashier = (Cashier) CollectionUtils.find(
                allCashierInfoList, new Predicate() {

            @Override
            public boolean evaluate(Object object) {
                Cashier c = (Cashier) object;
                return StringUtils.equalsIgnoreCase(thisCashierName,
                        c.getAcf2id());
            }
        });

        assertNotNull(thisCashier);
        assertEquals("Sapalicio", thisCashier.getLastname());
        assertEquals("Joy", thisCashier.getFirstname());
        assertEquals("Joy.Sapalicion@sunlife.com", thisCashier.getEmailaddress());

    }

    public void testGetCCNameAndCodes() throws Exception {
        List<String> ccNamesAndCodes = cashierDao.getCCNameAndCodes();

        assertEquals(74, ccNamesAndCodes.size());
    }

    public void testGetCCNameById() throws Exception {
        String ccName = cashierDao.getCCNameById("A2");

        assertEquals("Ace Customer Center", ccName);
    }

//    public void testJdbcTemplate() throws Exception {
//        StringBuilder sb = new StringBuilder();
//
//        sb.append("declare \n");
//        sb.append("p_result sys_refcursor; \n");
//        sb.append("p_error VARCHAR2(1000) := 'Error Message: '; \n");
//        sb.append("begin \n");
//        sb.append("ipac_report.sp_wms_daily_coll_rep('A1','22NOV2012','PK53',p_result,p_error); \n");
//        sb.append("? := p_result; \n");
//        sb.append("? := p_error; \n");
//        sb.append("end; ");
//
//        final String sql = sb.toString();
//
//        System.out.println(sql);
//        jdbcTemplate.execute(new CallableStatementCreator() {
//
//            public CallableStatement createCallableStatement(Connection con) throws SQLException {
//                CallableStatement cs = con.prepareCall(sql);
//                cs.registerOutParameter(1, OracleTypes.CURSOR);
//                cs.registerOutParameter(2, OracleTypes.VARCHAR);
//
//                return cs;
//            }
//        }, new CallableStatementCallback() {
//
//            @Override
//            public Object doInCallableStatement(CallableStatement cs)
//                    throws SQLException, DataAccessException {
//                cs.execute();
//
//                String errorMsg = cs.getString(2);
//                System.out.println(errorMsg);
//                return null;
//            }
//        });
//    }

    public static void main(String args[]) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String moneyString = formatter.format(Double.MAX_VALUE);
        System.out.println(moneyString);
    }
}
