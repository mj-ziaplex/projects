package ph.com.sunlife.wms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

public class SaveMeTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public SaveMeTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testMe() throws Exception {
		JdbcTemplate template = new JdbcTemplate(dataSource);
		
		
		String sql = "select * from hubccs where HCC_HUB_ID NOT IN ('CCH001', 'CCH002', 'CCH003', 'CCH004')";
		template.execute(sql, new PreparedStatementCallback() {
			
			@Override
			public Object doInPreparedStatement(PreparedStatement ps)
					throws SQLException, DataAccessException {
				
				ResultSet rs = ps.executeQuery();
				
				while (rs.next()) {
					StringBuilder sb = new StringBuilder();
					sb.append("INSERT INTO HubCCs (HCC_HUB_ID, HCC_CC_ID, HCC_Active, HCC_CRE_User, HCC_CRE_Date) ");
					sb.append("VALUES (");
					sb.append("'" + rs.getString("HCC_HUB_ID") + "', ");
					sb.append("'" + rs.getString("HCC_CC_ID") + "', ");
					boolean isActive = rs.getBoolean("HCC_Active");
					sb.append("" + (isActive ? "1":"0") + ", ");
					sb.append("'wfms01dbo', ");
					sb.append("getdate()) ");
//					sb.append("'" + rs.getString("HCC_UPD_User") + "', ");
//					sb.append("'" + rs.getDate("HCC_UPD_Date") + "') ");
					
					System.out.println(sb);
				}
				return null;
			}
		});
	}
}
