package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRMixedChequePayeeDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRMixedChequePayee;

public class DCRMixedChequePayeeDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRMixedChequePayeeDao dcrMixedChequePayeeDao;

	public DCRMixedChequePayeeDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrMixedChequePayeeDao(
			DCRMixedChequePayeeDao dcrMixedChequePayeeDao) {
		this.dcrMixedChequePayeeDao = dcrMixedChequePayeeDao;
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		Bank bank = new Bank();
		bank.setId("Lorem Ipsum Bank");

		DCRMixedChequePayee p1 = new DCRMixedChequePayee();
		p1.setDcrCashier(Long.MAX_VALUE);
		p1.setBank(bank);
		p1.setCheckAmount(1000.0);
		p1.setCheckNumber("102-120-1029");
		p1.setCheckType(CheckType.LOCAL);
		p1.setCurrency(Currency.PHP);
		p1.setPayee(Company.SLFPI);
		p1.setOthers(500.0);
		p1.setSlamci(100.0);
		p1.setSlocpi(100.0);
		p1.setSlfpi(200.0);
		p1.setSlgfi(100.0);

		DCRMixedChequePayee p2 = new DCRMixedChequePayee();
		p2.setDcrCashier(Long.MAX_VALUE);
		p2.setBank(bank);
		p2.setCheckAmount(1000.0);
		p2.setCheckNumber("102-120-1029");
		p2.setCheckType(CheckType.LOCAL);
		p2.setCurrency(Currency.PHP);
		p2.setPayee(Company.SLFPI);
		p2.setOthers(500.0);
		p2.setSlamci(100.0);
		p2.setSlocpi(100.0);
		p2.setSlfpi(200.0);
		p2.setSlgfi(100.0);

		DCRMixedChequePayee p3 = new DCRMixedChequePayee();
		p3.setDcrCashier(Long.MAX_VALUE);
		p3.setBank(bank);
		p3.setCheckAmount(1000.0);
		p3.setCheckNumber("102-120-1029");
		p3.setCheckType(CheckType.LOCAL);
		p3.setCurrency(Currency.PHP);
		p3.setPayee(Company.SLFPI);
		p3.setOthers(500.0);
		p3.setSlamci(100.0);
		p3.setSlocpi(100.0);
		p3.setSlfpi(200.0);
		p3.setSlgfi(100.0);

		p1 = dcrMixedChequePayeeDao.save(p1);
		p2 = dcrMixedChequePayeeDao.save(p2);
		p3 = dcrMixedChequePayeeDao.save(p3);

		Long idToDelete = p1.getId();
		assertNotNull(idToDelete);
		assertNotNull(p2.getId());
		assertNotNull(p3.getId());

		List<DCRMixedChequePayee> list = dcrMixedChequePayeeDao
				.getAllByDcrCashier(Long.MAX_VALUE);

		assertNotNull(list);
		assertEquals(3, list.size());

		boolean isDeleted = dcrMixedChequePayeeDao.deleteById(idToDelete);
		assertTrue(isDeleted);

		list = dcrMixedChequePayeeDao.getAllByDcrCashier(Long.MAX_VALUE);

		assertNotNull(list);
		assertEquals(2, list.size());

		p2.setCheckAmount(2000.0);
		p2.setSlfpi(1200.0);

		boolean isUpdated = dcrMixedChequePayeeDao.updateMixedChequePayee(p2);
		assertTrue(isUpdated);

		DCRMixedChequePayee updatedEntity = dcrMixedChequePayeeDao.getById(p2
				.getId());
		assertEquals(1200.0, updatedEntity.getSlfpi());
		assertEquals(2000.0, updatedEntity.getCheckAmount());

		endTransaction();
	}

}
