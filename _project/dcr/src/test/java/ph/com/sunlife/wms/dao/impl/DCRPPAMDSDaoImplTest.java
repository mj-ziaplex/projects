package ph.com.sunlife.wms.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRPPAMDSDao;
import ph.com.sunlife.wms.dao.domain.DCRPPAMDS;

public class DCRPPAMDSDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRPPAMDSDao dcrPPAMDSDao;

	public DCRPPAMDSDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}
	
	public void setDcrPPAMDSDao(DCRPPAMDSDao dcrPPAMDSDao) {
		this.dcrPPAMDSDao = dcrPPAMDSDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testAllMethodsDCRPPAMDS() throws Exception {
		endTransaction();
		startNewTransaction();
		
		long dcrId = Long.MAX_VALUE;
		
		DCRPPAMDS mds1 = new DCRPPAMDS();
		mds1.setDcrId(dcrId);
		mds1.setSalesSlipNumber("99999999999999");
		mds1.setAccountNumber("99999999999999");
		mds1.setApprovalNumber("99999999999999");
		
		Thread.sleep(2000);
		
		DCRPPAMDS mds2 = new DCRPPAMDS();
		mds2.setDcrId(dcrId);
		mds2.setSalesSlipNumber("888888888888");
		mds2.setAccountNumber("888888888888");
		mds2.setApprovalNumber("888888888888");
		
		Thread.sleep(3000);
		
		DCRPPAMDS mds3 = new DCRPPAMDS();
		mds3.setDcrId(dcrId);
		mds3.setSalesSlipNumber("77777");
		mds3.setAccountNumber("77777");
		mds3.setApprovalNumber("77777");
		
		mds1 = dcrPPAMDSDao.insertDCRPPAMDS(mds1);
		Long mds1Id = mds1.getId();
		assertNotNull(mds1Id);
		assertEquals("Y",dcrPPAMDSDao.getPPAMDSById(mds1Id).getReconciled());//mds1.getReconciled()
		assertEquals(mds1Id,dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(mds1).getId());
		
		mds2 = dcrPPAMDSDao.insertDCRPPAMDS(mds2);
		mds2.setReconBy("PW12");
		Long mds2Id = mds2.getId();
		assertNotNull(mds2Id);
		assertEquals("Y",dcrPPAMDSDao.getPPAMDSById(mds2Id).getReconciled());
		assertEquals(mds2Id,dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(mds2).getId());
		
		mds3 = dcrPPAMDSDao.insertDCRPPAMDS(mds3);
		Long mds3Id = mds3.getId();
		assertNotNull(mds3Id);
		assertEquals("Y",dcrPPAMDSDao.getPPAMDSById(mds3Id).getReconciled());
		assertEquals(mds3Id,dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(mds3).getId());
		
		List<DCRPPAMDS> list = dcrPPAMDSDao.getPPAMDSListByDcrId(dcrId);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(3, list.size());
		
		dcrPPAMDSDao.unreconcilePPAMDS(mds1Id);
		assertEquals("N", dcrPPAMDSDao.getPPAMDSById(mds1Id).getReconciled());
		
		dcrPPAMDSDao.reconcilePPAMDS(mds2);
		DCRPPAMDS mds2Updated = dcrPPAMDSDao.getPPAMDSById(mds2Id);
		assertEquals("Y", mds2Updated.getReconciled());
		assertEquals("PW12", mds2Updated.getReconBy());
		
		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(3, list.size());
		
		Thread.sleep(2000);
		
		DCRPPAMDS mds4 = new DCRPPAMDS();
		mds4.setDcrId(dcrId);
		mds4.setSalesSlipNumber("000");
		mds4.setAccountNumber("000");
		mds4.setApprovalNumber("000");
		mds4 = dcrPPAMDSDao.insertDCRPPAMDS(mds4);
		Long mds4Id = mds4.getId();
		assertNotNull(mds4Id);
		assertEquals("Y",dcrPPAMDSDao.getPPAMDSById(mds4Id).getReconciled());
		assertEquals(mds4Id,dcrPPAMDSDao.getIdByDcrIdAndSalesSlipNumber(mds4).getId());
		
		List<DCRPPAMDS> newList = dcrPPAMDSDao.getPPAMDSListByDcrId(dcrId);
		
		assertTrue(CollectionUtils.isNotEmpty(newList));
		assertEquals(4, newList.size());
		
		endTransaction();
	}
	
//	public void testLongValueOf() throws Exception{
//		String dcrIdFromForm = "37";
//		assertEquals(37L, Long.valueOf(dcrIdFromForm));
//	}
}
