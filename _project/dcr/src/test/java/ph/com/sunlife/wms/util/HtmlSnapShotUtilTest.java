package ph.com.sunlife.wms.util;

import java.io.File;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;

public class HtmlSnapShotUtilTest extends TestCase {

	public void testToSnapshotHtml() throws Exception {
		File file = new File("src/test/resources/WcmSignIn.jsp.html");
		assertTrue(file.isFile());
		
		String htmlCode = FileUtils.readFileToString(file);
		
		htmlCode = HtmlSnapShotUtil.toSnapshotHtml(htmlCode);
		
//		List<HtmlLink> htmlLinks = HtmlSnapShotUtil.grabHtmlLinks(htmlCode);
//		assertEquals(3, htmlLinks.size());
		
	}
//	
//	public void testReplaceLinkWithDeadLinks() throws Exception {
//		File file = new File("src/test/resources/WcmSignIn.jsp.html");
//		assertTrue(file.isFile());
//		String htmlCode = FileUtils.readFileToString(file);
//		htmlCode = HtmlSnapShotUtil.replaceLinksWithDeadLinks(htmlCode);
//	}
//	
//	public void testDisableButtons() throws Exception {
//		File file = new File("src/test/resources/WcmSignIn.jsp.html");
//		assertTrue(file.isFile());
//		String htmlCode = FileUtils.readFileToString(file);
//		htmlCode = HtmlSnapShotUtil.disableButtons(htmlCode);
//	}
//	
//	public void testLinkContainSpecials() throws Exception {
//		String s1 = "/WEB-INF/jsp/collectionsTemplate.jsp";
//		assertFalse(HtmlSnapShotUtil.linkContainSpecials(s1));
//		
//		String s2 = "dassa showEditReversalFormWithData dasmkldasm";
//		assertTrue(HtmlSnapShotUtil.linkContainSpecials(s2));
//		
//		String s3 = "/WEB-INF/jsp/mainTemplate.jsp";
//		assertFalse(HtmlSnapShotUtil.linkContainSpecials(s3));
//	}
	
}
