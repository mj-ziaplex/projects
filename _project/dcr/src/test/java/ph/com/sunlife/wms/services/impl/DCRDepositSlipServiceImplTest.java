package ph.com.sunlife.wms.services.impl;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCashDepositSlipDao;
import ph.com.sunlife.wms.dao.DCRChequeDepositSlipDao;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.services.bo.BankBO;
import ph.com.sunlife.wms.services.bo.DCRCashDepositSlipBO;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.DCRChequeDepositSlipBO;

public class DCRDepositSlipServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRCashDepositSlipDao dcrCashDepositSlipDao;
	private DCRChequeDepositSlipDao dcrChequeDepositSlipDao;

	public DCRDepositSlipServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrCashDepositSlipDao(
			DCRCashDepositSlipDao dcrCashDepositSlipDao) {
		this.dcrCashDepositSlipDao = dcrCashDepositSlipDao;
	}

	public void setDcrChequeDepositSlipDao(
			DCRChequeDepositSlipDao dcrChequeDepositSlipDao) {
		this.dcrChequeDepositSlipDao = dcrChequeDepositSlipDao;
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRDepositSlipServiceImpl service = new DCRDepositSlipServiceImpl();
		service.setDcrCashDepositSlipDao(dcrCashDepositSlipDao);

		DCRCashDepositSlipBO businessObject = new DCRCashDepositSlipBO();
		businessObject.setAccountNumber("some account number");
		businessObject.setRequiredTotalAmount(1886.4);
		businessObject.setDepositDate(new Date());

		BankBO bankBO = new BankBO();
		bankBO.setId("We find ways");
		businessObject.setDepositoryBank(bankBO);

		DCRCashierBO dcrCashierBO = new DCRCashierBO();
		dcrCashierBO.setId(Long.MAX_VALUE);
		businessObject.setDcrCashier(dcrCashierBO);

		businessObject.setCompanyId(1L);
		businessObject.setCurrencyId(1L);
		businessObject.setDcrDepoSlipVersionSerialId("some filenet id");

		businessObject.setDenom1000(1);
		businessObject.setDenom500(1);
		businessObject.setDenom200(1);
		businessObject.setDenom100(1);
		businessObject.setDenom50(1);
		businessObject.setDenom20(1);
		businessObject.setDenom10(1);
		businessObject.setDenom5(1);
		businessObject.setDenom1(1);
		businessObject.setDenom25c(1);
		businessObject.setDenom10c(1);
		businessObject.setDenom5c(1);
		businessObject.setCustomerCenter("test");
		businessObject.setDcrDate(new Date());
		businessObject.setProdCode("Cash");

		service.saveCashDepositSlip(businessObject);
		assertNotNull(businessObject.getId());

		DCRCashDepositSlipBO retrievedBO = service.getCashDepositSlip(
				Long.MAX_VALUE, 1L, 1L, "Cash");

		assertNotNull(retrievedBO);
		assertEquals(businessObject, retrievedBO);

		assertEquals(1886.4, retrievedBO.getTotalCashAmount());

		endTransaction();

	}

	public void testUsingMapAsParameter() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRDepositSlipServiceImpl service = new DCRDepositSlipServiceImpl();
		service.setDcrCashDepositSlipDao(dcrCashDepositSlipDao);

		Map<String, Object> map = new TreeMap<String, Object>();
		map.put("accountNumber", "some account number");
		map.put("requiredTotalAmount", 1886.4);
		map.put("depositDate", new Date());
		map.put("dcrDate", new Date());
		map.put("customerCenter", "test");

		BankBO bankBO = new BankBO();
		bankBO.setId("We find ways");
		map.put("depositoryBank", bankBO);

		DCRCashierBO dcrCashierBO = new DCRCashierBO();
		dcrCashierBO.setId(Long.MAX_VALUE);
		map.put("dcrCashier", dcrCashierBO);

		map.put("companyId", 1L);
		map.put("currencyId", 1L);
		map.put("dcrDepoSlipVersionSerialId", "some filenet id");

		map.put("denom1000", 1);
		map.put("denom500", 1);
		map.put("denom200", 1);
		map.put("denom100", 1);
		map.put("denom50", 1);
		map.put("denom20", 1);
		map.put("denom10", 1);
		map.put("denom5", 1);
		map.put("denom1", 1);
		map.put("denom25c", 1);
		map.put("denom10c", 1);
		map.put("denom5c", 1);
		map.put("prodCode", "Cash");
		service.saveCashDepositSlip(map);

		DCRCashDepositSlipBO retrievedBO = service.getCashDepositSlip(
				Long.MAX_VALUE, 1L, 1L, "Cash");

		assertNotNull(retrievedBO);

		assertEquals(1886.4, retrievedBO.getTotalCashAmount());

		endTransaction();

	}

	public void testChequeMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRDepositSlipServiceImpl service = new DCRDepositSlipServiceImpl();
		service.setDcrChequeDepositSlipDao(dcrChequeDepositSlipDao);

		DCRChequeDepositSlipBO businessObject = new DCRChequeDepositSlipBO();
		businessObject.setAccountNumber("the account number");
		businessObject.setRequiredTotalAmount(2400.5);
		businessObject.setTotalCheckAmount(2250.4);
		businessObject.setCheckType(CheckType.ON_US);
		businessObject.setDepositDate(new Date());
		businessObject.setDcrDate(new Date());
		BankBO bankBO = new BankBO();
		bankBO.setId("We found a way");
		businessObject.setDepositoryBank(bankBO);

		DCRCashierBO dcrCashierBO = new DCRCashierBO();
		dcrCashierBO.setId(Long.MAX_VALUE);
		businessObject.setDcrCashier(dcrCashierBO);

		businessObject.setCompanyId(1L);
		businessObject.setCurrencyId(1L);
		businessObject.setDcrDepoSlipVersionSerialId("some filenet id");
		businessObject.setCustomerCenter("test");
		businessObject.setProdCode("GAF");

		service.saveChequeDepositSlip(businessObject);
		assertNotNull(businessObject.getId());

		DCRChequeDepositSlipBO retrievedBO = service.getChequeDepositSlip(
				Long.MAX_VALUE, 1L, 1L, CheckType.ON_US, "GAF");

		assertNotNull(retrievedBO);
		assertEquals(businessObject, retrievedBO);

		assertEquals(2400.5, retrievedBO.getRequiredTotalAmount());
		assertEquals(2250.4, retrievedBO.getTotalCheckAmount());
		assertEquals("test", retrievedBO.getCustomerCenter());
		endTransaction();
	}

}
