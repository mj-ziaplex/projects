package ph.com.sunlife.wms.services.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.CashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.UserGroupsDao;
import ph.com.sunlife.wms.security.LdapManager;
import ph.com.sunlife.wms.security.Role;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.DCRLogInService;
import ph.com.sunlife.wms.services.bo.CashierBO;
import ph.com.sunlife.wms.services.bo.UserSession;

public class DCRLogInServiceImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private DCRLogInService dcrLogInService;
    private LdapManager ldapManager;
    private CashierDao cashierDao;
    private DCRDao dcrDao;
    private UserGroupsDao userGroupsDao;
    private CachingService cachingService;

    public DCRLogInServiceImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/service/spring/spring-config.xml"};
    }

    public void setCachingService(CachingService cachingService) {
        this.cachingService = cachingService;
    }

    public void setUserGroupsDao(UserGroupsDao userGroupsDao) {
        this.userGroupsDao = userGroupsDao;
    }

    public void setLdapManager(LdapManager ldapManager) {
        this.ldapManager = ldapManager;
    }

    public void setCashierDao(CashierDao cashierDao) {
        this.cashierDao = cashierDao;
    }

    public void setDcrDao(DCRDao dcrDao) {
        this.dcrDao = dcrDao;
    }

    public void setDcrLogInService(DCRLogInService dcrLogInService) {
        this.dcrLogInService = dcrLogInService;
    }

    public void testLogInUser() throws Exception {
        DCRLogInServiceImpl thisService = (DCRLogInServiceImpl) dcrLogInService;
        thisService.setLdapReady(false);

        UserSession user = thisService.logInUser("p181", "somePassword", "DAV", "cashier");

        assertNotNull(user);

        CashierBO result = user.getCashierBO();
        assertNotNull(result);
        assertEquals("Sagaoinit", result.getLastname());
        assertEquals("Menchu", result.getFirstname());
        assertEquals(" @sunlife.com", result.getEmailaddress());
    }

    public void testLogInUserGivenActualLogIn() throws Exception {
        DCRLogInServiceImpl dcrLogInService = new DCRLogInServiceImpl();
        dcrLogInService.setCashierDao(cashierDao);
        dcrLogInService.setDcrDao(dcrDao);
        dcrLogInService.setLdapManager(ldapManager);
        dcrLogInService.setUserGroupsDao(userGroupsDao);
        dcrLogInService.setCachingService(cachingService);

        dcrLogInService.setManagerUserGroups("G176");
        dcrLogInService.setPpaUserGroups("G140,G141, G161,G162, G163,G164,G165,G166,G167");
        dcrLogInService.setLdapReady(true);
        dcrLogInService.afterPropertiesSet();
        UserSession user;

        // Change according to Healthcheck
        // *Problem was identified as inconsistent logging
        // *Change was added assertion on catch
        //  -also removed the hard coded setting of user credentials, directly pasing to method
        try {
            user = dcrLogInService.logInUser("fmaskldfmaskl", "Sunlife05dfasmdjkasmnjkl", "DAV", "cashier");
            fail("Expected an ServiceException to be thrown");
        } catch (Exception ex) {
            assertThat(ex.getMessage(), is("Invalid username and/or password"));
        }

        dcrLogInService.setLdapReady(false);
        // Change according to Healthcheck
        // *Problem was identified as inconsistent logging
        // *Change was was added assertion on catch
        //  -also removed the hard coded setting of user credentials, directly pasing to method
        try {
            user = dcrLogInService.logInUser("pv70", "Sunlife07", "dasdasdas", "manager");;
            fail("Expected an ServiceException to be thrown");
        } catch (Exception ex) {
            assertThat(ex.getMessage(), is("dasdasdas is not a valid site code"));
        }

        dcrLogInService.setLdapReady(false);
        // Change according to Healthcheck
        // *Problem was identified as inconsistent logging
        // *Change was was added assertion on catch
        //  -also removed the hard coded setting of user credentials, directly pasing to method
        try {
            user = dcrLogInService.logInUser("pv70", "Sunlife07", "ACE", "cashier");
            fail("Expected an ServiceException to be thrown");
        } catch (Exception ex) {
            assertThat(ex.getMessage(), is("Cashier role is not valid for user"));
        }

        dcrLogInService.setLdapReady(false);
        user = dcrLogInService.logInUser("P525", "thequickbrownfox101dalmatians", "MCC", "manager");
        assertTrue(user.getRoles().contains(Role.MANAGER));
    }

    public void testAddRolesAndHub() throws Exception {
        DCRLogInServiceImpl dcrLogInService = new DCRLogInServiceImpl();
        dcrLogInService.setCashierDao(cashierDao);
        dcrLogInService.setDcrDao(dcrDao);
        dcrLogInService.setLdapManager(ldapManager);
        dcrLogInService.setUserGroupsDao(userGroupsDao);
        dcrLogInService.setCachingService(cachingService);

        List<String> validGroupNamesForManagers = new ArrayList<String>();
        validGroupNamesForManagers.add("WMSPRCSCNonCashDisbApproverL5");

        List<String> validGroupnamesForPPA = new ArrayList<String>();
        validGroupnamesForPPA.add("PPA Supervisor");

        dcrLogInService
                .setValidGroupNamesForManagers(validGroupNamesForManagers);

        dcrLogInService.setValidGroupnamesForPPA(validGroupnamesForPPA);

        UserSession userSession = new UserSession();
        userSession.setUserId("P683");

        dcrLogInService.addRolesAndHub(userSession, "manager",
                new ArrayList<String>(Collections.singleton("CCH003")));

        String hubId = userSession.getHubId();
        List<Role> roles = userSession.getRoles();

        assertEquals("CCH003", hubId);
        assertTrue(roles.contains(Role.MANAGER));

    }
}
