package ph.com.sunlife.wms.integration.client;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.sunlife.ingeniumutil.container.UserCollectionsInfo;

//@Ignore
public class UserCollectionsTotalUtilTest {

	// private UserCollectionsTotalUtil userCollectionsTotalUtil;

	private static final String USER_LOGIN_NAME = "WR01W";
	private static final String USER_CO_ID = "CP";
	private static final String LOCATION = "C:/key/the.key";
	private static final String SLOCPI_WSDL_LOCATION = "http://sv5913.ph.sunlife:80/PFTXLifeWebservices/services/TXLifeService?WSDL";
	private static final String SLGFI_WSDL_LOCATION = "http://sv590081/PFTXLifeWebservices/services/TXLifeService/wsdl/TXLifeService.wsdl";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testUserCollectionsInquiry() throws Exception {
		UserCollectionsTotalUtil userCollectionsTotalUtil = new UserCollectionsTotalUtil(
				USER_LOGIN_NAME, USER_CO_ID, LOCATION, SLOCPI_WSDL_LOCATION);
		userCollectionsTotalUtil.setPswd("WR01W");
		boolean isWaiting = true;

		UserCollectionsInfo info = new UserCollectionsInfo();
		info.setProcessDate("2012-09-18");
		info.setUserId("PQ50");
		info.setCurrencyId("PS");

		List<String> outMessages = new ArrayList<String>();
		info = userCollectionsTotalUtil.userCollectionsInquiry(info,
				outMessages);
		
		if(info != null){
			isWaiting = false;
		}
		
		while(isWaiting){
			System.out.println("waiting..");
		}
		
		System.out.println("SLOCPI PESO SESSION TOTAL: " + info.getTotalCollectionAmount());
		// assertEquals(1214510.0, info.getTotalCollectionAmount());
	}
	
	@Test
	public void testUserCollectionsInquiryGF() throws Exception {
		UserCollectionsTotalUtil userCollectionsTotalUtil = new UserCollectionsTotalUtil(
				"WR01W", "GF", LOCATION, SLGFI_WSDL_LOCATION);
		userCollectionsTotalUtil.setPswd("WR01W");

		UserCollectionsInfo info = new UserCollectionsInfo();
		info.setProcessDate("2012-09-18");
		info.setUserId("PQ50");
		info.setCurrencyId("PS");

		List<String> outMessages = new ArrayList<String>();
		info = userCollectionsTotalUtil.userCollectionsInquiry(info,
				outMessages);

		System.out.println("SLGFI PESO SESSION TOTAL: " + info.getTotalCollectionAmount());
		// assertEquals(1214510.0, info.getTotalCollectionAmount());
	}

}
