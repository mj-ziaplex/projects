package ph.com.sunlife.wms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

public class WMSMimeUtilTest extends TestCase {

    public void testGetMimeType() throws Exception {
        File file = new File("C:\\Users\\ia23\\Desktop\\Test.docx");

        InputStream is = null;
        List<String> validFileTypes = new ArrayList<String>();
        validFileTypes.add("text/html");
        validFileTypes.add("image/png");
        validFileTypes.add("image/gif");
        validFileTypes.add("image/jpeg");
        validFileTypes.add("application/pdf");
        validFileTypes.add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        validFileTypes.add("application/vnd.ms-excel");
        validFileTypes.add("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        validFileTypes.add("application/msword");
        validFileTypes.add("application/zip");
        validFileTypes.add("message/rfc822");
        validFileTypes.add("image/tiff");

        try {
            is = new FileInputStream(file);
            byte[] arr = IOUtils.toByteArray(is);
            System.out.println("BEFORE GETTING MIME");

            String mimeType = WMSMimeUtil.getMimeType(arr);
            System.out.println("THIS IS THE MIME TYPE: " + mimeType);
            System.out.println("ARRAY ZIP: " + WMSMimeUtil.getZIP_CONTENT_TYPE());

            System.out.println("CHECK ZIP: " + WMSMimeUtil.MSWORD_CONTENT_TYPE.equals(mimeType));
//            String fileSize = WMSMimeUtil.humanReadableByteCount(arr.length);
        } finally {
            IOUtils.closeQuietly(is);
        }

//        if (rootFolder.isDirectory()) {
//            System.out.println("DIR PO");
//            for (File f : rootFolder.listFiles()) {
//                if (!f.isDirectory()) {
//                    InputStream is = null;
//                    
//                    try {
//                        is = new FileInputStream(f);
//                        byte[] arr = IOUtils.toByteArray(is);
//
//                        String mimeType = WMSMimeUtil.getMimeType(arr);
//                        String fileSize = WMSMimeUtil.humanReadableByteCount(arr.length);
//                    } finally {
//                        IOUtils.closeQuietly(is);
//                    }
//                }
//            }
//        }
    }
}
