package ph.com.sunlife.wms.web.controller.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import ph.com.sunlife.wms.services.DCRCreationService;
import ph.com.sunlife.wms.services.bo.DCRCashierBO;
import ph.com.sunlife.wms.services.bo.UserSession;
import ph.com.sunlife.wms.util.WMSConstants;
import ph.com.sunlife.wms.web.controller.form.CashierWorkItemForm;
import ph.com.sunlife.wms.web.controller.form.ReversalForm;

public class CashierWorkItemFormValidatorTest {

    @Test
    public void testSupports() {
        CashierWorkItemFormValidator validator = new CashierWorkItemFormValidator();

        assertFalse(validator.supports(Integer.class));
        assertTrue(validator.supports(CashierWorkItemForm.class));
    }

    @Test
    public void testValidate() throws Exception {
        CashierWorkItemFormValidator validator = new CashierWorkItemFormValidator();

        UserSession userSession = new UserSession();
        userSession.setUserId("pv70");

        MockHttpSession session = new MockHttpSession();
        session.putValue(WMSConstants.USER_SESSION, userSession);

        CashierWorkItemForm form = new CashierWorkItemForm();
        form.setSession(session);
        form.setDcrCashierId(123L);

        DCRCreationService service = mock(DCRCreationService.class);
        when(service.getDCRCashierBO(123L, "pv70")).thenReturn(new DCRCashierBO());
        when(service.getDCRCashierBO(100L, "pv70")).thenReturn(null);

        validator.setDcrCreationService(service);

        Errors errors = new BindException(form, "command");
        validator.validate(form, errors);
        assertEquals(0, errors.getErrorCount());

        form = new CashierWorkItemForm();
        form.setSession(session);
        form.setDcrCashierId(100L);

        validator.setDcrCreationService(service);

        errors = new BindException(form, "command");
        validator.validate(form, errors);
        assertEquals(1, errors.getErrorCount());
    }

    @Test
    public void testValidateUsingReversalForm() throws Exception {
        CashierWorkItemFormValidator validator = new CashierWorkItemFormValidator();

        UserSession userSession = new UserSession();
        userSession.setUserId("pv70");

        MockHttpSession session = new MockHttpSession();
        session.putValue(WMSConstants.USER_SESSION, userSession);

        ReversalForm form = new ReversalForm();
        form.setSession(session);
        form.setDcrCashierId(123L);

        DCRCreationService service = mock(DCRCreationService.class);
        when(service.getDCRCashierBO(123L, "pv70")).thenReturn(new DCRCashierBO());
        when(service.getDCRCashierBO(100L, "pv70")).thenReturn(null);

        validator.setDcrCreationService(service);

        Errors errors = new BindException(form, "command");
        validator.validate(form, errors);
        assertEquals(0, errors.getErrorCount());

        form = new ReversalForm();
        form.setSession(session);
        form.setDcrCashierId(100L);

        validator.setDcrCreationService(service);

        errors = new BindException(form, "command");
        validator.validate(form, errors);
        assertEquals(1, errors.getErrorCount());
    }
}
