package ph.com.sunlife.wms.dao.impl;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRAddingMachineDao;
import ph.com.sunlife.wms.dao.domain.DCRAddingMachine;

public class DCRAddingMachineDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRAddingMachineDao dcrAddingMachineDao;

	public DCRAddingMachineDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrAddingMachineDao(DCRAddingMachineDao dcrAddingMachineDao) {
		this.dcrAddingMachineDao = dcrAddingMachineDao;
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		long dcrCashierId = Long.MAX_VALUE - 1;

		DCRAddingMachine a1 = new DCRAddingMachine();
		a1.setDcrAddingMachineVrsSerId("lorem ipsum id");
		a1.setDcrCashier(Long.MAX_VALUE);
		a1.setAddingMachineFile("lorem ipsum file".getBytes());

		a1 = dcrAddingMachineDao.save(a1);
		assertNotNull(a1.getId());

		DCRAddingMachine a2 = new DCRAddingMachine();
		a2.setDcrAddingMachineVrsSerId("lorem ipsum id 2");
		a2.setDcrCashier(dcrCashierId);

		File file = new File("src/test/resources/IPAC_VALUE_mapping.xlsx");
		assertTrue(file.exists());

		byte[] fileData = FileUtils.readFileToByteArray(file);
		int byteLength = fileData.length;
		a2.setAddingMachineFile(fileData);

		a2 = dcrAddingMachineDao.save(a2);
		assertNotNull(a2.getId());

		DCRAddingMachine retrievedAM = dcrAddingMachineDao
				.getByDcrCashierId(dcrCashierId);
		assertNotNull(retrievedAM);
		assertEquals(a2, retrievedAM);
		assertEquals(byteLength, retrievedAM.getAddingMachineFile().length);

		// setComplete();
		endTransaction();
	}
}
