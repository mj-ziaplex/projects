package ph.com.sunlife.wms.dao.impl;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRReversalDao;

public class DCRReversalDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private DCRReversalDao dcrReversalDao;

    public DCRReversalDaoImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
            "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }

    public void setDcrReversalDao(DCRReversalDao dcrReversalDao) {
        this.dcrReversalDao = dcrReversalDao;
    }

//    public void testAllMethods() throws Exception {
//        endTransaction();
//        startNewTransaction();
//
//        DCRReversal r1 = new DCRReversal();
//        r1.setAmount(1000.0);
//        r1.setCurrency(Currency.PHP);
//        r1.setDateTime(new Date());
//        r1.setRemarks("Lorem Ipsum Dolor");
//        r1.setDcrBalancingToolProductId(Long.MAX_VALUE);
//
//        r1 = dcrReversalDao.save(r1);
//        Long id1 = r1.getId();
//        assertNotNull(id1);
//
//        DCRReversal reversalFromDB = dcrReversalDao.getById(id1);
//        assertNotNull(reversalFromDB);
//        assertEquals(r1, reversalFromDB);
//
//        r1.setAmount(2000.0);
//        r1.setRemarks("Lorem Ipsum Dolor 2");
//
//        dcrReversalDao.updateReversal(r1);
//        reversalFromDB = dcrReversalDao.getById(id1);
//
//        assertEquals(2000.0, reversalFromDB.getAmount());
//        assertEquals("Lorem Ipsum Dolor 2", reversalFromDB.getRemarks());
//
//        DCRReversal r2 = new DCRReversal();
//        r2.setAmount(1000.0);
//        r2.setCurrency(Currency.PHP);
//        r2.setDateTime(new Date());
//        r2.setRemarks("Lorem Ipsum Dolor");
//        r2.setDcrBalancingToolProductId(Long.MAX_VALUE);
//
//        r2 = dcrReversalDao.save(r2);
//        Long id2 = r2.getId();
//        assertNotNull(id2);
//
//        List<DCRReversal> list = dcrReversalDao
//                .getAllByBalancingToolProduct(Long.MAX_VALUE);
//
//        assertNotNull(list);
//        assertTrue(CollectionUtils.isNotEmpty(list));
//        assertEquals(2, list.size());
//
//        endTransaction();
//    }
    
    public void testDeleteById() throws Exception {
        boolean testVal = true;
        boolean expected;
        
        Long id = new Long(3);
        expected = dcrReversalDao.deleteById(id);
        assertEquals(expected, testVal);
    }
}
