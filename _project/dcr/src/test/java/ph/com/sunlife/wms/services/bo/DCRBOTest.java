package ph.com.sunlife.wms.services.bo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRBOTest {

	@Test
	public void testCompareToAgain() {
		DCRBO dcrBO1 = new DCRBO();
		dcrBO1.setId(5L);
		dcrBO1.setDcrDate(WMSDateUtil.toDate("13NOV2012"));
		
		DCRBO dcrBO2 = new DCRBO();
		dcrBO2.setId(3L);
		dcrBO2.setDcrDate(WMSDateUtil.toDate("20NOV2012"));
		
		DCRBO dcrBO3 = new DCRBO();
		dcrBO3.setId(10L);
		dcrBO3.setDcrDate(WMSDateUtil.toDate("14NOV2012"));
		
		DCRBO dcrBO4 = new DCRBO();
		dcrBO4.setId(11L);
		dcrBO4.setDcrDate(WMSDateUtil.toDate("19OCT2012"));
		
		DCRBO dcrBO5 = new DCRBO();
		dcrBO5.setId(12L);
		dcrBO5.setDcrDate(WMSDateUtil.toDate("21NOV2012"));

		List<DCRBO> list = new ArrayList<DCRBO>();
		list.add(dcrBO1);
		list.add(dcrBO2);
		list.add(dcrBO3);
		list.add(dcrBO4);
		list.add(dcrBO5);
		
		Collections.sort(list);
		
		assertEquals(dcrBO5, list.get(0));
		assertEquals(dcrBO2, list.get(1));
		assertEquals(dcrBO3, list.get(2));
		assertEquals(dcrBO1, list.get(3));
		assertEquals(dcrBO4, list.get(4));
	}

	
	@Test
	public void testCompareTo() {
		Date today = new Date();
		
		DCRBO dcrBO1 = new DCRBO();
		dcrBO1.setId(5L);
		dcrBO1.setDcrDate(DateUtils.addDays(today, -1));
		
		DCRBO dcrBO2 = new DCRBO();
		dcrBO2.setId(3L);
		dcrBO2.setDcrDate(DateUtils.addDays(today, +1));
		
		DCRBO dcrBO3 = new DCRBO();
		dcrBO3.setId(10L);
		dcrBO3.setDcrDate(DateUtils.addDays(today, -5));
		
		List<DCRBO> list = new ArrayList<DCRBO>();
		list.add(dcrBO1);
		list.add(dcrBO2);
		list.add(dcrBO3);
		
		Collections.sort(list);
		
		assertEquals(dcrBO2, list.get(0));
		assertEquals(dcrBO1, list.get(1));
		assertEquals(dcrBO3, list.get(2));
	}
}
