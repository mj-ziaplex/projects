package ph.com.sunlife.wms.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRBalancingToolDao;
import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRBalancingTool;
import ph.com.sunlife.wms.dao.domain.DCRBalancingToolProduct;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRIpacLookup;
import ph.com.sunlife.wms.dao.domain.DCROther;
import ph.com.sunlife.wms.dao.domain.ProductType;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRBalancingToolDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private static final int TEST_DAYS_AGO = -720;

	private DCRCashierDao dcrCashierDao;

	private DCRDao dcrDao;

	private DCRBalancingToolDao dcrBalancingToolDao;

	public DCRBalancingToolDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
		this.dcrBalancingToolDao = dcrBalancingToolDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testSave() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

		DCR dcr = new DCR();
		dcr.setCcId("G8");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("PK53");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRBalancingTool obj = new DCRBalancingTool();
		obj.setCompany(Company.SLOCPI);
		obj.setDcrCashier(dcrCashier);

		obj = dcrBalancingToolDao.save(obj);

		assertNotNull(obj.getId());
		assertEquals(dcrCashier.getId(), obj.getDcrCashier().getId());
		assertEquals(Company.SLOCPI, obj.getCompany());

		endTransaction();
	}

	public void testGetBalancingTools() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

		DCR dcr = new DCR();
		dcr.setCcId("G8");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("PK53");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRBalancingTool obj = new DCRBalancingTool();
		obj.setDcrCashier(dcrCashier);

		obj.setCompany(Company.SLOCPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLGFI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLFPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLAMCIP);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.OTHER);
		dcrBalancingToolDao.save(obj);

		List<DCRBalancingTool> list = dcrBalancingToolDao
				.getBalancingTools(dcrCashier.getId());

		assertEquals(5, list.size());
		endTransaction();
	}

	public void testGetMultipleBalancingTools() throws Exception {
		
		List<Long> cashierWorkItemIds = new ArrayList<Long>();
		cashierWorkItemIds.add(3L);
		cashierWorkItemIds.add(5L);
		cashierWorkItemIds.add(8L);
		
		List<DCRBalancingTool> multipleBalancingTools = dcrBalancingToolDao
				.getMultipleBalancingTools(cashierWorkItemIds);
		
		assertNotNull(multipleBalancingTools);
		assertTrue(CollectionUtils.isNotEmpty(multipleBalancingTools));
		assertEquals(18, multipleBalancingTools.size());
	}

	public void testSaveBalancingToolProduct() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

		DCR dcr = new DCR();
		dcr.setCcId("G8");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("PK53");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRBalancingTool obj = new DCRBalancingTool();
		obj.setDcrCashier(dcrCashier);

		obj.setCompany(Company.SLOCPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLGFI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLFPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLAMCIP);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.OTHER);
		dcrBalancingToolDao.save(obj);

		List<DCRBalancingTool> list = dcrBalancingToolDao
				.getBalancingTools(dcrCashier.getId());

		Long dcrBalancingToolId = null;
		for (DCRBalancingTool tool : list) {
			Company company = tool.getCompany();

			for (ProductType type : ProductType.values()) {
				if (company.equals(type.getCompany())) {
					DCRBalancingToolProduct prod = new DCRBalancingToolProduct();
					prod.setDcrBalancingTool(tool);
					prod.setProductType(type);
					prod.setWorksiteAmount(0.00);

					prod = dcrBalancingToolDao.saveBalancingToolProduct(prod);
					assertNotNull(prod.getId());
				}
			}

			if (Company.SLAMCIP.equals(company)) {
				dcrBalancingToolId = tool.getId();
			}
		}

		List<DCRBalancingToolProduct> prodList = dcrBalancingToolDao
				.getAllBalancingToolProducts(dcrBalancingToolId);

		assertEquals(10, prodList.size());

		endTransaction();
	}

	void printInserts(List<DCRIpacLookup> list) {
		StringBuilder sb = new StringBuilder();
		for (DCRIpacLookup lookup : list) {
			sb.append("INSERT INTO dbo.DCR_ipaclookup ");
			sb.append("(prod_code, pay_code, pay_sub_code, pay_sub_cur, pay_sub_desc, product_type_id) ");
			sb.append("VALUES (");
			sb.append("'" + lookup.getProdCode() + "', ");
			sb.append("'" + lookup.getPayCode() + "', ");
			sb.append("'" + lookup.getPaySubCode() + "', ");
			sb.append("'" + lookup.getPaySubCurr() + "', ");
			sb.append("'" + lookup.getPaySubDesc() + "', ");
			sb.append(lookup.getProductTypeId());
			sb.append(") \nGO \n");
		}

		System.out.println(sb);

	}

	public void testConfirmBalancingTool() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

		DCR dcr = new DCR();
		dcr.setCcId("G8");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("PK53");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRBalancingTool obj = new DCRBalancingTool();
		obj.setDcrCashier(dcrCashier);

		obj.setCompany(Company.SLOCPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLGFI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLFPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLAMCIP);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.OTHER);
		dcrBalancingToolDao.save(obj);

		List<DCRBalancingTool> list = dcrBalancingToolDao
				.getBalancingTools(dcrCashier.getId());

		for (DCRBalancingTool tool : list) {
			assertNull(tool.getDateConfirmed());
		}

		Long dcrBalancingToolId = null;
		for (DCRBalancingTool tool : list) {
			Company company = tool.getCompany();

			for (ProductType type : ProductType.values()) {
				if (company.equals(type.getCompany())) {
					DCRBalancingToolProduct prod = new DCRBalancingToolProduct();
					prod.setDcrBalancingTool(tool);
					prod.setProductType(type);
					prod.setWorksiteAmount(0.00);

					prod = dcrBalancingToolDao.saveBalancingToolProduct(prod);
					assertNotNull(prod.getId());
				}
			}

			if (Company.SLAMCIP.equals(company)) {
				dcrBalancingToolId = tool.getId();
			}
		}

		List<DCRBalancingToolProduct> prodList = dcrBalancingToolDao
				.getAllBalancingToolProducts(dcrBalancingToolId);

		assertEquals(10, prodList.size());

		for (DCRBalancingTool tool : list) {
			tool.setDateConfirmed(DateUtils.addDays(lastWeek, 1));
			boolean success = dcrBalancingToolDao.confirmBalancingTool(tool,
					true);
			assertTrue(success);
		}

		list = dcrBalancingToolDao.getBalancingTools(dcrCashier.getId());

		for (DCRBalancingTool tool : list) {
			assertNotNull(tool.getDateConfirmed());
		}

		endTransaction();
	}

	public void testSaveWorkSite() throws Exception {
		endTransaction();
		startNewTransaction();
		Date today = new Date();
		Date lastWeek = DateUtils.addDays(today, TEST_DAYS_AGO);
		Date startOfLastWeek = WMSDateUtil.startOfTheDay(lastWeek);

		DCR dcr = new DCR();
		dcr.setCcId("G8");
		dcr.setDcrDate(startOfLastWeek);
		dcr.setStatus("NEW");

		DCRCashier dcrCashier = new DCRCashier();
		dcrCashier.setCashier("PK53");
		dcrCashier.setLastUpdateDate(startOfLastWeek);
		dcrCashier.setStatus("NEW");

		dcr = dcrDao.save(dcr);

		dcrCashier.setDcr(dcr);
		dcrCashier = dcrCashierDao.save(dcrCashier);

		DCRBalancingTool obj = new DCRBalancingTool();
		obj.setDcrCashier(dcrCashier);

		obj.setCompany(Company.SLOCPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLGFI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLFPI);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.SLAMCIP);
		dcrBalancingToolDao.save(obj);

		obj.setCompany(Company.OTHER);
		dcrBalancingToolDao.save(obj);

		List<DCRBalancingTool> list = dcrBalancingToolDao
				.getBalancingTools(dcrCashier.getId());

		for (DCRBalancingTool tool : list) {
			assertNull(tool.getDateConfirmed());
		}

		Long dcrBalancingToolId = null;
		for (DCRBalancingTool tool : list) {
			Company company = tool.getCompany();

			for (ProductType type : ProductType.values()) {
				if (company.equals(type.getCompany())) {
					DCRBalancingToolProduct prod = new DCRBalancingToolProduct();
					prod.setDcrBalancingTool(tool);
					prod.setProductType(type);
					prod.setWorksiteAmount(0.00);

					prod = dcrBalancingToolDao.saveBalancingToolProduct(prod);
					assertNotNull(prod.getId());
				}
			}

			if (Company.SLOCPI.equals(company)) {
				dcrBalancingToolId = tool.getId();
			}
		}

		List<DCRBalancingToolProduct> prodList = dcrBalancingToolDao
				.getAllBalancingToolProducts(dcrBalancingToolId);

		for (DCRBalancingToolProduct column : prodList) {
			assertEquals(0.0, column.getWorksiteAmount());

			column.setWorksiteAmount(1000.0);
			dcrBalancingToolDao.saveWorkSite(column);
		}

		assertEquals(3, prodList.size());

		prodList = dcrBalancingToolDao
				.getAllBalancingToolProducts(dcrBalancingToolId);

		for (DCRBalancingToolProduct column : prodList) {
			assertEquals(1000.0, column.getWorksiteAmount());
		}

		endTransaction();
	}

	public void testSaveAndGetDcrOther() throws Exception {
		endTransaction();
		startNewTransaction();
		DCRBalancingTool others = new DCRBalancingTool();
		others.setId(99998L);

		DCRBalancingToolProduct prod = new DCRBalancingToolProduct();
		prod.setDcrBalancingTool(others);
		prod.setProductType(ProductType.OTHER_PESO);
		prod = dcrBalancingToolDao.saveBalancingToolProduct(prod);

		DCROther otherPeso = new DCROther();
		otherPeso.setCashCounterAmt(0.0);
		otherPeso.setCashNonCounter(0.0);
		otherPeso.setChequeNonCounter(0.0);
		otherPeso.setChequeOnUs(0.0);
		otherPeso.setChequeRegional(0.0);
		otherPeso.setDcrBalancingToolProductId(prod.getId());

		List<Long> productIds = new ArrayList<Long>();
		productIds.add(prod.getId());

		otherPeso = dcrBalancingToolDao.saveOther(otherPeso);

		List<DCROther> retrievedDCROthers = dcrBalancingToolDao
				.getDcrOthers(productIds);

		assertEquals(1, retrievedDCROthers.size());
		assertEquals(otherPeso, retrievedDCROthers.get(0));

		endTransaction();
	}

	// public void testGetExcemptions() throws Exception {
	// List<Excemption> list = dcrBalancingToolDao.getExcemptions(3L);
	//
	// assertEquals(20, list.size());
	// }
}
