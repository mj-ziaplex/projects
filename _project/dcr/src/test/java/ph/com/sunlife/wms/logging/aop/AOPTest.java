///*
// * COMMENTED AS CLASS WAS NOT BEING USED
// */
//package ph.com.sunlife.wms.logging.aop;
//
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//
//import java.util.Map;
//
//import org.junit.Ignore;
//import org.junit.Test;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit38.AbstractJUnit38SpringContextTests;
//
//@Ignore
//// Insert Configuration Here.
//@ContextConfiguration(locations = { "classpath:" })
//public class AOPTest extends AbstractJUnit38SpringContextTests {
//
//	/**
//	 * Test aop advice.
//	 * 
//	 * @throws Exception
//	 */
//	@Test
//	public void testAOPAdvice() throws Exception {
//
//		SimpleSpyProfiler origProfiler = (SimpleSpyProfiler) applicationContext
//				.getBean("simpleProfiler");
//		Map<String, Object> loadTransferInformation = null;
//		try {
//			assertNotNull(loadTransferInformation);
//		} catch (RuntimeException e) {
//			fail(e.getMessage());
//		}
//
//		verify(origProfiler, times(1)).log("message");
//
//	}
//
//}
