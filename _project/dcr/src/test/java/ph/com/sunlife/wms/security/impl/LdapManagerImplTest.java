package ph.com.sunlife.wms.security.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

//@Ignore
public class LdapManagerImplTest {

    @Test
    public void testAuthenticateUser() throws Exception {
        LdapManagerImpl mngr = new LdapManagerImpl();
        mngr.setUrl("ldap://sv5100.ph.sunlife");
        mngr.setAuthentication("simple");
        assertTrue(mngr.authenticateUser("wc01", "Sunlife_04"));
    }
}
