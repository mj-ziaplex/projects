package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.IpacDao;
import ph.com.sunlife.wms.dao.domain.Bank;
import ph.com.sunlife.wms.dao.domain.CheckType;
import ph.com.sunlife.wms.dao.domain.Company;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCRCashDepositSlip;
import ph.com.sunlife.wms.dao.domain.DCRChequeDepositSlip;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class IpacDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {

    private IpacDao ipacDao;

    public IpacDaoImplTest() {
        this.setDependencyCheck(false);
        this.setAutowireMode(AUTOWIRE_BY_NAME);
    }

    public void setIpacDao(IpacDao ipacDao) {
        this.ipacDao = ipacDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"ph/com/sunlife/wms/dao/spring/dao-config.xml",
                    "ph/com/sunlife/wms/service/spring/properties-config.xml"};
    }

    public void testHasCollections() throws Exception {
        Date processDate = WMSDateUtil.toDate("22NOV2012");
        boolean hasCollections = ipacDao.hasCollections("A1", processDate, "PK53");

        assertFalse(hasCollections);

        processDate = WMSDateUtil.toDate("19OCT2012");
        hasCollections = ipacDao.hasCollections("A2", processDate, "WC01");

        assertTrue(hasCollections);

    }
    
    public void testPopulateBankAccount() throws Exception {
        
        String centerCode = "A7";
        
        DCRChequeDepositSlip entity = new DCRChequeDepositSlip();
        entity.setCompany(Company.SLFPI);
        entity.setCheckType(CheckType.LOCAL);
        entity = ipacDao.populateBankAccount(centerCode, entity);
        assertEquals("0514-018790-200", entity.getAccountNumber());
        assertEquals("SBC", entity.getDepositoryBank().getId());

        DCRCashDepositSlip entity2 = new DCRCashDepositSlip();
        entity2.setCompany(Company.SLFPI);
        entity2 = ipacDao.populateBankAccount(centerCode, entity2);
        assertEquals("0514-018790-200", entity2.getAccountNumber());
        assertEquals("SBC", entity2.getDepositoryBank().getId());

        // Testing for OTHER company and check for healthcheck changes for check
        DCRChequeDepositSlip entity3 = new DCRChequeDepositSlip();
        entity3.setCompany(Company.OTHER);
        entity3.setCheckType(CheckType.LOCAL);
        entity3 = ipacDao.populateBankAccount(centerCode, entity3);
        assertEquals("0514-018635-200", entity3.getAccountNumber());
        assertEquals("SBC", entity3.getDepositoryBank().getId());

        DCRCashDepositSlip entity4 = new DCRCashDepositSlip();
        entity4.setCompany(Company.SLFPI);
        entity4 = ipacDao.populateBankAccount(centerCode, entity4);
        assertEquals("0514-018790-200", entity4.getAccountNumber());
        assertEquals("SBC", entity4.getDepositoryBank().getId());
    }

    public void testGetCurrentPdcCount() throws Exception {
        int pdcCount = ipacDao.getCurrentPdcCount("A2");
        assertEquals(60, pdcCount);
    }
    
    public void testGetCurrentTradVulPdcCount() throws Exception {
        int pdcCount = ipacDao.getCurrentTradVulPdcCount("A2");
        assertEquals(18, pdcCount);
    }

    public void testGetCurrentPnPdcCount() throws Exception {
        int pdcCount = ipacDao.getCurrentPNPdcCount("A2");
        assertEquals(5, pdcCount);
    }

    public void testGetAllDraweeBank() throws Exception {
        List<Bank> draweeBanks = ipacDao.getAllDraweeBanks();
        assertEquals(94, draweeBanks.size());
    }

    public void testGetSalesSlipCount() throws Exception {
        int count = ipacDao.getSalesSlipCount("A2", "IL", WMSDateUtil.toDate("02MAY2013"), Currency.PHP, "WC01");
        assertEquals(1, count);
}
}
