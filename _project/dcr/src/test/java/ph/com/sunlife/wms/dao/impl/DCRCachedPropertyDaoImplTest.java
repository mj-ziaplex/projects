package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCachedPropertyDao;
import ph.com.sunlife.wms.dao.domain.DCRCachedProperty;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRCachedPropertyDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRCachedPropertyDao dcrCachedPropertyDao;

	public DCRCachedPropertyDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrCachedPropertyDao(
			DCRCachedPropertyDao dcrCachedPropertyDao) {
		this.dcrCachedPropertyDao = dcrCachedPropertyDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testGetAllCachedProperties() throws Exception {
		dcrCachedPropertyDao.reset();

		Map<String, Object> results = dcrCachedPropertyDao
				.getAllCachedProperties();
		assertTrue(results.size() > 0);

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

		dcrCachedPropertyDao.reset();
		System.out.println("reset has been selected");

		results = dcrCachedPropertyDao.getAllCachedProperties();
		assertTrue(results.size() > 0);

	}

	public void testGetBFPReportDate() throws Exception {
		Map<String, Object> results = dcrCachedPropertyDao
				.getAllCachedProperties();
		assertTrue(results.size() > 0);

		String wmsDcrSystemDateKey = "WMSDCRSYSTEMDATE";
		DCRCachedProperty bfpReportCachedProp = (DCRCachedProperty) results
				.get(wmsDcrSystemDateKey);
		String dateStr = bfpReportCachedProp.getPropValue();
		Date bfpReportDate = WMSDateUtil.toDate(dateStr);
		System.out.println(bfpReportDate);

		assertTrue(dcrCachedPropertyDao.incrementWmsDcrSystemDate(
				wmsDcrSystemDateKey, bfpReportDate));

		results = dcrCachedPropertyDao.getAllCachedProperties();
		DCRCachedProperty bfpReportCachedProp2 = (DCRCachedProperty) results
				.get(wmsDcrSystemDateKey);
		String dateStr2 = bfpReportCachedProp2.getPropValue();
		Date bfpReportDate2 = WMSDateUtil.toDate(dateStr2);
		assertTrue(DateUtils.isSameDay(bfpReportDate, bfpReportDate2));

		dcrCachedPropertyDao.reset();

//		results = dcrCachedPropertyDao.getAllCachedProperties();
//		bfpReportCachedProp2 = (DCRCachedProperty) results
//				.get(wmsDcrSystemDateKey);
//		dateStr2 = bfpReportCachedProp2.getPropValue();
//		bfpReportDate2 = WMSDateUtil.toDate(dateStr2);
//		assertFalse(DateUtils.isSameDay(bfpReportDate, bfpReportDate2));
	}

	public void testIncrementDate() throws Exception {
//		endTransaction();
//		startNewTransaction();
//
//		String propKey = "WMSDCRSYSTEMDATE";
//		assertTrue(dcrCachedPropertyDao.incrementWmsDcrSystemDate(
//				propKey, null));
//
//		dcrCachedPropertyDao.reset();
//		
//		Map<String,Object> props = dcrCachedPropertyDao.getAllCachedProperties();
//		
//		assertEquals("23Apr2013", ((DCRCachedProperty) props.get(propKey)).getPropValue());
//
//		endTransaction();
	}
}
