package ph.com.sunlife.wms.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorNote;
import ph.com.sunlife.wms.dao.domain.DCRStepProcessorReconciled;
import ph.com.sunlife.wms.services.CachingService;
import ph.com.sunlife.wms.services.bo.DCRStepProcessorNoteBO;

public class ConsolidatedDataServiceImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRDao dcrDao;

//	private IpacDao ipacDao;
//
//	private DCRBalancingToolDao dcrBalancingToolDao;
//
//	private CashierDao cashierDao;

	private CachingService cachingService;

	public ConsolidatedDataServiceImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setCachingService(CachingService cachingService) {
		this.cachingService = cachingService;
	}

//	public void setCashierDao(CashierDao cashierDao) {
//		this.cashierDao = cashierDao;
//	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

//	public void setIpacDao(IpacDao ipacDao) {
//		this.ipacDao = ipacDao;
//	}

//	public void setDcrBalancingToolDao(DCRBalancingToolDao dcrBalancingToolDao) {
//		this.dcrBalancingToolDao = dcrBalancingToolDao;
//	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml",
				"ph/com/sunlife/wms/service/spring/service-config.xml" };
	}

	public void testGetConsolidatedData() throws Exception {
		// TODO: write a test with independent test data
		// TODO: use a mock IpacDao

		// ConsolidatedDataServiceImpl service = new
		// ConsolidatedDataServiceImpl();
		// service.setDcrDao(dcrDao);
		// service.setIpacDao(ipacDao);
		// service.setDcrBalancingToolDao(dcrBalancingToolDao);
		// service.setCashierDao(cashierDao);
		//
		// ConsolidatedData data = service.getConsolidatedData(3L);
		//
		// assertNotNull(data);
	}

	public void testGetReconciledRowNums() throws Exception {
		DCRStepProcessorReconciled dspr = new DCRStepProcessorReconciled();
		dspr.setReconciledRowNums("1, 2, 3, 4, 5, 6, 7,8");

		List<Integer> reconciledRowNums = new ConsolidatedDataServiceImpl()
				.getReconciledRowNums(dspr);

		assertTrue(reconciledRowNums.contains(1));
		assertTrue(reconciledRowNums.contains(2));
		assertTrue(reconciledRowNums.contains(3));
		assertTrue(reconciledRowNums.contains(4));
		assertTrue(reconciledRowNums.contains(5));
		assertTrue(reconciledRowNums.contains(6));
		assertTrue(reconciledRowNums.contains(7));
		assertTrue(reconciledRowNums.contains(8));
		assertEquals(8, reconciledRowNums.size());
	}

	public void testSaveInlineNotes() throws Exception {
		endTransaction();
		startNewTransaction();

		ConsolidatedDataServiceImpl service = new ConsolidatedDataServiceImpl();
		service.setDcrDao(dcrDao);
		service.setCachingService(cachingService);

		long dcrId = Long.MAX_VALUE;

		// Existing Note
		DCRStepProcessorNote prevNote = new DCRStepProcessorNote();
		prevNote.setAcf2id("WC01");
		prevNote.setNoteContent("bla bla bla, bree bree bree");
		prevNote.setNoteType("CCM");
		prevNote.setDcrId(dcrId);
		prevNote.setRowNum(30);
		dcrDao.saveNote(prevNote);

		// Submitted Notes
		List<DCRStepProcessorNoteBO> notes = new ArrayList<DCRStepProcessorNoteBO>();

		DCRStepProcessorNoteBO note1 = new DCRStepProcessorNoteBO();
		note1.setAcf2id("WC01");
		note1.setDcrId(dcrId);
		note1.setNoteContent("this is a note content");
		note1.setNoteType("CCM");
		note1.setRowNum(10);
		notes.add(note1);

		DCRStepProcessorNoteBO note2 = new DCRStepProcessorNoteBO();
		note2.setAcf2id("WC01");
		note2.setDcrId(dcrId);
		note2.setNoteContent("this is a note content");
		note2.setNoteType("FINDINGS");
		note2.setRowNum(12);
		notes.add(note2);

		DCRStepProcessorNoteBO note3 = new DCRStepProcessorNoteBO();
		note3.setAcf2id("WC01");
		note3.setDcrId(dcrId);
		note3.setNoteContent("this is a note content");
		note3.setNoteType("PPA");
		note3.setRowNum(11);
		notes.add(note3);

		DCRStepProcessorNoteBO note4 = new DCRStepProcessorNoteBO();
		note4.setAcf2id("WC01");
		note4.setNoteContent("bla bla bla, bree bree bree");
		note4.setNoteType("CCM");
		note4.setDcrId(dcrId);
		note4.setRowNum(30);
		notes.add(note4);

		List<DCRStepProcessorNoteBO> results = service.saveInlineNotes("WC01",
				dcrId, notes);
		assertEquals(4, results.size());

		for (DCRStepProcessorNoteBO result : results) {
			assertNotNull(result.getId());
		}

		endTransaction();
	}

	public void testSavePPAStepProcessor() throws Exception {
		endTransaction();
		startNewTransaction();

		ConsolidatedDataServiceImpl service = new ConsolidatedDataServiceImpl();
		service.setDcrDao(dcrDao);
		service.setCachingService(cachingService);

		long dcrId = Long.MAX_VALUE;

		List<DCRStepProcessorNoteBO> notes = new ArrayList<DCRStepProcessorNoteBO>();

		DCRStepProcessorNoteBO note1 = new DCRStepProcessorNoteBO();
		note1.setAcf2id("WC01");
		note1.setDcrId(dcrId);
		note1.setNoteContent("this is a note content");
		note1.setNoteType("CCM");
		note1.setRowNum(10);
		notes.add(note1);

		DCRStepProcessorNoteBO note2 = new DCRStepProcessorNoteBO();
		note2.setAcf2id("WC01");
		note2.setDcrId(dcrId);
		note2.setNoteContent("this is a note content");
		note2.setNoteType("FINDINGS");
		note2.setRowNum(12);
		notes.add(note2);

		DCRStepProcessorNoteBO note3 = new DCRStepProcessorNoteBO();
		note3.setAcf2id("WC01");
		note3.setDcrId(dcrId);
		note3.setNoteContent("this is a note content");
		note3.setNoteType("PPA");
		note3.setRowNum(11);
		notes.add(note3);

		List<Integer> rowNums = new ArrayList<Integer>();
		rowNums.add(1);
		rowNums.add(2);
		rowNums.add(3);
		rowNums.add(4);
		rowNums.add(5);

		DCRStepProcessorReconciled rc = new DCRStepProcessorReconciled();
		rc.setDcrId(dcrId);
		rc.setUserId("WC01");
		rc.setDateTime(new Date());
		// rc = dcrDao.createDCRStepProcessorReconciled(rc);

		service.savePPAStepProcessor("WC01", dcrId, rowNums, notes);

		for (DCRStepProcessorNoteBO note : notes) {
			assertNotNull(note.getId());
		}

		List<DCRStepProcessorReconciled> list = dcrDao
				.getDCRStepProcessorReconciled(dcrId);
		rc = list.get(0);
		assertNotNull(rc);
		assertEquals("1,2,3,4,5", rc.getReconciledRowNums());

		// List<DCRStepProcessorNoteBO> results =
		// service.saveInlineNotes(notes);
		//
		// for (DCRStepProcessorNoteBO result : results) {
		// assertNotNull(result.getId());
		// }

		endTransaction();
	}
	
	public void testCheckAgeingDcrWorkItems() throws Exception {
		ConsolidatedDataServiceImpl service = new ConsolidatedDataServiceImpl();
		service.setDcrDao(dcrDao);
		service.setCachingService(cachingService);
		
//		service.checkAgeingDcrWorkItems();
	}
}
