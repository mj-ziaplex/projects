package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCommentDao;
import ph.com.sunlife.wms.dao.domain.DCRComment;

public class DCRCommentDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private static final int TEST_DAYS_AGO = -366;

	private DCRCommentDao dcrCommentDao;

	public DCRCommentDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void setDcrCommentDao(DCRCommentDao dcrCommentDao) {
		this.dcrCommentDao = dcrCommentDao;
	}

	public void testAllMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		String acf2id = "pw12";
		long dcrCashierId = Long.MAX_VALUE;

		DCRComment c1 = new DCRComment();
		c1.setAcf2id(acf2id);
		c1.setDatePosted(getDateLastYear());
		c1.setDcrCashier(dcrCashierId);
		c1.setRemarks("Lorem ipsum comment 1");

		Thread.sleep(2000);

		DCRComment c2 = new DCRComment();
		c2.setAcf2id(acf2id);
		c2.setDatePosted(getDateLastYear());
		c2.setDcrCashier(dcrCashierId);
		c2.setRemarks("Lorem ipsum comment 2");

		Thread.sleep(3000);

		DCRComment c3 = new DCRComment();
		c3.setAcf2id(acf2id);
		c3.setDatePosted(getDateLastYear());
		c3.setDcrCashier(dcrCashierId);
		c3.setRemarks("Lorem ipsum comment 2");

		c1 = dcrCommentDao.save(c1);
		assertNotNull(c1.getId());
		
		c2 = dcrCommentDao.save(c2);
		assertNotNull(c2.getId());
		
		c3 = dcrCommentDao.save(c3);
		assertNotNull(c3.getId());

		List<DCRComment> list = dcrCommentDao.getAllComments(dcrCashierId);

		assertTrue(CollectionUtils.isNotEmpty(list));
		assertEquals(3, list.size());

		endTransaction();
	}

	private Date getDateLastYear() {
		return DateUtils.addDays(new Date(), TEST_DAYS_AGO);
	}
}
