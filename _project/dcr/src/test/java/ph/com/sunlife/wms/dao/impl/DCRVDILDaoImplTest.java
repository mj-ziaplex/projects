package ph.com.sunlife.wms.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import ph.com.sunlife.wms.dao.DCRCashierDao;
import ph.com.sunlife.wms.dao.DCRDao;
import ph.com.sunlife.wms.dao.DCRVDILDao;
import ph.com.sunlife.wms.dao.domain.Currency;
import ph.com.sunlife.wms.dao.domain.DCR;
import ph.com.sunlife.wms.dao.domain.DCRCashier;
import ph.com.sunlife.wms.dao.domain.DCRCenterCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinCollection;
import ph.com.sunlife.wms.dao.domain.DCRDollarCoinExchange;
import ph.com.sunlife.wms.dao.domain.DCRVDIL;
import ph.com.sunlife.wms.dao.domain.DCRVDILISOCollection;
import ph.com.sunlife.wms.dao.domain.DCRVDILPRSeriesNumber;
import ph.com.sunlife.wms.util.WMSDateUtil;

public class DCRVDILDaoImplTest extends
		AbstractTransactionalDataSourceSpringContextTests {

	private DCRVDILDao dcrVdilDao;

	private DCRDao dcrDao;

	private DCRCashierDao dcrCashierDao;

	public DCRVDILDaoImplTest() {
		this.setDependencyCheck(false);
		this.setAutowireMode(AUTOWIRE_BY_NAME);
	}

	public void setDcrVdilDao(DCRVDILDao dcrVdilDao) {
		this.dcrVdilDao = dcrVdilDao;
	}

	public void setDcrDao(DCRDao dcrDao) {
		this.dcrDao = dcrDao;
	}

	public void setDcrCashierDao(DCRCashierDao dcrCashierDao) {
		this.dcrCashierDao = dcrCashierDao;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "ph/com/sunlife/wms/dao/spring/dao-config.xml",
				"ph/com/sunlife/wms/service/spring/properties-config.xml" };
	}

	public void testSave() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		assertNotNull(entity.getId());

		endTransaction();
	}

	public void testGetByDCRCashierId() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		Long id = entity.getId();

		DCRVDIL retrievedEntity = dcrVdilDao.getByDCRCashierId(Long.MAX_VALUE);
		assertNotNull(retrievedEntity);
		assertEquals(id, retrievedEntity.getId());

		endTransaction();
	}

	public void testInsertDcrVdilIsoCollection() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);

		Date today = new Date();

		DCRVDILISOCollection iso1 = new DCRVDILISOCollection();
		iso1.setAmount(1000);
		iso1.setCurrencyId(Currency.PHP.getId());
		iso1.setDcrDate(today);
		iso1.setDcrvdil(entity);
		iso1.setPickupDate(today);
		iso1.setTypeId(1L);

		DCRVDILISOCollection iso2 = new DCRVDILISOCollection();
		iso2.setAmount(1000);
		iso2.setCurrencyId(Currency.PHP.getId());
		iso2.setDcrDate(today);
		iso2.setDcrvdil(entity);
		iso2.setPickupDate(today);
		iso2.setTypeId(1L);

		DCRVDILISOCollection iso3 = new DCRVDILISOCollection();
		iso3.setAmount(1000);
		iso3.setCurrencyId(Currency.PHP.getId());
		iso3.setDcrDate(today);
		iso3.setDcrvdil(entity);
		iso3.setPickupDate(today);
		iso3.setTypeId(1L);

		DCRVDILISOCollection iso4 = new DCRVDILISOCollection();
		iso4.setAmount(1000);
		iso4.setCurrencyId(Currency.PHP.getId());
		iso4.setDcrDate(today);
		iso4.setDcrvdil(entity);
		iso4.setPickupDate(today);
		iso4.setTypeId(1L);

		iso1 = dcrVdilDao.insertDcrVdilIsoCollection(iso1);
		iso2 = dcrVdilDao.insertDcrVdilIsoCollection(iso2);
		iso3 = dcrVdilDao.insertDcrVdilIsoCollection(iso3);
		iso4 = dcrVdilDao.insertDcrVdilIsoCollection(iso4);

		assertNotNull(iso1.getId());
		assertNotNull(iso2.getId());
		assertNotNull(iso3.getId());
		assertNotNull(iso4.getId());

		endTransaction();
	}

	public void testGetDcrVdilIsoCollections() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		Long id = entity.getId();

		Date today = new Date();

		DCRVDILISOCollection iso1 = new DCRVDILISOCollection();
		iso1.setAmount(1000);
		iso1.setCurrencyId(Currency.PHP.getId());
		iso1.setDcrDate(today);
		iso1.setDcrvdil(entity);
		iso1.setPickupDate(today);
		iso1.setTypeId(1L);

		DCRVDILISOCollection iso2 = new DCRVDILISOCollection();
		iso2.setAmount(1000);
		iso2.setCurrencyId(Currency.PHP.getId());
		iso2.setDcrDate(today);
		iso2.setDcrvdil(entity);
		iso2.setPickupDate(today);
		iso2.setTypeId(1L);

		DCRVDILISOCollection iso3 = new DCRVDILISOCollection();
		iso3.setAmount(1000);
		iso3.setCurrencyId(Currency.PHP.getId());
		iso3.setDcrDate(today);
		iso3.setDcrvdil(entity);
		iso3.setPickupDate(today);
		iso3.setTypeId(1L);

		DCRVDILISOCollection iso4 = new DCRVDILISOCollection();
		iso4.setAmount(1000);
		iso4.setCurrencyId(Currency.PHP.getId());
		iso4.setDcrDate(today);
		iso4.setDcrvdil(entity);
		iso4.setPickupDate(today);
		iso4.setTypeId(1L);

		iso1 = dcrVdilDao.insertDcrVdilIsoCollection(iso1);
		iso2 = dcrVdilDao.insertDcrVdilIsoCollection(iso2);
		iso3 = dcrVdilDao.insertDcrVdilIsoCollection(iso3);
		iso4 = dcrVdilDao.insertDcrVdilIsoCollection(iso4);

		assertNotNull(iso1.getId());
		assertNotNull(iso2.getId());
		assertNotNull(iso3.getId());
		assertNotNull(iso4.getId());

		List<DCRVDILISOCollection> list = dcrVdilDao
				.getDcrVdilIsoCollections(id);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(iso1));
		assertTrue(list.contains(iso2));
		assertTrue(list.contains(iso3));
		assertTrue(list.contains(iso4));

		endTransaction();
	}

	public void testInsertDcrVdilPrSeriesNumber() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);

		DCRVDILPRSeriesNumber pr1 = new DCRVDILPRSeriesNumber();
		pr1.setCompanyId(1L);
		pr1.setDcrvdil(entity);
		pr1.setFrom(1);
		pr1.setTo(2);
		pr1.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr2 = new DCRVDILPRSeriesNumber();
		pr2.setCompanyId(1L);
		pr2.setDcrvdil(entity);
		pr2.setFrom(1);
		pr2.setTo(2);
		pr2.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr3 = new DCRVDILPRSeriesNumber();
		pr3.setCompanyId(1L);
		pr3.setDcrvdil(entity);
		pr3.setFrom(1);
		pr3.setTo(2);
		pr3.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr4 = new DCRVDILPRSeriesNumber();
		pr4.setCompanyId(1L);
		pr4.setDcrvdil(entity);
		pr4.setFrom(1);
		pr4.setTo(2);
		pr4.setReason("lorem ipsum");

		pr1 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr1);
		pr2 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr2);
		pr3 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr3);
		pr4 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr4);

		assertNotNull(pr1.getId());
		assertNotNull(pr2.getId());
		assertNotNull(pr3.getId());
		assertNotNull(pr4.getId());

		endTransaction();
	}

	public void testGetDcrVdilPrSeriesNumbers() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		Long id = entity.getId();

		DCRVDILPRSeriesNumber pr1 = new DCRVDILPRSeriesNumber();
		pr1.setCompanyId(1L);
		pr1.setDcrvdil(entity);
		pr1.setFrom(1);
		pr1.setTo(2);
		pr1.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr2 = new DCRVDILPRSeriesNumber();
		pr2.setCompanyId(1L);
		pr2.setDcrvdil(entity);
		pr2.setFrom(1);
		pr2.setTo(2);
		pr2.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr3 = new DCRVDILPRSeriesNumber();
		pr3.setCompanyId(1L);
		pr3.setDcrvdil(entity);
		pr3.setFrom(1);
		pr3.setTo(2);
		pr3.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr4 = new DCRVDILPRSeriesNumber();
		pr4.setCompanyId(1L);
		pr4.setDcrvdil(entity);
		pr4.setFrom(1);
		pr4.setTo(2);
		pr4.setReason("lorem ipsum");

		pr1 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr1);
		pr2 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr2);
		pr3 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr3);
		pr4 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr4);

		assertNotNull(pr1.getId());
		assertNotNull(pr2.getId());
		assertNotNull(pr3.getId());
		assertNotNull(pr4.getId());

		List<DCRVDILPRSeriesNumber> list = dcrVdilDao
				.getDcrVdilPrSeriesNumbers(id);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(pr1));
		assertTrue(list.contains(pr2));
		assertTrue(list.contains(pr3));
		assertTrue(list.contains(pr4));

		endTransaction();
	}

	public void testUpdateDcrVdilPrSeriesNumbers() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		Long dcrVdilId = entity.getId();

		DCRVDILPRSeriesNumber pr1 = new DCRVDILPRSeriesNumber();
		pr1.setCompanyId(1L);
		pr1.setDcrvdil(entity);
		pr1.setFrom(1);
		pr1.setTo(2);
		pr1.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr2 = new DCRVDILPRSeriesNumber();
		pr2.setCompanyId(1L);
		pr2.setDcrvdil(entity);
		pr2.setFrom(1);
		pr2.setTo(2);
		pr2.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr3 = new DCRVDILPRSeriesNumber();
		pr3.setCompanyId(1L);
		pr3.setDcrvdil(entity);
		pr3.setFrom(1);
		pr3.setTo(2);
		pr3.setReason("lorem ipsum");

		DCRVDILPRSeriesNumber pr4 = new DCRVDILPRSeriesNumber();
		pr4.setCompanyId(1L);
		pr4.setDcrvdil(entity);
		pr4.setFrom(1);
		pr4.setTo(2);
		pr4.setReason("lorem ipsum");

		pr1 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr1);
		pr2 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr2);
		pr3 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr3);
		pr4 = dcrVdilDao.insertDcrVdilPrSeriesNumber(pr4);

		assertNotNull(pr1.getId());
		assertNotNull(pr2.getId());
		assertNotNull(pr3.getId());
		assertNotNull(pr4.getId());

		List<DCRVDILPRSeriesNumber> list = dcrVdilDao
				.getDcrVdilPrSeriesNumbers(dcrVdilId);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(pr1));
		assertTrue(list.contains(pr2));
		assertTrue(list.contains(pr3));
		assertTrue(list.contains(pr4));

		pr1.setCompanyId(2L);
		pr1.setFrom(2);
		pr1.setTo(2);
		pr1.setReason("new lorem ipsum");

		pr2.setCompanyId(2L);
		pr2.setFrom(2);
		pr2.setTo(2);
		pr2.setReason("new lorem ipsum");

		pr3.setCompanyId(2L);
		pr3.setFrom(2);
		pr3.setTo(2);
		pr3.setReason("new lorem ipsum");

		pr4.setCompanyId(2L);
		pr4.setFrom(2);
		pr4.setTo(2);
		pr4.setReason("new lorem ipsum");

		assertTrue(dcrVdilDao.updateDcrVdilPrSeriesNumber(pr1));
		assertTrue(dcrVdilDao.updateDcrVdilPrSeriesNumber(pr2));
		assertTrue(dcrVdilDao.updateDcrVdilPrSeriesNumber(pr3));
		assertTrue(dcrVdilDao.updateDcrVdilPrSeriesNumber(pr4));

		list = dcrVdilDao.getDcrVdilPrSeriesNumbers(dcrVdilId);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(pr1));
		assertTrue(list.contains(pr2));
		assertTrue(list.contains(pr3));
		assertTrue(list.contains(pr4));

		DCRVDILPRSeriesNumber selectedPR = list.get(0);

		assertEquals(2, selectedPR.getFrom());
		assertEquals(2, selectedPR.getTo());
		assertEquals("new lorem ipsum", selectedPR.getReason());
		assertEquals(Long.valueOf(2L), selectedPR.getCompanyId());

		endTransaction();
	}

	public void testUpdateVdil() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);

		entity.setAfterCutoffCard(2);
		entity.setAfterCutoffCashDollar(2000.0);
		entity.setAfterCutoffCashPeso(2000.0);
		entity.setAfterCutoffCheck(2);
		entity.setForSafeKeeping("lorem ipsum 2");
		entity.setOverages(2000.0);
		entity.setPettyCash(2000.0);
		entity.setPhotocopy(2000.0);
		entity.setPnPDCPendingWarehouse(2);
		entity.setTotalDollarCoins(2000.0);
		entity.setUnclaimedChange(2000.0);
		entity.setVultradPDCRPendingWarehouse(2);
		entity.setWorkingFund(2000.0);

		dcrVdilDao.updateVdil(entity);

		DCRVDIL newEntity = dcrVdilDao.getByDCRCashierId(Long.MAX_VALUE);
		assertEquals(entity, newEntity);
		assertEquals(2000.0, newEntity.getAfterCutoffCashDollar());
		assertEquals(2000.0, newEntity.getAfterCutoffCashPeso());
		assertEquals(2, newEntity.getAfterCutoffCheck());
		assertEquals("lorem ipsum 2", newEntity.getForSafeKeeping());
		assertEquals(2000.0, newEntity.getOverages());
		assertEquals(2000.0, newEntity.getPettyCash());
		assertEquals(2000.0, newEntity.getPhotocopy());
		assertEquals(2, newEntity.getPnPDCPendingWarehouse());
		assertEquals(2000.0, newEntity.getTotalDollarCoins());
		assertEquals(2000.0, newEntity.getUnclaimedChange());
		assertEquals(2, newEntity.getVultradPDCRPendingWarehouse());
		assertEquals(2000.0, newEntity.getWorkingFund());

		endTransaction();
	}

	public void testUpdateDcrVdilIsoCollections() throws Exception {
		endTransaction();
		startNewTransaction();

		DCRCashier parent = new DCRCashier();
		parent.setId(Long.MAX_VALUE);

		DCRVDIL entity = new DCRVDIL();
		entity.setAfterCutoffCard(1);
		entity.setAfterCutoffCashDollar(1000.0);
		entity.setAfterCutoffCashPeso(1000.0);
		entity.setAfterCutoffCheck(1);
		entity.setDcrCashier(parent);
		entity.setForSafeKeeping("lorem ipsum");
		entity.setOverages(1000.0);
		entity.setPettyCash(1000.0);
		entity.setPhotocopy(1000.0);
		entity.setPnPDCPendingWarehouse(1);
		entity.setTotalDollarCoins(1000.0);
		entity.setUnclaimedChange(1000.0);
		entity.setVultradPDCRPendingWarehouse(1);
		entity.setWorkingFund(1000.0);

		entity = dcrVdilDao.save(entity);
		Long dcrVdilId = entity.getId();

		Date today = new Date();

		DCRVDILISOCollection iso1 = new DCRVDILISOCollection();
		iso1.setAmount(1000);
		iso1.setCurrencyId(Currency.PHP.getId());
		iso1.setDcrDate(today);
		iso1.setDcrvdil(entity);
		iso1.setPickupDate(today);
		iso1.setTypeId(1L);

		DCRVDILISOCollection iso2 = new DCRVDILISOCollection();
		iso2.setAmount(1000);
		iso2.setCurrencyId(Currency.PHP.getId());
		iso2.setDcrDate(today);
		iso2.setDcrvdil(entity);
		iso2.setPickupDate(today);
		iso2.setTypeId(1L);

		DCRVDILISOCollection iso3 = new DCRVDILISOCollection();
		iso3.setAmount(1000);
		iso3.setCurrencyId(Currency.PHP.getId());
		iso3.setDcrDate(today);
		iso3.setDcrvdil(entity);
		iso3.setPickupDate(today);
		iso3.setTypeId(1L);

		DCRVDILISOCollection iso4 = new DCRVDILISOCollection();
		iso4.setAmount(1000);
		iso4.setCurrencyId(Currency.PHP.getId());
		iso4.setDcrDate(today);
		iso4.setDcrvdil(entity);
		iso4.setPickupDate(today);
		iso4.setTypeId(1L);

		iso1 = dcrVdilDao.insertDcrVdilIsoCollection(iso1);
		iso2 = dcrVdilDao.insertDcrVdilIsoCollection(iso2);
		iso3 = dcrVdilDao.insertDcrVdilIsoCollection(iso3);
		iso4 = dcrVdilDao.insertDcrVdilIsoCollection(iso4);

		assertNotNull(iso1.getId());
		assertNotNull(iso2.getId());
		assertNotNull(iso3.getId());
		assertNotNull(iso4.getId());

		List<DCRVDILISOCollection> list = dcrVdilDao
				.getDcrVdilIsoCollections(dcrVdilId);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(iso1));
		assertTrue(list.contains(iso2));
		assertTrue(list.contains(iso3));
		assertTrue(list.contains(iso4));

		Date tomorrow = DateUtils.addDays(new Date(), 1);

		iso1.setAmount(2000.0);
		iso1.setCurrencyId(2L);
		iso1.setDcrDate(tomorrow);
		iso1.setPickupDate(tomorrow);
		iso1.setTypeId(2L);

		iso2.setAmount(2000.0);
		iso2.setCurrencyId(2L);
		iso2.setDcrDate(tomorrow);
		iso2.setPickupDate(tomorrow);
		iso2.setTypeId(2L);

		iso3.setAmount(2000.0);
		iso3.setCurrencyId(2L);
		iso3.setDcrDate(tomorrow);
		iso3.setPickupDate(tomorrow);
		iso3.setTypeId(2L);

		iso4.setAmount(2000.0);
		iso4.setCurrencyId(2L);
		iso4.setDcrDate(tomorrow);
		iso4.setPickupDate(tomorrow);
		iso4.setTypeId(2L);

		assertTrue(dcrVdilDao.updateDcrVdilIsoCollection(iso1));
		assertTrue(dcrVdilDao.updateDcrVdilIsoCollection(iso2));
		assertTrue(dcrVdilDao.updateDcrVdilIsoCollection(iso3));
		assertTrue(dcrVdilDao.updateDcrVdilIsoCollection(iso4));

		list = dcrVdilDao.getDcrVdilIsoCollections(dcrVdilId);
		assertNotNull(list);
		assertEquals(4, list.size());

		assertTrue(list.contains(iso1));
		assertTrue(list.contains(iso2));
		assertTrue(list.contains(iso3));
		assertTrue(list.contains(iso4));

		DCRVDILISOCollection selectedISO = list.get(0);
		assertEquals(2000.0, selectedISO.getAmount());
		assertEquals(Long.valueOf(2L), selectedISO.getCurrencyId());
		assertTrue(DateUtils.isSameDay(tomorrow, selectedISO.getDcrDate()));
		assertTrue(DateUtils.isSameDay(tomorrow, selectedISO.getPickupDate()));
		assertEquals(Long.valueOf(2L), selectedISO.getTypeId());

		endTransaction();
	}

	public void testGetPreviousDollarCoinCollectionByDcrCashierId()
			throws Exception {
		endTransaction();

		try {
			startNewTransaction();

			DCR dcr1 = new DCR();
			dcr1.setCcId("A2");
			dcr1.setCcName("Ace");
			dcr1.setDcrDate(WMSDateUtil.toDate("17DEC2011"));
			dcr1.setRequiredCompletionDate(WMSDateUtil.toDate("20DEC2011"));
			dcr1.setStatus("PENDING");

			DCR dcr2 = new DCR();
			dcr2.setCcId("A2");
			dcr2.setCcName("Ace");
			dcr2.setDcrDate(WMSDateUtil.toDate("18DEC2011"));
			dcr2.setRequiredCompletionDate(WMSDateUtil.toDate("21DEC2011"));
			dcr2.setStatus("PENDING");

			dcr1 = dcrDao.save(dcr1);
			dcr2 = dcrDao.save(dcr2);

			DCRCashier dcrCashier1 = new DCRCashier();
			dcrCashier1.setCashier("wc01");
			dcrCashier1.setDcr(dcr1);
			dcrCashier1.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			DCRCashier dcrCashier2 = new DCRCashier();
			dcrCashier2.setCashier("wc01");
			dcrCashier2.setDcr(dcr2);
			dcrCashier2.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			dcrCashier1 = dcrCashierDao.save(dcrCashier1);
			dcrCashier2 = dcrCashierDao.save(dcrCashier2);

			DCRVDIL dv1 = new DCRVDIL();
			dv1.setDcrCashier(dcrCashier1);

			// dv2 is current day, dv1 is previous day
			DCRVDIL dv2 = new DCRVDIL();
			dv2.setDcrCashier(dcrCashier2);

			dv1 = dcrVdilDao.save(dv1);
			dv2 = dcrVdilDao.save(dv2);

			DCRDollarCoinCollection coll1 = new DCRDollarCoinCollection();
			coll1.setDateCreated(WMSDateUtil.toDate("18DEC2011"));
			coll1.setDcrvdil(dv1);
			coll1.setDenom1(10);
			coll1.setDenom50c(15);
			coll1.setDenom25c(20);
			coll1.setDenom10c(25);
			coll1.setDenom5c(30);
			coll1.setDenom1c(35);

			coll1 = dcrVdilDao.saveDCRDollarCoinCollection(coll1);

			Long previousDcrId = dcrVdilDao.getPreviousDayDcrId(dcr2);

			DCRDollarCoinCollection prevDayCollection = dcrVdilDao
					.getDollarCoinCollectionByDcrId(previousDcrId);

			// assertEquals(coll1, prevDayCollection);
			assertEquals(10, prevDayCollection.getDenom1());
			assertEquals(15, prevDayCollection.getDenom50c());
			assertEquals(20, prevDayCollection.getDenom25c());
			assertEquals(25, prevDayCollection.getDenom10c());
			assertEquals(30, prevDayCollection.getDenom5c());
			assertEquals(35, prevDayCollection.getDenom1c());
		} finally {
			endTransaction();
		}
	}

	public void testUpdateNetDollarCoinCollectionByDcrCashierId()
			throws Exception {
		endTransaction();

		try {
			startNewTransaction();

			DCR dcr1 = new DCR();
			dcr1.setCcId("A2");
			dcr1.setCcName("Ace");
			dcr1.setDcrDate(WMSDateUtil.toDate("17DEC2011"));
			dcr1.setRequiredCompletionDate(WMSDateUtil.toDate("20DEC2011"));
			dcr1.setStatus("PENDING");

			DCR dcr2 = new DCR();
			dcr2.setCcId("A2");
			dcr2.setCcName("Ace");
			dcr2.setDcrDate(WMSDateUtil.toDate("18DEC2011"));
			dcr2.setRequiredCompletionDate(WMSDateUtil.toDate("21DEC2011"));
			dcr2.setStatus("PENDING");

			dcr1 = dcrDao.save(dcr1);
			dcr2 = dcrDao.save(dcr2);

			DCRCashier dcrCashier1 = new DCRCashier();
			dcrCashier1.setCashier("wc01");
			dcrCashier1.setDcr(dcr1);
			dcrCashier1.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			DCRCashier dcrCashier2 = new DCRCashier();
			dcrCashier2.setCashier("wc01");
			dcrCashier2.setDcr(dcr2);
			dcrCashier2.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			dcrCashier1 = dcrCashierDao.save(dcrCashier1);
			dcrCashier2 = dcrCashierDao.save(dcrCashier2);

			DCRVDIL dv1 = new DCRVDIL();
			dv1.setDcrCashier(dcrCashier1);

			DCRVDIL dv2 = new DCRVDIL();
			dv2.setDcrCashier(dcrCashier2);

			dv1 = dcrVdilDao.save(dv1);
			dv2 = dcrVdilDao.save(dv2);

			DCRDollarCoinCollection coll1 = new DCRDollarCoinCollection();
			coll1.setDateCreated(WMSDateUtil.toDate("18DEC2011"));
			coll1.setDcrvdil(dv1);
			coll1.setDenom1(10);
			coll1.setDenom50c(15);
			coll1.setDenom25c(20);
			coll1.setDenom10c(25);
			coll1.setDenom5c(30);
			coll1.setDenom1c(35);

			coll1 = dcrVdilDao.saveDCRDollarCoinCollection(coll1);

			DCRDollarCoinCollection coll2 = new DCRDollarCoinCollection();
			coll2.setDateCreated(WMSDateUtil.toDate("18DEC2011"));
			coll2.setDcrvdil(dv2);

			coll1 = dcrVdilDao.saveDCRDollarCoinCollection(coll1);
			coll2 = dcrVdilDao.saveDCRDollarCoinCollection(coll2);

			coll2.setDenom1(50);
			coll2.setDenom50c(50);
			coll2.setDenom25c(50);
			coll2.setDenom10c(50);
			coll2.setDenom5c(50);
			coll2.setDenom1c(50);
			boolean isUpdated = dcrVdilDao
					.updateNetDCRDollarCoinCollection(coll2);

			assertTrue(isUpdated);

			DCRDollarCoinCollection currentDayCollection = dcrVdilDao
					.getDollarCoinCollectionByDcrCashierId(dcrCashier2.getId());

			assertEquals(coll2, currentDayCollection);
			assertEquals(50, currentDayCollection.getDenom1());
			assertEquals(50, currentDayCollection.getDenom50c());
			assertEquals(50, currentDayCollection.getDenom25c());
			assertEquals(50, currentDayCollection.getDenom10c());
			assertEquals(50, currentDayCollection.getDenom5c());
			assertEquals(50, currentDayCollection.getDenom1c());
		} finally {
			endTransaction();
		}

	}

	public void testDollarCoinExchange() throws Exception {
		endTransaction();

		try {
			startNewTransaction();

			DCR dcr1 = new DCR();
			dcr1.setCcId("A2");
			dcr1.setCcName("Ace");
			dcr1.setDcrDate(WMSDateUtil.toDate("17DEC2011"));
			dcr1.setRequiredCompletionDate(WMSDateUtil.toDate("20DEC2011"));
			dcr1.setStatus("PENDING");

			DCR dcr2 = new DCR();
			dcr2.setCcId("A2");
			dcr2.setCcName("Ace");
			dcr2.setDcrDate(WMSDateUtil.toDate("18DEC2011"));
			dcr2.setRequiredCompletionDate(WMSDateUtil.toDate("21DEC2011"));
			dcr2.setStatus("PENDING");

			dcr1 = dcrDao.save(dcr1);
			dcr2 = dcrDao.save(dcr2);

			DCRCashier dcrCashier1 = new DCRCashier();
			dcrCashier1.setCashier("wc01");
			dcrCashier1.setDcr(dcr1);
			dcrCashier1.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			DCRCashier dcrCashier2 = new DCRCashier();
			dcrCashier2.setCashier("wc01");
			dcrCashier2.setDcr(dcr2);
			dcrCashier2.setLastUpdateDate(WMSDateUtil.toDate("21DEC2011"));

			dcrCashier1 = dcrCashierDao.save(dcrCashier1);
			dcrCashier2 = dcrCashierDao.save(dcrCashier2);

			DCRVDIL dv1 = new DCRVDIL();
			dv1.setDcrCashier(dcrCashier1);

			DCRVDIL dv2 = new DCRVDIL();
			dv2.setDcrCashier(dcrCashier2);

			dv1 = dcrVdilDao.save(dv1);
			dv2 = dcrVdilDao.save(dv2);

			DCRDollarCoinExchange ex1 = new DCRDollarCoinExchange();
			ex1.setDcrvdil(dv1);

			DCRDollarCoinExchange ex2 = new DCRDollarCoinExchange();
			ex2.setDcrvdil(dv2);

			ex1 = dcrVdilDao.createDCRDollarCoinExchange(ex1);
			ex2 = dcrVdilDao.createDCRDollarCoinExchange(ex2);

			ex1.setIncomingDenom1(10);
			ex1.setIncomingDenom10c(15);
			ex1.setIncomingDenom25c(20);
			ex1.setIncomingDenom1c(25);
			ex1.setIncomingDenom50c(30);

			ex1.setOutgoingDenom1(10);
			ex1.setOutgoingDenom10c(30);
			ex1.setOutgoingDenom25c(25);
			ex1.setOutgoingDenom1c(15);
			ex1.setOutgoingDenom50c(20);

			ex2.setIncomingDenom1(5);
			ex2.setIncomingDenom10c(35);
			ex2.setIncomingDenom25c(40);
			ex2.setIncomingDenom1c(20);
			ex2.setIncomingDenom50c(15);

			ex2.setOutgoingDenom1(10);
			ex2.setOutgoingDenom10c(25);
			ex2.setOutgoingDenom25c(30);
			ex2.setOutgoingDenom1c(45);
			ex2.setOutgoingDenom50c(15);

			boolean isEx1Updated = dcrVdilDao.updateDCRDollarCoinExchange(ex1);
			boolean isEx2Updated = dcrVdilDao.updateDCRDollarCoinExchange(ex2);

			assertTrue(isEx1Updated);
			assertTrue(isEx2Updated);

			Long dcrCashierId1 = dcrCashier1.getId();
			Long dcrCashierId2 = dcrCashier2.getId();

			DCRDollarCoinExchange rex1 = dcrVdilDao
					.getDollarCoinExchangeByCashierId(dcrCashierId1);
			DCRDollarCoinExchange rex2 = dcrVdilDao
					.getDollarCoinExchangeByCashierId(dcrCashierId2);

			assertEquals(ex1, rex1);
			assertEquals(ex2, rex2);

			assertEquals(ex1.getIncomingDenom1(), rex1.getIncomingDenom1());
			assertEquals(ex1.getIncomingDenom50c(), rex1.getIncomingDenom50c());
			assertEquals(ex1.getIncomingDenom25c(), rex1.getIncomingDenom25c());
			assertEquals(ex1.getIncomingDenom10c(), rex1.getIncomingDenom10c());
			assertEquals(ex1.getIncomingDenom5c(), rex1.getIncomingDenom5c());
			assertEquals(ex1.getIncomingDenom1c(), rex1.getIncomingDenom1c());

			assertEquals(ex1.getOutgoingDenom1(), rex1.getOutgoingDenom1());
			assertEquals(ex1.getOutgoingDenom50c(), rex1.getOutgoingDenom50c());
			assertEquals(ex1.getOutgoingDenom25c(), rex1.getOutgoingDenom25c());
			assertEquals(ex1.getOutgoingDenom10c(), rex1.getOutgoingDenom10c());
			assertEquals(ex1.getOutgoingDenom5c(), rex1.getOutgoingDenom5c());
			assertEquals(ex1.getOutgoingDenom1c(), rex1.getOutgoingDenom1c());

			assertEquals(ex2.getIncomingDenom1(), rex2.getIncomingDenom1());
			assertEquals(ex2.getIncomingDenom50c(), rex2.getIncomingDenom50c());
			assertEquals(ex2.getIncomingDenom25c(), rex2.getIncomingDenom25c());
			assertEquals(ex2.getIncomingDenom10c(), rex2.getIncomingDenom10c());
			assertEquals(ex2.getIncomingDenom5c(), rex2.getIncomingDenom5c());
			assertEquals(ex2.getIncomingDenom1c(), rex2.getIncomingDenom1c());

			assertEquals(ex2.getOutgoingDenom1(), rex2.getOutgoingDenom1());
			assertEquals(ex2.getOutgoingDenom50c(), rex2.getOutgoingDenom50c());
			assertEquals(ex2.getOutgoingDenom25c(), rex2.getOutgoingDenom25c());
			assertEquals(ex2.getOutgoingDenom10c(), rex2.getOutgoingDenom10c());
			assertEquals(ex2.getOutgoingDenom5c(), rex2.getOutgoingDenom5c());
			assertEquals(ex2.getOutgoingDenom1c(), rex2.getOutgoingDenom1c());

		} finally {
			endTransaction();
		}

	}

	public void testCurrentCenterCoinCollectionMethods() throws Exception {
		endTransaction();
		startNewTransaction();

		String thisCcId = "X5";
		DCRCenterCoinCollection thisCollection = dcrVdilDao
				.getCurrentCenterCoinCollection(thisCcId);
		assertNull(thisCollection);

		thisCollection = dcrVdilDao.createCurrentCenterCoinCollection(thisCcId);
		assertNotNull(thisCollection);

		thisCollection.setDenom1(1);
		thisCollection.setDenom10c(2);
		thisCollection.setDenom1c(10);
		thisCollection.setDenom50c(1);
		thisCollection.setDenom25c(4);
		thisCollection.setDenom5c(10);

		boolean isUpdated = dcrVdilDao
				.updateCurrentCenterCoinCollection(thisCollection);
		assertTrue(isUpdated);

		DCRCenterCoinCollection updatedCollection = dcrVdilDao
				.getCurrentCenterCoinCollection(thisCcId);
		assertNotNull(updatedCollection);
		
		assertEquals(1, updatedCollection.getDenom1());
		assertEquals(2, updatedCollection.getDenom10c());
		assertEquals(10, updatedCollection.getDenom1c());
		assertEquals(1, updatedCollection.getDenom50c());
		assertEquals(4, updatedCollection.getDenom25c());
		assertEquals(10, updatedCollection.getDenom5c());
		assertEquals(thisCcId, updatedCollection.getCcId());
		assertEquals(thisCollection, updatedCollection);
		
		endTransaction();
	}
	// public void testWTF() throws Exception {
	// DCRDollarCoinExchange exchange =
	// dcrVdilDao.getDollarCoinExchangeByCashierId(27L);
	//
	// assertNotNull(exchange);
	// }
}
