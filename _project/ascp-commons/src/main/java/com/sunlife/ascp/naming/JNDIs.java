/*
 * Copyright 2010-2020 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sunlife.ascp.naming;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class JNDIs {

  public static final Object fetchObject(final String name)
      throws NamingException {
    return (new InitialContext()).lookup(name);
  }

  public static final DataSource getDataSource(final String name)
      throws NamingException {
    return (DataSource) fetchObject(name);
  }



  private static JNDIs instance = new JNDIs();
  public static JNDIs getInstance() { return instance; }
  private JNDIs() { }
}
