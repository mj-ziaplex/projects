package com.sunlife.ascp.text;

public enum ListSeparator {

  COMMA(","), SEMICOLON(";");

  private String separator;

  private ListSeparator(String separator) {
    this.separator = separator;
  }

  public String getSeparator() {
    return separator;
  }
}
