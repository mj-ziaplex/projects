package com.sunlife.ascp.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class BasicSocketClient {

  private final Socket SOCKET_CLIENT;
  private PrintWriter input;
  private BufferedReader output;


  public BasicSocketClient(final String host,
                           final int port) throws IOException {
    SOCKET_CLIENT = new Socket(host, port);
  }

  public BasicSocketClient(final InetAddress host,
                           final int port) throws IOException {
    SOCKET_CLIENT = new Socket(host, port);
  }


  public void connect() throws IOException {
    input = new PrintWriter(SOCKET_CLIENT.getOutputStream(), true);
    output = new BufferedReader(new InputStreamReader(SOCKET_CLIENT.getInputStream()));
  }

  public String sendCommand(final String command) throws IOException {
    input.println(command);
    return output.readLine();
  }

  public void disconnect() throws IOException {
    input.close();
    output.close();
    SOCKET_CLIENT.close();
  }
}
