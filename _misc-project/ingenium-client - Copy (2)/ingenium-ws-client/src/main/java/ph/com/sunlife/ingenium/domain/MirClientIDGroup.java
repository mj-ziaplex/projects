package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIdG")
public class MirClientIDGroup {

    @XStreamImplicit(itemFieldName = "MirCliIdT")
    private List<Long> mirClientIDTexts;

    public List<Long> getMirClientIDTexts() {
        return mirClientIDTexts;
    }

    public void setMirClientIDTexts(List<Long> mirClientIDTexts) {
        this.mirClientIDTexts = mirClientIDTexts;
    }
}
