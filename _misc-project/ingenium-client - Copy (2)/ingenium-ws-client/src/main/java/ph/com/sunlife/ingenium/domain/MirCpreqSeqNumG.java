package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqSeqNumG")
public class MirCpreqSeqNumG {

	@XStreamImplicit(itemFieldName = "MirCpreqSeqNumT")
	protected List<String> mirCpreqSeqNumT;

	public List<String> getMirCpreqSeqNumT() {
		if (mirCpreqSeqNumT == null) {
			mirCpreqSeqNumT = new ArrayList<String>();
		}
		return this.mirCpreqSeqNumT;
	}

}
