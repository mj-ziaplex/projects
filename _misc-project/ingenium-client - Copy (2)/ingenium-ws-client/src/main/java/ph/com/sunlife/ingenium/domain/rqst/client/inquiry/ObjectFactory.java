//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.10 at 02:45:15 PM CST 
//


package ph.com.sunlife.ingenium.domain.reqst.client.inquiry;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ph.com.sunlife.domain.request package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserLoginName_QNAME = new QName("http://ACORD.org/Standards/Life/2", "UserLoginName");
    private final static QName _TransExeTime_QNAME = new QName("http://ACORD.org/Standards/Life/2", "TransExeTime");
    private final static QName _TransRefGUID_QNAME = new QName("http://ACORD.org/Standards/Life/2", "TransRefGUID");
    private final static QName _TransExeDate_QNAME = new QName("http://ACORD.org/Standards/Life/2", "TransExeDate");
    private final static QName _SrcSystem_QNAME = new QName("http://ACORD.org/Standards/Life/2", "SrcSystem");
    private final static QName _CryptType_QNAME = new QName("http://ACORD.org/Standards/Life/2", "CryptType");
    private final static QName _MirCliId_QNAME = new QName("http://ACORD.org/Standards/Life/2", "MirCliId");
    private final static QName _ReturnCds_QNAME = new QName("http://ACORD.org/Standards/Life/2", "ReturnCds");
    private final static QName _Pswd_QNAME = new QName("http://ACORD.org/Standards/Life/2", "Pswd");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ph.com.sunlife.domain.request
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransType }
     * 
     */
    public TransType createTransType() {
        return new TransType();
    }

    /**
     * Create an instance of {@link UserPswd }
     * 
     */
    public UserPswd createUserPswd() {
        return new UserPswd();
    }

    /**
     * Create an instance of {@link UserAuthRequest }
     * 
     */
    public UserAuthRequest createUserAuthRequest() {
        return new UserAuthRequest();
    }

    /**
     * Create an instance of {@link TXLife }
     * 
     */
    public TXLife createTXLife() {
        return new TXLife();
    }

    /**
     * Create an instance of {@link TXLifeRequest }
     * 
     */
    public TXLifeRequest createTXLifeRequest() {
        return new TXLifeRequest();
    }

    /**
     * Create an instance of {@link OLifE }
     * 
     */
    public OLifE createOLifE() {
        return new OLifE();
    }

    /**
     * Create an instance of {@link ClientData }
     * 
     */
    public ClientData createClientData() {
        return new ClientData();
    }

    /**
     * Create an instance of {@link MirCliInfo }
     * 
     */
    public MirCliInfo createMirCliInfo() {
        return new MirCliInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "UserLoginName")
    public JAXBElement<String> createUserLoginName(String value) {
        return new JAXBElement<String>(_UserLoginName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "TransExeTime")
    public JAXBElement<String> createTransExeTime(String value) {
        return new JAXBElement<String>(_TransExeTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "TransRefGUID")
    public JAXBElement<String> createTransRefGUID(String value) {
        return new JAXBElement<String>(_TransRefGUID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "TransExeDate")
    public JAXBElement<String> createTransExeDate(String value) {
        return new JAXBElement<String>(_TransExeDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "SrcSystem")
    public JAXBElement<String> createSrcSystem(String value) {
        return new JAXBElement<String>(_SrcSystem_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "CryptType")
    public JAXBElement<String> createCryptType(String value) {
        return new JAXBElement<String>(_CryptType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "MirCliId")
    public JAXBElement<String> createMirCliId(String value) {
        return new JAXBElement<String>(_MirCliId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "ReturnCds")
    public JAXBElement<String> createReturnCds(String value) {
        return new JAXBElement<String>(_ReturnCds_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ACORD.org/Standards/Life/2", name = "Pswd")
    public JAXBElement<String> createPswd(String value) {
        return new JAXBElement<String>(_Pswd_QNAME, String.class, null, value);
    }

}
