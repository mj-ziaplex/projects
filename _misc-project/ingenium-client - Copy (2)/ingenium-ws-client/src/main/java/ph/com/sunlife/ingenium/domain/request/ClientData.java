package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ClientData")
public class ClientData {

  @XStreamAlias("MirCliInfo")
  private ClientInformation clientInformation;

  public ClientData() { }

  public ClientInformation getClientInformation() {
    return clientInformation;
  }

  public void setClientInformation(ClientInformation clientInformation) {
    this.clientInformation = clientInformation;
  }
}
