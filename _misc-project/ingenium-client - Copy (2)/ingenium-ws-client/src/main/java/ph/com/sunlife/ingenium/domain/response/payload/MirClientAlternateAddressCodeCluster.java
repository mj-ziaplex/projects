package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAltAddrCdG")
public class MirClientAlternateAddressCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliAltAddrCdT")
    private List<String> clientAlternateAddressCodes;

    public List<String> getClientAlternateAddressCodes() {
        return clientAlternateAddressCodes;
    }

    public void setClientAlternateAddressCodes(final List<String> addressCodes) {
        clientAlternateAddressCodes = addressCodes;
    }
}
