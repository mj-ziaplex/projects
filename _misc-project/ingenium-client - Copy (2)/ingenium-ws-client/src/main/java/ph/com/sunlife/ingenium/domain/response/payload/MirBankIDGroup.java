package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkIdG")
public class MirBankIDGroup {

    @XStreamImplicit(itemFieldName = "MirBnkIdT")
    private List<String> mirBankIdTypes;

    public MirBankIDGroup() { }

    public List<String> getMirBankIdTypes() {
        return mirBankIdTypes;
    }

    public MirBankIDGroup setMirBankIdTypes(final List<String> types) {
        this.mirBankIdTypes = types;
        return this;
    }
}
