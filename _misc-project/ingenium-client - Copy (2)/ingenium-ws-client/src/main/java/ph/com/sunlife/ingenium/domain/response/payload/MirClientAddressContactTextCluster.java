package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrCntctTxtG")
public class MirClientAddressContactTextCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrCntctTxtT")
    private List<String> clientAddressContactTexts;

    public List<String> getClientAddressContactTexts() {
        return clientAddressContactTexts;
    }

    public void setClientAddressContactTexts(List<String> texts) {
        clientAddressContactTexts = texts;
    }
}
