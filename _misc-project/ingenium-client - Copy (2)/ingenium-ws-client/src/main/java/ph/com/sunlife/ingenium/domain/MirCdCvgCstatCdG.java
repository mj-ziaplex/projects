package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCdCvgCstatCdG")
public class MirCdCvgCstatCdG {

	@XStreamImplicit(itemFieldName = "MirCdCvgCstatCdT")
	private List<String> mirCdCvgCstatCdT;

	public List<String> getMirCdCvgCstatCdT() {
		return mirCdCvgCstatCdT;
	}

	public void setMirCdCvgCstatCdT(List<String> mirCdCvgCstatCdT) {
		this.mirCdCvgCstatCdT = mirCdCvgCstatCdT;
	}
}
