package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.ingenium.IngeniumWebServiceClient;
import ph.com.sunlife.wms.ingenium.domain.ConsolidatedInformation;
import ph.com.sunlife.wms.ingenium.domain.Coverage;
import ph.com.sunlife.wms.ingenium.domain.Policy;
import ph.com.sunlife.wms.ingenium.domain.Requirement;
import ph.com.sunlife.wms.ingenium.handler.ListRequirementsResponseHandler;
import ph.com.sunlife.wms.ingenium.handler.PolicyInquiryResponseHandler;
import ph.com.sunlife.wms.ingenium.ws.client.ListRequirementsRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.PolicyInquiryRequestBuilder;

import java.util.List;

public class PolicyServiceImpl implements PolicyService {

  @Override
  public Coverage getCoverageInformation(final String policyId) {


    return new Coverage();
  }

  @Override
  public ConsolidatedInformation getPolicyClientDetails(final String policyId) {
    Policy policy = (Policy) new PolicyInquiryResponseHandler()
        .setResponseString(new IngeniumWebServiceClient()
            .getResponse(new PolicyInquiryRequestBuilder(policyId)
                .create()))
        .getResponse();

    return new ConsolidatedInformation();
  }

  @Override
  public List<Requirement> getRequirement(final String policyId) {
    return (List<Requirement>) new ListRequirementsResponseHandler()
                                       .setResponseString(new IngeniumWebServiceClient()
                                                                  .getResponse(new ListRequirementsRequestBuilder(policyId)
                                                                                       .create()))
                                       .getResponse();
  }

  @Override
  public String getPlanId(final String policyId) {
    return ((Policy) new PolicyInquiryResponseHandler()
        .setResponseString(new IngeniumWebServiceClient()
            .getResponse(new PolicyInquiryRequestBuilder(policyId)
                .create()))
        .getResponse())
        .getPlanId();
  }

  @Override
  public boolean createRequirement(String mirPolicyIdBase, String requirementId, String status) {
    return true;
  }
}
