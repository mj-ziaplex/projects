package ph.com.sunlife.wms.ingenium.domain;

public class ConsolidatedInformation {

  private Client owner;
  private Client insured;
  private String agentCode;
  private String branchCode;
  private String companyName;
  private String applicationSignDate;
  private String servicingAgentName;

  public Client getOwner() {
    return owner;
  }

  public void setOwner(Client owner) {
    this.owner = owner;
  }

  public Client getInsured() {
    return insured;
  }

  public void setInsured(Client insured) {
    this.insured = insured;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getBranchCode() {
    return branchCode;
  }

  public void setBranchCode(String branchCode) {
    this.branchCode = branchCode;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getApplicationSignDate() {
    return applicationSignDate;
  }

  public void setApplicationSignDate(String applicationSignDate) {
    this.applicationSignDate = applicationSignDate;
  }

  public String getServicingAgentName() {
    return servicingAgentName;
  }

  public void setServicingAgentName(String servicingAgentName) {
    this.servicingAgentName = servicingAgentName;
  }
}
