package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirBusAppInfo")
public class MirBusAppInfo {

    @XStreamAlias("MirEntyTaxId")
    private String mirEntyTaxId;

    public String getMirEntyTaxId() {
        return mirEntyTaxId;
    }

    public void setMirEntyTaxId(String mirEntyTaxId) {
        this.mirEntyTaxId = mirEntyTaxId;
    }
}
