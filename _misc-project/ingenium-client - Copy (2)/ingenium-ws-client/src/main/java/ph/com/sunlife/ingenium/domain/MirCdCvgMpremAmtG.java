package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgMpremAmtG")
public class MirCdCvgMpremAmtG {

	@XStreamImplicit(itemFieldName = "MirCdCvgMpremAmtT")
	protected List<String> mirCdCvgMpremAmtT;

	public List<String> getMirCdCvgMpremAmtT() {
		if (mirCdCvgMpremAmtT == null) {
			mirCdCvgMpremAmtT = new ArrayList<String>();
		}
		return this.mirCdCvgMpremAmtT;
	}

}
