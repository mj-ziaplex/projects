package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XStreamAlias("MirDvCliAddrUpdByLnG")
public class MirDvClientAddressUpdatedByLineGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrUpdByLnT")
    private List<String> mirDvClientAddressUpdatedByLineTexts;

    public List<String> getMirDvClientAddressUpdatedByLineTexts() {
        return mirDvClientAddressUpdatedByLineTexts;
    }

    public void setMirDvClientAddressUpdatedByLineTexts(final List<String> updatedByLines) {
        mirDvClientAddressUpdatedByLineTexts = updatedByLines;
    }
}
