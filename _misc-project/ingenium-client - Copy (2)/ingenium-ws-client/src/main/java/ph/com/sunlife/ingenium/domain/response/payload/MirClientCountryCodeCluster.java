package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliCtryCdG")
public class MirClientCountryCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliCtryCdT")
    private List<String> mirClientCountryCodes;

    public List<String> getMirClientCountryCodes() {
        return mirClientCountryCodes;
    }

    public void setMirClientCountryCodes(List<String> mirClientCountryCodes) {
        this.mirClientCountryCodes = mirClientCountryCodes;
    }
}
