package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvEffDtG")
public class MirClientIndvEffectivityDateGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvEffDtT")
    private List<String> mirClientIndividualEffectivDateTypes;

    public List<String> getMirClientIndividualEffectivDateTypes() {
        return mirClientIndividualEffectivDateTypes;
    }

    public void setMirClientIndividualEffectivDateTypes(final List<String> types) {
        this.mirClientIndividualEffectivDateTypes = types;
    }
}
