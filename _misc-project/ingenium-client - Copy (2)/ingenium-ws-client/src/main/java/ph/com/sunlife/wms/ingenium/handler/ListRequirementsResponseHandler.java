package ph.com.sunlife.wms.ingenium.handler;

import ph.com.sunlife.ingenium.ws.domain.InquiryConsolidatedInformationResponse;
import ph.com.sunlife.ingenium.ws.domain.RequirementsListResponse;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;
import ph.com.sunlife.wms.ingenium.domain.Requirement;

import java.util.ArrayList;
import java.util.List;

public class ListRequirementsResponseHandler extends ResponseHandler {

  public ListRequirementsResponseHandler() {
    xstream.processAnnotations(RequirementsListResponse.class);
    xstream.registerConverter(new SuccessResultCodeAttribute());
  }

  @Override
  public List<Requirement> getResponse() {
    Requirement requirement;
    List<Requirement> requirements = new ArrayList<Requirement>();
    RequirementsListResponse response;

    response = (RequirementsListResponse) xstream.fromXML(getResponseString());

    List<String> requirementIDs = response.getTransaction().getContainer().getRequirementData().getMirListFields().getMirRequirementIDGroup().getMirRequirementIDTexts();
    List<String> requirementTypeCodes = response.getTransaction().getContainer().getRequirementData().getMirListFields().getMirReqirTypCdG().getMirReqirTypCdT();

    for (int i = 0; !"*".equals(requirementIDs.get(i)) ||
                    !"*".equals(requirementTypeCodes.get(i)); i++) {
      requirement = new Requirement();
      requirement.setIndex(requirementIDs.get(i))
                 .setRequirementCode(requirementTypeCodes.get(i));
      requirements.add(requirement);
    }
    
    return requirements;
  }
}
