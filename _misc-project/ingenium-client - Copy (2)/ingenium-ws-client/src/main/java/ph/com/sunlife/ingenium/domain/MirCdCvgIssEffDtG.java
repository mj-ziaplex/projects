package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgIssEffDtG")
public class MirCdCvgIssEffDtG {

	@XStreamImplicit(itemFieldName = "MirCdCvgIssEffDtT")
	protected List<String> mirCdCvgIssEffDtT;

	public List<String> getMirCdCvgIssEffDtT() {
		if (mirCdCvgIssEffDtT == null) {
			mirCdCvgIssEffDtT = new ArrayList<String>();
		}
		return this.mirCdCvgIssEffDtT;
	}

}
