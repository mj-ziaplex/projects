package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCommonFields")
public class MirCommonFields {

    @XStreamAlias("MirDvInsrdCliNm")
    private String mirDvInsuredClientNumber;
    @XStreamAlias("MirPolId")
    private MirPolicyID mirPolicyId;
    @XStreamAlias("MirCpreqCreatDt")
    private String mirCpreqCreatDt;

    public MirCommonFields() { }

    public String getMirDvInsuredClientNumber() {
        return mirDvInsuredClientNumber;
    }

    public void setMirDvInsuredClientNumber(final String mirDvInsuredClientNumber) {
        this.mirDvInsuredClientNumber = mirDvInsuredClientNumber;
    }

    public MirPolicyID getMirPolicyId() {
        return mirPolicyId;
    }

    public MirCommonFields setMirPolicyId(final MirPolicyID mirPolicyId) {
        this.mirPolicyId = mirPolicyId;
        return this;
    }

    public String getMirCpreqCreatDt() {
        return mirCpreqCreatDt;
    }

    public void setMirCpreqCreatDt(final String mirCpreqCreatDt) {
        this.mirCpreqCreatDt = mirCpreqCreatDt;
    }
}
