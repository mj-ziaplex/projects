package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqFolwupNumG")
public class MirCpreqFolwupNumG {

	@XStreamImplicit(itemFieldName = "MirCpreqFolwupNumT")
	protected List<String> mirCpreqFolwupNumT;

	public List<String> getMirCpreqFolwupNumT() {
		if (mirCpreqFolwupNumT == null) {
			mirCpreqFolwupNumT = new ArrayList<String>();
		}
		return this.mirCpreqFolwupNumT;
	}

}
