package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiSeqNumG")
public class MirBiSeqNumG {

	@XStreamImplicit(itemFieldName = "MirBiSeqNumT")
	protected List<String> mirBiSeqNumT;

	public List<String> getMirBiSeqNumT() {
		if (mirBiSeqNumT == null) {
			mirBiSeqNumT = new ArrayList<String>();
		}
		return this.mirBiSeqNumT;
	}

}
