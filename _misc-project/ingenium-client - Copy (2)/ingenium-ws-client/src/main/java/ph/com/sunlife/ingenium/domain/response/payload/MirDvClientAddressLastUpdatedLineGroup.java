package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrLstUpdLnG")
public class MirDvClientAddressLastUpdatedLineGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrLstUpdLnT")
    private List<String> dvClientAddrLastUpdatedLines;

    public List<String> getDvClientAddrLastUpdatedLines() {
        return dvClientAddrLastUpdatedLines;
    }

    public void setDvClientAddrLastUpdatedLines(final List<String> lastUpdatedLines) {
        this.dvClientAddrLastUpdatedLines = lastUpdatedLines;
    }
}
