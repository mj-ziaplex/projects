package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgFeUpremG")
public class MirRtCvgFeUpremG {

	@XStreamImplicit(itemFieldName = "MirRtCvgFeUpremAmtT")
	protected List<String> mirRtCvgFeUpremAmtT;

	public List<String> getMirRtCvgFeUpremAmtT() {
		if (mirRtCvgFeUpremAmtT == null) {
			mirRtCvgFeUpremAmtT = new ArrayList<String>();
		}
		return this.mirRtCvgFeUpremAmtT;
	}

}
