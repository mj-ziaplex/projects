package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrAddr1TxtG")
public class MirDvEmployerAddress1TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrAddr1TxtT")
    private List<String> mirDvEmployerAddress1Texts;

    public List<String> getMirDvEmployerAddress1Texts() {
        return mirDvEmployerAddress1Texts;
    }

    public void setMirDvEmployerAddress1Texts(final List<String> address1Texts) {
        mirDvEmployerAddress1Texts = address1Texts;
    }
}
