package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XStreamAlias("MirCliCoNmG")
public class MirClientCompanyNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliCoNmT")
    private List<String> mirClientCompanyNameTypes;

    public List<String> getMirClientCompanyNameTypes() {
        return mirClientCompanyNameTypes;
    }

    public void setMirClientCompanyNameTypes(final List<String> types) {
        this.mirClientCompanyNameTypes = types;
    }
}
