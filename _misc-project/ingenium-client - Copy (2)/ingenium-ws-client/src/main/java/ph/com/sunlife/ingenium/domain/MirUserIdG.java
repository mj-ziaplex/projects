package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirUserIdG")
public class MirUserIdG {

	@XStreamImplicit(itemFieldName = "MirUserIdT")
	protected List<String> mirUserIdT;

	public List<String> getMirUserIdT() {
		if (mirUserIdT == null) {
			mirUserIdT = new ArrayList<String>();
		}
		return this.mirUserIdT;
	}

}
