package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvSurNmG")
public class MirClientIndividualSurnameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvSurNmT")
    private List<String> mirClientIndividualSurnameTypes;

    public List<String> getMirClientIndividualSurnameTexts() {
        return mirClientIndividualSurnameTypes;
    }

    public void setMirClientIndividualSurnameTypes(final List<String> familyNames) {
        this.mirClientIndividualSurnameTypes = familyNames;
    }
}
