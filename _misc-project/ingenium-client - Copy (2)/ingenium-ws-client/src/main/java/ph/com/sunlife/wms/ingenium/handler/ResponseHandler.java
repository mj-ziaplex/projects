package ph.com.sunlife.wms.ingenium.handler;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang3.StringUtils;
import ph.com.sunlife.ingenium.ws.domain.ReturnCodes;

public abstract class ResponseHandler<Handler extends ResponseHandler> {

  protected XStream xstream;
  private String responseString;

  public ResponseHandler() {
    xstream = new XStream();
    xstream.allowTypesByWildcard(new String[] {
        "ph.com.sunlife.ingenium.domain.**",
        "ph.com.sunlife.ingenium.ws.domain.**"
    });
  }

  public ResponseHandler(final String responseString) {
    this();
    this.setResponseString(responseString);
  }

  public String getResponseString() {
    return responseString;
  }

  public Handler setResponseString(final String responseString) {
    this.responseString = StringUtils.defaultString(responseString);
    return (Handler) this;
  }

  public abstract Object getResponse();
}
