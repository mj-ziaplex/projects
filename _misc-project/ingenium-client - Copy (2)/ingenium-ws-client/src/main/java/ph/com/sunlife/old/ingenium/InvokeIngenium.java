package ph.com.sunlife.old.ingenium;

import org.xml.sax.InputSource;

import javax.xml.parsers.SAXParser;
import java.io.*;
import java.util.HashMap;

public class InvokeIngenium {

  private String m_sIngeniumServer = "";
  private String m_sIngeniumWSTarget = "";
  private String response = "";
  private BufferedWriter out = null;

  public InvokeIngenium() { }

  public InvokeIngenium(String sServerName, String sWSTarget) {
    m_sIngeniumServer = sServerName;
    m_sIngeniumWSTarget = sWSTarget;
  }

  public void initializeLog(){
    try {
      File logFile = new File(rbe.getString("WMS_LOG_PATH")+"\\InvokeIngenium.log");

      if (logFile.exists()) {
        logFile.createNewFile();
      }

      out = new BufferedWriter(new FileWriter(logFile, true));
      //out.write(aString);
      //out.close();
    } catch (IOException e) {
    }
  }

  public HashMap submit(String strTXLife, String BussinessProcess)
      throws Exception{
    System.out.println("submit (String strTXLife, String BussinessProcess)  start");
    // Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - start
    String isXMLWrite = rb.getString("XMLWrite");
    System.out.println("isXMLWrite: " + String.valueOf(isXMLWrite));
    // Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - end
    HashMap hs = null;

    File fileResponseXML = null;
    File fileRequestXML = null;
    long startTime;
    long endTime;
    long totalTime;

//    WMSLoggerLog w = new WMSLoggerLog("testing");
    try {
      initializeLog();
      out.write("entered submit");
      out.newLine();
      long longTime = System.currentTimeMillis();

      //Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - start
      FileWriter fw = null;
      FileWriter fw1 = null;
      if(isXMLWrite.equalsIgnoreCase("true")){
        fileResponseXML = new File(rb.getString("log_dir")+BussinessProcess+"_Response"+ longTime+".xml");
        fw = new FileWriter(fileResponseXML);
        fileRequestXML = new File(rb.getString("log_dir")+BussinessProcess+"_Request"+ longTime+".xml");
        fw1 = new FileWriter(fileRequestXML);
        fw1.write(strTXLife);
        fw1.flush();
        fw1.close();
      }
      out.write("done writing xmls");
      out.newLine();
      //Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - end
      Configuration conf = Configuration.getInstanceOf();
      //conf.setHost("sv5913");
      out.write("done with conf");
      out.newLine();

      //ROBIN Mods -mcasio 4/5/2011 (start)
      //conf.setHost(rb.getString("Server"));
      if (m_sIngeniumWSTarget.trim().length() > 0 ) {
        conf.setHost(m_sIngeniumWSTarget);
      } else {
        conf.setHost(rb.getString("Server"));
      }
      out.write("host set");
      out.newLine();
      //conf.setHost("sv5122.ph.sunlife");
      //ROBIN Mods -mcasio 4/5/2011 (end)

      XMLSoapHandler webRequest = new XMLSoapHandler();
      out.write("web request set");
      out.newLine();
      TXLifeService TXLifeService = new TXLifeService_Impl();
      out.write("TXLLIFESERVICE SET");
      out.newLine();
      TXLifeServicePort myPort = TXLifeService.getTXLifeService();
      out.write("myport set");
      out.newLine();
      webRequest.setRequestString(strTXLife);
      out.write("webrequest setrequeststring");
      out.newLine();
      startTime = System.currentTimeMillis();
      System.out.println("strtxlife: " + strTXLife);
      response = myPort.callTXLife(webRequest.getRequestString());
      System.out.println("WebResponse: " + String.valueOf(response));
      out.write("response set");
      out.newLine();
      endTime = System.currentTimeMillis();
      totalTime = endTime - startTime;
//      w.write(BussinessProcess +" took "+totalTime + " ms", "INFO");
//      TheLogger.debug("Total Time spent is " + totalTime );
      if(isXMLWrite.equalsIgnoreCase("true")){
        fw.write(response);
        fw.flush();
        fw.close();
        System.out.println("output:" + fileResponseXML);
      }
      out.write("fileresponse done");
      out.newLine();
      out.close();
    } catch(java.rmi.RemoteException ex) {
      // TODO handle remote exception
      ex.printStackTrace(System.out);
//      TheLogger.error(ex.getMessage());
    } catch(Exception ex) {
      if(out!= null){
        try {
          out.write(ex.toString());
          out.newLine();
          out.close();
          out = null;
        } catch (Exception e2) {
          e2.printStackTrace(System.out);
        }

      }
      ex.printStackTrace(System.out);
//      TheLogger.error(ex.getMessage());
    }
    try { // This code block invokes the callTXLife operation on web service
      initializeLog();
      out.write("entered second try catch");
      out.newLine();
      // Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - start
      SAXParser myparser = new SAXParser();
      out.write("myparser initialized");
      out.newLine();
      IngeniumHandler handler = new IngeniumHandler();
      out.write("handler initialized");
      out.newLine();
      myparser.setContentHandler(handler);
      out.write("content handler set");
      out.newLine();
      myparser.setErrorHandler(handler);
      out.write("error handler set");
      out.newLine();
      if(isXMLWrite.equalsIgnoreCase("true")){
        FileReader reader = new FileReader(fileResponseXML);
        myparser.parse(new InputSource(reader));
        out.write("myparser parsed1");
        out.newLine();
        reader.close();
      }else{
        byte buf[] = response.getBytes();
        ByteArrayInputStream in = new ByteArrayInputStream(buf);
        BufferedInputStream f = new BufferedInputStream(in);
        InputStreamReader reader = new InputStreamReader(f);
        myparser.parse(new InputSource(reader));
        out.write("myparser parsed2");
        out.newLine();
        reader.close();
      }
      //Andre Ceasar Dacanay - Ingenium xmlwrite enhancement - end
      hs = handler.getHashmap();
      out.write("got hs");
      out.newLine();
      out.close();
    } catch(Exception ex) {
      if(out!= null){
        try {
          out.write(ex.toString());
          out.newLine();
          out.close();
          out = null;
        } catch (Exception e2) {
        }

      }
      ex.printStackTrace();
//      TheLogger.error(ex.getMessage());
    }
    initializeLog();
    out.write("returning hs");
    out.newLine();
    out.write(hs.toString());
    out.close();
    System.out.println("submit (String strTXLife, String BussinessProcess)  end");
    return hs;
  }
}
