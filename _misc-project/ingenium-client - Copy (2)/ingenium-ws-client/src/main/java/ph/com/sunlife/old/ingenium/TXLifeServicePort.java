package ph.com.sunlife.old.ingenium;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TXLifeServicePort extends Remote {

  String callTXLife(String request) throws RemoteException;
}
