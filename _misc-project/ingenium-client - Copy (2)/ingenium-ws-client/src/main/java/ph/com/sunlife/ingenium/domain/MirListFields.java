package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirListFields")
public class MirListFields {

	@XStreamAlias("MirReqirIdG")
	private MirRequirementIDGroup mirRequirementIDGroup;
	@XStreamAlias("MirCpreqDesgntIdG")
	private MirCpreqDesgntIdG mirCpreqDesgntIdG;
	@XStreamAlias("MirCpreqFolwupDtG")
	private MirCpreqFolwupDtG mirCpreqFolwupDtG;
	@XStreamAlias("MirCpreqFolwupNumG")
	private MirCpreqFolwupNumG mirCpreqFolwupNumG;
	@XStreamAlias("MirCpreqSeqNumG")
	private MirCpreqSeqNumG mirCpreqSeqNumG;
	@XStreamAlias("MirCpreqStatCdG")
	private MirCpreqStatCdG mirCpreqStatCdG;
	@XStreamAlias("MirCpreqEffDtG")
	private MirCpreqEffDtG mirCpreqEffDtG;
	@XStreamAlias("MirCpreqTstDtG")
	private MirCpreqTstDtG mirCpreqTstDtG;
	@XStreamAlias("MirCpreqTstRsltCdG")
	private MirCpreqTstRsltCdG mirCpreqTstRsltCdG;
	@XStreamAlias("MirReqirTypCdG")
	private MirReqirTypCdG mirReqirTypCdG;
	@XStreamAlias("MirCpreqValidDtG")
	private MirCpreqValidDtG mirCpreqValidDtG;
	@XStreamAlias("MirUserIdG")
	private MirUserIdG mirUserIdG;
	@XStreamAlias("MirCmpltnessIndG")
	private MirCompletenessIndGroup mirCompletenessIndGroup;
	@XStreamAlias("MirIndexNumG")
	private MirIndexNumG mirIndexNumG;

	public MirRequirementIDGroup getMirRequirementIDGroup() {
		return mirRequirementIDGroup;
	}

	public void setMirRequirementIDGroup(MirRequirementIDGroup mirRequirementIDGroup) {
		this.mirRequirementIDGroup = mirRequirementIDGroup;
	}

	public MirCpreqDesgntIdG getMirCpreqDesgntIdG() {
		return mirCpreqDesgntIdG;
	}

	public void setMirCpreqDesgntIdG(MirCpreqDesgntIdG mirCpreqDesgntIdG) {
		this.mirCpreqDesgntIdG = mirCpreqDesgntIdG;
	}

	public MirCpreqFolwupDtG getMirCpreqFolwupDtG() {
		return mirCpreqFolwupDtG;
	}

	public void setMirCpreqFolwupDtG(MirCpreqFolwupDtG mirCpreqFolwupDtG) {
		this.mirCpreqFolwupDtG = mirCpreqFolwupDtG;
	}

	public MirCpreqFolwupNumG getMirCpreqFolwupNumG() {
		return mirCpreqFolwupNumG;
	}

	public void setMirCpreqFolwupNumG(MirCpreqFolwupNumG mirCpreqFolwupNumG) {
		this.mirCpreqFolwupNumG = mirCpreqFolwupNumG;
	}

	public MirCpreqSeqNumG getMirCpreqSeqNumG() {
		return mirCpreqSeqNumG;
	}

	public void setMirCpreqSeqNumG(MirCpreqSeqNumG mirCpreqSeqNumG) {
		this.mirCpreqSeqNumG = mirCpreqSeqNumG;
	}

	public MirCpreqStatCdG getMirCpreqStatCdG() {
		return mirCpreqStatCdG;
	}

	public void setMirCpreqStatCdG(MirCpreqStatCdG mirCpreqStatCdG) {
		this.mirCpreqStatCdG = mirCpreqStatCdG;
	}

	public MirCpreqEffDtG getMirCpreqEffDtG() {
		return mirCpreqEffDtG;
	}

	public void setMirCpreqEffDtG(MirCpreqEffDtG mirCpreqEffDtG) {
		this.mirCpreqEffDtG = mirCpreqEffDtG;
	}

	public MirCpreqTstDtG getMirCpreqTstDtG() {
		return mirCpreqTstDtG;
	}

	public void setMirCpreqTstDtG(MirCpreqTstDtG mirCpreqTstDtG) {
		this.mirCpreqTstDtG = mirCpreqTstDtG;
	}

	public MirCpreqTstRsltCdG getMirCpreqTstRsltCdG() {
		return mirCpreqTstRsltCdG;
	}

	public void setMirCpreqTstRsltCdG(MirCpreqTstRsltCdG mirCpreqTstRsltCdG) {
		this.mirCpreqTstRsltCdG = mirCpreqTstRsltCdG;
	}

	public MirReqirTypCdG getMirReqirTypCdG() {
		return mirReqirTypCdG;
	}

	public void setMirReqirTypCdG(MirReqirTypCdG mirReqirTypCdG) {
		this.mirReqirTypCdG = mirReqirTypCdG;
	}

	public MirCpreqValidDtG getMirCpreqValidDtG() {
		return mirCpreqValidDtG;
	}

	public void setMirCpreqValidDtG(MirCpreqValidDtG mirCpreqValidDtG) {
		this.mirCpreqValidDtG = mirCpreqValidDtG;
	}

	public MirUserIdG getMirUserIdG() {
		return mirUserIdG;
	}

	public void setMirUserIdG(MirUserIdG mirUserIdG) {
		this.mirUserIdG = mirUserIdG;
	}

	public MirCompletenessIndGroup getMirCompletenessIndGroup() {
		return mirCompletenessIndGroup;
	}

	public void setMirCompletenessIndGroup(MirCompletenessIndGroup mirCompletenessIndGroup) {
		this.mirCompletenessIndGroup = mirCompletenessIndGroup;
	}

	public MirIndexNumG getMirIndexNumG() {
		return mirIndexNumG;
	}

	public void setMirIndexNumG(MirIndexNumG mirIndexNumG) {
		this.mirIndexNumG = mirIndexNumG;
	}
}
