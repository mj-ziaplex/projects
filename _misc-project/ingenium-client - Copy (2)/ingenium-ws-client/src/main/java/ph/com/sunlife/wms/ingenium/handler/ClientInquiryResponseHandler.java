package ph.com.sunlife.wms.ingenium.handler;

import com.thoughtworks.xstream.converters.ConversionException;
import ph.com.sunlife.ingenium.ws.domain.ClientInquiryResponse;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;
import ph.com.sunlife.wms.ingenium.domain.Client;

import java.text.DateFormat;

public class ClientInquiryResponseHandler extends ResponseHandler {

  public ClientInquiryResponseHandler() {
    xstream.processAnnotations(ClientInquiryResponse.class);
    xstream.registerConverter(new SuccessResultCodeAttribute());
  }

  @Override
  public Client getResponse() {
    DateFormat dateFormmatter;
    Client client = null;
    ClientInquiryResponse response = null;

    try {
      response = (ClientInquiryResponse) xstream.fromXML(getResponseString());
    } catch (ConversionException ce) {

    }

    if (response.getMetadata()
        .getTransactionResult()
        .getResultCode().getCode() == 1 &&
        "INQUIRE COMPLETED - CONTINUE".equalsIgnoreCase(response.getTransaction()
            .getContainer()
            .getmMirUserMessageGroup()
            .getMirUserMessageTextGroup()
            .getMirUserMessageTexts().get(0))) {
      client = new Client().setId(response.getTransaction().getContainer()
          .getClientData().getMirClientInformation().getId())
          .setGivenName(response.getTransaction().getContainer()
              .getClientData().getMirNameInformation()
              .getMirClientIndividualGivenNameGroup()
              .getMirClientIndividualGivenNameTexts().get(0))
          .setMiddlename(response.getTransaction().getContainer()
              .getClientData().getMirNameInformation()
              .getMirClientIndividualMiddlenameGroup()
              .getMirClientIndividualMiddlenameTexts().get(0))
          .setFamilyName(response.getTransaction().getContainer()
              .getClientData().getMirNameInformation()
              .getMirClientIndividualSurnameGroup()
              .getMirClientIndividualSurnameTexts().get(0));
      return client;
    }

    return null;
  }
}
