//package ph.com.sunlife.ingenium.domain.request;
//
//import com.thoughtworks.xstream.annotations.XStreamAlias;
//import ph.com.sunlife.ingenium.domain.Container;
//
//@XStreamAlias("OLifE")
//public class ConsolidatedInformationDataContainer implements Container {
//
//    @XStreamAlias("InquiryConsolidatedInformationData")
//    private ConsolidatedInformationData data;
//
//    public ConsolidatedInformationDataContainer() { }
//
//    public ConsolidatedInformationDataContainer(final ConsolidatedInformationData data) {
//        this.data = data;
//    }
//
//    @Override
//    public ConsolidatedInformationData getPayload() {
//        return data;
//    }
//
//    public ConsolidatedInformationDataContainer setPayload(final ConsolidatedInformationData data) {
//        this.data = data;
//        return this;
//    }
//}
