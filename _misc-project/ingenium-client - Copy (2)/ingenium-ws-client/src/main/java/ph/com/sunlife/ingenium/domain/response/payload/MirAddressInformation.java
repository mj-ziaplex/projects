package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirAddrInfo")
public class MirAddressInformation {

    @XStreamAlias("MirCliAddrEffDtG")
    private MirClientAddressEffectivityDateGroup mirClientAddressEffectivityDateGroup;
    @XStreamAlias("MirCliAddrLn1TxtG")
    private MirClientAddressLine1TextGroup mirClientAddressLine1TextGroup;
    @XStreamAlias("MirCliAddrLn2TxtG")
    private MirClientAddressLine2TextGroup mirClientAddressLine2TextGroup;
    @XStreamAlias("MirCliAddrLn3TxtG")
    private MirClientAddressLine3TextGroup mirClientAddressLine3TextGroup;
    @XStreamAlias("MirCliAddrAddlTxtG")
    private MirClientAddressAdditionalTextGroup mirClientAddressAdditionalTextGroup;
    @XStreamAlias("MirCliCityNmTxtG")
    private MirClientCityNameTextCluster cityNames;
    @XStreamAlias("MirCliCtryCdG")
    private MirClientCountryCodeCluster countryCodes;
    @XStreamAlias("MirCliAddrCntctTxtG")
    private MirClientAddressContactTextCluster addressContacts;
    @XStreamAlias("MirCliPstlCdG")
    private MirClientPostalCodeCluster postalCodeCluster;
    @XStreamAlias("MirCliCrntLocCdG")
    private MirClientCrntLocationCodeCluster crntLocationCodeCluster;
    @XStreamAlias("MirCliAddrMunCdG")
    private MirClientAddressMuninicipalityCodeCluster addressMuninicipalityCodeCluster;
    @XStreamAlias("MirCliAddrCntyCdG")
    private MirClientAddressCountryCodeCluster addressCountryCodeCluster;
    @XStreamAlias("MirCliAltAddrCdG")
    private MirClientAlternateAddressCodeCluster alternateAddressCodeCluster;
    @XStreamAlias("MirCliResTypCdG")
    private MirClientResidenceTypeCodeCluster residenceTypeCodeCluster;
    @XStreamAlias("MirCliResNumG")
    private MirClientResidenceNumberCluster residenceNumberCluster;
    @XStreamAlias("MirCliAddrYrDurG")
    private MirClientAddressYearDurationCluster yearDurationCluster;
    @XStreamAlias("MirCliAddrChngDtG")
    private MirClientAddressChangeDateCluster changeDateCluster;
    @XStreamAlias("MirCliAddrStatCdG")
    private MirClientAddressStatusCodeCluster statusCodeCluster;
    @XStreamAlias("MirAddrStatChngDtG")
    private MirAddressStatusChangeDateGroup statusChangeDateCluster;
    @XStreamAlias("MirCliAddrGrCd")
    private String mirClientAddressGrCode;

    public MirClientAddressEffectivityDateGroup getMirClientAddressEffectivityDateGroup() {
        return mirClientAddressEffectivityDateGroup;
    }

    public void setMirClientAddressEffectivityDateGroup(MirClientAddressEffectivityDateGroup mirClientAddressEffectivityDateGroup) {
        this.mirClientAddressEffectivityDateGroup = mirClientAddressEffectivityDateGroup;
    }

    public MirClientAddressLine1TextGroup getMirClientAddressLine1TextGroup() {
        return mirClientAddressLine1TextGroup;
    }

    public void setMirClientAddressLine1TextGroup(MirClientAddressLine1TextGroup mirClientAddressLine1TextGroup) {
        this.mirClientAddressLine1TextGroup = mirClientAddressLine1TextGroup;
    }

    public MirClientAddressLine2TextGroup getMirClientAddressLine2TextGroup() {
        return mirClientAddressLine2TextGroup;
    }

    public void setMirClientAddressLine2TextGroup(MirClientAddressLine2TextGroup mirClientAddressLine2TextGroup) {
        this.mirClientAddressLine2TextGroup = mirClientAddressLine2TextGroup;
    }

    public MirClientAddressLine3TextGroup getMirClientAddressLine3TextGroup() {
        return mirClientAddressLine3TextGroup;
    }

    public void setMirClientAddressLine3TextGroup(MirClientAddressLine3TextGroup mirClientAddressLine3TextGroup) {
        this.mirClientAddressLine3TextGroup = mirClientAddressLine3TextGroup;
    }

    public MirClientAddressAdditionalTextGroup getMirClientAddressAdditionalTextGroup() {
        return mirClientAddressAdditionalTextGroup;
    }

    public void setMirClientAddressAdditionalTextGroup(MirClientAddressAdditionalTextGroup mirClientAddressAdditionalTextGroup) {
        this.mirClientAddressAdditionalTextGroup = mirClientAddressAdditionalTextGroup;
    }

    public MirClientCityNameTextCluster getCityNames() {
        return cityNames;
    }

    public void setCityNames(MirClientCityNameTextCluster cityNames) {
        this.cityNames = cityNames;
    }

    public MirClientCountryCodeCluster getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(MirClientCountryCodeCluster countryCodes) {
        this.countryCodes = countryCodes;
    }

    public MirClientAddressContactTextCluster getAddressContacts() {
        return addressContacts;
    }

    public void setAddressContacts(MirClientAddressContactTextCluster addressContacts) {
        this.addressContacts = addressContacts;
    }

    public MirClientPostalCodeCluster getPostalCodeCluster() {
        return postalCodeCluster;
    }

    public void setPostalCodeCluster(MirClientPostalCodeCluster postalCodeCluster) {
        this.postalCodeCluster = postalCodeCluster;
    }

    public MirClientCrntLocationCodeCluster getCrntLocationCodeCluster() {
        return crntLocationCodeCluster;
    }

    public void setCrntLocationCodeCluster(MirClientCrntLocationCodeCluster crntLocationCodeCluster) {
        this.crntLocationCodeCluster = crntLocationCodeCluster;
    }

    public MirClientAddressMuninicipalityCodeCluster getAddressMuninicipalityCodeCluster() {
        return addressMuninicipalityCodeCluster;
    }

    public void setAddressMuninicipalityCodeCluster(MirClientAddressMuninicipalityCodeCluster addressMuninicipalityCodeCluster) {
        this.addressMuninicipalityCodeCluster = addressMuninicipalityCodeCluster;
    }

    public MirClientAddressCountryCodeCluster getAddressCountryCodeCluster() {
        return addressCountryCodeCluster;
    }

    public void setAddressCountryCodeCluster(MirClientAddressCountryCodeCluster addressCountryCodeCluster) {
        this.addressCountryCodeCluster = addressCountryCodeCluster;
    }

    public MirClientAlternateAddressCodeCluster getAlternateAddressCodeCluster() {
        return alternateAddressCodeCluster;
    }

    public void setAlternateAddressCodeCluster(MirClientAlternateAddressCodeCluster alternateAddressCodeCluster) {
        this.alternateAddressCodeCluster = alternateAddressCodeCluster;
    }

    public MirClientResidenceTypeCodeCluster getResidenceTypeCodeCluster() {
        return residenceTypeCodeCluster;
    }

    public void setResidenceTypeCodeCluster(MirClientResidenceTypeCodeCluster residenceTypeCodeCluster) {
        this.residenceTypeCodeCluster = residenceTypeCodeCluster;
    }

    public MirClientResidenceNumberCluster getResidenceNumberCluster() {
        return residenceNumberCluster;
    }

    public void setResidenceNumberCluster(MirClientResidenceNumberCluster residenceNumberCluster) {
        this.residenceNumberCluster = residenceNumberCluster;
    }

    public MirClientAddressYearDurationCluster getYearDurationCluster() {
        return yearDurationCluster;
    }

    public void setYearDurationCluster(MirClientAddressYearDurationCluster yearDurationCluster) {
        this.yearDurationCluster = yearDurationCluster;
    }

    public MirClientAddressChangeDateCluster getChangeDateCluster() {
        return changeDateCluster;
    }

    public void setChangeDateCluster(MirClientAddressChangeDateCluster changeDateCluster) {
        this.changeDateCluster = changeDateCluster;
    }

    public MirClientAddressStatusCodeCluster getStatusCodeCluster() {
        return statusCodeCluster;
    }

    public void setStatusCodeCluster(MirClientAddressStatusCodeCluster statusCodeCluster) {
        this.statusCodeCluster = statusCodeCluster;
    }

    public MirAddressStatusChangeDateGroup getStatusChangeDateCluster() {
        return statusChangeDateCluster;
    }

    public void setStatusChangeDateCluster(MirAddressStatusChangeDateGroup statusChangeDateCluster) {
        this.statusChangeDateCluster = statusChangeDateCluster;
    }

    public String getMirClientAddressGrCode() {
        return mirClientAddressGrCode;
    }

    public void setMirClientAddressGrCode(String mirClientAddressGrCode) {
        this.mirClientAddressGrCode = mirClientAddressGrCode;
    }
}
