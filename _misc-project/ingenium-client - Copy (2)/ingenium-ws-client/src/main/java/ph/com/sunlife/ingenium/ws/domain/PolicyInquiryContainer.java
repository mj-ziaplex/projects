package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("OLifE")
public class PolicyInquiryContainer extends Container {

    @XStreamAlias("PolicyData")
    private PolicyData policyData;

    public PolicyData getPolicyData() {
        return policyData;
    }

    public PolicyInquiryContainer setPolicyData(PolicyData policyData) {
        this.policyData = policyData;
        return this;
    }
}
