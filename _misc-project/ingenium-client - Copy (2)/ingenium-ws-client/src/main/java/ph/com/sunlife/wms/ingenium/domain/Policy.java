package ph.com.sunlife.wms.ingenium.domain;

import java.util.List;

public class Policy {

    private String planId;
    private List<String> policyInformationAssignee;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public List<String> getPolicyInformationAssignee() {
        return policyInformationAssignee;
    }

    public void setPolicyInformationAssignee(List<String> policyInformationAssignee) {
        this.policyInformationAssignee = policyInformationAssignee;
    }
}
