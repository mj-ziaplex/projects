package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirAgtIdG")
public class MirAgentIDGroup {

    @XStreamImplicit(itemFieldName = "MirAgtIdT")
    private List<Long> mirAgentIDTexts;

    public List<Long> getMirAgentIDTexts() {
        return mirAgentIDTexts;
    }

    public void setMirAgentIDTexts(List<Long> mirAgentIDTexts) {
        this.mirAgentIDTexts = mirAgentIDTexts;
    }
}
