package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAnutntSubCdG")
public class MirDvAnutntSubCdG {

    @XStreamImplicit(itemFieldName = "MirDvAnutntSubCdT")
    private List<String> mirDvAnutntSubCdT;

    public List<String> getMirDvAnutntSubCdT() {
        return mirDvAnutntSubCdT;
    }

    public void setMirDvAnutntSubCdT(List<String> mirDvAnutntSubCdT) {
        this.mirDvAnutntSubCdT = mirDvAnutntSubCdT;
    }
}
