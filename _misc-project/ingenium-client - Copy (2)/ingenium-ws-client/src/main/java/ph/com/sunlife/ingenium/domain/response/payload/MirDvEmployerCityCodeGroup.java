package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrCityCdG")
public class MirDvEmployerCityCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrCityCdT")
    private List<String> mirDvEmployerCityCodeTexts;

    public List<String> getMirDvEmployerCityCodeTexts() {
        return mirDvEmployerCityCodeTexts;
    }

    public void setMirDvEmployerCityCodeTexts(List<String> cityCodeTexts) {
        mirDvEmployerCityCodeTexts = cityCodeTexts;
    }
}
