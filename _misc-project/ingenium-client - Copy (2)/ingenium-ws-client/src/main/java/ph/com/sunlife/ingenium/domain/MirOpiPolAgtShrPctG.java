package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirOpiPolAgtShrPctG")
public class MirOpiPolAgtShrPctG {

	@XStreamImplicit(itemFieldName = "MirOpiPolAgtShrPctT")
	protected List<String> mirOpiPolAgtShrPctT;

	public List<String> getMirOpiPolAgtShrPctT() {
		if (mirOpiPolAgtShrPctT == null) {
			mirOpiPolAgtShrPctT = new ArrayList<String>();
		}
		return this.mirOpiPolAgtShrPctT;
	}

}
