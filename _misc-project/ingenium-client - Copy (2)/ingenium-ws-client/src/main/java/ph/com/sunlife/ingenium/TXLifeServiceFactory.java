package ph.com.sunlife.ingenium;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
class TXLifeServiceFactory {

  private final static QName CALL_TXLIFEREQUEST_QNAME =
      new QName("http://schema/webservices.elink.solcorp.com", "callTXLifeRequest");
  private final static QName CALL_TXLIFERESPONSE_QNAME =
      new QName("http://schema/webservices.elink.solcorp.com", "callTXLifeResponse");

  public TXLifeServiceFactory() { }

  @XmlElementDecl(namespace = "http://schema/webservices.elink.solcorp.com", name = "callTXLifeRequest")
  public JAXBElement<String> createCallTXLifeRequest(final String value) {
    return new JAXBElement<String>(CALL_TXLIFEREQUEST_QNAME, String.class, null, value);
  }

  @XmlElementDecl(namespace = "http://schema/webservices.elink.solcorp.com", name = "callTXLifeResponse")
  public JAXBElement<String> createCallTXLifeResponse(final String value) {
    return new JAXBElement<String>(CALL_TXLIFERESPONSE_QNAME, String.class, null, value);
  }
}
