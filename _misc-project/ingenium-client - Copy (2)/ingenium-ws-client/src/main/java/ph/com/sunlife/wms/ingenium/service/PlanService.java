package ph.com.sunlife.wms.ingenium.service;

public interface PlanService {

  byte getReturnCode(final String planId);
}
