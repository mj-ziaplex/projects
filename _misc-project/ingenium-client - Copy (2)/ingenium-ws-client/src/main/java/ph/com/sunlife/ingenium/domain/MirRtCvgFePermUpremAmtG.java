package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgFePermUpremAmtG")
public class MirRtCvgFePermUpremAmtG {

	@XStreamImplicit(itemFieldName = "MirRtCvgFePermUpremAmtT")
	protected List<String> mirRtCvgFePermUpremAmtT;

	public List<String> getMirRtCvgFePermUpremAmtT() {
		if (mirRtCvgFePermUpremAmtT == null) {
			mirRtCvgFePermUpremAmtT = new ArrayList<String>();
		}
		return this.mirRtCvgFePermUpremAmtT;
	}

}
