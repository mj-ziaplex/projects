package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqEffDtG")
public class MirCpreqEffDtG {

	@XStreamImplicit(itemFieldName = "MirCpreqEffDtT")
	protected List<String> mirCpreqEffDtT;

	public List<String> getMirCpreqEffDtT() {
		if (mirCpreqEffDtT == null) {
			mirCpreqEffDtT = new ArrayList<String>();
		}
		return this.mirCpreqEffDtT;
	}

}
