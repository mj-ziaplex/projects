package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirClftInfo")
public class MirClftInfo {

    @XStreamAlias("MirCliResCtry1Cd")
    private String mirCliResCtry1Cd;

    public String getMirCliResCtry1Cd() {
        return mirCliResCtry1Cd;
    }

    public void setMirCliResCtry1Cd(String mirCliResCtry1Cd) {
        this.mirCliResCtry1Cd = mirCliResCtry1Cd;
    }
}
