package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkNmG")
public class MirBankNameGroup {

    @XStreamImplicit(itemFieldName = "MirBnkNmT")
    private List<String> mirBankNameTexts;

    public MirBankNameGroup() { }

    public List<String> getMirBankNameTexts() {
        return mirBankNameTexts;
    }

    public void setMirBankNameTexts(final List<String> types) {
        this.mirBankNameTexts = types;
    }
}
