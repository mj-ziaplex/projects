package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.MirCommonFields;
import ph.com.sunlife.ingenium.domain.MirPolicyID;
import ph.com.sunlife.ingenium.ws.domain.RequirementData;
import ph.com.sunlife.ingenium.ws.domain.ListRequirementsContainer;
import ph.com.sunlife.ingenium.ws.domain.Transaction;

public class ListRequirementsRequestBuilder extends IngeniumRequestBuilder {

    private String policyId;

    public ListRequirementsRequestBuilder(final String policyId) {
        this.setPolicyID(policyId);
    }

    public String getPolicyID() {
        return policyId;
    }

    public void setPolicyID(String policyId) {
        this.policyId = policyId;
    }

    @Override
    protected Transaction getTransaction() {
        ListRequirementsContainer box = new ListRequirementsContainer()
                .setRequirementData(new RequirementData()
                        .setMirCommonFields(new MirCommonFields()
                                .setMirPolicyId(new MirPolicyID(policyId))));
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("RequirementsList");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(box);
        return transaction;
    }
}
