package ph.com.sunlife.old.ingenium;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class IngBusinessProcesses {

  private InvokeIngenium invokeIngenium;
  boolean printParsedXML = true;
  private Date date = new Date();
  private SimpleDateFormat exeDateF = new SimpleDateFormat("yyyy-MM-dd");
  private SimpleDateFormat exeTimeF = new SimpleDateFormat("HH:mm:ss");
  private String exeDate = exeDateF.format(date);
  private String exeTime = exeTimeF.format(date);

  public IngBusinessProcesses(){
    invokeIngenium = new InvokeIngenium();

  }

  public IngBusinessProcesses(String sIngeniumServer, String sWSTarget){
    invokeIngenium = new InvokeIngenium(sIngeniumServer, sWSTarget);
  }

  public String createLogin(String Authenticate[])
  {
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> ");
    strBuffer.append("<TXLife>");
    strBuffer.append("<UserAuthRequest>");
    strBuffer.append("<UserLoginName>");
    strBuffer.append(Authenticate[0]);
    strBuffer.append("</UserLoginName>");
    strBuffer.append("<UserPswd>");
    strBuffer.append("<CryptType>");
    strBuffer.append("NONE");
    strBuffer.append("</CryptType>");
    strBuffer.append("<Pswd>");
    strBuffer.append(Authenticate[1]);
    strBuffer.append("</Pswd>");
    strBuffer.append("</UserPswd>");
    strBuffer.append("</UserAuthRequest>");
    return strBuffer.toString();
  }

  public Object BF6925_Append(String Authenticate[], String strPolicyId, String CoverageNum)
  {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if(strPolicyId.length() > 9)
    {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = ""+strPolicyId.charAt(9);
    }else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"CoverageList\">CoverageList</TransType>");
    strBuffer.append("<TransExeDate>2006-05-31</TransExeDate> ");
    strBuffer.append("<TransExeTime>09:00:00-05:00</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<CoverageData>");
    strBuffer.append("<MirCommonFields>");
    strBuffer.append("<MirCvgNum>");
    strBuffer.append(CoverageNum);
    strBuffer.append("</MirCvgNum>");
    strBuffer.append("<MirPolId> ");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId>");
    strBuffer.append("</MirCommonFields>");
    strBuffer.append("</CoverageData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF6925 - CoverageList_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
      if(printParsedXML)
      {
        Object strkeys[] = hs_result.keySet().toArray();
        for(int i=0;i<strkeys.length;i++)
        {
          ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
//          TheLogger.info(strkeys[i] + ": ");
          for(int j=0;j<al_result.size();j++) {
//            TheLogger.info("\t" + al_result.get(j));
          }
        }
      }
//      TheLogger.debug("BF6925 was called");
    } catch (Exception ex) {
//      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object InquiryBilling_Append(String Authenticate[],String strMirPolIdBase){
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    StringBuffer strBuffer = new StringBuffer();
    if(strMirPolIdBase.length() > 9)
    {
      PolicyNo = strMirPolIdBase.substring(0, 9);
      suffix = ""+strMirPolIdBase.charAt(9);
    }
    else
    {
      PolicyNo = strMirPolIdBase;
    }
    strBuffer.append(createLogin(Authenticate))
        .append("<TXLifeRequest>")
        .append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ")
        .append("<TransType tc=\"InquiryBilling\">InquiryBilling</TransType> ")
        .append("<TransExeDate>")
        .append(exeDate)
        .append("</TransExeDate> ")
        .append("<TransExeTime>")
        .append(exeTime)
        .append("</TransExeTime>")
        .append("<OLifE>")
        .append("<InquiryBillingData>")
        .append("<MirCommonFields>")
        .append("<MirPolId>")
        .append("<MirPolIdBase>")
        .append(PolicyNo)
        .append("</MirPolIdBase> ")
        .append("<MirPolIdSfx>")
        .append(suffix)
        .append("</MirPolIdSfx> ")
        .append("</MirPolId>")
        .append("</MirCommonFields>")
        .append("<SrcSystem>WMS</SrcSystem> ")
        .append("</InquiryBillingData>")
        .append("</OLifE>")
        .append("</TXLifeRequest>")
        .append("</TXLife>");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("InquiryBilling_PolNo-");
      sb.append(strMirPolIdBase);
      hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());

      if(printParsedXML)
      {
        Object strkeys[] = hs_result.keySet().toArray();
        for(int i=0;i<strkeys.length;i++)
        {
          ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
//          TheLogger.info(strkeys[i] + ": ");
          for(int j=0;j<al_result.size();j++) {
//            TheLogger.info("\t" + al_result.get(j));
          }
        }
      }
//      TheLogger.debug("BF1191 was called using client number");
    } catch (Exception ex) {
//      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF1220C_Append(String Authenticate[],String strClientID)
  {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";

    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"ClientInquiry\">ClientInquiry</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE>");
    strBuffer.append("<ReturnCds/>");
    strBuffer.append("<ClientData>");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("<MirCliInfo>");
    strBuffer.append("<MirCliId>");
    strBuffer.append(strClientID);
    strBuffer.append("</MirCliId>");
    strBuffer.append("</MirCliInfo>");
    strBuffer.append("</ClientData>");
    strBuffer.append("</OLifE>");
    strBuffer.append("</TXLifeRequest>");
    strBuffer.append("</TXLife> ");

    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF1220 - ClientInquiry_CliId-");
      sb.append(strClientID);
      hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
      if(printParsedXML)
      {
        Object strkeys[] = hs_result.keySet().toArray();
        for(int i=0;i<strkeys.length;i++)
        {
          ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
//          TheLogger.info(strkeys[i] + ": ");
          for(int j=0;j<al_result.size();j++) {
//            TheLogger.info("\t" + al_result.get(j));
          }
        }
      }
//      TheLogger.debug("BF1220 was called using client no");
    } catch (Exception ex) {
//      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }

  public Object BF8000_Append(String Authenticate[],String strPolicyId)
  {
    HashMap hs_result = null;
    String PolicyNo = "";
    String suffix = "";
    if(strPolicyId.length() > 9)
    {
      PolicyNo = strPolicyId.substring(0, 9);
      suffix = ""+strPolicyId.charAt(9);
    }
    else
      PolicyNo = strPolicyId;
    StringBuffer strBuffer = new StringBuffer();
    strBuffer.append(createLogin(Authenticate));
    strBuffer.append("<TXLifeRequest>");
    strBuffer.append("<TransRefGUID>2996b11f-e5b2-d00f-281b-03582683cd49</TransRefGUID> ");
    strBuffer.append("<TransType tc=\"PolicyInquiry\">PolicyInquiry</TransType>");
    strBuffer.append("<TransExeDate>");
    strBuffer.append(exeDate);
    strBuffer.append("</TransExeDate> ");
    strBuffer.append("<TransExeTime>");
    strBuffer.append(exeTime);
    strBuffer.append("</TransExeTime> ");
    strBuffer.append("<OLifE> ");
    strBuffer.append("<PolicyData> ");
    strBuffer.append("<MirPolId> ");
    strBuffer.append("<MirPolIdBase>");
    strBuffer.append(PolicyNo);
    strBuffer.append("</MirPolIdBase>");
    strBuffer.append("<MirPolIdSfx>");
    strBuffer.append(suffix);
    strBuffer.append("</MirPolIdSfx>");
    strBuffer.append("</MirPolId> ");
    strBuffer.append("<SrcSystem>WMS</SrcSystem>");
    strBuffer.append("</PolicyData> ");
    strBuffer.append("</OLifE> ");
    strBuffer.append("</TXLifeRequest> ");
    strBuffer.append("</TXLife> ");
    try {
      StringBuffer sb = new StringBuffer();
      sb.append("BF8000 - PolicyInquiry_PolNo-");
      sb.append(strPolicyId);
      hs_result = invokeIngenium.submit(strBuffer.toString(),sb.toString());
      if(printParsedXML)
      {
        Object strkeys[] = hs_result.keySet().toArray();
        for(int i=0;i<strkeys.length;i++)
        {
          ArrayList al_result = (ArrayList)hs_result.get(strkeys[i]);
//          TheLogger.info(strkeys[i] + ": ");
          for(int j=0;j<al_result.size();j++) {
//            TheLogger.info("\t" + al_result.get(j));
          }
        }
      }
//      TheLogger.debug("BF8000 was called");
    } catch (Exception ex) {
//      TheLogger.error(ex.getMessage());
    }
    return hs_result;
  }
}
