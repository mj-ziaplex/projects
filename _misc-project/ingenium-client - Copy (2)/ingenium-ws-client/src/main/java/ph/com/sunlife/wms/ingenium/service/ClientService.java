package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.wms.ingenium.domain.Client;

public interface ClientService {

    Client getClientDetails(String clientId);

    Client getClientDetails(final String clientId, final String guid);
}
