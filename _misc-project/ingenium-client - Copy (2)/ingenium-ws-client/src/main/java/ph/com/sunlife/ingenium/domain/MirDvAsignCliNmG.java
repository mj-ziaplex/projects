package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAsignCliNmG")
public class MirDvAsignCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvAsignCliNmT")
    private List<String> mMirDvAsignCliNmT;

    public List<String> getmMirDvAsignCliNmT() {
        return mMirDvAsignCliNmT;
    }

    public void setmMirDvAsignCliNmT(List<String> mMirDvAsignCliNmT) {
        this.mMirDvAsignCliNmT = mMirDvAsignCliNmT;
    }
}
