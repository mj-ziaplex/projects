package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliResNumG")
public class MirDvClientResidenceNumberGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliResNumT")
    private List<String> mirDvClientResidenceNumberTexts;

    public List<String> getMirDvClientResidenceNumberTexts() {
        return mirDvClientResidenceNumberTexts;
    }

    public void setMirDvClientResidenceNumberTexts(final List<String> residenceNumbers) {
        mirDvClientResidenceNumberTexts = residenceNumbers;
    }
}
