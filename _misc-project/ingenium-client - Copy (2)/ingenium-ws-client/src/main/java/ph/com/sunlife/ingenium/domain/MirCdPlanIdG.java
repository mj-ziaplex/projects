package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdPlanIdG")
public class MirCdPlanIdG {

	@XStreamImplicit(itemFieldName = "MirCdPlanIdT")
	private List<String> mirCdPlanIdT;

	public List<String> getMirCdPlanIdT() {
		return mirCdPlanIdT;
	}

	public void setMirCdPlanIdT(List<String> mirCdPlanIdT) {
		this.mirCdPlanIdT = mirCdPlanIdT;
	}
}
