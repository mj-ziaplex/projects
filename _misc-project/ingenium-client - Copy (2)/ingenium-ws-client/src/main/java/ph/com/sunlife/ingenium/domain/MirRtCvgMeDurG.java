package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgMeDurG")
public class MirRtCvgMeDurG {

	@XStreamImplicit(itemFieldName = "MirRtCvgMeDurT")
	protected List<String> mirRtCvgMeDurT;

	public List<String> getMirRtCvgMeDurT() {
		if (mirRtCvgMeDurT == null) {
			mirRtCvgMeDurT = new ArrayList<String>();
		}
		return this.mirRtCvgMeDurT;
	}

}
