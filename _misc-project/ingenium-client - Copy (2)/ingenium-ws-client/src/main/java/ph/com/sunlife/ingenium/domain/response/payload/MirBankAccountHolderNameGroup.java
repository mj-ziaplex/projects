package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkAcctHldrNmG")
public class MirBankAccountHolderNameGroup {

    @XStreamImplicit(itemFieldName = "MirBnkAcctHldrNmT")
    private List<String> mirBankAccountHolderNameTypes;

    public List<String> getMirBankAccountHolderNameTypes() {
        return mirBankAccountHolderNameTypes;
    }

    public MirBankAccountHolderNameGroup setMirBankAccountHolderNameTypes(final List<String> types) {
        this.mirBankAccountHolderNameTypes = types;
        return this;
    }
}
