package ph.com.sunlife.wms.ingenium.handler;

import com.thoughtworks.xstream.converters.ConversionException;
import ph.com.sunlife.ingenium.ws.domain.PolicyInquiryResponse;
import ph.com.sunlife.ingenium.ws.domain.ReturnCodes;
import ph.com.sunlife.ingenium.ws.domain.converter.SuccessResultCodeAttribute;
import ph.com.sunlife.wms.ingenium.domain.Policy;

public class PolicyInquiryResponseHandler extends ResponseHandler {

  public PolicyInquiryResponseHandler() {
    xstream.processAnnotations(PolicyInquiryResponse.class);
    xstream.registerConverter(new SuccessResultCodeAttribute());
  }

  @Override
  public Policy getResponse() {
    Policy policy = null;
    PolicyInquiryResponse response = null;

    try {
      response = (PolicyInquiryResponse) xstream.fromXML(getResponseString());
      policy = new Policy();
      policy.setPlanId(response.getTransaction().getContainer().getPolicyData().getMirPlanID());

    } catch (ConversionException ce) {
      // failed, handle it
    }

    return policy;
  }

//  @Override
//  public ReturnCodes getResponseReturnCodes() {
//    PolicyInquiryResponse response = null;
//
//    try {
//      response = (PolicyInquiryResponse) xstream.fromXML(getResponseString());
//      return response.getTransaction().getContainer().getReturnCodes();
//    } catch (ConversionException ce) {
//      // failed, handle it
//    }
//
//    return null;
//  }
}
