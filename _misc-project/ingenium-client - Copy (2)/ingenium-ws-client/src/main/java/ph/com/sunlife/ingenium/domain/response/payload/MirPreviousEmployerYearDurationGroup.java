package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirPrevEmplYrDurG")
public class MirPreviousEmployerYearDurationGroup {

    @XStreamImplicit(itemFieldName = "MirPrevEmplYrDurT")
    private List<Byte> mirPreviousEmployerYearDurationTexts;

    public List<Byte> getMirPreviousEmployerYearDurationTexts() {
        return mirPreviousEmployerYearDurationTexts;
    }

    public void setMirPreviousEmployerYearDurationTexts(final List<Byte> yearDurationTexts) {
        this.mirPreviousEmployerYearDurationTexts = yearDurationTexts;
    }
}
