package ph.com.sunlife.old.ingenium;

import javax.xml.rpc.Service;

public interface TXLifeService extends Service {

  TXLifeServicePort getTXLifeService();
}
