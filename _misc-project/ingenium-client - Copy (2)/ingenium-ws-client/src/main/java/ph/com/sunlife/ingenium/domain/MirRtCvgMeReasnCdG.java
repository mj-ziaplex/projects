package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgMeReasnCdG")
public class MirRtCvgMeReasnCdG {

	@XStreamImplicit(itemFieldName = "MirRtCvgMeReasnCdT")
	protected List<String> mirRtCvgMeReasnCdT;

	public List<String> getMirRtCvgMeReasnCdT() {
		if (mirRtCvgMeReasnCdT == null) {
			mirRtCvgMeReasnCdT = new ArrayList<String>();
		}
		return this.mirRtCvgMeReasnCdT;
	}

}
