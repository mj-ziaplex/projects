package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class InquiryConsolidatedInformationResponse extends RequestResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private InquiryConsolidatedInformationTransactionResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public InquiryConsolidatedInformationTransactionResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(InquiryConsolidatedInformationTransactionResponse transaction) {
        this.transaction = transaction;
    }
}
