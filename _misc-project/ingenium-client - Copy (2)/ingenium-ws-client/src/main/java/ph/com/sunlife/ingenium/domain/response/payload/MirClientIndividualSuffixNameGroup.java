package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvSfxNmG")
public class MirClientIndividualSuffixNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvSfxNmT")
    private List<String> mirClientIndividualSuffixNameTypes;

    public List<String> getMirClientIndividualSuffixNameTypes() {
        return mirClientIndividualSuffixNameTypes;
    }

    public void setMirClientIndividualSuffixNameTypes(final List<String> types) {
        mirClientIndividualSuffixNameTypes = types;
    }
}
