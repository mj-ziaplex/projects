package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvIpartyCliNmG")
public class MirDvIpartyCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvIpartyCliNmT")
    private List<String> mirDvIpartyCliNmT;

    public List<String> getMirDvIpartyCliNmT() {
        return mirDvIpartyCliNmT;
    }

    public void setMirDvIpartyCliNmT(List<String> mirDvIpartyCliNmT) {
        this.mirDvIpartyCliNmT = mirDvIpartyCliNmT;
    }
}
