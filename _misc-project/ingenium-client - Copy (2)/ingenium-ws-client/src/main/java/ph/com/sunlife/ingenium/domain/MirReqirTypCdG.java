package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirReqirTypCdG")
public class MirReqirTypCdG {

	@XStreamImplicit(itemFieldName = "MirReqirTypCdT")
	protected List<String> mirReqirTypCdT;

	public List<String> getMirReqirTypCdT() {
		if (mirReqirTypCdT == null) {
			mirReqirTypCdT = new ArrayList<String>();
		}
		return this.mirReqirTypCdT;
	}

}
