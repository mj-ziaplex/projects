package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgFaceAmtG")
public class MirCdCvgFaceAmtG {

	@XStreamImplicit(itemFieldName = "MirCdCvgFaceAmtT")
	protected List<String> mirCdCvgFaceAmtT;

	public List<String> getMirCdCvgFaceAmtT() {
		if (mirCdCvgFaceAmtT == null) {
			mirCdCvgFaceAmtT = new ArrayList<String>();
		}
		return this.mirCdCvgFaceAmtT;
	}

}
