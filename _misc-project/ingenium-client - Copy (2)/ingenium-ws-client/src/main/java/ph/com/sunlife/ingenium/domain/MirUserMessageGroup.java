package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.response.payload.MirUserMessageSeverityGroup;
import ph.com.sunlife.ingenium.domain.response.payload.MirUserMessageTextGroup;

@XStreamAlias("MirUserMsgG")
public class MirUserMessageGroup {

    @XStreamAlias("MirUserMsgTxtG")
    private MirUserMessageTextGroup mirUserMessageTextGroup;
    @XStreamAlias("MirUserMsgSevrtyG")
    private MirUserMessageSeverityGroup mirUserMessageSeverityGroup;

    public MirUserMessageTextGroup getMirUserMessageTextGroup() {
        return mirUserMessageTextGroup;
    }

    public void setMirUserMessageTextGroup(MirUserMessageTextGroup mirUserMessageTextGroup) {
        this.mirUserMessageTextGroup = mirUserMessageTextGroup;
    }

    public MirUserMessageSeverityGroup getMirUserMessageSeverityGroup() {
        return mirUserMessageSeverityGroup;
    }

    public void setMirUserMessageSeverityGroup(MirUserMessageSeverityGroup mirUserMessageSeverityGroup) {
        this.mirUserMessageSeverityGroup = mirUserMessageSeverityGroup;
    }
}
