package ph.com.sunlife.old.ingenium;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.util.ArrayList;
import java.util.HashMap;

public class IngeniumDispatcher {

  private IngBusinessProcesses ingBusProcess;
  private String m_sIngeniumServer = "";
  private String m_sIngeniumWSTarget = "";
  private String Authenticate[] = new String[2];
  private PasswordGenerator pass;

  public void IngeniumInit(String p_username, String sServer, String wsTarget) {
    m_sIngeniumServer = sServer;
    m_sIngeniumWSTarget = wsTarget;
    IngeniumInit(p_username);
  }

  public void IngeniumInit(String p_username) {
    if (m_sIngeniumWSTarget.trim().length() > 0) {
      ingBusProcess = new IngBusinessProcesses(m_sIngeniumServer, m_sIngeniumWSTarget);
    } else {
      ingBusProcess = new IngBusinessProcesses();
    }
    pass = new PasswordGenerator();

    StringBuffer sb = new StringBuffer();
    sb.append(p_username).append("W");
    System.out.println("sb.toString(): " + sb.toString());
    Authenticate[0] = sb.toString();
    Authenticate[1] = "";

    try {
      Authenticate[0] = sb.toString();
      Authenticate[1] = pass.generatePassword(sb.toString());

      System.out.println("username is " + sb.toString() + " password is "
          + pass.generatePassword(sb.toString()));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public HashMap getPolicyClientDetails(String strPolicyId) {
    HashMap hashMap;
    HashMap HashPolOwner = this.getPolicyOwnerDetails(strPolicyId);
    HashMap HasInsDetails = this.getPolicyInsuredDetails(strPolicyId, "01");
    hashMap = new HashMap();
    if (HashPolOwner != null) {
      if (HasInsDetails != null) {
        if (HashPolOwner.get("BRANCH_CODE") != null)
          hashMap.put("BRANCH_CODE", HashPolOwner.get("BRANCH_CODE"));
        if (HashPolOwner.get("SERVC_AGENT_CLIENT_CODE") != null)
          hashMap.put("SERVC_AGENT_CLIENT_CODE",
              HashPolOwner.get("SERVC_AGENT_CLIENT_CODE"));
        if (HashPolOwner.get("APP_SIGN_DATE") != null)
          hashMap.put("APP_SIGN_DATE",
              HashPolOwner.get("APP_SIGN_DATE"));
        if (HashPolOwner.get("OWNER_TITLE") != null)
          hashMap.put("OWNER_TITLE", HashPolOwner.get("OWNER_TITLE"));
        if (HashPolOwner.get("CLIENT_FIRSTNAME") != null)
          hashMap.put("CLIENT_FIRSTNAME",
              HashPolOwner.get("CLIENT_FIRSTNAME"));
        if (HashPolOwner.get("CLIENT_LASTNAME") != null)
          hashMap.put("CLIENT_LASTNAME",
              HashPolOwner.get("CLIENT_LASTNAME"));
        if (HashPolOwner.get("CLIENT_MIDDLENAME") != null)
          hashMap.put("CLIENT_MIDDLENAME",
              HashPolOwner.get("CLIENT_MIDDLENAME"));
        if (HashPolOwner.get("ADDRESS_1") != null)
          hashMap.put("ADDRESS_1", HashPolOwner.get("ADDRESS_1"));
        if (HashPolOwner.get("ADDRESS_2") != null)
          hashMap.put("ADDRESS_2", HashPolOwner.get("ADDRESS_2"));
        if (HashPolOwner.get("ADDRESS_3") != null)
          hashMap.put("ADDRESS_3", HashPolOwner.get("ADDRESS_3"));
        if (HashPolOwner.get("ADDRESS_4") != null)
          hashMap.put("ADDRESS_4", HashPolOwner.get("ADDRESS_4"));
        if (HashPolOwner.get("ADDRESS_5") != null)
          hashMap.put("ADDRESS_5", HashPolOwner.get("ADDRESS_5"));
        if (HashPolOwner.get("CITY") != null)
          hashMap.put("CITY", HashPolOwner.get("CITY"));
        if (HashPolOwner.get("PROVINCE_CODE") != null)
          hashMap.put("PROVINCE_CODE",
              HashPolOwner.get("PROVINCE_CODE"));
        if (HashPolOwner.get("CLIENT_BDAY") != null)
          hashMap.put("CLIENT_BDAY", HashPolOwner.get("CLIENT_BDAY"));
        if (HashPolOwner.get("PLAN_ID") != null)
          hashMap.put("PLAN_ID", HashPolOwner.get("PLAN_ID"));
        if (HashPolOwner.get("INS_TYPE_CODE") != null)
          hashMap.put("INS_TYPE_CODE",
              HashPolOwner.get("INS_TYPE_CODE"));
        if (HashPolOwner.get("OWNER_ID") != null)
          hashMap.put("OWNER_ID", HashPolOwner.get("OWNER_ID"));
        if (HashPolOwner.get("BRANCH") != null)
          hashMap.put("BRANCH", HashPolOwner.get("BRANCH"));
        if (HashPolOwner.get("COMPANY_NAME") != null)
          hashMap.put("COMPANY_NAME",
              HashPolOwner.get("COMPANY_NAME"));
        if (HashPolOwner.get("AGENT_CODE") != null)
          hashMap.put("AGENT_CODE", HashPolOwner.get("AGENT_CODE"));
        if (HashPolOwner.get("AGENT_NAME") != null)
          hashMap.put("AGENT_NAME", HashPolOwner.get("AGENT_NAME"));
        if (HashPolOwner.get("SERVICING_AGENT_NAME") != null)
          hashMap.put("SERVICING_AGENT_NAME",
              HashPolOwner.get("SERVICING_AGENT_NAME"));
        if (HashPolOwner.get("ZIPCODE") != null)
          hashMap.put("ZIPCODE", HashPolOwner.get("ZIPCODE"));
        else
          hashMap.put("ZIPCODE", HashPolOwner.get("ZIPCODE_PREVIOUS"));
        if (HashPolOwner.get("COUNTRY_CODE") != null)
          hashMap.put("COUNTRY_CODE",
              HashPolOwner.get("COUNTRY_CODE"));
        if (HashPolOwner.get("POLICY_STATUS_DESC") != null)
          hashMap.put("POLICY_STATUS_DESC",
              HashPolOwner.get("POLICY_STATUS_DESC"));
        if (HashPolOwner.get("CURRENCY_CODE") != null)
          hashMap.put("CURRENCY_CODE",
              HashPolOwner.get("CURRENCY_CODE"));
        if (HashPolOwner.get("SEX") != null)
          hashMap.put("SEX", HashPolOwner.get("SEX"));
        if (HasInsDetails.get("INSURED_NAME") != null)
          hashMap.put("INSURED_NAME",
              HasInsDetails.get("INSURED_NAME"));
        if (HasInsDetails.get("INSURED_CLIENT_NO") != null)
          hashMap.put("INSURED_CLIENT_NO",
              HasInsDetails.get("INSURED_CLIENT_NO"));
        if (HasInsDetails.get("ORIGINAL_FACE_AMOUNT") != null)
          hashMap.put("ORIGINAL_FACE_AMOUNT",
              HasInsDetails.get("ORIGINAL_FACE_AMOUNT"));
        if (HasInsDetails.get("COVERAGE_PLAN_DESC") != null)
          hashMap.put("COVERAGE_PLAN_DESC",
              HasInsDetails.get("COVERAGE_PLAN_DESC"));
      }
    }

    System.out
        .println("//.............getPolicyClientDetails...........//");
    printHashMap(hashMap);
    System.out
        .println("//.............getPolicyClientDetails...........//");
    return hashMap;
  }

  public HashMap getPolicyOwnerDetails(String strPolicyId) {
    HashMap hashMap;
    String strClientNo;
    hashMap = (HashMap) ingBusProcess.BF8000_Append(Authenticate,
        strPolicyId);
    ArrayList planId = null;
    ArrayList ServicingAgent = null;
    ArrayList Agent = null;
    ArrayList Status = null;
    ArrayList Count = null;
    ArrayList Currency = null;
    ArrayList AgentCode = null;
    ArrayList Branch = null;
    ArrayList InsType = null;
    ArrayList BranchCode = null;
    ArrayList AppSignDate = null;
    ArrayList SrvcAgent = null;

    ArrayList Address1 = null;
    ArrayList Address2 = null;
    ArrayList Address3 = null;
    ArrayList Address4 = null;
    ArrayList Address5 = null;

    try {
      if (hashMap.get("PLAN_ID") != null)
        planId = (ArrayList) hashMap.get("PLAN_ID");
      if (hashMap.get("SERVC_AGENT_CLIENT_CODE") != null)
        SrvcAgent = (ArrayList) hashMap.get("SERVC_AGENT_CLIENT_CODE");
      if (hashMap.get("SERVICING_AGENT_NAME") != null)
        ServicingAgent = (ArrayList) hashMap
            .get("SERVICING_AGENT_NAME");
      if (hashMap.get("AGENT_NAME") != null)
        Agent = (ArrayList) hashMap.get("AGENT_NAME");
      if (hashMap.get("POLICY_STATUS_DESC") != null)
        Status = (ArrayList) hashMap.get("POLICY_STATUS_DESC");
      if (hashMap.get("COVERAGE_COUNT") != null)
        Count = (ArrayList) hashMap.get("COVERAGE_COUNT");
      if (hashMap.get("CURRENCY_CODE") != null)
        Currency = (ArrayList) hashMap.get("CURRENCY_CODE");
      if (hashMap.get("AGENT_CODE") != null)
        AgentCode = (ArrayList) hashMap.get("AGENT_CODE");
      if (hashMap.get("BRANCH") != null)
        Branch = (ArrayList) hashMap.get("BRANCH");
      if (hashMap.get("INS_TYPE_CODE") != null)
        InsType = (ArrayList) hashMap.get("INS_TYPE_CODE");
      if (hashMap.get("BRANCH_CODE") != null)
        BranchCode = (ArrayList) hashMap.get("BRANCH_CODE");
      if (hashMap.get("APP_SIGN_DATE") != null)
        AppSignDate = (ArrayList) hashMap.get("APP_SIGN_DATE");

    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      ArrayList al_Client_no = (ArrayList) hashMap.get("CLIENT_NO");
      strClientNo = (String) al_Client_no.get(0);
      hashMap = (HashMap) ingBusProcess.BF1220C_Append(Authenticate,
          strClientNo);
      if (planId != null)
        hashMap.put("PLAN_ID", planId);
      if (ServicingAgent != null)
        hashMap.put("SERVICING_AGENT_NAME", ServicingAgent);
      if (Agent != null)
        hashMap.put("AGENT_NAME", Agent);
      if (Status != null)
        hashMap.put("POLICY_STATUS_DESC", Status);
      if (Count != null)
        hashMap.put("COVERAGE_COUNT", Count);
      if (Currency != null)
        hashMap.put("CURRENCY_CODE", Currency);
      if (AgentCode != null)
        hashMap.put("AGENT_CODE", AgentCode);
      if (Branch != null)
        hashMap.put("BRANCH", Branch);
      if (InsType != null)
        hashMap.put("INS_TYPE_CODE", InsType);
      if (BranchCode != null)
        hashMap.put("BRANCH_CODE", BranchCode);
      if (AppSignDate != null)
        hashMap.put("APP_SIGN_DATE", AppSignDate);
      if (SrvcAgent != null)
        hashMap.put("SERVC_AGENT_CLIENT_CODE", SrvcAgent);

      // Ingenium Mailing Address fix - Andre Ceasar Dacanay - start
      HashMap hs2 = (HashMap) ingBusProcess.InquiryBilling_Append(
          Authenticate, strPolicyId);

      if (hs2.get("ADDRESS_1") != null) {
        Address1 = (ArrayList) hs2.get("ADDRESS_1");
      }
      if (hs2.get("ADDRESS_2") != null) {
        Address2 = (ArrayList) hs2.get("ADDRESS_2");
      }
      if (hs2.get("ADDRESS_3") != null) {
        Address3 = (ArrayList) hs2.get("ADDRESS_3");
      }
      if (hs2.get("ADDRESS_4") != null) {
        Address4 = (ArrayList) hs2.get("ADDRESS_4");
      }
      if (hs2.get("ADDRESS_5") != null) {
        Address5 = (ArrayList) hs2.get("ADDRESS_5");
      }

      if (Address1 != null) {
        hashMap.put("ADDRESS_1", Address1);
      }
      if (Address2 != null) {
        hashMap.put("ADDRESS_2", Address2);
      }
      if (Address3 != null) {
        hashMap.put("ADDRESS_3", Address3);
      }
      if (Address4 != null) {
        hashMap.put("ADDRESS_4", Address4);
      }
      if (Address5 != null) {
        hashMap.put("ADDRESS_5", Address5);
      }

      printHashMap(hashMap);
    } catch (Exception e) {
      return null;
    }
    return hashMap;
  }

  public HashMap getPolicyInsuredDetails(String strPolicyId, String CvgNum) {
    HashMap hashMap = (HashMap) ingBusProcess.BF6925_Append(Authenticate, strPolicyId, CvgNum);
    return hashMap;
  }

  private void printHashMap(HashMap hashMap) {
    try {
      if (hashMap == null)
        return;
      Object strkeys[] = hashMap.keySet().toArray();
      for (int i = 0; i < strkeys.length; i++) {
        ArrayList al_result = (ArrayList) hashMap.get(strkeys[i]);
        System.out.println(strkeys[i] + ": ");
        for (int j = 0; j < al_result.size(); j++)
          System.out.println("\t" + al_result.get(j));
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
