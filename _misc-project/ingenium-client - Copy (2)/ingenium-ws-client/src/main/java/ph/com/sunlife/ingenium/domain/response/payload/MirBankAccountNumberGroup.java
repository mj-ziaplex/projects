package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliBnkAcctNumG")
public class MirBankAccountNumberGroup {

    @XStreamImplicit(itemFieldName = "MirCliBnkAcctNumT")
    private List<String> mirClientBankAccountNumberTypes;

    public List<String> getMirClientBankAccountNumberTypes() {
        return mirClientBankAccountNumberTypes;
    }

    public MirBankAccountNumberGroup setMirClientBankAccountNumberTypes(final List<String> types) {
        this.mirClientBankAccountNumberTypes = types;
        return this;
    }
}
