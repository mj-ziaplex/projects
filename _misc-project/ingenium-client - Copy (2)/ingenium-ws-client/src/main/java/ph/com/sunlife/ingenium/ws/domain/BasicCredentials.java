package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserAuthRequest")
public class BasicCredentials extends Credentials {

  public BasicCredentials() {
    super("", new PlainPassword());
  }

  public BasicCredentials(final String username, final String password) {
    super("", password);
  }

  public BasicCredentials(final String username, final String password, final String algorithm) {
    super("", password, algorithm);
  }

  public BasicCredentials(final String username, final Password password) {
    super(username, password);
  }
}
