package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvTrstCliNmG")
public class MirDvTrstCliNmG {

    @XStreamImplicit(itemFieldName = "MirDvTrstCliNmT")
    private List<String> mirDvTrstCliNmT;

    public List<String> getMirDvTrstCliNmT() {
        return mirDvTrstCliNmT;
    }

    public void setMirDvTrstCliNmT(List<String> mirDvTrstCliNmT) {
        this.mirDvTrstCliNmT = mirDvTrstCliNmT;
    }
}
