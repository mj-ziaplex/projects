package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrLn3TxtG")
public class MirDvClientAddressLine3TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrLn3TxtT")
    private List<String> mirDvClientAddressLine3Texts;

    public List<String> getMirDvClientAddressLine3Texts() {
        return mirDvClientAddressLine3Texts;
    }

    public void setMirDvClientAddressLine3Texts(final List<String> line3Texts) {
        mirDvClientAddressLine3Texts = line3Texts;
    }
}
