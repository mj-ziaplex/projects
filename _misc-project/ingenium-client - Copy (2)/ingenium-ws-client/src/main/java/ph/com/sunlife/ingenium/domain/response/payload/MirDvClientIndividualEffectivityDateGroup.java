package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliIndvEffDtG")
public class MirDvClientIndividualEffectivityDateGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliIndvEffDtT")
    private List<String> mirDvClientIndividualEffectivityDateTypes;

    public List<String> getMirDvClientIndividualEffectivityDateTypes() {
        return mirDvClientIndividualEffectivityDateTypes;
    }

    public void setMirDvClientIndividualEffectivityDateTypes(List<String> effectivityDates) {
        mirDvClientIndividualEffectivityDateTypes = effectivityDates;
    }
}
