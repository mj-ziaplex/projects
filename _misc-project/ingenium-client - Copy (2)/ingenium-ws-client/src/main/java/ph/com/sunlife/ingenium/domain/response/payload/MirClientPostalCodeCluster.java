package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliPstlCdG")
public class MirClientPostalCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliPstlCdT")
    private List<Short> clientPostalCodes;

    public List<Short> getClientPostalCodes() {
        return clientPostalCodes;
    }

    public void setClientPostalCodes(final List<Short> postalCodes) {
        clientPostalCodes = postalCodes;
    }
}
