package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCdCvgSexCdG")
public class MirCdCvgSexCdG {

	@XStreamImplicit(itemFieldName = "MirCdCvgSexCdT")
	protected List<String> mirCdCvgSexCdT;

	public List<String> getMirCdCvgSexCdT() {
		if (mirCdCvgSexCdT == null) {
			mirCdCvgSexCdT = new ArrayList<String>();
		}
		return this.mirCdCvgSexCdT;
	}

}
