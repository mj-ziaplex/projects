package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("OLifE")
public abstract class Container<Box extends Container> {

  @XStreamAlias("ReturnCds")
  private ReturnCodes returnCodes;

  public ReturnCodes getReturnCodes() {
    return returnCodes;
  }

  public void setReturnCodes(ReturnCodes returnCodes) {
    this.returnCodes = returnCodes;
  }
}
