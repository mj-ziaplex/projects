package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrAddr3TxtG")
public class MirDvEmployerAddress3TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrAddr3TxtT")
    private List<String> mirDvEmployerAddress3Texts;

    public List<String> getMirDvEmployerAddress3Texts() {
        return mirDvEmployerAddress3Texts;
    }

    public void setMirDvEmployerAddress3Texts(final List<String> address3Texts) {
        mirDvEmployerAddress3Texts = address3Texts;
    }
}
