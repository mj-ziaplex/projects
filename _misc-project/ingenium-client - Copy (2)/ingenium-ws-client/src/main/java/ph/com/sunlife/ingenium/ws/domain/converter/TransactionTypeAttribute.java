package ph.com.sunlife.ingenium.ws.domain.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import ph.com.sunlife.ingenium.ws.domain.TransactionType;

public class TransactionTypeAttribute implements Converter {

  @Override
  public void marshal(final Object object,
                      final HierarchicalStreamWriter writer,
                      final MarshallingContext context) {
    TransactionType transactionType = (TransactionType) object;
    writer.addAttribute("tc", transactionType.getValue());
    writer.setValue(transactionType.getValue());
  }

  @Override
  public TransactionType unmarshal(final HierarchicalStreamReader reader,
                                   final UnmarshallingContext context) {
    return new TransactionType(reader.getValue());
  }

  @Override
  public boolean canConvert(final Class klazz) {
    return TransactionType.class.equals(klazz);
  }
}
