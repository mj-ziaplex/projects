package ph.com.sunlife.wms.ingenium.domain;

public class Requirement {

  private String index;
  private String requirementCode;

  public String getIndex() {
    return index;
  }

  public Requirement setIndex(final String index) {
    this.index = index;
    return this;
  }

  public String getRequirementCode() {
    return requirementCode;
  }

  public Requirement setRequirementCode(final String requirementCode) {
    this.requirementCode = requirementCode;
    return this;
  }
}
