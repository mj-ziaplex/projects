package ph.com.sunlife.wms.ingenium.configuration;

import com.sunlife.ascp.exception.InvalidConfigurationException;
import com.sunlife.ascp.exception.MissingConfigurationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ph.com.sunlife.wms.ingenium.authentication.IngeniumPasswordBuilder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class IngeniumClientConfigurator {

    private static final String INVALID_WSDL_URL_MESSAGE =
            "The Ingenium web service configuration for its WSDL URL is improperly formatted.";

    private final IngeniumPropertiesConfigurationReader propsConfig;

    public IngeniumClientConfigurator() throws MissingConfigurationException {
        propsConfig = new IngeniumPropertiesConfigurationReader();
    }

    @Bean
    public File xmlLogLocation() {
        return new File(propsConfig.getRequestResponseLogLocation());
    }

    @Bean
    public URL wsdlURL() throws InvalidConfigurationException {
        try {
            return new URL(propsConfig.getServerHost());
        } catch (MalformedURLException murle) {
            throw new InvalidConfigurationException(INVALID_WSDL_URL_MESSAGE, murle);
        }
    }

    @Bean
    public String username() {
        return propsConfig.getUsername();
    }

    @Bean
    public String password() {
        return new IngeniumPasswordBuilder(propsConfig.getUsername(),
                                           propsConfig.getPasswordSalt())
                .setPasswordPrefix(propsConfig.getPasswordPrefix())
                .setDisplacement(propsConfig.getPasswordOffset())
                .build();
    }

    @Bean
    public DateFormat dateFormatter() {
        return new SimpleDateFormat(propsConfig.getDateFormat());
    }

    @Bean
    public DateFormat timeFormatter() {
        return new SimpleDateFormat(propsConfig.getTimeFormat());
    }

    @Bean
    public String defaulTransactionReferenceGUID() {
        return propsConfig.getDefaultTransactionReferenceGUID();
    }

    // delete these
    @Bean
    public String sample() {
        return "";
    }
}
