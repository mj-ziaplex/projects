package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgFeDurG")
public class MirRtCvgFeDurG {

	@XStreamImplicit(itemFieldName = "MirRtCvgFeDurT")
	protected List<String> mirRtCvgFeDurT;

	public List<String> getMirRtCvgFeDurT() {
		if (mirRtCvgFeDurT == null) {
			mirRtCvgFeDurT = new ArrayList<String>();
		}
		return this.mirRtCvgFeDurT;
	}

}
