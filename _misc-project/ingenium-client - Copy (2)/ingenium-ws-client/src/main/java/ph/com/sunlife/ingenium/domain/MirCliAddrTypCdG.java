package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrTypCdG")
public class MirCliAddrTypCdG {

    @XStreamImplicit(itemFieldName = "MirCliAddrTypCdT")
    private List<String> mirCliAddrTypCdT;

    public List<String> getMirCliAddrTypCdT() {
        return mirCliAddrTypCdT;
    }

    public void setMirCliAddrTypCdT(List<String> mirCliAddrTypCdT) {
        this.mirCliAddrTypCdT = mirCliAddrTypCdT;
    }
}
