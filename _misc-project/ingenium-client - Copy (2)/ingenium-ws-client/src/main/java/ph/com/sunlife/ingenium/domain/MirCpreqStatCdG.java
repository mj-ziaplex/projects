package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqStatCdG")
public class MirCpreqStatCdG {

	@XStreamImplicit(itemFieldName = "MirCpreqStatCdT")
	protected List<String> mirCpreqStatCdT;

	public List<String> getMirCpreqStatCdT() {
		if (mirCpreqStatCdT == null) {
			mirCpreqStatCdT = new ArrayList<String>();
		}
		return this.mirCpreqStatCdT;
	}

}
