package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyRelinsG")
public class MirBiBnfyRelinsG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyRelinsT")
	protected List<String> mirBiBnfyRelinsT;

	public List<String> getMirBiBnfyRelinsT() {
		if (mirBiBnfyRelinsT == null) {
			mirBiBnfyRelinsT = new ArrayList<String>();
		}
		return this.mirBiBnfyRelinsT;
	}

}
