package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAsignSubCdG")
public class MirDvAsignSubCdG {

    @XStreamImplicit(itemFieldName = "MirDvAsignSubCdT")
    private List<String> mirDvAsignSubCdT;

    public List<String> getMirDvAsignSubCdT() {
        return mirDvAsignSubCdT;
    }

    public void setMirDvAsignSubCdT(List<String> mirDvAsignSubCdT) {
        this.mirDvAsignSubCdT = mirDvAsignSubCdT;
    }
}
