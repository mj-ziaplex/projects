package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvPrevAddrIndG")
public class MirDvPreviousAddressIndGroup {

    @XStreamImplicit(itemFieldName = "MirDvPrevAddrIndT")
    private List<String> mirDvPreviousAddressIndTexts;

    public List<String> getMirDvPreviousAddressIndTexts() {
        return mirDvPreviousAddressIndTexts;
    }

    public void setMirDvPreviousAddressIndTexts(final List<String> addressInds) {
        mirDvPreviousAddressIndTexts = addressInds;
    }
}
