package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAltAddrCdG")
public class MirDvClientAlternateAddressCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAltAddrCdT")
    private List<String> mirDvClientAlternateAddressCodeTexts;

    public List<String> getMirDvClientAlternateAddressCodeTexts() {
        return mirDvClientAlternateAddressCodeTexts;
    }

    public void setMirDvClientAlternateAddressCodeTexts(final List<String> addressCodes) {
        mirDvClientAlternateAddressCodeTexts = addressCodes;
    }
}
