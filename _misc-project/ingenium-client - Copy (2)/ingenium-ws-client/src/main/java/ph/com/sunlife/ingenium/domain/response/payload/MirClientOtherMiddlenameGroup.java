package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliOtherMidNmG")
public class MirClientOtherMiddlenameGroup {

    @XStreamImplicit(itemFieldName = "MirCliOtherMidNmT")
    private List<String> mirClientOtherMiddlenameTypes;

    public List<String> getMirClientOtherMiddlenameTypes() {
        return mirClientOtherMiddlenameTypes;
    }

    public void setMirClientOtherMiddlenameTypes(List<String> mirClientOtherMiddlenameTypes) {
        this.mirClientOtherMiddlenameTypes = mirClientOtherMiddlenameTypes;
    }
}
