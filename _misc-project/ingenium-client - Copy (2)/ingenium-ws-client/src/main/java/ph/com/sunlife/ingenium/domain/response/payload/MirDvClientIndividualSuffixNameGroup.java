package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliIndvSfxNmG")
public class MirDvClientIndividualSuffixNameGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliIndvSfxNmT")
    private List<String> mirDvClientIndividualSuffixNameTypes;

    public List<String> getMirDvClientIndividualSuffixNameTypes() {
        return mirDvClientIndividualSuffixNameTypes;
    }

    public void setMirDvClientIndividualSuffixNameTypes(final List<String> suffixes) {
        this.mirDvClientIndividualSuffixNameTypes = suffixes;
    }
}
