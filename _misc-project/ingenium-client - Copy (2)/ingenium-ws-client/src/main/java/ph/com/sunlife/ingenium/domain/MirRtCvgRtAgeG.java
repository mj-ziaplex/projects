package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirRtCvgRtAgeG")
public class MirRtCvgRtAgeG {

	@XStreamImplicit(itemFieldName = "MirRtCvgRtAgeT")
	private List<String> mirRtCvgRtAgeT;

	public List<String> getMirRtCvgRtAgeT() {
		return mirRtCvgRtAgeT;
	}

	public void setMirRtCvgRtAgeT(List<String> mirRtCvgRtAgeT) {
		this.mirRtCvgRtAgeT = mirRtCvgRtAgeT;
	}
}
