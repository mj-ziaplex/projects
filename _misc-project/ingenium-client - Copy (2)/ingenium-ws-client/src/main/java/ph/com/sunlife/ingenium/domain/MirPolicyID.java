package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirPolId")
public class MirPolicyID {

    @XStreamAlias("MirDvEffDt")
    private String mirDvEffectivityDate;
    @XStreamAlias("MirPolIdBase")
    private String mirPolicyIDBase;
    @XStreamAlias("MirPolIdSfx")
    private String mirPolIdSfx;

    public MirPolicyID() { }

    public MirPolicyID(final String policyId) {
        mirPolicyIDBase = policyId.substring(0, 9);
        if (policyId.length() > 9) {
            mirPolIdSfx = policyId.substring(9, policyId.length());
        } else {
            mirPolIdSfx = "";
        }
    }

    public MirPolicyID(final String policyIdBase, final String policyIdSuffix) {
        mirPolicyIDBase = policyIdBase;
        mirPolIdSfx = policyIdSuffix;
    }

    public String getMirDvEffectivityDate() {
        return mirDvEffectivityDate;
    }

    public MirPolicyID setMirDvEffectivityDate(String mirDvEffectivityDate) {
        this.mirDvEffectivityDate = mirDvEffectivityDate;
        return this;
    }

    public String getMirPolicyIDBase() {
        return mirPolicyIDBase;
    }

    public MirPolicyID setMirPolicyIDBase(String mirPolicyIDBase) {
        this.mirPolicyIDBase = mirPolicyIDBase;
        return this;
    }

    public String getMirPolIdSfx() {
        return mirPolIdSfx;
    }

    public MirPolicyID setMirPolIdSfx(String mirPolIdSfx) {
        this.mirPolIdSfx = mirPolIdSfx;
        return  this;
    }
}
