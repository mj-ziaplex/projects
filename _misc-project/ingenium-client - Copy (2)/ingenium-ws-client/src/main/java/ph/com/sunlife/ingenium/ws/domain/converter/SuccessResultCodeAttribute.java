package ph.com.sunlife.ingenium.ws.domain.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.lang3.StringUtils;
import ph.com.sunlife.ingenium.ws.domain.ResultCode;

public class SuccessResultCodeAttribute implements Converter {

    @Override
    public void marshal(Object xmlObject,
                        HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        ResultCode resultCode = (ResultCode) xmlObject;
        writer.addAttribute("tc", String.valueOf(resultCode.getCode()));
        if (StringUtils.isNotBlank(resultCode.getValue())) {
            writer.setValue(resultCode.getValue());
        }
    }

    @Override
    public ResultCode unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        ResultCode resultCode = new ResultCode();
        resultCode.setCode(Byte.valueOf(reader.getAttribute("tc")));
        if (StringUtils.isNotBlank(reader.getValue())) {
            resultCode.setValue(reader.getValue());
        }
        return resultCode;
    }

    @Override
    public boolean canConvert(Class klazz) {
        return ResultCode.class.equals(klazz);
    }
}
