package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyTypG")
public class MirBiBnfyTypG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyTypT")
	protected List<String> mirBiBnfyTypT;

	public List<String> getMirBiBnfyTypT() {
		if (mirBiBnfyTypT == null) {
			mirBiBnfyTypT = new ArrayList<String>();
		}
		return this.mirBiBnfyTypT;
	}

}
