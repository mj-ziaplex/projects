package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirUserMsgTxtG")
public class MirUserMessageTextGroup {

    @XStreamImplicit(itemFieldName = "MirUserMsgTxtT")
    private List<String> mirUserMessageTexts;

    public List<String> getMirUserMessageTexts() {
        return mirUserMessageTexts;
    }

    public void setMirUserMessageTexts(final List<String> userMessages) {
        this.mirUserMessageTexts = userMessages;
    }
}
