package ph.com.sunlife.wms.ingenium.domain;

import java.util.Date;

public class Client {

    private String id;
    private String givenName;
    private String middlename;
    private String familyName;
    private Date birthdate;
    private String otherLegalGivenName;
    private String otherLegalMiddleName;
    private String otherLegalFamilyName;

    public String getId() {
        return id;
    }

    public Client setId(String id) {
        this.id = id;
        return this;
    }

    public String getGivenName() {
        return givenName;
    }

    public Client setGivenName(String givenName) {
        this.givenName = givenName;
        return this;
    }

    public String getMiddlename() {
        return middlename;
    }

    public Client setMiddlename(String middlename) {
        this.middlename = middlename;
        return this;
    }

    public String getFamilyName() {
        return familyName;
    }

    public Client setFamilyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public Client setBirthdate(final Date birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public String getOtherLegalGivenName() {
        return otherLegalGivenName;
    }

    public void setOtherLegalGivenName(final String name) {
        this.otherLegalGivenName = name;
    }

    public String getOtherLegalMiddleName() {
        return otherLegalMiddleName;
    }

    public void setOtherLegalMiddleName(final String name) {
        this.otherLegalMiddleName = name;
    }

    public String getOtherLegalFamilyName() {
        return otherLegalFamilyName;
    }

    public void setOtherLegalFamilyName(final String name) {
        this.otherLegalFamilyName = name;
    }
}
