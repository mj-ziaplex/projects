package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliEmplYrDurG")
public class MirClientEmployeeYearDurationGroup {

    @XStreamImplicit(itemFieldName = "MirCliEmplYrDurT")
    private List<Byte> mirClientEmployeeYearDurationTexts;

    public List<Byte> getMirClientEmployeeYearDurationTexts() {
        return mirClientEmployeeYearDurationTexts;
    }

    public void setMirClientEmployeeYearDurationTexts(final List<Byte> yearDurations) {
        mirClientEmployeeYearDurationTexts = yearDurations;
    }
}
