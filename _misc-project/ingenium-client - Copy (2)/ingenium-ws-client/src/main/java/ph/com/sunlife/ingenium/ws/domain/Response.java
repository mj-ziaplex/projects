package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLife")
public class Response extends RequestResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private ClientInquiryTransactionResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public ClientInquiryTransactionResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(ClientInquiryTransactionResponse transaction) {
        this.transaction = transaction;
    }
}
