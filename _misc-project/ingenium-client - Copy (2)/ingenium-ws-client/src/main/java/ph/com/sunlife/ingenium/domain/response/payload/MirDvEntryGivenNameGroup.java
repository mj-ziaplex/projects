package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEntrGivNmG")
public class MirDvEntryGivenNameGroup {

    @XStreamImplicit(itemFieldName = "MirDvEntrGivNmT")
    private List<String> mirDvEntryGivenNameTypes;

    public List<String> getMirDvEntryGivenNameTypes() {
        return mirDvEntryGivenNameTypes;
    }

    public void setMirDvEntryGivenNameTypes(final List<String> entryGivenNames) {
        this.mirDvEntryGivenNameTypes = entryGivenNames;
    }
}
