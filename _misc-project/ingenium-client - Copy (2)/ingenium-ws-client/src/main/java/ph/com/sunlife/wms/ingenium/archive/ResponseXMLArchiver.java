package ph.com.sunlife.wms.ingenium.archive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseXMLArchiver extends XMLArchiver {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ResponseXMLArchiver.class);

    public ResponseXMLArchiver(final String name) {
        super(name + "_RESPONSE");

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Response XML Archiver initiated for {}",
                         name);
        }
    }
}
