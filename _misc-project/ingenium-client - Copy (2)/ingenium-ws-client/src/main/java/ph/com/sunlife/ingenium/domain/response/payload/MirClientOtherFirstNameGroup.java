package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliOtherFirstNmG")
public class MirClientOtherFirstNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliOtherFirstNmT")
    private List<String> mirClientOtherFirstNameTypes;

    public List<String> getMirClientOtherFirstNameTypes() {
        return mirClientOtherFirstNameTypes;
    }

    public void setMirClientOtherFirstNameTypes(final List<String> types) {
        this.mirClientOtherFirstNameTypes = types;
    }
}
