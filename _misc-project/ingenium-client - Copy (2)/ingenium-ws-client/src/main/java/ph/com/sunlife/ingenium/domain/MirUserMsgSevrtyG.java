package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirUserMsgSevrtyG")
public class MirUserMsgSevrtyG {

	@XStreamImplicit(itemFieldName = "MirUserMsgSevrtyT")
	protected List<Byte> mirUserMsgSevrtyT;

	public List<Byte> getMirUserMsgSevrtyT() {
		if (mirUserMsgSevrtyT == null) {
			mirUserMsgSevrtyT = new ArrayList<Byte>();
		}
		return this.mirUserMsgSevrtyT;
	}

}
