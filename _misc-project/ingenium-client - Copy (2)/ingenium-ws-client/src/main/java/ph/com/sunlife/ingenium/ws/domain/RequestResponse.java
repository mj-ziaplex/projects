package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public abstract class RequestResponse {

  @XStreamAsAttribute
  @XStreamAlias("xmlns")
  protected final String XMLNS = "http://ACORD.org/Standards/Life/2";

  @XStreamAsAttribute
  @XStreamAlias("xmlns:xsi")
  protected final String XSI = "http://www.w3.org/2001/XMLSchema-instance";

  @XStreamAsAttribute
  @XStreamAlias("xmlns:xsd")
  protected final String XSD ="http://www.w3.org/2001/XMLSchema";

  @XStreamAsAttribute
  @XStreamAlias("xsi:schemaLocation")
  protected final String SCHEMA_LOCATION = "http://ACORD.org/Standards/Life/2 ../xsd/TXLife2.9.00.xsd";
}
