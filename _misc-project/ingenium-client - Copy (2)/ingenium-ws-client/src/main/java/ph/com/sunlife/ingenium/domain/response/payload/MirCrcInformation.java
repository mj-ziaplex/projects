package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCrcInfo")
public class MirCrcInformation {

    @XStreamAlias("MirCliCrcNumG")
    private MirClientCrcNumGroup mirClientCrcNumGroup;
    @XStreamAlias("MirCrcIdG")
    private MirCrcIdGroup mirCrcIdGroup;
    @XStreamAlias("MirCrcTypCdG")
    private MirCrcTypeCodeGroup mirCrcTypeCodeGroup;
    @XStreamAlias("MirCrcHldrNmG")
    private MirCrcHolderNameGroup mirCrcHolderNameGroup;
    @XStreamAlias("MirCrcXpryYrG")
    private MirCrcExpiryYearGroup mirCrcExpiryYearGroup;
    @XStreamAlias("MirCrcXpryMoG")
    private MirCrcExpiryMoGroup mirCrcExpiryMoGroup;
    @XStreamAlias("MirCrcBnkIdG")
    private MirCrcBankIdGroup mirCrcBankIdGroup;
    @XStreamAlias("MirCrcCvv2NoG")
    private MirCrcCvv2NumberGroup mirCrcCvv2NumberGroup;

    public MirClientCrcNumGroup getMirClientCrcNumGroup() {
        return mirClientCrcNumGroup;
    }

    public void setMirClientCrcNumGroup(MirClientCrcNumGroup mirClientCrcNumGroup) {
        this.mirClientCrcNumGroup = mirClientCrcNumGroup;
    }

    public MirCrcIdGroup getMirCrcIdGroup() {
        return mirCrcIdGroup;
    }

    public void setMirCrcIdGroup(MirCrcIdGroup mirCrcIdGroup) {
        this.mirCrcIdGroup = mirCrcIdGroup;
    }

    public MirCrcTypeCodeGroup getMirCrcTypeCodeGroup() {
        return mirCrcTypeCodeGroup;
    }

    public void setMirCrcTypeCodeGroup(MirCrcTypeCodeGroup mirCrcTypeCodeGroup) {
        this.mirCrcTypeCodeGroup = mirCrcTypeCodeGroup;
    }

    public MirCrcHolderNameGroup getMirCrcHolderNameGroup() {
        return mirCrcHolderNameGroup;
    }

    public void setMirCrcHolderNameGroup(MirCrcHolderNameGroup mirCrcHolderNameGroup) {
        this.mirCrcHolderNameGroup = mirCrcHolderNameGroup;
    }

    public MirCrcExpiryYearGroup getMirCrcExpiryYearGroup() {
        return mirCrcExpiryYearGroup;
    }

    public void setMirCrcExpiryYearGroup(MirCrcExpiryYearGroup mirCrcExpiryYearGroup) {
        this.mirCrcExpiryYearGroup = mirCrcExpiryYearGroup;
    }

    public MirCrcExpiryMoGroup getMirCrcExpiryMoGroup() {
        return mirCrcExpiryMoGroup;
    }

    public void setMirCrcExpiryMoGroup(MirCrcExpiryMoGroup mirCrcExpiryMoGroup) {
        this.mirCrcExpiryMoGroup = mirCrcExpiryMoGroup;
    }

    public MirCrcBankIdGroup getMirCrcBankIdGroup() {
        return mirCrcBankIdGroup;
    }

    public void setMirCrcBankIdGroup(MirCrcBankIdGroup mirCrcBankIdGroup) {
        this.mirCrcBankIdGroup = mirCrcBankIdGroup;
    }

    public MirCrcCvv2NumberGroup getMirCrcCvv2NumberGroup() {
        return mirCrcCvv2NumberGroup;
    }

    public void setMirCrcCvv2NumberGroup(MirCrcCvv2NumberGroup mirCrcCvv2NumberGroup) {
        this.mirCrcCvv2NumberGroup = mirCrcCvv2NumberGroup;
    }
}
