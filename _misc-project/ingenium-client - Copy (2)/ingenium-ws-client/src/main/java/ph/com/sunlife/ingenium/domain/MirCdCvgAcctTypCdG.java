package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCdCvgAcctTypCdG")
public class MirCdCvgAcctTypCdG {

	@XStreamImplicit(itemFieldName = "MirCdCvgNumT")
	private List<String> mirCdCvgNumT;

	public List<String> getMirCdCvgNumT() {
		return mirCdCvgNumT;
	}

	public void setMirCdCvgNumT(List<String> mirCdCvgNumT) {
		this.mirCdCvgNumT = mirCdCvgNumT;
	}
}
