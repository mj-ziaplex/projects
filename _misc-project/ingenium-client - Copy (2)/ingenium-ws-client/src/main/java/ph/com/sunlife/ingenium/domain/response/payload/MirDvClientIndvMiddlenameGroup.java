package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliIndvMidNmG")
public class MirDvClientIndvMiddlenameGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliIndvMidNmT")
    private List<String> mirDvClientIndividualMiddlenames;

    public List<String> getMirDvClientIndividualMiddlenames() {
        return mirDvClientIndividualMiddlenames;
    }

    public void setMirDvClientIndividualMiddlenames(final List<String> middlenames) {
        this.mirDvClientIndividualMiddlenames = middlenames;
    }
}
