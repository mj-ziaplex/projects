package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.MirPolicyID;
import ph.com.sunlife.ingenium.ws.domain.PolicyData;
import ph.com.sunlife.ingenium.ws.domain.PolicyInquiryContainer;
import ph.com.sunlife.ingenium.ws.domain.Transaction;

public class PolicyInquiryRequestBuilder extends IngeniumRequestBuilder {

    private String policyId;

    public PolicyInquiryRequestBuilder(final String policyId) {
        this.policyId = policyId;
    }

    public String getPoliycID() {
        return policyId;
    }

    @Override
    protected Transaction getTransaction() {
        PolicyInquiryContainer box = new PolicyInquiryContainer()
                .setPolicyData(new PolicyData()
                        .setPolicyID(new MirPolicyID(policyId))
                        .setSourceSystem("WMS"));
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("PolicyInquiry");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(box);
        return transaction;
    }
}
