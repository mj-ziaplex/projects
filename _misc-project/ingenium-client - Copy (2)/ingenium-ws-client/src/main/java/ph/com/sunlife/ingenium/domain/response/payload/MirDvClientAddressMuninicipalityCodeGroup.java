package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrMunCdG")
public class MirDvClientAddressMuninicipalityCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrMunCdT")
    private List<String> mirDvClientAddressMunicipalityCodeTexts;

    public List<String> getMirDvClientAddressMunicipalityCodeTexts() {
        return mirDvClientAddressMunicipalityCodeTexts;
    }

    public void setMirDvClientAddressMunicipalityCodeTexts(final List<String> unicipalityCodes) {
        mirDvClientAddressMunicipalityCodeTexts = unicipalityCodes;
    }
}
