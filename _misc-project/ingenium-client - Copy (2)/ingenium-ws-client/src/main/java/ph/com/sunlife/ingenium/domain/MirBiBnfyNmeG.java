package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirBiBnfyNmeG")
public class MirBiBnfyNmeG {

	@XStreamImplicit(itemFieldName = "MirBiBnfyNmeT")
	protected List<String> mirBiBnfyNmeT;

	public List<String> getMirBiBnfyNmeT() {
		if (mirBiBnfyNmeT == null) {
			mirBiBnfyNmeT = new ArrayList<String>();
		}
		return this.mirBiBnfyNmeT;
	}

}
