package ph.com.sunlife.old.ingenium;

import org.xml.sax.helpers.DefaultHandler;

import java.util.ResourceBundle;

public class IngeniumHandler extends DefaultHandler {

  HashMap hm = new HashMap();

  String strHead = "";

  ArrayList alValuesFound = new ArrayList();

  StringBuffer sb;

  boolean bKeyFound;

  String strKey = "";

  IngeniumConstants ingConstants = new IngeniumConstants();

  boolean ResultCode = false;

  /** Creates a new instance of IngeniumHandler */
  public IngeniumHandler() {
    super();
  }

  public void startDocument() {
    // System.out.println("Start document");
  }

  public HashMap getHashmap() {

    return hm;
  }

  public void notationDecl(String name, String publicId, String systemId) {
    System.out.println("notationDecl " + name + " , " + publicId + " , "
        + systemId);
  }

  public void unparsedEntityDecl(String name, String publicId,
                                 String systemId, String notationName) {
    System.out.println("unparsedEntityDecl " + name + " , " + publicId
        + " , " + systemId);

  }

  public void endDocument() {
    // System.out.println("End document");
  }

  private void outLog(String methodName, String message, String value) {
    ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.Ingenium");
    String write = rb.getString("XMLWrite");
    if(write.equalsIgnoreCase("true")){
      System.out.println("[IngeniumHandler]["+methodName+"]["+message+"]["+value+"]");
    }
  }

  public String translate(String strCode) {
    outLog("translate","strCode",strCode);
    if (strCode.equals(ingConstants.CITY_PREVIOUS)) {
      return "CITY_PREVIOUS";
    } else if (strCode.equals(ingConstants.ADDRESS_3_PREVIOUS)) {
      return "ADDRESS_3_PREVIOUS";
    } else if (strCode.equals(ingConstants.INSURED_CLIENT_NO)) {
      return "INSURED_CLIENT_NO";
    } else if (strCode.equals(ingConstants.COUNTRY_CODE_PREVIOUS)) {
      return "COUNTRY_CODE_PREVIOUS";
    } else if (strCode.equals(ingConstants.ADDRESS_2)) {
      return "ADDRESS_2";
    } else if (strCode.equals(ingConstants.ADDRESS_4)) {
      return "ADDRESS_4";
    } else if (strCode.equals(ingConstants.ADDRESS_5)) {
      return "ADDRESS_5";
    } else if (strCode.equals(ingConstants.ADDRESS_6)) {
      return "ADDRESS_6";
    } else if (strCode.equals(ingConstants.ADDRESS_3)) {
      return "ADDRESS_3";
    } else if (strCode.equals(ingConstants.ADDRESS_1)) {
      return "ADDRESS_1";
    } else if (strCode.equals(ingConstants.PROVINCE_CODE)) {
      return "PROVINCE_CODE";
    } else if (strCode.equals(ingConstants.PROVINCE_PREVIOUS)) {
      return "PROVINCE_PREVIOUS";
    } else if (strCode.equals(ingConstants.ZIPCODE_PREVIOUS)) {
      return "ZIPCODE_PREVIOUS";
    } else if (strCode.equals(ingConstants.COUNTRY_CODE)) {
      return "COUNTRY_CODE";
    } else if (strCode.equals(ingConstants.CITY)) {
      return "CITY";
    } else if (strCode.equals(ingConstants.ZIPCODE)) {
      return "ZIPCODE";
    } else if (strCode.equals(ingConstants.CLIENT_FIRSTNAME)) {
      return "CLIENT_FIRSTNAME";
    } else if (strCode.equals(ingConstants.CLIENT_MIDDLENAME)) {
      return "CLIENT_MIDDLENAME";
    } else if (strCode.equals(ingConstants.CLIENT_LASTNAME)) {
      return "CLIENT_LASTNAME";
    } else if (strCode.equals(ingConstants.PLAN_ID)) {
      return "PLAN_ID";
    } else if (strCode.equals(ingConstants.ORIGINAL_FACE_AMOUNT)) {
      return "ORIGINAL_FACE_AMOUNT";
    } else if (strCode.equals(ingConstants.CVG_FACE_AMOUNT)) {
      return "CVG_FACE_AMOUNT";
    } else if (strCode.equals(ingConstants.REFUND_AMOUNT)) {
      return "REFUND_AMOUNT";
    } else if (strCode.equals(ingConstants.REQUIREMENT_ID)) {
      return "REQUIREMENT_ID";
    } else if (strCode.equals(ingConstants.INDEX_NUMBER)) {
      return "INDEX_NUMBER";
    } else if (strCode.equals(ingConstants.INDEX_NUMBERS)) {
      return "INDEX_NUMBERS";
    } else if (strCode.equals(ingConstants.SEQUENCE_NUMBER)) {
      return "SEQUENCE_NUMBER";
    } else if (strCode.equals(ingConstants.SEQUENCE_NUMBERS)) {
      return "SEQUENCE_NUMBERS";
    } else if (strCode.equals(ingConstants.OWNER_TITLE)) {
      return "OWNER_TITLE";
    } else if (strCode.equals(ingConstants.REQUIREMENT_CODE)) {
      return "REQUIREMENT_CODE";
    } else if (strCode.equals(ingConstants.USER_MESSAGE)) {
      return "USER_MESSAGE";
    } else if (strCode.equals(ingConstants.RETURN_CODE)) {
      return "RETURN_CODE";
    } else if (strCode.equals(ingConstants.INSURED_NAME)) {
      return "INSURED_NAME";
    } else if (strCode.equals(ingConstants.MINOR_CI_CLAIM_STATUS)) {
      return "MINOR_CI_CLAIM_STATUS";
    } else if (strCode.equals(ingConstants.MINOR_CI_CLAIM_STATUS_DESC)) {
      return "MINOR_CI_CLAIM_STATUS_DESC";
    } else if (strCode.equals(ingConstants.FACE_AMOUNT)) {
      return "FACE_AMOUNT";
    } else if (strCode.equals(ingConstants.PLAN_ID_T)) {
      return "PLAN_ID";
    } else if (strCode.equals(ingConstants.COVERAGE_NUMBER)) {
      return "COVERAGE_NUMBER";
    } else if (strCode.equals(ingConstants.BENEFICIARY_NAME)) {
      return "BENEFICIARY_NAME";
    } else if (strCode.equals(ingConstants.COVERAGE_NUMBER_T)) {
      return "COVERAGE_NUMBER";
    } else if (strCode.equals(ingConstants.CLIENT_NO)) {
      return "CLIENT_NO";
    } else if (strCode.equals(ingConstants.STAT_CODE)) {
      return "STAT_CODE";
    } else if (strCode.equals(ingConstants.STAT_CODES)) {
      return "STAT_CODES";
    } else if (strCode.equals(ingConstants.SEX)) {
      return "SEX";
    } else if (strCode.equals(ingConstants.CLIENT_BDAY)) {
      return "CLIENT_BDAY";
    } else if (strCode.equals(ingConstants.SERVICING_AGENT_NAME)) {
      return "SERVICING_AGENT_NAME";
    } else if (strCode.equals(ingConstants.AGENT_NAME)) {
      return "AGENT_NAME";
    } else if (strCode.equals(ingConstants.COVERAGE_COUNT)) {
      return "COVERAGE_COUNT";
    } else if (strCode.equals(ingConstants.BENEFIT_CODE)) {
      return "BENEFIT_CODE";
    } else if (strCode.equals(ingConstants.POLICY_STATUS_DESC)) {
      return "POLICY_STATUS_DESC";
    } else if (strCode.equals(ingConstants.COVERAGE_PLAN_DESC)) {
      return "COVERAGE_PLAN_DESC";
    } else if (strCode.equals(ingConstants.CURRENCY_CODE)) {
      return "CURRENCY_CODE";
    } else if (strCode.equals(ingConstants.PREMIUM_SUSPENSE)) {
      return "PREMIUM_SUSPENSE";
    } else if (strCode.equals(ingConstants.MODE_PREMIUM)) {
      return "MODE_PREMIUM";
    } else if (strCode.equals(ingConstants.MISC_SUSPENSE_AMOUNT)) {
      return "MISC_SUSPENSE_AMOUNT";
    } else if (strCode.equals(ingConstants.AGENT_CODE)) {
      return "AGENT_CODE";
    } else if (strCode.equals(ingConstants.BRANCH)) {
      return "BRANCH";
    } else if (strCode.equals(ingConstants.COMPANY_NAME)) {
      return "COMPANY_NAME";
    } else if (strCode.equals(ingConstants.OWNER_ID)) {
      return "OWNER_ID";
    } else if (strCode.equals(ingConstants.INSURED_ID)) {
      return "INSURED_ID";
    } else if (strCode.equals(ingConstants.INS_TYPE_CODE)) {
      return "INS_TYPE_CODE";
    } else if (strCode.equals(ingConstants.CLEAR_CASE_RESP)) {
      return "CLEAR_CASE_RESP";
    } else if (strCode.equals(ingConstants.CLIENT_INDV_SURNAME)) {
      return "CLIENT_INDV_SURNAME";
    } else if (strCode.equals(ingConstants.MESSAGE_TEXT)) {
      return "MESSAGE_TEXT";
    } else if (strCode.equals(ingConstants.POLICY_BASE)) {
      return "POLICY_BASE";
    } else if (strCode.equals(ingConstants.PREVIOUS_UPDATE_DATE)) {
      return "PREVIOUS_UPDATE_DATE";
    } else if (strCode.equals(ingConstants.MISS_INFO_IND)) {
      return "MISS_INFO_IND";
    } else if (strCode.equals(ingConstants.SEVERITY)) {
      return "SEVERITY";
    } else if (strCode.equals(ingConstants.CLRCASE_SEQUENCE_NUMBER)) {
      return "CLRCASE_SEQUENCE_NUMBER";
    } else if (strCode.equals(ingConstants.LSIR_RETURN_CODE)) {
      return "LSIR_RETURN_CODE";
    } else if (strCode.equals(ingConstants.RESULT_CODE)) {
      return "RESULT_CODE";
    } else if (strCode.equals(ingConstants.COVERAGE_DECISION)) {
      return "COVERAGE_DECISION";
    } else if (strCode.equals(ingConstants.APP_SIGN_DATE)) {
      return "APP_SIGN_DATE";
    } else if (strCode.equals(ingConstants.BRANCH_CODE)) {
      return "BRANCH_CODE";
    } else if (strCode.equals(ingConstants.POLICY_STATUS)) {
      return "POLICY_STATUS";
    } else if (strCode.equals(ingConstants.HI_OWNER_ID)) {
      return "HI_OWNER_ID";
    } else if (strCode.equals(ingConstants.OWNER_NAME)) {
      return "OWNER_NAME";
    } else if (strCode.equals(ingConstants.OWNER_BIRTH_DATE)) {
      return "OWNER_BIRTH_DATE";
    } else if (strCode.equals(ingConstants.HI_INSURED_ID)) {
      return "HI_INSURED_ID";
    } else if (strCode.equals(ingConstants.HI_INSURED_NAME)) {
      return "HI_INSURED_NAME";
    } else if (strCode.equals(ingConstants.INSURED_BIRTH_DATE)) {
      return "INSURED_BIRTH_DATE";
    } else if (strCode.equals(ingConstants.CURRENT_POLICY_STATUS)) {
      return "CURRENT_POLICY_STATUS";
    } else if (strCode.equals(ingConstants.PRODUCT_CODE)) {
      return "PRODUCT_CODE";
    } else if (strCode.equals(ingConstants.BILLING_TYPE_CODE)) {
      return "BILLING_TYPE_CODE";
    } else if (strCode.equals(ingConstants.POLICY_ISSUE_DATE)) {
      return "POLICY_ISSUE_DATE";
    } else if (strCode.equals(ingConstants.POLICY_PAID_TO_DATE)) {
      return "POLICY_PAID_TO_DATE";
    } else if (strCode.equals(ingConstants.AMOUNT_BILLED)) {
      return "AMOUNT_BILLED";
    } else if (strCode.equals(ingConstants.PREMIUM_MODE)) {
      return "PREMIUM_MODE";
    } else if (strCode.equals(ingConstants.SUNDRY_AMOUNT)) {
      return "SUNDRY_AMOUNT";
    } else if (strCode.equals(ingConstants.TRUE_PREMIUM)) {
      return "TRUE_PREMIUM";
    } else if (strCode.equals(ingConstants.LAST_MODE_PREMIUM)) {
      return "LAST_MODE_PREMIUM";
    } else if (strCode.equals(ingConstants.CURRENCY)) {
      return "CURRENCY";
    } else if (strCode.equals(ingConstants.SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS)) {
      return "SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_NAME)) {
      return "BENEFICIARY_INFORMATION_BENEFICIARY_NAME";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE)) {
      return "BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS)) {
      return "BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION)) {
      return "BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_RELATION_TO_INSURED)) {
      return "BENEFICIARY_INFORMATION_RELATION_TO_INSURED";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_TYPE)) {
      return "BENEFICIARY_INFORMATION_BENEFICIARY_TYPE";
    } else if (strCode.equals(ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE)) {
      return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE";
    } else if (strCode.equals(ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE)) {
      return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE";
    } else if (strCode.equals(ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT)) {
      return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT";
    } else if (strCode.equals(ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS)) {
      return "POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS";
    } else if (strCode.equals(ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID)) {
      return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID";
    } else if (strCode.equals(ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY)) {
      return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY";
    } else if (strCode.equals(ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM)) {
      return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM";
    } else if (strCode.equals(ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO)) {
      return "FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW)) {
      return "POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW)) {
      return "POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW)) {
      return "POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
      return "POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
      return "POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
      return "POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
    } else if (strCode.equals(ingConstants.POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM)) {
      return "POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_LOAN_AMOUNT)) {
      return "LOAN_INFORMATION_LOAN_AMOUNT";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_BASE_CASH_VALUE)) {
      return "LOAN_INFORMATION_BASE_CASH_VALUE";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE)) {
      return "LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE";
    } else if (strCode.equals(ingConstants.OTHER_OPTIONS_NON_FORFEITURE_OPTION)) {
      return "OTHER_OPTIONS_NON_FORFEITURE_OPTION";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_PERCENTAGE)) {
      return "BENEFICIARY_INFORMATION_PERCENTAGE";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INSTRUCTIONS)) {
      return "BENEFICIARY_INSTRUCTIONS";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_CASH_SURRENDER_VALUE)) {
      return "LOAN_INFORMATION_CASH_SURRENDER_VALUE";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_LOAN_AMOUNT)) {
      return "LOAN_INFORMATION_LOAN_AMOUNT";
    } else if (strCode.equals(ingConstants.LOAN_INFORMATION_BASE_CASH_VALUE)) {
      return "LOAN_INFORMATION_BASE_CASH_VALUE";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION)) {
      return "DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT)) {
      return "DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT";
    } else if (strCode.equals(ingConstants.PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT)) {
      return "PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT";
    } else if (strCode.equals(ingConstants.AE_FUND_DETAILS_AE_FUND)) {
      return "AE_FUND_DETAILS_AE_FUND";
    } else if (strCode.equals(ingConstants.AE_FUND_DETAILS_AE_OPTION)) {
      return "AE_FUND_DETAILS_AE_OPTION";
    } else if (strCode.equals(ingConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE)) {
      return "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE";
    } else if (strCode.equals(ingConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION)) {
      return "OTHER_OPTIONS_PREMIUM_OFFSET_OPTION";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_YEAR_LAST_DECLARED)) {
      return "DIVIDEND_INFORMATION_YEAR_LAST_DECLARED";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_DIVIDEND_DECLARED)){
      return "DIVIDEND_INFORMATION_DIVIDEND_DECLARED";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT)) {
      return "DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT";
    } else if (strCode.equals(ingConstants.DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR)) {
      return "DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR";
    } else if (strCode.equals(ingConstants.OTHER_OPTIONS_NON_FORFEITURE_OPTION)) {
      return "OTHER_OPTIONS_NON_FORFEITURE_OPTION";
    } else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CODE)) {
      return "AGENT_INFORMATION_SERVICE_AGENT_CODE";
    } else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_NAME)) {
      return "AGENT_INFORMATION_SERVICE_AGENT_NAME";
    } else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE)) {
      return "AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE";
    } else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_NBO_CODE)) {
      return "AGENT_INFORMATION_SERVICE_NBO_CODE";
    } else if (strCode.equals(ingConstants.AGENT_INFORMATION_COVERAGE_CODE)) {
      return "AGENT_INFORMATION_COVERAGE_CODE";
    } else if (strCode.equals(ingConstants.POLICY_INFORMATION_PREMIUM_SUSPENSE)) {
      return "POLICY_INFORMATION_PREMIUM_SUSPENSE";
    } else if (strCode.equals(ingConstants.POLICY_INFORMATION_PREMIUM_REFUND)) {
      return "POLICY_INFORMATION_PREMIUM_REFUND";
    }else if (strCode.equals(ingConstants.SMOKER_CODE)){
      return "SMOKER_CODE";
    }else if (strCode.equals(ingConstants.EFFECTIVE_DATE_OF_ASSIGNMENT)){
      return "EFFECTIVE_DATE_OF_ASSIGNMENT";
    }else if (strCode.equals(ingConstants.COMMENTS_REMARKS)){
      return "COMMENTS_REMARKS";
    }else if (strCode.equals(ingConstants.COVERAGE_DETAILS_AGE)){
      return "COVERAGE_DETAILS_AGE";
    }else if (strCode.equals(ingConstants.COVERAGE_DETAILS_SEX_CODE)){
      return "COVERAGE_DETAILS_SEX_CODE";
    }else if (strCode.equals(ingConstants.COVERAGE_DETAILS_SMOKER_CODE)){
      return "COVERAGE_DETAILS_SMOKER_CODE";
    }else if (strCode.equals(ingConstants.CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE)){
      return "CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE";
    }else if (strCode.equals(ingConstants.CUMULATIVE_SURRENDER_LIFE_TO_DATE)){
      return "CUMULATIVE_SURRENDER_LIFE_TO_DATE";
    }else if (strCode.equals(ingConstants.ANNUAL_PREMIUM_FOR_COVERAGE)){
      return "ANNUAL_PREMIUM_FOR_COVERAGE";
    }else if (strCode.equals(ingConstants.POLICY_INFORMATION_ASSIGNEE)){
      return "POLICY_INFORMATION_ASSIGNEE";
    }else if (strCode.equals(ingConstants.OTHER_LEGAL_FIRST_NAME)){
      return "OTHER_LEGAL_FIRST_NAME";
    }else if (strCode.equals(ingConstants.OTHER_LEGAL_MID_NAME)){
      return "OTHER_LEGAL_MID_NAME";
    }else if (strCode.equals(ingConstants.OTHER_LEGAL_LAST_NAME)){
      return "OTHER_LEGAL_LAST_NAME";
    }else if (strCode.equals(ingConstants.SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE)){
      return "SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE";
    }else if (strCode.equals(ingConstants.APP_FUND_AMT)){
      return "APP_FUND_AMT";
    }else if (strCode.equals(ingConstants.APP_FUND_SUSP_AMT)){
      return "APP_FUND_SUSP_AMT";
    }else if (strCode.equals(ingConstants.ANNUAL_PREMIUM_FOR_COVERAGE )){
      return "ANNUAL_PREMIUM_FOR_COVERAGE";
    }else if (strCode.equals(ingConstants.AMOUNT_OF_ADVANCE )){
      return "AMOUNT_OF_ADVANCE";
    }else if (strCode.equals(ingConstants.APA_AMOUNT )){
      return "APA_AMOUNT";
    }else if (strCode.equals(ingConstants.ACCRUED_INTEREST_ON_ADVANCE )){
      return "ACCRUED_INTEREST_ON_ADVANCE";
    }else if (strCode.equals(ingConstants.ACCRUED_INTEREST_ON_APA)){
      return "ACCRUED_INTEREST_ON_APA";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_CASH_SURRENDER )){
      return "POLICY_VALUES_CASH_SURRENDER";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_INFORMATION_PAID_UP_ADD_CASH )){
      return "POLICY_VALUES_INFORMATION_PAID_UP_ADD_CASH";
    }else if (strCode.equals(ingConstants.POLICY_INFORMATION_1 )){
      return "POLICY_INFORMATION_1";
    }else if (strCode.equals(ingConstants.CLIENT_FLAG_INDICIA )){
      return "CLIENT_FLAG_INDICIA";
    }else if (strCode.equals(ingConstants.CLIENT_FLAG_PERSON )){
      return "CLIENT_FLAG_PERSON";
    }else if (strCode.equals(ingConstants.ICI_ADDRESS_CHANGE_DATE)){
      return "ICI_ADDRESS_CHANGE_DATE";
    }else if (strCode.equals(ingConstants.OWNER_RELATIONSHIP_TO_INSURED)){
      return "OWNER_RELATIONSHIP_TO_INSURED";
    }else if (strCode.equals(ingConstants.OWNER_CLIENT_RELATIONSHIP_TYPE)){
      return "OWNER_CLIENT_RELATIONSHIP_TYPE";
    }else if (strCode.equals(ingConstants.CLIENT_ADDRESS_TYPE)){
      return "CLIENT_ADDRESS_TYPE";
    }else if (strCode.equals(ingConstants.SERVICING_FEE_OPTION)){
      return "SERVICING_FEE_OPTION";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_1 )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_1";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_2 )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_2";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_3 )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_3";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_4 )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_4";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_5 )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_5";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_CHANGED_DATE )){
      return "AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_CHANGED_DATE";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_BILLING_CLIENT_ADDRESS_NAME )){
      return "AGENT_INFORMATION_SERVICE_AGENT_BILLING_CLIENT_ADDRESS_NAME";
    }else if (strCode.equals(ingConstants.COVERAGE_PREMIUM_AMOUNTS )){
      return "COVERAGE_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_ISSUE_EFFECTIVE_DATES )){
      return "COVERAGE_ISSUE_EFFECTIVE_DATES";
    }else if (strCode.equals(ingConstants.COVERAGE_SMOKER_CODES )){
      return "COVERAGE_SMOKER_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_SEX_CODES )){
      return "COVERAGE_SEX_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_CD_RT_AGES )){
      return "COVERAGE_CD_RT_AGES";
    }else if (strCode.equals(ingConstants.COVERAGE_PAR_CODES )){
      return "COVERAGE_PAR_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_MAT_EXPIRY_DATES )){
      return "COVERAGE_MAT_EXPIRY_DATES";
    }else if (strCode.equals(ingConstants.COVERAGE_NUMBERS )){
      return "COVERAGE_NUMBERS";
    }else if (strCode.equals(ingConstants.COVERAGE_FLAT_EXTRA_PREMIUM_AMOUNTS )){
      return "COVERAGE_FLAT_EXTRA_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_MORTALITY_EXTRA_PREMIUM_AMOUNTS )){
      return "COVERAGE_MORTALITY_EXTRA_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_FLAT_EXTRA_U_PREMIUM_AMOUNTS )){
      return "COVERAGE_FLAT_EXTRA_U_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_FLAT_EXTRA_DURS )){
      return "COVERAGE_FLAT_EXTRA_DURS";
    }else if (strCode.equals(ingConstants.COVERAGE_FLAT_EXTRA_PERMANENT_U_PREMIUM_AMOUNTS )){
      return "COVERAGE_FLAT_EXTRA_PERMANENT_U_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_MORTALITY_EXTRA_DURS )){
      return "COVERAGE_MORTALITY_EXTRA_DURS";
    }else if (strCode.equals(ingConstants.COVERAGE_MORTALITY_EXTRA_FCTS )){
      return "COVERAGE_MORTALITY_EXTRA_FCTS";
    }else if (strCode.equals(ingConstants.COVERAGE_RT_RT_AGES )){
      return "COVERAGE_RT_RT_AGES";
    }else if (strCode.equals(ingConstants.COVERAGE_FLAT_EXTRA_REASON_CODES )){
      return "COVERAGE_FLAT_EXTRA_REASON_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_MORTALITY_EXTRA_REASON_CODES )){
      return "COVERAGE_MORTALITY_EXTRA_REASON_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_MORTALITY_EXTRA_RATING_CODES )){
      return "COVERAGE_MORTALITY_EXTRA_RATING_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_RESTRICTED_AUTOMATIC_PREMIUM )){
      return "COVERAGE_RESTRICTED_AUTOMATIC_PREMIUM";
    }else if (strCode.equals(ingConstants.COVERAGE_BASIC_PREMIUM_AMOUNT )){
      return "COVERAGE_BASIC_PREMIUM_AMOUNT";
    }else if (strCode.equals(ingConstants.COVERAGE_PARTICIPATION_CODE )){
      return "COVERAGE_PARTICIPATION_CODE";
    }else if (strCode.equals(ingConstants.COVERAGE_POLICY_FEE )){
      return "COVERAGE_POLICY_FEE";
    }else if (strCode.equals(ingConstants.COVERAGE_RT_NUMBERS )){
      return "COVERAGE_RT_NUMBERS";
    }else if (strCode.equals(ingConstants.COVERAGE_PLANS )){
      return "COVERAGE_PLANS";
    }else if (strCode.equals(ingConstants.COVERAGE_FACE_AMOUNTS )){
      return "COVERAGE_FACE_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_MODE_PREMIUM_AMOUNTS )){
      return "COVERAGE_MODE_PREMIUM_AMOUNTS";
    }else if (strCode.equals(ingConstants.COVERAGE_ISSUE_DATES )){
      return "COVERAGE_ISSUE_DATES";
    }else if (strCode.equals(ingConstants.COVERAGE_PARTICIPATION_CODES )){
      return "COVERAGE_PARTICIPATION_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_STATUS_CODES )){
      return "COVERAGE_STATUS_CODES";
    }else if (strCode.equals(ingConstants.COVERAGE_MATURITY_EXPIRY_DATES )){
      return "COVERAGE_MATURITY_EXPIRY_DATES";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_APP_FUND_AVERAGE_BALANCE )){
      return "POLICY_VALUES_APP_FUND_AVERAGE_BALANCE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_INTEREST_RATE )){
      return "POLICY_VALUES_INTEREST_RATE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_APP_FUND )){
      return "POLICY_VALUES_APP_FUND";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_APP_FUND_ACCUMULATED_INTEREST )){
      return "POLICY_VALUES_APP_FUND_ACCUMULATED_INTEREST";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_APP_FUND_INTEREST_RATE )){
      return "POLICY_VALUES_APP_FUND_INTEREST_RATE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_DIVIDEND_INTEREST_RATE )){
      return "POLICY_VALUES_DIVIDEND_INTEREST_RATE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_PREVIOUS_DIVIDEND_ACCUMULATION )){
      return "POLICY_VALUES_PREVIOUS_DIVIDEND_ACCUMULATION";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_AE_FUND_INTEREST )){
      return "POLICY_VALUES_AE_FUND_INTEREST";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_LAST_WITHDRAWAL_DATE )){
      return "POLICY_VALUES_LAST_WITHDRAWAL_DATE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_LAST_WITHDRAWAL_AMOUNT )){
      return "POLICY_VALUES_LAST_WITHDRAWAL__AMOUNT";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_LOAN_BALANCE )){
      return "POLICY_VALUES_LOAN_BALANCE";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_PREVIOUS_TOTAL_PAID_UP_ADDITIONS )){
      return "POLICY_VALUES_PREVIOUS_TOTAL_PAID_UP_ADDITIONS";
    }else if (strCode.equals(ingConstants.POLICY_VALUES_MAXIMUM_PUA_FA_WITHDRAWALBLE_AMOUNT )){
      return "POLICY_VALUES_MAXIMUM_PUA_FA_WITHDRAWALBLE_AMOUNT";
    }else if (strCode.equals(ingConstants.NAME_EFFECTIVE_DATE )){
      return "NAME_EFFECTIVE_DATE";
    }else if (strCode.equals(ingConstants.SMOKER )){
      return "SMOKER";
    }else if (strCode.equals(ingConstants.MARITAL_STATUS )){
      return "MARITAL_STATUS";
    }else if (strCode.equals(ingConstants.COUNTRY_OF_CITIZENSHIP )){
      return "COUNTRY_OF_CITIZENSHIP";
    }else if (strCode.equals(ingConstants.CITIZENSHIP )){
      return "CITIZENSHIP";
    }else if (strCode.equals(ingConstants.SYSTEM_DATE )){
      return "SYSTEM_DATE";
    }else if (strCode.equals(ingConstants.REINSURANCE_INDICATOR )){
      return "REINSURANCE_INDICATOR";
    }else if (strCode.equals(ingConstants.REINSURANCE_FACE_AMOUNT )){
      return "REINSURANCE_FACE_AMOUNT";
    }else if (strCode.equals(ingConstants.REINSURANCE_ADB_FACE_AMOUNT )){
      return "REINSURANCE_ADB_FACE_AMOUNT";
    }else if (strCode.equals(ingConstants.CLIENT_ID_AR )){
      return "CLIENT_ID_AR";
    }else if (strCode.equals(ingConstants.RELATIONSHIP_TYPE )){
      return "RELATIONSHIP_TYPE";
    }else if (strCode.equals(ingConstants.POLICY_INFORMATION_ASSIGNEE )){
      return "POLICY_INFORMATION_ASSIGNEE";
    }else if (strCode.equals(ingConstants.EFFECTIVE_DATE_OF_ASSIGNMENT )){
      return "EFFECTIVE_DATE_OF_ASSIGNMENT";
    }else if (strCode.equals(ingConstants.ADDRESS_TYPE_ )){
      return "ADDRESS_TYPE_";
    }else if (strCode.equals(ingConstants.FIRST_NAME_OLN )){
      return "FIRST_NAME_OLN";
    }else if (strCode.equals(ingConstants.MIDDLE_NAME_OLN )){
      return "MIDDLE_NAME_OLN";
    }else if (strCode.equals(ingConstants.LAST_NAME_OLN )){
      return "LAST_NAME_OLN";
    }else if (strCode.equals(ingConstants.BIRTH_LOCATION )){
      return "BIRTH_LOCATION";
    }else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_SEQUENCE_NUMBER )){
      return "BENEFICIARY_INFORMATION_SEQUENCE_NUMBER";
    }else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_COVERAGE_NUMBER )){
      return "BENEFICIARY_INFORMATION_COVERAGE_NUMBER";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_PREVIOUS_AGENT_ID )){
      return "AGENT_INFORMATION_SERVICE_AGENT_PREVIOUS_AGENT_ID";
    }else if (strCode.equals(ingConstants.LAPSE_INFORMATION_SHORTAGE_AMOUNT )){
      return "LAPSE_INFORMATION_SHORTAGE_AMOUNT";
    }else if (strCode.equals(ingConstants.LAPSE_INFORMATION_REACTIVE_POLICY_AMOUNT )){
      return "LAPSE_INFORMATION_REACTIVE_POLICY_AMOUNT";
    }else if (strCode.equals(ingConstants.LAPSE_INFORMATION_START_DATE )){
      return "LAPSE_INFORMATION_START_DATE";
    }else if (strCode.equals(ingConstants.LAPSE_INFORMATION_COI_REINSTATEMENT_DATE )){
      return "LAPSE_INFORMATION_COI_REINSTATEMENT_DATE";
    }else if (strCode.equals(ingConstants.LAPSE_INFORMATION_APP_REINSTATEMENT_DATE )){
      return "LAPSE_INFORMATION_APP_REINSTATEMENT_DATE";
    }else if (strCode.equals(ingConstants.SERVICING_AGENT_ASSIGN_DATE )){
      return "SERVICING_AGENT_ASSIGN_DATE";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_SERVICE_FEE_OPTION )){
      return "AGENT_INFORMATION_SERVICE_AGENT_SERVICE_FEE_OPTION";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_ASSIGNMENT_DATE )){
      return "AGENT_INFORMATION_SERVICE_AGENT_ASSIGNMENT_DATE";
    }else if (strCode.equals(ingConstants.AGENT_INFORMATION_SERVICE_AGENT_ENCODE_DATE )){
      return "AGENT_INFORMATION_SERVICE_AGENT_ENCODE_DATE";
    }else if (strCode.equals(ingConstants.AGENT_TERMINATION_DATE )){
      return "AGENT_TERMINATION_DATE";
    } else if (strCode.equals(ingConstants.APPLIED_FOR_BR_TAG )){
      return ingConstants.APPLIED_FOR_BR_KEY;
    } else if (strCode.equals(ingConstants.PENDING_BR_AMOUNT_TAG )){
      return ingConstants.PENDING_BR_AMOUNT_KEY;
    } else if (strCode.equals(ingConstants.EXISTING_BR_TAG )){
      return ingConstants.EXISTING_BR_KEY;
    } else if (strCode.equals(ingConstants.OTHER_INSURER_BR_TAG )){
      return ingConstants.OTHER_INSURER_BR_KEY;
    } else if (strCode.equals(ingConstants.POLICY_VALUES_PO_EFFECTIVE_DATE )){
      return "POLICY_VALUES_PO_EFFECTIVE_DATE";
    } else if (strCode.equals(ingConstants.AGENT_START_DATE )){
      return "AGENT_START_DATE";
    } else if (strCode.equals(ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_UPDATED_DATE)){
      return "BENEFICIARY_INFORMATION_BENEFICIARY_UPDATED_DATE";
    } else if (strCode.equals(ingConstants.POLICY_INFORMATION_PAID_UP_ADD_CASH)){
      return "POLICY_INFORMATION_PAID_UP_ADD_CASH";
    } else if (strCode.equals(IngeniumConstants.MIR_PLAN_BTCH_SETL_IND)) {
      return IngeniumConstants.MIR_PLAN_BTCH_SETL_IND;
    }
    /*
     * list bill -vsantos
     * 12/09/2013
     */
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_CLIENT_ID)) {
      return "LIST_BILL_CLIENT_ID";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_CLIENT_NAME)) {
      return "LIST_BILL_CLIENT_NAME";
    }else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_ADDRESS_CODE)) {
      return "LIST_BILL_ADDRESS_CODE";
    }

    /*
     * list bill employee info
     * 12/12/2013
     */
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_EMPLOYEE_ACCOUNT_NUMBER)) {
      return "LIST_BILL_EMPLOYEE_ACCOUNT_NUMBER";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_EMPLOYEE_RANK)) {
      return "LIST_BILL_EMPLOYEE_RANK";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_EMPLOYEE_BRANCH)) {
      return "LIST_BILL_EMPLOYEE_BRANCH";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_REGION)) {
      return "LIST_BILL_REGION";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_DIVISION)) {
      return "LIST_BILL_DIVISION";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_STATION)) {
      return "LIST_BILL_STATION";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_UNIT)) {
      return "LIST_BILL_UNIT";
    }

    /*
     * vsantos
     * additional list bill codes
     * 01/29/2014
     */
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_TYPE_CODE)) {
      return "LIST_BILL_TYPE_CODE";
    }
    else if (strCode.equalsIgnoreCase(ingConstants.LIST_BILL_FORM_CODE)) {
      return "LIST_BILL_FORM_CODE";
    }

    /* Gross Premium Amount
     * Policy Inquiry - all details
     * GFrag
     * 8/12/2014
     */
    else if (strCode.equalsIgnoreCase(ingConstants.GROSS_PREMIUM_AMOUNT)) {
      return "GROSS_PREMIUM_AMOUNT";
    }


    /*
     * LACZ
     * 10/20/2014
     * added for FATCA 2B -start
     */
    else if (strCode.equalsIgnoreCase(ingConstants.US_INDICIA_DATE)) {
      return "US_INDICIA_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_DATE)) {
      return "FATCA_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_STATUS)) {
      return "FATCA_STATUS";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_REASON)) {
      return "FATCA_REASON";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_REMARKS)){
      return "FATCA_REMARKS";
    }else if (strCode.equalsIgnoreCase(ingConstants.US_PERSON_DATE)) {
      return "US_PERSON_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.CLIENT_FLAG_PERSON)) {
      return "CLIENT_FLAG_PERSON";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_CITIZENSHIP)) {
      return "COUNTRY_OF_CITIZENSHIP";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_CITIZENSHIP_1)) {
      return "COUNTRY_OF_CITIZENSHIP_1";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_CITIZENSHIP_2)) {
      return "COUNTRY_OF_CITIZENSHIP_2";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_1)) {
      return "COUNTRY_OF_LEGAL_RESIDENCE_1";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_2)) {
      return "COUNTRY_OF_LEGAL_RESIDENCE_2";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_3)) {
      return "COUNTRY_OF_LEGAL_RESIDENCE_3";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_OF_INCORPORATION)) {
      return "COUNTRY_OF_INCORPORATION";
    }else if (strCode.equalsIgnoreCase(ingConstants.ID_PRESENTED_OR_IRS_FORM)) {
      return "ID_PRESENTED_OR_IRS_FORM";
    }else if (strCode.equalsIgnoreCase(ingConstants.REFERENCE_NO)) {
      return "REFERENCE_NO";
    }else if (strCode.equalsIgnoreCase(ingConstants.EXPIRY_DATE)) {
      return "EXPIRY_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.VALIDITY_DATE)) {
      return "VALIDITY_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.CONTACT_TYPE)) {
      return "CONTACT_TYPE";
    }else if (strCode.equalsIgnoreCase(ingConstants.COUNTRY_CALLING_CODE)) {
      return "COUNTRY_CALLING_CODE";
    }else if (strCode.equalsIgnoreCase(ingConstants.AREA_CODE)) {
      return "AREA_CODE";
    }else if (strCode.equalsIgnoreCase(ingConstants.CONTACT_DETAIL)) {
      return "CONTACT_DETAIL";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_TYPE)) {
      return "ADDRESS_TYPE";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_EFF_DATE)) {
      return "ADDRESS_EFF_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_START_DATE)) {
      return "ADDRESS_START_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_END_DATE)) {
      return "ADDRESS_END_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_1)) {
      return "ADDRESS_1";
    }else if (strCode.equalsIgnoreCase(ingConstants.CLIENT_ADDRESS_22)) {
      return "CLIENT_ADDRESS_22";
    }else if (strCode.equalsIgnoreCase(ingConstants.CLIENT_ADDRESS_23)) {
      return "CLIENT_ADDRESS_23";
    }else if (strCode.equalsIgnoreCase(ingConstants.CITY)) {
      return "CITY";
    }else if (strCode.equalsIgnoreCase(ingConstants.PROVINCE_STATE)) {
      return "PROVINCE_STATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.POSTAL_CODE)) {
      return "POSTAL_CODE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_COUNTRY)) {
      return "FATCA_COUNTRY";
    }else if (strCode.equalsIgnoreCase(ingConstants.CONTACT_INFO)) {
      return "CONTACT_INFO";
    }else if (strCode.equalsIgnoreCase(ingConstants.ADDRESS_STATUS)) {
      return "ADDRESS_STATUS";

      // added for FATCA 2B - end

      // added for FATCA 3B - Start
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_SIGNING_DATE)) {
      return "FATCA_SIGNING_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_SUBMISSION_DATE)) {
      return "FATCA_SUBMISSION_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_FORM_STATUS)) {
      return "FATCA_FORM_STATUS";

    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_USTIN)) {
      return "FATCA_ENTITY_USTIN";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_GIIN)) {
      return "FATCA_ENTITY_GIIN";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_CLASSIFICATION)) {
      return "FATCA_ENTITY_CLASSIFICATION";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_STATUS)) {
      return "FATCA_ENTITY_STATUS";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_FOREIGN_TIN)) {
      return "FATCA_ENTITY_FOREIGN_TIN";

    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RECALCITRANT_DATE)) {
      return "FATCA_RECALCITRANT_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RECALCITRANT_STATUS)) {
      return "FATCA_RECALCITRANT_STATUS";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RECALCITRANT_REASON)) {
      return "FATCA_RECALCITRANT_REASON";

    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RECALCITRANT_USERID)) {
      return "FATCA_RECALCITRANT_USERID";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RECALCITRANT_REMARKS)) {
      return "FATCA_RECALCITRANT_REMARKS";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RED_FLAG_DATE)) {
      return "FATCA_RED_FLAG_DATE";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RED_FLAG_USERID)) {
      return "FATCA_RED_FLAG_USERID";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RED_FLAG_INDICATOR)) {
      return "FATCA_RED_FLAG_INDICATOR";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RED_FLAG_REASON)) {
      return "FATCA_RED_FLAG_REASON";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_RED_FLAG_REMARKS)) {
      return "FATCA_RED_FLAG_REMARKS";

    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO)) {
      return "FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME)) {
      return "FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS)) {
      return "FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY)) {
      return "FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY";
    }else if (strCode.equalsIgnoreCase(ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN)) {
      return "FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN";
      // added for FATCA 3B - end
      // added CHMEN for transmittal form
    }else if (strCode.equalsIgnoreCase(ingConstants.SERVICING_BANK_BRANCH)) {
      return "SERVICING_BANK_BRANCH";
    }
    // added by IY20 for Auto Partial Withdrawal Trans Type
    else if(strCode.equalsIgnoreCase(ingConstants.MIR_PD_AUTO_WTHDR_IND)){
      return "MIR_PD_AUTO_WTHDR_IND";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_IND)){
      return "MIR_AUTO_WTHDR_IND";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_TERM_RSN)){
      return "MIR_AUTO_WTHDR_TERM_RSN";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_EFF_DT)){
      return "MIR_AUTO_WTHDR_EFF_DT";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_NEXT_WTHDR_DT)){
      return "MIR_NEXT_WTHDR_DT";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_AMT)){
      return "MIR_AUTO_WTHDR_AMT";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_PCT)){
      return "MIR_AUTO_WTHDR_PCT";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_DUR)){
      return "MIR_AUTO_WTHDR_DUR";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_REMN_WTHDR_DUR)){
      return "MIR_REMN_WTHDR_DUR";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_TERM_RSN_DESC)){
      return "MIR_AUTO_WTHDR_TERM_RSN_DESC";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_TERM_RSN_)){
      return "MIR_AUTO_WTHDR_TERM_RSN_";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_IND_DESC)){
      return "MIR_AUTO_WTHDR_IND_DESC";
    }else if(strCode.equalsIgnoreCase(ingConstants.MIR_AUTO_WTHDR_IND_)){
      return "MIR_AUTO_WTHDR_IND_";
    }
    else if (strCode.equals("")) {
      return "";
    }
    return null;

  }

  public void startElement(String uri, String name, String qName,
                           Attributes atts) {
    String Array[] = {
        ingConstants.ADDRESS_1,
        ingConstants.ADDRESS_2,
        ingConstants.ADDRESS_3,
        ingConstants.ADDRESS_4,
        ingConstants.ADDRESS_5,
        ingConstants.ADDRESS_6,
        ingConstants.ADDRESS_3_PREVIOUS,
        ingConstants.BENEFICIARY_NAME,
        ingConstants.CITY,
        ingConstants.CLIENT_FIRSTNAME,
        ingConstants.CLIENT_LASTNAME,
        ingConstants.CLIENT_MIDDLENAME,
        ingConstants.CLIENT_NO,
        ingConstants.COUNTRY_CODE,
        ingConstants.COUNTRY_CODE_PREVIOUS,
        ingConstants.COVERAGE_NUMBER,
        ingConstants.COVERAGE_NUMBER_T,
        ingConstants.FACE_AMOUNT,
        ingConstants.INDEX_NUMBER,
        ingConstants.INSURED_CLIENT_NO,
        ingConstants.INSURED_NAME,
        ingConstants.MINOR_CI_CLAIM_STATUS,
        ingConstants.MINOR_CI_CLAIM_STATUS_DESC,
        ingConstants.ORIGINAL_FACE_AMOUNT,
        ingConstants.CITY_PREVIOUS,
        ingConstants.PLAN_ID,
        ingConstants.PLAN_ID_T,
        ingConstants.PROVINCE_CODE,
        ingConstants.PROVINCE_PREVIOUS,
        ingConstants.REFUND_AMOUNT,
        ingConstants.REQUIREMENT_CODE,
        ingConstants.REQUIREMENT_ID,
        ingConstants.RETURN_CODE,
        ingConstants.SEQUENCE_NUMBER,
        ingConstants.USER_MESSAGE,
        ingConstants.ZIPCODE,
        ingConstants.ZIPCODE_PREVIOUS,
        ingConstants.OWNER_TITLE,
        ingConstants.STAT_CODE,
        ingConstants.STAT_CODES,
        ingConstants.INDEX_NUMBERS,
        ingConstants.SEQUENCE_NUMBERS,
        ingConstants.SEX,
        ingConstants.CLIENT_BDAY,
        ingConstants.AGENT_NAME,
        ingConstants.SERVICING_AGENT_NAME,
        ingConstants.COVERAGE_COUNT,
        ingConstants.BENEFIT_CODE,
        ingConstants.POLICY_STATUS_DESC,
        ingConstants.COVERAGE_PLAN_DESC,
        ingConstants.CURRENCY_CODE,
        ingConstants.MISC_SUSPENSE_AMOUNT,
        ingConstants.MODE_PREMIUM,
        ingConstants.PREMIUM_SUSPENSE,
        ingConstants.BRANCH,
        ingConstants.AGENT_CODE,
        ingConstants.COMPANY_NAME,
        ingConstants.INSURED_ID,
        ingConstants.OWNER_ID,
        ingConstants.INS_TYPE_CODE,
        ingConstants.CLRCASE_SEQUENCE_NUMBER,
        ingConstants.SEVERITY,
        ingConstants.MISS_INFO_IND,
        ingConstants.PREVIOUS_UPDATE_DATE,
        ingConstants.POLICY_BASE,
        ingConstants.MESSAGE_TEXT,
        ingConstants.CLIENT_INDV_SURNAME,
        ingConstants.CLEAR_CASE_RESP,
        ingConstants.LSIR_RETURN_CODE,
        ingConstants.RESULT_CODE,
        ingConstants.COVERAGE_DECISION,
        ingConstants.APP_SIGN_DATE,
        ingConstants.BRANCH_CODE,
        ingConstants.POLICY_STATUS,
        ingConstants.OWNER_ID,
        ingConstants.OWNER_NAME,
        ingConstants.OWNER_BIRTH_DATE,
        ingConstants.INSURED_ID,
        ingConstants.INSURED_NAME,
        ingConstants.INSURED_BIRTH_DATE,
        ingConstants.CURRENT_POLICY_STATUS,
        ingConstants.PRODUCT_CODE,
        ingConstants.BILLING_TYPE_CODE,
        ingConstants.POLICY_ISSUE_DATE,
        ingConstants.POLICY_PAID_TO_DATE,
        ingConstants.AMOUNT_BILLED,
        ingConstants.PREMIUM_MODE,
        ingConstants.SUNDRY_AMOUNT,
        ingConstants.TRUE_PREMIUM,
        ingConstants.LAST_MODE_PREMIUM,
        ingConstants.CURRENCY,
        ingConstants.SUSPENSE_DETAILS_OUTSTANDING_DISBURSEMENTS,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_NAME,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_BIRTHDATE,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_PROCEEDS,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_DESIGNATION,
        ingConstants.BENEFICIARY_INFORMATION_RELATION_TO_INSURED,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_TYPE,
        ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_EFFECTIVE_DATE,
        ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_TYPE,
        ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_GROSS_AMOUNT,
        ingConstants.POLICY_FINANCIAL_ACTIVITY_DEFERRED_ACTIVITY_LIST_FINANCIAL_TRANSACTION_STATUS,
        ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_FUND_ID,
        ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_APPROXIMATE_VALUE_IN_FUND_CURRENCY,
        ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_FROM,
        ingConstants.FUND_TRANSFER_PERCENT_TO_PERCENT_CONFIRMATION_PERCENTAGE_TO,
        ingConstants.POLICY_ALLOCATION_LIST_ALLOCATION_TYPE_2ND_ROW,
        ingConstants.POLICY_ALLOCATION_LIST_EFFECTIVE_DATE_2ND_ROW,
        ingConstants.POLICY_ALLOCATION_LIST_APPLICATION_STATUS_2ND_ROW,
        ingConstants.POLICY_ALLOCATION_INQUIRY_APPLICATION_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
        ingConstants.POLICY_ALLOCATION_INQUIRY_ALLOCATION_PERCENTAGE_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
        ingConstants.POLICY_ALLOCATION_INQUIRY_FUND_DESCRIPTION_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
        ingConstants.POLICY_ALLOCATION_INQUIRY_COVERAGE_STATUS_OF_ALLOCATION_TYPE_SUBSEQUENT_PREMIUM,
        ingConstants.LOAN_INFORMATION_LOAN_AMOUNT,
        ingConstants.LOAN_INFORMATION_BASE_CASH_VALUE,
        ingConstants.LOAN_INFORMATION_MAXIMUM_ADVANCE_AMOUNT_AVAILABLE,
        ingConstants.OTHER_OPTIONS_NON_FORFEITURE_OPTION,
        ingConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION_STATUS_CODE,
        ingConstants.OTHER_OPTIONS_PREMIUM_OFFSET_OPTION,
        ingConstants.BENEFICIARY_INSTRUCTIONS,
        ingConstants.LOAN_INFORMATION_CASH_SURRENDER_VALUE,
        ingConstants.DIVIDEND_INFORMATION_POLICY_DIVIDEND_OPTION,
        ingConstants.DIVIDEND_INFORMATION_DIVIDENDS_ON_DEPOSIT,
        ingConstants.PUA_DETAILS_MAXIMUM_PUA_CV_WITHDRAWABLE_AMOUNT,
        ingConstants.AE_FUND_DETAILS_AE_FUND, ingConstants.AE_FUND_DETAILS_AE_OPTION,
        ingConstants.DIVIDEND_INFORMATION_YEAR_LAST_DECLARED,
        ingConstants.DIVIDEND_INFORMATION_DIVIDEND_DECLARED,
        ingConstants.DIVIDEND_INFORMATION_MAXIMUM_DOD_WITHDRAWABLE_AMOUNT,
        ingConstants.DIVIDEND_INFORMATION_ANNIVERSARY_PROCESSING_YEAR,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CODE,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_NAME,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_STATUS_CODE,
        ingConstants.AGENT_INFORMATION_SERVICE_NBO_CODE,
        ingConstants.AGENT_INFORMATION_COVERAGE_CODE,
        ingConstants.HI_INSURED_ID,
        ingConstants.HI_OWNER_ID,
        ingConstants.HI_INSURED_NAME,
        ingConstants.BENEFICIARY_INFORMATION_PERCENTAGE,
        ingConstants.POLICY_INFORMATION_PREMIUM_REFUND,
        ingConstants.POLICY_INFORMATION_PREMIUM_SUSPENSE,
        ingConstants.SMOKER_CODE,
        ingConstants.EFFECTIVE_DATE_OF_ASSIGNMENT,
        ingConstants.COMMENTS_REMARKS,
        ingConstants.COVERAGE_DETAILS_AGE,
        ingConstants.COVERAGE_DETAILS_SEX_CODE,
        ingConstants.COVERAGE_DETAILS_SMOKER_CODE,
        ingConstants.CUMULATIVE_EXCESS_PREMIUM_LIFE_TO_DATE,
        ingConstants.CUMULATIVE_SURRENDER_LIFE_TO_DATE,
        ingConstants.ANNUAL_PREMIUM_FOR_COVERAGE,
        ingConstants.POLICY_INFORMATION_ASSIGNEE,
        ingConstants.OTHER_LEGAL_FIRST_NAME,
        ingConstants.OTHER_LEGAL_LAST_NAME,
        ingConstants.OTHER_LEGAL_MID_NAME,
        ingConstants.SUSPENSE_DETAILS_MISCELLANEOUS_SUSPENSE,
        ingConstants.APP_FUND_AMT,
        ingConstants.APP_FUND_SUSP_AMT,
        ingConstants.ANNUAL_PREMIUM_FOR_COVERAGE,
        ingConstants.AMOUNT_OF_ADVANCE,
        ingConstants.APA_AMOUNT,
        ingConstants.ACCRUED_INTEREST_ON_ADVANCE,
        ingConstants.ACCRUED_INTEREST_ON_APA,
        ingConstants.POLICY_VALUES_CASH_SURRENDER,
        ingConstants.POLICY_INFORMATION_1,
        ingConstants.CLIENT_FLAG_INDICIA,
        ingConstants.CLIENT_FLAG_PERSON,
        ingConstants.CVG_FACE_AMOUNT,
        ingConstants.POLICY_VALUES_INFORMATION_PAID_UP_ADD_CASH,
        ingConstants.ICI_ADDRESS_CHANGE_DATE,
        ingConstants.OWNER_RELATIONSHIP_TO_INSURED,
        ingConstants.OWNER_CLIENT_RELATIONSHIP_TYPE,
        ingConstants.CLIENT_ADDRESS_TYPE,
        ingConstants.SERVICING_FEE_OPTION,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_1,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_2,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_3,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_4,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_5,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_CLIENT_ADDRESS_CHANGED_DATE,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_BILLING_CLIENT_ADDRESS_NAME,
        ingConstants.COVERAGE_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_ISSUE_EFFECTIVE_DATES,
        ingConstants.COVERAGE_SMOKER_CODES,
        ingConstants.COVERAGE_SEX_CODES,
        ingConstants.COVERAGE_CD_RT_AGES,
        ingConstants.COVERAGE_PAR_CODES,
        ingConstants.COVERAGE_MAT_EXPIRY_DATES,
        ingConstants.COVERAGE_NUMBERS,
        ingConstants.COVERAGE_FLAT_EXTRA_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_MORTALITY_EXTRA_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_FLAT_EXTRA_U_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_FLAT_EXTRA_DURS,
        ingConstants.COVERAGE_FLAT_EXTRA_PERMANENT_U_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_MORTALITY_EXTRA_DURS,
        ingConstants.COVERAGE_MORTALITY_EXTRA_FCTS,
        ingConstants.COVERAGE_RT_RT_AGES,
        ingConstants.COVERAGE_FLAT_EXTRA_REASON_CODES,
        ingConstants.COVERAGE_MORTALITY_EXTRA_REASON_CODES,
        ingConstants.COVERAGE_MORTALITY_EXTRA_RATING_CODES,
        ingConstants.COVERAGE_RESTRICTED_AUTOMATIC_PREMIUM,
        ingConstants.COVERAGE_BASIC_PREMIUM_AMOUNT,
        ingConstants.COVERAGE_PARTICIPATION_CODE,
        ingConstants.COVERAGE_POLICY_FEE,
        ingConstants.COVERAGE_RT_NUMBERS,
        ingConstants.COVERAGE_PLANS,
        ingConstants.COVERAGE_FACE_AMOUNTS,
        ingConstants.COVERAGE_MODE_PREMIUM_AMOUNTS,
        ingConstants.COVERAGE_ISSUE_DATES,
        ingConstants.COVERAGE_PARTICIPATION_CODES,
        ingConstants.COVERAGE_STATUS_CODES,
        ingConstants.COVERAGE_MATURITY_EXPIRY_DATES,
        ingConstants.POLICY_VALUES_APP_FUND_AVERAGE_BALANCE,
        ingConstants.POLICY_VALUES_INTEREST_RATE,
        ingConstants.POLICY_VALUES_APP_FUND,
        ingConstants.POLICY_VALUES_APP_FUND_ACCUMULATED_INTEREST,
        ingConstants.POLICY_VALUES_APP_FUND_INTEREST_RATE,
        ingConstants.POLICY_VALUES_DIVIDEND_INTEREST_RATE,
        ingConstants.POLICY_VALUES_PREVIOUS_DIVIDEND_ACCUMULATION,
        ingConstants.POLICY_VALUES_AE_FUND_INTEREST,
        ingConstants.POLICY_VALUES_LAST_WITHDRAWAL_DATE,
        ingConstants.POLICY_VALUES_LAST_WITHDRAWAL_AMOUNT,
        ingConstants.POLICY_VALUES_LOAN_BALANCE,
        ingConstants.POLICY_VALUES_PREVIOUS_TOTAL_PAID_UP_ADDITIONS,
        ingConstants.POLICY_VALUES_MAXIMUM_PUA_FA_WITHDRAWALBLE_AMOUNT,
        ingConstants.NAME_EFFECTIVE_DATE,
        ingConstants.SMOKER,
        ingConstants.MARITAL_STATUS,
        ingConstants.COUNTRY_OF_CITIZENSHIP,
        ingConstants.CITIZENSHIP,
        ingConstants.SYSTEM_DATE,
        ingConstants.REINSURANCE_INDICATOR,
        ingConstants.REINSURANCE_FACE_AMOUNT,
        ingConstants.REINSURANCE_ADB_FACE_AMOUNT,
        ingConstants.CLIENT_ID_AR,
        ingConstants.RELATIONSHIP_TYPE,
        ingConstants.POLICY_INFORMATION_ASSIGNEE,
        ingConstants.EFFECTIVE_DATE_OF_ASSIGNMENT,
        ingConstants.ADDRESS_TYPE_,
        ingConstants.BIRTH_LOCATION,
        ingConstants.BENEFICIARY_INFORMATION_SEQUENCE_NUMBER,
        ingConstants.BENEFICIARY_INFORMATION_COVERAGE_NUMBER,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_PREVIOUS_AGENT_ID,
        ingConstants.LAPSE_INFORMATION_SHORTAGE_AMOUNT,
        ingConstants.LAPSE_INFORMATION_REACTIVE_POLICY_AMOUNT,
        ingConstants.LAPSE_INFORMATION_START_DATE,
        ingConstants.LAPSE_INFORMATION_COI_REINSTATEMENT_DATE,
        ingConstants.LAPSE_INFORMATION_APP_REINSTATEMENT_DATE,
        ingConstants.SERVICING_AGENT_ASSIGN_DATE,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_SERVICE_FEE_OPTION,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_ASSIGNMENT_DATE,
        ingConstants.AGENT_INFORMATION_SERVICE_AGENT_ENCODE_DATE,
        ingConstants.AGENT_TERMINATION_DATE,
        ingConstants.APPLIED_FOR_BR_TAG,
        ingConstants.PENDING_BR_AMOUNT_TAG,
        ingConstants.EXISTING_BR_TAG,
        ingConstants.OTHER_INSURER_BR_TAG,
        ingConstants.POLICY_VALUES_PO_EFFECTIVE_DATE,
        ingConstants.AGENT_START_DATE,
        ingConstants.BENEFICIARY_INFORMATION_BENEFICIARY_UPDATED_DATE,
        ingConstants.POLICY_INFORMATION_PAID_UP_ADD_CASH,
        ingConstants.MIR_PLAN_BTCH_SETL_IND,			//Added to fix workitem going to Errorhandling 23Jan2014
        /*
         * List Bill
         * vsantos 12/09/2013
         */
        ingConstants.LIST_BILL_CLIENT_ID,
        ingConstants.LIST_BILL_CLIENT_NAME,
        ingConstants.LIST_BILL_ADDRESS_CODE,

        /*
         * List bill employee info
         * vsantos 12/12/2013
         */
        ingConstants.LIST_BILL_EMPLOYEE_ACCOUNT_NUMBER,
        ingConstants.LIST_BILL_EMPLOYEE_RANK,
        ingConstants.LIST_BILL_EMPLOYEE_BRANCH,
        ingConstants.LIST_BILL_REGION,
        ingConstants.LIST_BILL_DIVISION,
        ingConstants.LIST_BILL_STATION,
        ingConstants.LIST_BILL_UNIT,

        /*
         *
         * Additional List bill codes
         */
        ingConstants.LIST_BILL_TYPE_CODE,
        ingConstants.LIST_BILL_FORM_CODE,
        /*
         * Gross Premium Amount
         * GFraga 8/12/2014
         */
        ingConstants.GROSS_PREMIUM_AMOUNT,
        /*
         * LACZ
         * 10/20/2014
         * added for FATCA 2B -start
         */

        ingConstants.US_INDICIA_DATE,
        ingConstants.FATCA_DATE,
        ingConstants.FATCA_STATUS,
        ingConstants.FATCA_REASON,
        ingConstants.FATCA_REMARKS,
        ingConstants.US_PERSON_DATE,
        ingConstants.CLIENT_FLAG_PERSON,
        ingConstants.CITIZENSHIP,
        ingConstants.COUNTRY_OF_CITIZENSHIP_1,
        ingConstants.COUNTRY_OF_CITIZENSHIP_2,
        ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_1,
        ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_2,
        ingConstants.COUNTRY_OF_LEGAL_RESIDENCE_3,
        ingConstants.COUNTRY_OF_INCORPORATION,
        ingConstants.ID_PRESENTED_OR_IRS_FORM,
        ingConstants.REFERENCE_NO,
        ingConstants.EXPIRY_DATE,
        ingConstants.VALIDITY_DATE,
        ingConstants.CONTACT_TYPE,
        ingConstants.COUNTRY_CALLING_CODE,
        ingConstants.AREA_CODE,
        ingConstants.CONTACT_DETAIL,
        ingConstants.ADDRESS_TYPE,
        ingConstants.ADDRESS_EFF_DATE,
        ingConstants.ADDRESS_START_DATE,
        ingConstants.ADDRESS_END_DATE,
        ingConstants.ADDRESS_1,
        ingConstants.CLIENT_ADDRESS_22,
        ingConstants.CLIENT_ADDRESS_23,
        ingConstants.PROVINCE_STATE,
        ingConstants.POSTAL_CODE,
        ingConstants.FATCA_COUNTRY,
        ingConstants.CONTACT_INFO,
        ingConstants.ADDRESS_STATUS,

        // added for FATCA 2B - end

        /*
        * PLAYUG
         * 10/20/2015
         * added for FATCA 3B -start
         */
        ingConstants.FATCA_RECALCITRANT_DATE,
        ingConstants.FATCA_RECALCITRANT_STATUS,
        ingConstants.FATCA_RECALCITRANT_REASON,
        ingConstants.FATCA_SIGNING_DATE,
        ingConstants.FATCA_SUBMISSION_DATE,
        ingConstants.FATCA_FORM_STATUS,

        ingConstants.FATCA_ENTITY_USTIN,
        ingConstants.FATCA_ENTITY_GIIN,
        ingConstants.FATCA_ENTITY_CLASSIFICATION,
        ingConstants.FATCA_ENTITY_STATUS,
        ingConstants.FATCA_ENTITY_FOREIGN_TIN,

        ingConstants.FATCA_RECALCITRANT_USERID,
        ingConstants.FATCA_RECALCITRANT_REMARKS,
        ingConstants.FATCA_RED_FLAG_DATE,
        ingConstants.FATCA_RED_FLAG_USERID,
        ingConstants.FATCA_RED_FLAG_INDICATOR,
        ingConstants.FATCA_RED_FLAG_REASON,
        ingConstants.FATCA_RED_FLAG_REMARKS,

        ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_SEQUENCE_NO,
        ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_NAME,
        ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_ADDRESS,
        ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_COUNTRY,
        ingConstants.FATCA_ENTITY_SUBTANTIAL_US_OWNER_TIN,
        //added for FATCA 3B -end
        ingConstants.SERVICING_BANK_BRANCH,

        // added for Auto Partial Withdrawal
        ingConstants.MIR_PD_AUTO_WTHDR_IND,
        ingConstants.MIR_AUTO_WTHDR_IND,
        ingConstants.MIR_AUTO_WTHDR_TERM_RSN,
        ingConstants.MIR_AUTO_WTHDR_EFF_DT,
        ingConstants.MIR_NEXT_WTHDR_DT,
        ingConstants.MIR_AUTO_WTHDR_AMT,
        ingConstants.MIR_AUTO_WTHDR_PCT,
        ingConstants.MIR_AUTO_WTHDR_DUR,
        ingConstants.MIR_REMN_WTHDR_DUR,
        ingConstants.MIR_AUTO_WTHDR_TERM_RSN_DESC,
        ingConstants.MIR_AUTO_WTHDR_IND_DESC
    };
    // if ("".equals (uri))
    // System.out.println("Start element: " + qName);
    // else{}
    // System.out.println("Start element: {" + uri + "}" + name);

    // this would mean mark the end of the repeating objects
    sb = new StringBuffer();
    if (!strHead.equalsIgnoreCase(name) && bKeyFound) {
      // System.out.println(translate(strHead) + " : " + alValuesFound);
      if (!strHead.equalsIgnoreCase("ResultCode"))
        hm.put(translate(strHead), alValuesFound);

      // hm.put(strHead,alValuesFound);
      bKeyFound = false;
      alValuesFound = null;
      alValuesFound = new ArrayList();

    }

    for (int i = 0; i < Array.length; i++) {
      if (name.equalsIgnoreCase(Array[i])) {
        strHead = name;
        bKeyFound = true;
//				System.out.println("MATCHED IS " + Array[i]);
      }
    }

  }

  public void endElement(String uri, String name, String qName) {
    if ("".equals(uri))
      ; // System.out.println("End element: " + qName);
    else {
//			System.out.println("End element:   {" + uri + "}" + name);
      if (bKeyFound) {
        alValuesFound.add(sb.toString());

      }
      if (!strHead.equalsIgnoreCase(name) && bKeyFound) {
        // System.out.println(translate(strHead) + " : " +
        // alValuesFound);
        if (!strHead.equalsIgnoreCase("ResultCode"))
          hm.put(translate(strHead), alValuesFound);
        // hm.put(strHead,alValuesFound);
        bKeyFound = false;
        alValuesFound = null;
        alValuesFound = new ArrayList();
      }
    }

  }

  public void characters(char ch[], int start, int length) {
    if (strHead.equalsIgnoreCase("ResultCode") && bKeyFound) {
//			System.out.println("dsds" + ch[start - 3]);
      alValuesFound = new ArrayList();
      alValuesFound.add("" + ch[start - 3]);
      hm.put(translate(strHead), alValuesFound);
      bKeyFound = false;
      alValuesFound = null;
      alValuesFound = new ArrayList();
    }
    /*
     * ( if(strHead.equalsIgnoreCase("MirIndexNumT")) { if(length > 1 &&
     * length < 38) length = 38; }
     */
    char ch1[] = new char[length];
    int j = 0;

//		System.out.println("start " + start + ": length " + length);

    for (int i = start; i < start + length; i++) {
      // if(ResultCode)
      // System.out.println(ch[i]);
      switch (ch[i]) {

        case '\\':
          System.out.print("\\\\");
          break;
        case '"':
          System.out.print("\\\"");
          break;
        case '\n':
          System.out.print("\\n");
          break;
        case '\r':
          System.out.print("\\r");
          break;
        case '\t':
          System.out.print("\\t");
          break;
        default:
          // System.out.print(ch[i]);
          ch1[j] = ch[i];
          j++;
          break;
      }
    }
    if (bKeyFound) {
      String strTemp = new String(ch1);
      sb.append(strTemp.toString());
    }
  }
}
