package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirPolAgtShrPctG")
public class MirPolAgtShrPctG {

    @XStreamImplicit(itemFieldName = "MirPolAgtShrPctT")
    private List<Float> mirPolAgtShrPctT;

    public List<Float> getMirPolAgtShrPctT() {
        return mirPolAgtShrPctT;
    }

    public void setMirPolAgtShrPctT(List<Float> mirPolAgtShrPctT) {
        this.mirPolAgtShrPctT = mirPolAgtShrPctT;
    }
}
