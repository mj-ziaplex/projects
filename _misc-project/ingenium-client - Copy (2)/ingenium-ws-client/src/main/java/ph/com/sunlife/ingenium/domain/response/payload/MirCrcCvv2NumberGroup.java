package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcCvv2NoG")
public class MirCrcCvv2NumberGroup {

    @XStreamImplicit(itemFieldName = "MirCrcCvv2NoT")
    private List<String> mirCrcCvv2NumberTexts;

    public List<String> getMirCrcCvv2NumberTexts() {
        return mirCrcCvv2NumberTexts;
    }

    public void setMirCrcCvv2NumberTexts(List<String> mirCrcCvv2NumberTexts) {
        this.mirCrcCvv2NumberTexts = mirCrcCvv2NumberTexts;
    }
}
