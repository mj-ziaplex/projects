package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliResTypCdG")
public class MirDvClientResidenceTypeCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliResTypCdT")
    private List<String> mirDvClientResidenceTypeCodeTexts;

    public List<String> getMirDvClientResidenceTypeCodeTexts() {
        return mirDvClientResidenceTypeCodeTexts;
    }

    public void setMirDvClientResidenceTypeCodeTexts(final List<String> typeCodes) {
        mirDvClientResidenceTypeCodeTexts = typeCodes;
    }
}
