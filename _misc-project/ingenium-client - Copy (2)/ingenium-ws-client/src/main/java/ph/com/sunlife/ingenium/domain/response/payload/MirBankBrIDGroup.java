package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkBrIdG")
public class MirBankBrIDGroup {

    @XStreamImplicit(itemFieldName = "MirBnkBrIdT")
    private List<String> mirBankBrIdTypes;

    public List<String> getMirBankBrIdTypes() {
        return mirBankBrIdTypes;
    }

    public MirBankBrIDGroup setMirBankBrIdTypes(final List<String> types) {
        this.mirBankBrIdTypes = types;
        return this;
    }
}
