package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrAddr2TxtG")
public class MirDvEmployerAddress2TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrAddr2TxtT")
    private List<String> mirDvEmployerAddress2Texts;

    public List<String> getMirDvEmployerAddress2Texts() {
        return mirDvEmployerAddress2Texts;
    }

    public void setMirDvEmployerAddress2Texts(List<String> address2Texts) {
        mirDvEmployerAddress2Texts = address2Texts;
    }
}
