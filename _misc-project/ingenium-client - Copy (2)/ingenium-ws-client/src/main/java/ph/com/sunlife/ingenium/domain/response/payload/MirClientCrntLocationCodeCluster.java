package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliCrntLocCdG")
public class MirClientCrntLocationCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliCrntLocCdT")
    private List<String> clientCrntLocationCodes;

    public List<String> getClientCrntLocationCodes() {
        return clientCrntLocationCodes;
    }

    public void setClientCrntLocationCodes(final List<String> locationCodes) {
        this.clientCrntLocationCodes = locationCodes;
    }
}
