package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserAuthRequest")
public abstract class Credentials {

  @XStreamAlias("UserLoginName")
  private String username;
  @XStreamAlias("UserPswd")
  private Password password;

  public Credentials(final String username, final String password) {
    this(username, new PlainPassword(password));
  }

  public Credentials(final String username, final String password, final String algorithm) {
    this(username, new EncryptedPassword(password, algorithm));
  }

  public Credentials(final String username, final Password password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public <Credence extends Credentials> Credence setUsername(final String username) {
    this.username = username;
    return (Credence) this;
  }

  public String getPassword() {
    return password.getPasswordText();
  }

  public <Credence extends Credentials> Credence setPassword(final String password) {
    this.password.setPasswordText(password);
    return (Credence) this;
  }

  public <Credence extends Credentials> Credence setPassword(final Password password) {
    this.password = password;
    return (Credence) this;
  }

  public String getPasswordCryptographicAlgorithm() {
    return password.getCryptographicAlgorithm();
  }

  public <Credence extends Credentials> Credence setCryptographicAlgorithm(final String algorithm) {
    password.setCryptographicAlgorithm(algorithm);
    return (Credence) this;
  }
}
