package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliPstlCdG")
public class MirDvClientPostalCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliPstlCdT")
    private List<String> mirDvClientPostalCodeTexts;

    public List<String> getMirDvClientPostalCodeTexts() {
        return mirDvClientPostalCodeTexts;
    }

    public void setMirDvClientPostalCodeTexts(final List<String> postalCodes) {
        mirDvClientPostalCodeTexts = postalCodes;
    }
}
