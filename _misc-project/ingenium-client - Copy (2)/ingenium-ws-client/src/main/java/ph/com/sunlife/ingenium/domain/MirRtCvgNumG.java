package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirRtCvgNumG")
public class MirRtCvgNumG {

	@XStreamImplicit(itemFieldName = "MirRtCvgNumT")
	protected List<String> mirRtCvgNumT;

	public List<String> getMirRtCvgNumT() {
		if (mirRtCvgNumT == null) {
			mirRtCvgNumT = new ArrayList<String>();
		}
		return this.mirRtCvgNumT;
	}

}
