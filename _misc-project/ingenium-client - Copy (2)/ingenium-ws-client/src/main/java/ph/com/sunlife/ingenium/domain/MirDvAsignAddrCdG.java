package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvAsignAddrCdG")
public class MirDvAsignAddrCdG {

    @XStreamImplicit(itemFieldName = "MirDvAsignAddrCdT")
    private List<String> mirDvAsignAddrCdT;

    public List<String> getMirDvAsignAddrCdT() {
        return mirDvAsignAddrCdT;
    }

    public void setMirDvAsignAddrCdT(List<String> mirDvAsignAddrCdT) {
        this.mirDvAsignAddrCdT = mirDvAsignAddrCdT;
    }
}
