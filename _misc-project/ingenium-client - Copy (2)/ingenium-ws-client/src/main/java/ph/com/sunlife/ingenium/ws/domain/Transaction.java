package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("TXLifeRequest")
public class Transaction {

  @XStreamAlias("TransRefGUID")
  private String referenceGUID;
  @XStreamAlias("TransType")
  private TransactionType type;
  @XStreamAlias("TransExeDate")
  private String executionDate;
  @XStreamAlias("TransExeTime")
  private String executionTime;
  @XStreamAlias("OLifE")
  private Container container;

  public Transaction() { }

  public Transaction(final TransactionType type) {
    this.type = type;
  }

  public Transaction(final String type) {
    this.type = new TransactionType(type);
  }

  public Transaction(final TransactionType type, final Container container) {
    this.type = type;
    this.container = container;
  }

  public Transaction(final String type, final Container container) {
    this.type = new TransactionType(type);
    this.container = container;
  }

  public String getReferenceGUID() {
    return referenceGUID;
  }

  public Transaction setReferenceGUID(final String id) {
    this.referenceGUID = id;
    return this;
  }

  public String getType() {
    return type.getValue();
  }

  public Transaction setType(final TransactionType type) {
    this.type = type;
    return this;
  }

  public Transaction setType(final String type) {
    this.type = new TransactionType(type);
    this.type.setValue(type);
    return this;
  }

  public String getExecutionDate() {
    return executionDate;
  }

  public Transaction setExecutionDate(final String executionDate) {
    this.executionDate = executionDate;
    return this;
  }

  public String getExecutionTime() {
    return executionTime;
  }

  public Transaction setExecutionTime(final String executionTime) {
    this.executionTime = executionTime;
    return this;
  }

  public Container getContainer() {
    return container;
  }

  public void setContainer(final Container container) {
    this.container = container;
  }
}
