package ph.com.sunlife.wms.ingenium.ws.client;

import com.thoughtworks.xstream.XStream;

public abstract class IngeniumReponseHandler {

    protected XStream xstream;

    public IngeniumReponseHandler() {
        xstream = new XStream();
        xstream.allowTypesByWildcard(new String[] {
                "ph.com.sunlife.ingenium.domain.**",
                "ph.com.sunlife.ingenium.ws.domain.**"
        });
    }
}
