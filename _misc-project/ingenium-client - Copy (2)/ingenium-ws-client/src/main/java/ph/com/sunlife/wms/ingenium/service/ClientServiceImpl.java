package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.ingenium.IngeniumWebServiceClient;
import ph.com.sunlife.wms.ingenium.domain.Client;
import ph.com.sunlife.wms.ingenium.handler.ClientInquiryResponseHandler;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryRequestBuilder;

public class ClientServiceImpl implements ClientService {

  @Override
  public Client getClientDetails(String clientId) {
    return getClientDetails(clientId, "");
  }

  @Override
  public Client getClientDetails(String clientId, String guid) {
    return (Client) new ClientInquiryResponseHandler()
        .setResponseString(new IngeniumWebServiceClient()
                               .getResponse(new ClientInquiryRequestBuilder(clientId)
                                                .setTransactionReferenceGUID(guid)
                                                .create())).getResponse();
  }
}
