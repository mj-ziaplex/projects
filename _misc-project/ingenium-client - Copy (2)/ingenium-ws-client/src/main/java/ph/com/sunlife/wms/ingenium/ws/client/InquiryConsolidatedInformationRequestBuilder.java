package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.MirPolicyID;
import ph.com.sunlife.ingenium.ws.domain.InquiryConsolidatedInformationContainer;
import ph.com.sunlife.ingenium.ws.domain.InquiryConsolidatedInformationData;
import ph.com.sunlife.ingenium.ws.domain.Transaction;

public class InquiryConsolidatedInformationRequestBuilder extends IngeniumRequestBuilder {

    private String policyId;
    private String effectivityDate;

    public InquiryConsolidatedInformationRequestBuilder(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public String getEffectivityDate() {
        return effectivityDate;
    }

    public InquiryConsolidatedInformationRequestBuilder setEffectivityDate(String effectivityDate) {
        this.effectivityDate = effectivityDate;
        return this;
    }

    @Override
    protected Transaction getTransaction() {
        InquiryConsolidatedInformationContainer box =
                new InquiryConsolidatedInformationContainer()
                        .setInquiryConsolidatedInformationData(new InquiryConsolidatedInformationData()
                                .setPolicyID(new MirPolicyID(policyId)
                                        .setMirDvEffectivityDate(effectivityDate)));
        Transaction transaction = new Transaction();
        transaction.setReferenceGUID(transactionReferenceGUID);
        transaction.setType("InquiryConsolidatedInformation");
        transaction.setExecutionDate(getDateFormatter().format(executionTimestamp));
        transaction.setExecutionTime(getTimeFormatter().format(executionTimestamp));
        transaction.setContainer(box);
        return transaction;
    }
}
