package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirClientInformation;
import ph.com.sunlife.ingenium.domain.response.payload.*;

@XStreamAlias("ClientData")
public class ClientData {

    @XStreamAlias("MirPrcesInfo")
    private MirProcessInformation mirPrcesInfo;
    @XStreamAlias("MirCliInfo")
    private MirClientInformation mirCliInfo;
    @XStreamAlias("MirBnkInfo")
    private MirBankInformation mirBnkInfo;
    @XStreamAlias("MirNmInfo")
    private MirNameInformation mirNmInfo;
    @XStreamAlias("MirPrevNmInfo")
    private MirPreviousNameInformation mirPrevNmInfo;
    @XStreamAlias("MirAddrInfo")
    private MirAddressInformation mirAddrInfo;
    @XStreamAlias("MirPrevAddrInfo")
    private MirPreviousAddressInformation mirPreviousAddressInformation;
    @XStreamAlias("MirEmplrInfo")
    private MirEmployerInformation mirEmployerInformation;
    @XStreamAlias("MirPrevEmplrInfo")
    private MirPreviousEmployerInformation mirPreviousEmployerInformation;
    @XStreamAlias("MirCrcInfo")
    private MirCrcInformation mirCrcInformation;
    @XStreamAlias("MirListInfo")
    private String mirListInfo;
    @XStreamAlias("MirCliEnoticeInd")
    private String mirCliEnoticeInd;
    @XStreamAlias("MirCliSmsInd")
    private String mirCliSmsInd;
    @XStreamAlias("MirClftInfo")
    private MirClftInfo mirClftInfo;
    @XStreamAlias("MirDvFutDtTrxnTypCd")
    private byte mirDvFutDtTrxnTypCd;
    @XStreamAlias("MirCliSmsOrNotifInd")
    private String mirCliSmsOrNotifInd;
    @XStreamAlias("MirCliEorNotifInd")
    private String mirCliEorNotifInd;

    public MirProcessInformation getMirPrcesInfo() {
        return mirPrcesInfo;
    }

    public void setMirPrcesInfo(MirProcessInformation mirPrcesInfo) {
        this.mirPrcesInfo = mirPrcesInfo;
    }

    public MirClientInformation getMirClientInformation() {
        return mirCliInfo;
    }

    public void setMirClientInformation(MirClientInformation mirCliInfo) {
        this.mirCliInfo = mirCliInfo;
    }

    public MirBankInformation getMirBnkInfo() {
        return mirBnkInfo;
    }

    public void setMirBnkInfo(MirBankInformation mirBnkInfo) {
        this.mirBnkInfo = mirBnkInfo;
    }

    public MirNameInformation getMirNameInformation() {
        return mirNmInfo;
    }

    public void setMirNameInformation(MirNameInformation mirNmInfo) {
        this.mirNmInfo = mirNmInfo;
    }

    public MirPreviousNameInformation getMirPrevNmInfo() {
        return mirPrevNmInfo;
    }

    public void setMirPrevNmInfo(MirPreviousNameInformation mirPrevNmInfo) {
        this.mirPrevNmInfo = mirPrevNmInfo;
    }

    public MirAddressInformation getMirAddrInfo() {
        return mirAddrInfo;
    }

    public void setMirAddrInfo(MirAddressInformation mirAddrInfo) {
        this.mirAddrInfo = mirAddrInfo;
    }

    public MirPreviousAddressInformation getMirPreviousAddressInformation() {
        return mirPreviousAddressInformation;
    }

    public void setMirPreviousAddressInformation(MirPreviousAddressInformation mirPreviousAddressInformation) {
        this.mirPreviousAddressInformation = mirPreviousAddressInformation;
    }

    public MirEmployerInformation getMirEmployerInformation() {
        return mirEmployerInformation;
    }

    public void setMirEmployerInformation(MirEmployerInformation mirEmployerInformation) {
        this.mirEmployerInformation = mirEmployerInformation;
    }

    public MirPreviousEmployerInformation getMirPreviousEmployerInformation() {
        return mirPreviousEmployerInformation;
    }

    public void setMirPreviousEmployerInformation(MirPreviousEmployerInformation mirPreviousEmployerInformation) {
        this.mirPreviousEmployerInformation = mirPreviousEmployerInformation;
    }

    public MirCrcInformation getMirCrcInformation() {
        return mirCrcInformation;
    }

    public void setMirCrcInformation(MirCrcInformation mirCrcInformation) {
        this.mirCrcInformation = mirCrcInformation;
    }

    public String getMirListInfo() {
        return mirListInfo;
    }

    public void setMirListInfo(String mirListInfo) {
        this.mirListInfo = mirListInfo;
    }

    public String getMirCliEnoticeInd() {
        return mirCliEnoticeInd;
    }

    public void setMirCliEnoticeInd(String mirCliEnoticeInd) {
        this.mirCliEnoticeInd = mirCliEnoticeInd;
    }

    public String getMirCliSmsInd() {
        return mirCliSmsInd;
    }

    public void setMirCliSmsInd(String mirCliSmsInd) {
        this.mirCliSmsInd = mirCliSmsInd;
    }

    public MirClftInfo getMirClftInfo() {
        return mirClftInfo;
    }

    public void setMirClftInfo(MirClftInfo mirClftInfo) {
        this.mirClftInfo = mirClftInfo;
    }

    public byte getMirDvFutDtTrxnTypCd() {
        return mirDvFutDtTrxnTypCd;
    }

    public void setMirDvFutDtTrxnTypCd(byte mirDvFutDtTrxnTypCd) {
        this.mirDvFutDtTrxnTypCd = mirDvFutDtTrxnTypCd;
    }

    public String getMirCliSmsOrNotifInd() {
        return mirCliSmsOrNotifInd;
    }

    public void setMirCliSmsOrNotifInd(String mirCliSmsOrNotifInd) {
        this.mirCliSmsOrNotifInd = mirCliSmsOrNotifInd;
    }

    public String getMirCliEorNotifInd() {
        return mirCliEorNotifInd;
    }

    public void setMirCliEorNotifInd(String mirCliEorNotifInd) {
        this.mirCliEorNotifInd = mirCliEorNotifInd;
    }
}
