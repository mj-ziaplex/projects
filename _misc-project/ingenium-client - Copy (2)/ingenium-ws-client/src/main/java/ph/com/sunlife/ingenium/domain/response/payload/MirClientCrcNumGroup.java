package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliCrcNumG")
public class MirClientCrcNumGroup {

    @XStreamImplicit(itemFieldName = "MirCliCrcNumT")
    private List<String> mirClientCrcNumTexts;

    public List<String> getMirClientCrcNumTexts() {
        return mirClientCrcNumTexts;
    }

    public void setMirClientCrcNumTexts(List<String> mirClientCrcNumTexts) {
        this.mirClientCrcNumTexts = mirClientCrcNumTexts;
    }
}
