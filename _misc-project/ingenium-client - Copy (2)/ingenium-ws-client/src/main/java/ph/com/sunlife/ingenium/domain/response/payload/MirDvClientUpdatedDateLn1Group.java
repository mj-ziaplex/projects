package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliUpdDtLn1G")
public class MirDvClientUpdatedDateLn1Group {

    @XStreamImplicit(itemFieldName = "MirDvCliUpdDtLnT")
    private List<String> mirDvClientUpdateDateLnTypes;

    public List<String> getMirDvClientUpdateDateLnTypes() {
        return mirDvClientUpdateDateLnTypes;
    }

    public void setMirDvClientUpdateDateLnTypes(List<String> mirDvClientUpdateDateLnTypes) {
        this.mirDvClientUpdateDateLnTypes = mirDvClientUpdateDateLnTypes;
    }
}
