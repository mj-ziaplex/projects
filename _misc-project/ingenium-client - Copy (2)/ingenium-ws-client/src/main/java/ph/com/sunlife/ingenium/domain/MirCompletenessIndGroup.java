package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCmpltnessIndG")
public class MirCompletenessIndGroup {

	@XStreamImplicit(itemFieldName = "MirCmpltnessIndT")
	protected List<String> mirCompletenessIndTexts;

	public List<String> getMirCompletenessIndTexts() {
		if (mirCompletenessIndTexts == null) {
			mirCompletenessIndTexts = new ArrayList<String>();
		}
		return this.mirCompletenessIndTexts;
	}

}
