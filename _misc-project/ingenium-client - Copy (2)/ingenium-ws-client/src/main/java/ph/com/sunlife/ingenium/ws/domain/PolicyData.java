package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.*;

@XStreamAlias("PolicyData")
public class PolicyData {

    @XStreamAlias("MirPolId")
    private MirPolicyID mirPolicyId;
    @XStreamAlias("SrcSystem")
    private String sourceSystem;
    @XStreamAlias("MirDvFutDtTrxnTypCd")
    private String mirDvFutDtTrxnTypCd;
    @XStreamAlias("MirAcruCalcTimeCd")
    private String mirAcruCalcTimeCd;
    @XStreamAlias("MirAppFormTypId")
    private String mirAppFormTypId;
    @XStreamAlias("MirAutoPremPmtInd")
    private String mirAutoPremPmtInd;
    @XStreamAlias("MirBillLeadTimeDur")
    private String mirBillLeadTimeDur;
    @XStreamAlias("MirCliCrntLocCd")
    private String mirCliCrntLocCd;
    @XStreamAlias("MirCliIdG")
    private MirClientIDGroup mirCliIdG;
    @XStreamAlias("MirDvOwnSubCdG")
    private MirDvOwnSubCdG mirDvOwnSubCdG;
    @XStreamAlias("MirPolCliInsrdCdG")
    private MirPolCliInsrdCdG mirPolCliInsrdCdG;
    @XStreamAlias("MirCliAddrTypCdG")
    private MirCliAddrTypCdG mirCliAddrTypCdG;
    @XStreamAlias("MirDvOwnCliNmG")
    private MirDvOwnCliNmG mirDvOwnCliNmG;
    @XStreamAlias("MirCliBthDtG")
    private MirClientBirthDateGroup mirClientBirthDateGroup;
    @XStreamAlias("MirCliTaxIdG")
    private MirCliTaxIdG mirCliTaxIdG;
    @XStreamAlias("MirDvAsignCliIdG")
    private MirDvAsignCliIdG mirDvAsignCliIdG;
    @XStreamAlias("MirDvAsignSubCdG")
    private MirDvAsignSubCdG mirDvAsignSubCdG;
    @XStreamAlias("MirDvAsignAddrCdG")
    private MirDvAsignAddrCdG mirDvAsignAddrCdG;
    @XStreamAlias("MirPolCliAsignDtG")
    private MirPolCliAsignDtG mirPolCliAsignDtG;
    @XStreamAlias("MirDvAsignCliNmG")
    private MirDvAsignCliNmG mirDvAsignCliNmG;
    @XStreamAlias("MirDvAnutntCliIdG")
    private MirDvAnutntCliIdG mirDvAnutntCliIdG;
    @XStreamAlias("MirDvAnutntSubCdG")
    private MirDvAnutntSubCdG mirDvAnutntSubCdG;
    @XStreamAlias("MirDvAnutntAddrCdG")
    private MirDvAnutntAddrCdG mirDvAnutntAddrCdG;
    @XStreamAlias("MirDvAnutntCliNmG")
    private MirDvAnutntCliNmG mirDvAnutntCliNmG;
    @XStreamAlias("MirDvIpartyCliIdG")
    private MirDvIpartyCliIdG mirDvIpartyCliIdG;
    @XStreamAlias("MirDvIpartyAddrCdG")
    private MirDvIpartyAddrCdG mirDvIpartyAddrCdG;
    @XStreamAlias("MirDvIpartyCliNmG")
    private MirDvIpartyCliNmG mirDvIpartyCliNmG;
    @XStreamAlias("MirDvTrstCliIdG")
    private MirDvTrstCliIdG mirDvTrstCliIdG;
    @XStreamAlias("MirDvTrstAddrCdG")
    private MirDvTrstAddrCdG mirDvTrstAddrCdG;
    @XStreamAlias("MirDvTrstCliNmG")
    private MirDvTrstCliNmG mirDvTrstCliNmG;
    @XStreamAlias("MirClmPayePolId")
    private String mirClmPayePolId;
    @XStreamAlias("MirClmPayoMthdCd")
    private String mirClmPayoMthdCd;
    @XStreamAlias("MirConnOptlPolId")
    private String mirConnOptlPolId;
    @XStreamAlias("MirAgtIdG")
    private MirAgentIDGroup mirAgtIdG;
    @XStreamAlias("MirPolAgtShrPctG")
    private MirPolAgtShrPctG mirPolAgtShrPctG;
    @XStreamAlias("MirDvAgtCliNmG")
    private MirDvAgtCliNmG mirDvAgtCliNmG;
    @XStreamAlias("MirDvOwnCliNm")
    private String mirDvOwnCliNm;
    @XStreamAlias("MirGainRptTimeCd")
    private String mirGainRptTimeCd;
    @XStreamAlias("MirLinkPolIssInd")
    private String mirLinkPolIssInd;
    @XStreamAlias("MirLoanIntDedblInd")
    private String mirLoanIntDedblInd;
    @XStreamAlias("MirLxpctCalcCd")
    private String mirLxpctCalcCd;
    @XStreamAlias("MirLxpctRecalcInd")
    private String mirLxpctRecalcInd;
    @XStreamAlias("MirModeFctRestrInd")
    private String mirModeFctRestrInd;
    @XStreamAlias("MirMpremDscntAmt")
    private String mirMpremDscntAmt;
    @XStreamAlias("MirPremRecalcOptCd")
    private String mirPremRecalcOptCd;
    @XStreamAlias("MirNxtActvTypCd")
    private String mirNxtActvTypCd;
    @XStreamAlias("MirPlanId")
    private String mirPlanId;
    @XStreamAlias("MirPlanPeriPremAmt")
    private String mirPlanPeriPremAmt;
    @XStreamAlias("MirPolAnpayo1Amt")
    private String mirPolAnpayo1Amt;
    @XStreamAlias("MirPolAnpayo2Amt")
    private String mirPolAnpayo2Amt;
    @XStreamAlias("MirPolAnpayoModeCd")
    private String mirPolAnpayoModeCd;
    @XStreamAlias("MirPolAplInd")
    private String mirPolAplInd;
    @XStreamAlias("MirPolAppCmpltDt")
    private String mirPolAppCmpltDt;
    @XStreamAlias("MirPolAppCtr")
    private String mirPolAppCtr;
    @XStreamAlias("MirPolAppFormId")
    private String mirPolAppFormId;
    @XStreamAlias("MirPolAppRecvDt")
    private String mirPolAppRecvDt;
    @XStreamAlias("MirPolAppSignDt")
    private String mirPolAppSignDt;
    @XStreamAlias("MirPolAppSignInd")
    private String mirPolAppSignInd;
    @XStreamAlias("MirPolBillModeCd")
    private String mirPolBillModeCd;
    @XStreamAlias("MirPolBillTypCd")
    private String mirPolBillTypCd;
    @XStreamAlias("MirPolBillToDtNum")
    private String mirPolBillToDtNum;
    @XStreamAlias("MirPolCeasDt")
    private String mirPolCeasDt;
    @XStreamAlias("MirPolCeasReasnCd")
    private String mirPolCeasReasnCd;
    @XStreamAlias("MirPolCnfdInd")
    private String mirPolCnfdInd;
    @XStreamAlias("MirPolCrcyCd")
    private String mirPolCrcyCd;
    @XStreamAlias("MirPolCrntModeFct")
    private String mirPolCrntModeFct;
    @XStreamAlias("MirPolCstatCd")
    private String mirPolCstatCd;
    @XStreamAlias("MirPolCtryCd")
    private String mirPolCtryCd;
    @XStreamAlias("MirPolCvgRecCtr")
    private String mirPolCvgRecCtr;
    @XStreamAlias("MirPolDbOptCd")
    private String mirPolDbOptCd;
    @XStreamAlias("MirPolDivOptCd")
    private String mirPolDivOptCd;
    @XStreamAlias("MirPolEnhcNelctQty")
    private String mirPolEnhcNelctQty;
    @XStreamAlias("MirPolFinalDispDt")
    private String mirPolFinalDispDt;
    @XStreamAlias("MirPolFreeLkEndDt")
    private String mirPolFreeLkEndDt;
    @XStreamAlias("MirPolMiscSuspAmt")
    private String mirPolMiscSuspAmt;
    @XStreamAlias("MirPolGrsApremAmt")
    private String mirPolGrsApremAmt;
    @XStreamAlias("MirPolInsPurpCd")
    private String mirPolInsPurpCd;
    @XStreamAlias("MirPolInsTypCd")
    private String mirPolInsTypCd;
    @XStreamAlias("MirPolIssDtTypCd")
    private String mirPolIssDtTypCd;
    @XStreamAlias("MirPolIssEffDt")
    private String mirPolIssEffDt;
    @XStreamAlias("MirPolIssLocCd")
    private String mirPolIssLocCd;
    @XStreamAlias("MirPolLoanRepayAmt")
    private String mirPolLoanRepayAmt;
    @XStreamAlias("MirPolMailDt")
    private String mirPolMailDt;
    @XStreamAlias("MirPolMecCd")
    private String mirPolMecCd;
    @XStreamAlias("MirPolMibSignCd")
    private String mirPolMibSignCd;
    @XStreamAlias("MirPolMpremAmt")
    private String mirPolMpremAmt;
    @XStreamAlias("MirPolNfoCd")
    private String mirPolNfoCd;
    @XStreamAlias("MirPolNxtActvDt")
    private String mirPolNxtActvDt;
    @XStreamAlias("MirPolOptlCd")
    private String mirPolOptlCd;
    @XStreamAlias("MirPolPmtDrwDy")
    private String mirPolPmtDrwDy;
    @XStreamAlias("MirPolPdToDtNum")
    private String mirPolPdToDtNum;
    @XStreamAlias("MirPolPdfRfndCd")
    private String mirPolPdfRfndCd;
    @XStreamAlias("MirPolPfeeFct")
    private String mirPolPfeeFct;
    @XStreamAlias("MirPolPfeeRestrInd")
    private String mirPolPfeeRestrInd;
    @XStreamAlias("MirPolPnsnQualfCd")
    private String mirPolPnsnQualfCd;
    @XStreamAlias("MirPolPremDscntPct")
    private String mirPolPremDscntPct;
    @XStreamAlias("MirPolPremTypCd")
    private String mirPolPremTypCd;
    @XStreamAlias("MirPolRegFndSrcCd")
    private String mirPolRegFndSrcCd;
    @XStreamAlias("MirPolSndryAmt")
    private String mirPolSndryAmt;
    @XStreamAlias("MirPolSupresIssInd")
    private String mirPolSupresIssInd;
    @XStreamAlias("MirPolTxempCd")
    private String mirPolTxempCd;
    @XStreamAlias("MirPolVpoPuDur")
    private String mirPolVpoPuDur;
    @XStreamAlias("MirPrevServAgtId")
    private String mirPrevServAgtId;
    @XStreamAlias("MirRegSavPortnAmt")
    private String mirRegSavPortnAmt;
    @XStreamAlias("MirRegSavPortnCd")
    private String mirRegSavPortnCd;
    @XStreamAlias("MirSbsdryCoId")
    private String mirSbsdryCoId;
    @XStreamAlias("MirServAgtId")
    private String mirServAgtId;
    @XStreamAlias("MirServBrId")
    private String mirServBrId;
    @XStreamAlias("MirDvServAgtCliNm")
    private String mirDvServAgtCliNm;
    @XStreamAlias("MirSfbNxtBillAmt")
    private String mirSfbNxtBillAmt;
    @XStreamAlias("MirSfbSemiMthlyDy")
    private String mirSfbSemiMthlyDy;
    @XStreamAlias("MirTax1035AcbAmt")
    private String mirTax1035AcbAmt;
    @XStreamAlias("MirTaxRptFreqCd")
    private String mirTaxRptFreqCd;
    @XStreamAlias("MirUlInitPremAmt")
    private String mirUlInitPremAmt;
    @XStreamAlias("MirUnmtchMailInd")
    private String mirUnmtchMailInd;
    @XStreamAlias("MirUserId")
    private String mirUserId;
    @XStreamAlias("MirPolBusClasCd")
    private String mirPolBusClasCd;
    @XStreamAlias("MirPolSetlOptCd")
    private String mirPolSetlOptCd;
    @XStreamAlias("MirPolSetlOptDur")
    private String mirPolSetlOptDur;
    @XStreamAlias("MirPolDivXcesCd")
    private String mirPolDivXcesCd;
    @XStreamAlias("MirPolInvHistDt") 
    private String mirPolInvHistDt;
    @XStreamAlias("MirServFeeOpt")
    private String mirServFeeOpt;
    @XStreamAlias("MirOrigApremAmt")
    private String mirOrigApremAmt;
    @XStreamAlias("MirAppStatCd")
    private String mirAppStatCd;
    @XStreamAlias("MirNewAppPcDt") 
    private String mirNewAppPcDt;
    @XStreamAlias("MirPolCstatDesc")
    private String mirPolCstatDesc;
    @XStreamAlias("MirPolBillMdeDesc")
    private String mirPolBillMdeDesc;
    @XStreamAlias("MirPolBillTypDesc")
    private String mirPolBillTypDesc;
    @XStreamAlias("MirPolTpremAmt")
    private String mirPolTpremAmt;
    @XStreamAlias("MirOwnCliId")
    private String mirOwnCliId;
    @XStreamAlias("MirServAgtCliId")
    private String mirServAgtCliId;
    @XStreamAlias("MirServAgtBrNm")
    private String mirServAgtBrNm;
    @XStreamAlias("MirPolMailAddrInd")
    private String mirPolMailAddrInd;
    @XStreamAlias("MirPolMailOptInd")
    private String mirPolMailOptInd;
    @XStreamAlias("MirPolXtraRatInd")
    private String mirPolXtraRatInd;
    @XStreamAlias("MirOneTimePmtAmt")
    private String mirOneTimePmtAmt;
    @XStreamAlias("MirAppPdByLoanInd")
    private String mirAppPdByLoanInd;
    @XStreamAlias("MirAppPdByLoanAgtInd")
    private String mirAppPdByLoanAgtInd;
    @XStreamAlias("MirPolAppSgnLocCd")
    private String mirPolAppSgnLocCd;
    @XStreamAlias("MirPolSaleSrcCd")
    private String mirPolSaleSrcCd;
    @XStreamAlias("MirPolPrvsnRcptNum")
    private String mirPolPrvsnRcptNum;
    @XStreamAlias("MirPolPdAmt")
    private String mirPolPdAmt;
    @XStreamAlias("MirPolPdfInd")
    private String mirPolPdfInd;
    @XStreamAlias("MirPolAgtSignDt") 
    private String mirPolAgtSignDt;
    @XStreamAlias("MirPolEndBnftCd")
    private String mirPolEndBnftCd;
    @XStreamAlias("MirLifeAppFormTypCd")
    private String mirLifeAppFormTypCd;
    @XStreamAlias("MirXcesPmtVldtyInd")
    private String mirXcesPmtVldtyInd;
    @XStreamAlias("MirPolMedexInsrdInd")
    private String mirPolMedexInsrdInd;
    @XStreamAlias("MirPolMedexOwnerInd")
    private String mirPolMedexOwnerInd;
    @XStreamAlias("MirStaffAsstCoShrInd")
    private String mirStaffAsstCoShrInd;
    @XStreamAlias("MirPolSpclPdupBonOptCd")
    private String mirPolSpclPdupBonOptCd;
    @XStreamAlias("MirPolOwnerCd")
    private String mirPolOwnerCd;
    @XStreamAlias("MirThirdPartyInd")
    private String mirThirdPartyInd;
    @XStreamAlias("MirBenfOwnerInd")
    private String mirBenfOwnerInd;
    @XStreamAlias("MirPolFirstDueDt")
    private String mirPolFirstDueDt;
    @XStreamAlias("MirPolInitPymntInd")
    private String mirPolInitPymntInd;
    @XStreamAlias("MirPolPrvsnRcptDt") 
    private String mirPolPrvsnRcptDt;
    @XStreamAlias("MirPolPaymTypCsh")
    private String mirPolPaymTypCsh;
    @XStreamAlias("MirPolPaymTypChk")
    private String mirPolPaymTypChk;
    @XStreamAlias("MirPolPaymTypBkt")
    private String mirPolPaymTypBkt;
    @XStreamAlias("MirPolPaymTypCrd")
    private String mirPolPaymTypCrd;
    @XStreamAlias("MirPolApptypInd")
    private String mirPolApptypInd;
    @XStreamAlias("MirKnowInsrdCd1")
    private String mirKnowInsrdCd1;
    @XStreamAlias("MirKnowInsrdCd2")
    private String mirKnowInsrdCd2;
    @XStreamAlias("MirPolMaxGioInd")
    private String mirPolMaxGioInd;
    @XStreamAlias("MirPolRetenInd")
    private String mirPolRetenInd;
    @XStreamAlias("MirPolSmsInd")
    private String mirPolSmsInd;
    @XStreamAlias("MirPolEnoticeInd")
    private String mirPolEnoticeInd;
    @XStreamAlias("MirServBnkBrNm")
    private String mirServBnkBrNm;
    @XStreamAlias("MirPolPaymTypOth")
    private String mirPolPaymTypOth;
    @XStreamAlias("MirPtpdBthDt")
    private String mirPtpdBthDt;
    @XStreamAlias("MirEntityIncorpDt")
    private String mirEntityIncorpDt;
    @XStreamAlias("MirPolSmsOrNotifInd")
    private String mirPolSmsOrNotifInd;
    @XStreamAlias("MirPolEorNotifInd")
    private String mirPolEorNotifInd;
    @XStreamAlias("MirPolPrsrcAlwncInd")
    private String mirPolPrsrcAlwncInd;
    @XStreamAlias("MirPolPrsrcBusnsInd")
    private String mirPolPrsrcBusnsInd;
    @XStreamAlias("MirPolPrsrcPrfeeInd")
    private String mirPolPrsrcPrfeeInd;
    @XStreamAlias("MirPolPrsrcDonatInd")
    private String mirPolPrsrcDonatInd;
    @XStreamAlias("MirPolPrsrcInhrtInd")
    private String mirPolPrsrcInhrtInd;
    @XStreamAlias("MirPolPrsrcRremInd")
    private String mirPolPrsrcRremInd;
    @XStreamAlias("MirPolPrsrcRentInd")
    private String mirPolPrsrcRentInd;
    @XStreamAlias("MirPolPrsrcRetireInd")
    private String mirPolPrsrcRetireInd;
    @XStreamAlias("MirPolPrsrcSalaryInd")
    private String mirPolPrsrcSalaryInd;
    @XStreamAlias("MirPolPrsrcSoaInd")
    private String mirPolPrsrcSoaInd;
    @XStreamAlias("MirPolPrsrcSavingsInd")
    private String mirPolPrsrcSavingsInd;
    @XStreamAlias("MirPolPrsrcOtherInd")
    private String mirPolPrsrcOtherInd;
    @XStreamAlias("MirPolInsAppSgnDt")
    private String mirPolInsAppSgnDt;
    @XStreamAlias("MirPolPrtAppSgnDt")
    private String mirPolPrtAppSgnDt;
    @XStreamAlias("MirPolPaymTypDep")
    private String mirPolPaymTypDep;
    @XStreamAlias("MirBehlfTrxnInd")
    private String mirBehlfTrxnInd;
    @XStreamAlias("MirPolCntrctTypCd")
    private String mirPolCntrctTypCd;
    @XStreamAlias("MirPolPaymTypDepCsh")
    private String mirPolPaymTypDepCsh;
    @XStreamAlias("MirPolPaymTypDepChk")
    private String mirPolPaymTypDepChk;
    @XStreamAlias("MirPepDclrInd")
    private String mirPepDclrInd;

    public MirPolicyID getPolicyID() {
        return mirPolicyId;
    }

    public PolicyData setPolicyID(final MirPolicyID mirPolicyId) {
        this.mirPolicyId = mirPolicyId;
        return this;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public PolicyData setSourceSystem(final String sourceSystem) {
        this.sourceSystem = sourceSystem;
        return this;
    }

    public String getMirDvFutDtTrxnTypCd() {
        return mirDvFutDtTrxnTypCd;
    }

    public void setMirDvFutDtTrxnTypCd(String mirDvFutDtTrxnTypCd) {
        this.mirDvFutDtTrxnTypCd = mirDvFutDtTrxnTypCd;
    }

    public String getMirAcruCalcTimeCd() {
        return mirAcruCalcTimeCd;
    }

    public void setMirAcruCalcTimeCd(String mirAcruCalcTimeCd) {
        this.mirAcruCalcTimeCd = mirAcruCalcTimeCd;
    }

    public String getMirAppFormTypId() {
        return mirAppFormTypId;
    }

    public void setMirAppFormTypId(String mirAppFormTypId) {
        this.mirAppFormTypId = mirAppFormTypId;
    }

    public String getMirAutoPremPmtInd() {
        return mirAutoPremPmtInd;
    }

    public void setMirAutoPremPmtInd(String mirAutoPremPmtInd) {
        this.mirAutoPremPmtInd = mirAutoPremPmtInd;
    }

    public String getMirBillLeadTimeDur() {
        return mirBillLeadTimeDur;
    }

    public void setMirBillLeadTimeDur(String mirBillLeadTimeDur) {
        this.mirBillLeadTimeDur = mirBillLeadTimeDur;
    }

    public String getMirCliCrntLocCd() {
        return mirCliCrntLocCd;
    }

    public void setMirCliCrntLocCd(String mirCliCrntLocCd) {
        this.mirCliCrntLocCd = mirCliCrntLocCd;
    }

    public MirClientIDGroup getMirCliIdG() {
        return mirCliIdG;
    }

    public void setMirCliIdG(MirClientIDGroup mirCliIdG) {
        this.mirCliIdG = mirCliIdG;
    }

    public MirDvOwnSubCdG getMirDvOwnSubCdG() {
        return mirDvOwnSubCdG;
    }

    public void setMirDvOwnSubCdG(MirDvOwnSubCdG mirDvOwnSubCdG) {
        this.mirDvOwnSubCdG = mirDvOwnSubCdG;
    }

    public MirPolCliInsrdCdG getMirPolCliInsrdCdG() {
        return mirPolCliInsrdCdG;
    }

    public void setMirPolCliInsrdCdG(MirPolCliInsrdCdG mirPolCliInsrdCdG) {
        this.mirPolCliInsrdCdG = mirPolCliInsrdCdG;
    }

    public MirCliAddrTypCdG getMirCliAddrTypCdG() {
        return mirCliAddrTypCdG;
    }

    public void setMirCliAddrTypCdG(MirCliAddrTypCdG mirCliAddrTypCdG) {
        this.mirCliAddrTypCdG = mirCliAddrTypCdG;
    }

    public MirDvOwnCliNmG getMirDvOwnCliNmG() {
        return mirDvOwnCliNmG;
    }

    public void setMirDvOwnCliNmG(MirDvOwnCliNmG mirDvOwnCliNmG) {
        this.mirDvOwnCliNmG = mirDvOwnCliNmG;
    }

    public MirClientBirthDateGroup getMirClientBirthDateGroup() {
        return mirClientBirthDateGroup;
    }

    public void setMirClientBirthDateGroup(MirClientBirthDateGroup mirClientBirthDateGroup) {
        this.mirClientBirthDateGroup = mirClientBirthDateGroup;
    }

    public MirCliTaxIdG getMirCliTaxIdG() {
        return mirCliTaxIdG;
    }

    public void setMirCliTaxIdG(MirCliTaxIdG mirCliTaxIdG) {
        this.mirCliTaxIdG = mirCliTaxIdG;
    }

    public MirDvAsignCliIdG getMirDvAsignCliIdG() {
        return mirDvAsignCliIdG;
    }

    public void setMirDvAsignCliIdG(MirDvAsignCliIdG mirDvAsignCliIdG) {
        this.mirDvAsignCliIdG = mirDvAsignCliIdG;
    }

    public MirDvAsignSubCdG getMirDvAsignSubCdG() {
        return mirDvAsignSubCdG;
    }

    public void setMirDvAsignSubCdG(MirDvAsignSubCdG mirDvAsignSubCdG) {
        this.mirDvAsignSubCdG = mirDvAsignSubCdG;
    }

    public MirDvAsignAddrCdG getMirDvAsignAddrCdG() {
        return mirDvAsignAddrCdG;
    }

    public void setMirDvAsignAddrCdG(MirDvAsignAddrCdG mirDvAsignAddrCdG) {
        this.mirDvAsignAddrCdG = mirDvAsignAddrCdG;
    }

    public MirPolCliAsignDtG getMirPolCliAsignDtG() {
        return mirPolCliAsignDtG;
    }

    public void setMirPolCliAsignDtG(MirPolCliAsignDtG mirPolCliAsignDtG) {
        this.mirPolCliAsignDtG = mirPolCliAsignDtG;
    }

    public MirDvAsignCliNmG getMirDvAsignCliNmG() {
        return mirDvAsignCliNmG;
    }

    public void setMirDvAsignCliNmG(MirDvAsignCliNmG mirDvAsignCliNmG) {
        this.mirDvAsignCliNmG = mirDvAsignCliNmG;
    }

    public MirDvAnutntCliIdG getMirDvAnutntCliIdG() {
        return mirDvAnutntCliIdG;
    }

    public void setMirDvAnutntCliIdG(MirDvAnutntCliIdG mirDvAnutntCliIdG) {
        this.mirDvAnutntCliIdG = mirDvAnutntCliIdG;
    }

    public MirDvAnutntSubCdG getMirDvAnutntSubCdG() {
        return mirDvAnutntSubCdG;
    }

    public void setMirDvAnutntSubCdG(MirDvAnutntSubCdG mirDvAnutntSubCdG) {
        this.mirDvAnutntSubCdG = mirDvAnutntSubCdG;
    }

    public MirDvAnutntAddrCdG getMirDvAnutntAddrCdG() {
        return mirDvAnutntAddrCdG;
    }

    public void setMirDvAnutntAddrCdG(MirDvAnutntAddrCdG mirDvAnutntAddrCdG) {
        this.mirDvAnutntAddrCdG = mirDvAnutntAddrCdG;
    }

    public MirDvAnutntCliNmG getMirDvAnutntCliNmG() {
        return mirDvAnutntCliNmG;
    }

    public void setMirDvAnutntCliNmG(MirDvAnutntCliNmG mirDvAnutntCliNmG) {
        this.mirDvAnutntCliNmG = mirDvAnutntCliNmG;
    }

    public MirDvIpartyCliIdG getMirDvIpartyCliIdG() {
        return mirDvIpartyCliIdG;
    }

    public void setMirDvIpartyCliIdG(MirDvIpartyCliIdG mirDvIpartyCliIdG) {
        this.mirDvIpartyCliIdG = mirDvIpartyCliIdG;
    }

    public MirDvIpartyAddrCdG getMirDvIpartyAddrCdG() {
        return mirDvIpartyAddrCdG;
    }

    public void setMirDvIpartyAddrCdG(MirDvIpartyAddrCdG mirDvIpartyAddrCdG) {
        this.mirDvIpartyAddrCdG = mirDvIpartyAddrCdG;
    }

    public MirDvIpartyCliNmG getMirDvIpartyCliNmG() {
        return mirDvIpartyCliNmG;
    }

    public void setMirDvIpartyCliNmG(MirDvIpartyCliNmG mirDvIpartyCliNmG) {
        this.mirDvIpartyCliNmG = mirDvIpartyCliNmG;
    }

    public MirDvTrstCliIdG getMirDvTrstCliIdG() {
        return mirDvTrstCliIdG;
    }

    public void setMirDvTrstCliIdG(MirDvTrstCliIdG mirDvTrstCliIdG) {
        this.mirDvTrstCliIdG = mirDvTrstCliIdG;
    }

    public MirDvTrstAddrCdG getMirDvTrstAddrCdG() {
        return mirDvTrstAddrCdG;
    }

    public void setMirDvTrstAddrCdG(MirDvTrstAddrCdG mirDvTrstAddrCdG) {
        this.mirDvTrstAddrCdG = mirDvTrstAddrCdG;
    }

    public MirDvTrstCliNmG getMirDvTrstCliNmG() {
        return mirDvTrstCliNmG;
    }

    public void setMirDvTrstCliNmG(MirDvTrstCliNmG mirDvTrstCliNmG) {
        this.mirDvTrstCliNmG = mirDvTrstCliNmG;
    }

    public String getMirClmPayePolId() {
        return mirClmPayePolId;
    }

    public void setMirClmPayePolId(String mirClmPayePolId) {
        this.mirClmPayePolId = mirClmPayePolId;
    }

    public String getMirClmPayoMthdCd() {
        return mirClmPayoMthdCd;
    }

    public void setMirClmPayoMthdCd(String mirClmPayoMthdCd) {
        this.mirClmPayoMthdCd = mirClmPayoMthdCd;
    }

    public String getMirConnOptlPolId() {
        return mirConnOptlPolId;
    }

    public void setMirConnOptlPolId(String mirConnOptlPolId) {
        this.mirConnOptlPolId = mirConnOptlPolId;
    }

    public MirAgentIDGroup getMirAgtIdG() {
        return mirAgtIdG;
    }

    public void setMirAgtIdG(MirAgentIDGroup mirAgtIdG) {
        this.mirAgtIdG = mirAgtIdG;
    }

    public MirPolAgtShrPctG getMirPolAgtShrPctG() {
        return mirPolAgtShrPctG;
    }

    public void setMirPolAgtShrPctG(MirPolAgtShrPctG mirPolAgtShrPctG) {
        this.mirPolAgtShrPctG = mirPolAgtShrPctG;
    }

    public MirDvAgtCliNmG getMirDvAgtCliNmG() {
        return mirDvAgtCliNmG;
    }

    public void setMirDvAgtCliNmG(MirDvAgtCliNmG mirDvAgtCliNmG) {
        this.mirDvAgtCliNmG = mirDvAgtCliNmG;
    }

    public String getMirDvOwnCliNm() {
        return mirDvOwnCliNm;
    }

    public void setMirDvOwnCliNm(String mirDvOwnCliNm) {
        this.mirDvOwnCliNm = mirDvOwnCliNm;
    }

    public String getMirGainRptTimeCd() {
        return mirGainRptTimeCd;
    }

    public void setMirGainRptTimeCd(String mirGainRptTimeCd) {
        this.mirGainRptTimeCd = mirGainRptTimeCd;
    }

    public String getMirLinkPolIssInd() {
        return mirLinkPolIssInd;
    }

    public void setMirLinkPolIssInd(String mirLinkPolIssInd) {
        this.mirLinkPolIssInd = mirLinkPolIssInd;
    }

    public String getMirLoanStringDedblInd() {
        return mirLoanIntDedblInd;
    }

    public void setMirLoanStringDedblInd(String mirLoanIntDedblInd) {
        this.mirLoanIntDedblInd = mirLoanIntDedblInd;
    }

    public String getMirLxpctCalcCd() {
        return mirLxpctCalcCd;
    }

    public void setMirLxpctCalcCd(String mirLxpctCalcCd) {
        this.mirLxpctCalcCd = mirLxpctCalcCd;
    }

    public String getMirLxpctRecalcInd() {
        return mirLxpctRecalcInd;
    }

    public void setMirLxpctRecalcInd(String mirLxpctRecalcInd) {
        this.mirLxpctRecalcInd = mirLxpctRecalcInd;
    }

    public String getMirModeFctRestrInd() {
        return mirModeFctRestrInd;
    }

    public void setMirModeFctRestrInd(String mirModeFctRestrInd) {
        this.mirModeFctRestrInd = mirModeFctRestrInd;
    }

    public String getMirMpremDscntAmt() {
        return mirMpremDscntAmt;
    }

    public void setMirMpremDscntAmt(String mirMpremDscntAmt) {
        this.mirMpremDscntAmt = mirMpremDscntAmt;
    }

    public String getMirPremRecalcOptCd() {
        return mirPremRecalcOptCd;
    }

    public void setMirPremRecalcOptCd(String mirPremRecalcOptCd) {
        this.mirPremRecalcOptCd = mirPremRecalcOptCd;
    }

    public String getMirNxtActvTypCd() {
        return mirNxtActvTypCd;
    }

    public void setMirNxtActvTypCd(String mirNxtActvTypCd) {
        this.mirNxtActvTypCd = mirNxtActvTypCd;
    }

    public String getMirPlanID() {
        return mirPlanId;
    }

    public PolicyData setMirPlanId(String mirPlanId) {
        this.mirPlanId = mirPlanId;
        return this;
    }

    public String getMirPlanPeriPremAmt() {
        return mirPlanPeriPremAmt;
    }

    public void setMirPlanPeriPremAmt(String mirPlanPeriPremAmt) {
        this.mirPlanPeriPremAmt = mirPlanPeriPremAmt;
    }

    public String getMirPolAnpayo1Amt() {
        return mirPolAnpayo1Amt;
    }

    public void setMirPolAnpayo1Amt(String mirPolAnpayo1Amt) {
        this.mirPolAnpayo1Amt = mirPolAnpayo1Amt;
    }

    public String getMirPolAnpayo2Amt() {
        return mirPolAnpayo2Amt;
    }

    public void setMirPolAnpayo2Amt(String mirPolAnpayo2Amt) {
        this.mirPolAnpayo2Amt = mirPolAnpayo2Amt;
    }

    public String getMirPolAnpayoModeCd() {
        return mirPolAnpayoModeCd;
    }

    public void setMirPolAnpayoModeCd(String mirPolAnpayoModeCd) {
        this.mirPolAnpayoModeCd = mirPolAnpayoModeCd;
    }

    public String getMirPolAplInd() {
        return mirPolAplInd;
    }

    public void setMirPolAplInd(String mirPolAplInd) {
        this.mirPolAplInd = mirPolAplInd;
    }

    public String getMirPolAppCmpltDt() {
        return mirPolAppCmpltDt;
    }

    public void setMirPolAppCmpltDt(String mirPolAppCmpltDt) {
        this.mirPolAppCmpltDt = mirPolAppCmpltDt;
    }

    public String getMirPolAppCtr() {
        return mirPolAppCtr;
    }

    public void setMirPolAppCtr(String mirPolAppCtr) {
        this.mirPolAppCtr = mirPolAppCtr;
    }

    public String getMirPolAppFormId() {
        return mirPolAppFormId;
    }

    public void setMirPolAppFormId(String mirPolAppFormId) {
        this.mirPolAppFormId = mirPolAppFormId;
    }

    public String getMirPolAppRecvDt() {
        return mirPolAppRecvDt;
    }

    public void setMirPolAppRecvDt(String mirPolAppRecvDt) {
        this.mirPolAppRecvDt = mirPolAppRecvDt;
    }

    public String getMirPolAppSignDt() {
        return mirPolAppSignDt;
    }

    public void setMirPolAppSignDt(String mirPolAppSignDt) {
        this.mirPolAppSignDt = mirPolAppSignDt;
    }

    public String getMirPolAppSignInd() {
        return mirPolAppSignInd;
    }

    public void setMirPolAppSignInd(String mirPolAppSignInd) {
        this.mirPolAppSignInd = mirPolAppSignInd;
    }

    public String getMirPolBillModeCd() {
        return mirPolBillModeCd;
    }

    public void setMirPolBillModeCd(String mirPolBillModeCd) {
        this.mirPolBillModeCd = mirPolBillModeCd;
    }

    public String getMirPolBillTypCd() {
        return mirPolBillTypCd;
    }

    public void setMirPolBillTypCd(String mirPolBillTypCd) {
        this.mirPolBillTypCd = mirPolBillTypCd;
    }

    public String getMirPolBillToDtNum() {
        return mirPolBillToDtNum;
    }

    public void setMirPolBillToDtNum(String mirPolBillToDtNum) {
        this.mirPolBillToDtNum = mirPolBillToDtNum;
    }

    public String getMirPolCeasDt() {
        return mirPolCeasDt;
    }

    public void setMirPolCeasDt(String mirPolCeasDt) {
        this.mirPolCeasDt = mirPolCeasDt;
    }

    public String getMirPolCeasReasnCd() {
        return mirPolCeasReasnCd;
    }

    public void setMirPolCeasReasnCd(String mirPolCeasReasnCd) {
        this.mirPolCeasReasnCd = mirPolCeasReasnCd;
    }

    public String getMirPolCnfdInd() {
        return mirPolCnfdInd;
    }

    public void setMirPolCnfdInd(String mirPolCnfdInd) {
        this.mirPolCnfdInd = mirPolCnfdInd;
    }

    public String getMirPolCrcyCd() {
        return mirPolCrcyCd;
    }

    public void setMirPolCrcyCd(String mirPolCrcyCd) {
        this.mirPolCrcyCd = mirPolCrcyCd;
    }

    public String getMirPolCrntModeFct() {
        return mirPolCrntModeFct;
    }

    public void setMirPolCrntModeFct(String mirPolCrntModeFct) {
        this.mirPolCrntModeFct = mirPolCrntModeFct;
    }

    public String getMirPolCstatCd() {
        return mirPolCstatCd;
    }

    public void setMirPolCstatCd(String mirPolCstatCd) {
        this.mirPolCstatCd = mirPolCstatCd;
    }

    public String getMirPolCtryCd() {
        return mirPolCtryCd;
    }

    public void setMirPolCtryCd(String mirPolCtryCd) {
        this.mirPolCtryCd = mirPolCtryCd;
    }

    public String getMirPolCvgRecCtr() {
        return mirPolCvgRecCtr;
    }

    public void setMirPolCvgRecCtr(String mirPolCvgRecCtr) {
        this.mirPolCvgRecCtr = mirPolCvgRecCtr;
    }

    public String getMirPolDbOptCd() {
        return mirPolDbOptCd;
    }

    public void setMirPolDbOptCd(String mirPolDbOptCd) {
        this.mirPolDbOptCd = mirPolDbOptCd;
    }

    public String getMirPolDivOptCd() {
        return mirPolDivOptCd;
    }

    public void setMirPolDivOptCd(String mirPolDivOptCd) {
        this.mirPolDivOptCd = mirPolDivOptCd;
    }

    public String getMirPolEnhcNelctQty() {
        return mirPolEnhcNelctQty;
    }

    public void setMirPolEnhcNelctQty(String mirPolEnhcNelctQty) {
        this.mirPolEnhcNelctQty = mirPolEnhcNelctQty;
    }

    public String getMirPolFinalDispDt() {
        return mirPolFinalDispDt;
    }

    public void setMirPolFinalDispDt(String mirPolFinalDispDt) {
        this.mirPolFinalDispDt = mirPolFinalDispDt;
    }

    public String getMirPolFreeLkEndDt() {
        return mirPolFreeLkEndDt;
    }

    public void setMirPolFreeLkEndDt(String mirPolFreeLkEndDt) {
        this.mirPolFreeLkEndDt = mirPolFreeLkEndDt;
    }

    public String getMirPolMiscSuspAmt() {
        return mirPolMiscSuspAmt;
    }

    public void setMirPolMiscSuspAmt(String mirPolMiscSuspAmt) {
        this.mirPolMiscSuspAmt = mirPolMiscSuspAmt;
    }

    public String getMirPolGrsApremAmt() {
        return mirPolGrsApremAmt;
    }

    public void setMirPolGrsApremAmt(String mirPolGrsApremAmt) {
        this.mirPolGrsApremAmt = mirPolGrsApremAmt;
    }

    public String getMirPolInsPurpCd() {
        return mirPolInsPurpCd;
    }

    public void setMirPolInsPurpCd(String mirPolInsPurpCd) {
        this.mirPolInsPurpCd = mirPolInsPurpCd;
    }

    public String getMirPolInsTypCd() {
        return mirPolInsTypCd;
    }

    public void setMirPolInsTypCd(String mirPolInsTypCd) {
        this.mirPolInsTypCd = mirPolInsTypCd;
    }

    public String getMirPolIssDtTypCd() {
        return mirPolIssDtTypCd;
    }

    public void setMirPolIssDtTypCd(String mirPolIssDtTypCd) {
        this.mirPolIssDtTypCd = mirPolIssDtTypCd;
    }

    public String getMirPolIssEffDt() {
        return mirPolIssEffDt;
    }

    public void setMirPolIssEffDt(String mirPolIssEffDt) {
        this.mirPolIssEffDt = mirPolIssEffDt;
    }

    public String getMirPolIssLocCd() {
        return mirPolIssLocCd;
    }

    public void setMirPolIssLocCd(String mirPolIssLocCd) {
        this.mirPolIssLocCd = mirPolIssLocCd;
    }

    public String getMirPolLoanRepayAmt() {
        return mirPolLoanRepayAmt;
    }

    public void setMirPolLoanRepayAmt(String mirPolLoanRepayAmt) {
        this.mirPolLoanRepayAmt = mirPolLoanRepayAmt;
    }

    public String getMirPolMailDt() {
        return mirPolMailDt;
    }

    public void setMirPolMailDt(String mirPolMailDt) {
        this.mirPolMailDt = mirPolMailDt;
    }

    public String getMirPolMecCd() {
        return mirPolMecCd;
    }

    public void setMirPolMecCd(String mirPolMecCd) {
        this.mirPolMecCd = mirPolMecCd;
    }

    public String getMirPolMibSignCd() {
        return mirPolMibSignCd;
    }

    public void setMirPolMibSignCd(String mirPolMibSignCd) {
        this.mirPolMibSignCd = mirPolMibSignCd;
    }

    public String getMirPolMpremAmt() {
        return mirPolMpremAmt;
    }

    public void setMirPolMpremAmt(String mirPolMpremAmt) {
        this.mirPolMpremAmt = mirPolMpremAmt;
    }

    public String getMirPolNfoCd() {
        return mirPolNfoCd;
    }

    public void setMirPolNfoCd(String mirPolNfoCd) {
        this.mirPolNfoCd = mirPolNfoCd;
    }

    public String getMirPolNxtActvDt() {
        return mirPolNxtActvDt;
    }

    public void setMirPolNxtActvDt(String mirPolNxtActvDt) {
        this.mirPolNxtActvDt = mirPolNxtActvDt;
    }

    public String getMirPolOptlCd() {
        return mirPolOptlCd;
    }

    public void setMirPolOptlCd(String mirPolOptlCd) {
        this.mirPolOptlCd = mirPolOptlCd;
    }

    public String getMirPolPmtDrwDy() {
        return mirPolPmtDrwDy;
    }

    public void setMirPolPmtDrwDy(String mirPolPmtDrwDy) {
        this.mirPolPmtDrwDy = mirPolPmtDrwDy;
    }

    public String getMirPolPdToDtNum() {
        return mirPolPdToDtNum;
    }

    public void setMirPolPdToDtNum(String mirPolPdToDtNum) {
        this.mirPolPdToDtNum = mirPolPdToDtNum;
    }

    public String getMirPolPdfRfndCd() {
        return mirPolPdfRfndCd;
    }

    public void setMirPolPdfRfndCd(String mirPolPdfRfndCd) {
        this.mirPolPdfRfndCd = mirPolPdfRfndCd;
    }

    public String getMirPolPfeeFct() {
        return mirPolPfeeFct;
    }

    public void setMirPolPfeeFct(String mirPolPfeeFct) {
        this.mirPolPfeeFct = mirPolPfeeFct;
    }

    public String getMirPolPfeeRestrInd() {
        return mirPolPfeeRestrInd;
    }

    public void setMirPolPfeeRestrInd(String mirPolPfeeRestrInd) {
        this.mirPolPfeeRestrInd = mirPolPfeeRestrInd;
    }

    public String getMirPolPnsnQualfCd() {
        return mirPolPnsnQualfCd;
    }

    public void setMirPolPnsnQualfCd(String mirPolPnsnQualfCd) {
        this.mirPolPnsnQualfCd = mirPolPnsnQualfCd;
    }

    public String getMirPolPremDscntPct() {
        return mirPolPremDscntPct;
    }

    public void setMirPolPremDscntPct(String mirPolPremDscntPct) {
        this.mirPolPremDscntPct = mirPolPremDscntPct;
    }

    public String getMirPolPremTypCd() {
        return mirPolPremTypCd;
    }

    public void setMirPolPremTypCd(String mirPolPremTypCd) {
        this.mirPolPremTypCd = mirPolPremTypCd;
    }

    public String getMirPolRegFndSrcCd() {
        return mirPolRegFndSrcCd;
    }

    public void setMirPolRegFndSrcCd(String mirPolRegFndSrcCd) {
        this.mirPolRegFndSrcCd = mirPolRegFndSrcCd;
    }

    public String getMirPolSndryAmt() {
        return mirPolSndryAmt;
    }

    public void setMirPolSndryAmt(String mirPolSndryAmt) {
        this.mirPolSndryAmt = mirPolSndryAmt;
    }

    public String getMirPolSupresIssInd() {
        return mirPolSupresIssInd;
    }

    public void setMirPolSupresIssInd(String mirPolSupresIssInd) {
        this.mirPolSupresIssInd = mirPolSupresIssInd;
    }

    public String getMirPolTxempCd() {
        return mirPolTxempCd;
    }

    public void setMirPolTxempCd(String mirPolTxempCd) {
        this.mirPolTxempCd = mirPolTxempCd;
    }

    public String getMirPolVpoPuDur() {
        return mirPolVpoPuDur;
    }

    public void setMirPolVpoPuDur(String mirPolVpoPuDur) {
        this.mirPolVpoPuDur = mirPolVpoPuDur;
    }

    public String getMirPrevServAgtId() {
        return mirPrevServAgtId;
    }

    public void setMirPrevServAgtId(String mirPrevServAgtId) {
        this.mirPrevServAgtId = mirPrevServAgtId;
    }

    public String getMirRegSavPortnAmt() {
        return mirRegSavPortnAmt;
    }

    public void setMirRegSavPortnAmt(String mirRegSavPortnAmt) {
        this.mirRegSavPortnAmt = mirRegSavPortnAmt;
    }

    public String getMirRegSavPortnCd() {
        return mirRegSavPortnCd;
    }

    public void setMirRegSavPortnCd(String mirRegSavPortnCd) {
        this.mirRegSavPortnCd = mirRegSavPortnCd;
    }

    public String getMirSbsdryCoId() {
        return mirSbsdryCoId;
    }

    public void setMirSbsdryCoId(String mirSbsdryCoId) {
        this.mirSbsdryCoId = mirSbsdryCoId;
    }

    public String getMirServAgtId() {
        return mirServAgtId;
    }

    public void setMirServAgtId(String mirServAgtId) {
        this.mirServAgtId = mirServAgtId;
    }

    public String getMirServBrId() {
        return mirServBrId;
    }

    public void setMirServBrId(String mirServBrId) {
        this.mirServBrId = mirServBrId;
    }

    public String getMirDvServAgtCliNm() {
        return mirDvServAgtCliNm;
    }

    public void setMirDvServAgtCliNm(String mirDvServAgtCliNm) {
        this.mirDvServAgtCliNm = mirDvServAgtCliNm;
    }

    public String getMirSfbNxtBillAmt() {
        return mirSfbNxtBillAmt;
    }

    public void setMirSfbNxtBillAmt(String mirSfbNxtBillAmt) {
        this.mirSfbNxtBillAmt = mirSfbNxtBillAmt;
    }

    public String getMirSfbSemiMthlyDy() {
        return mirSfbSemiMthlyDy;
    }

    public void setMirSfbSemiMthlyDy(String mirSfbSemiMthlyDy) {
        this.mirSfbSemiMthlyDy = mirSfbSemiMthlyDy;
    }

    public String getMirTax1035AcbAmt() {
        return mirTax1035AcbAmt;
    }

    public void setMirTax1035AcbAmt(String mirTax1035AcbAmt) {
        this.mirTax1035AcbAmt = mirTax1035AcbAmt;
    }

    public String getMirTaxRptFreqCd() {
        return mirTaxRptFreqCd;
    }

    public void setMirTaxRptFreqCd(String mirTaxRptFreqCd) {
        this.mirTaxRptFreqCd = mirTaxRptFreqCd;
    }

    public String getMirUlInitPremAmt() {
        return mirUlInitPremAmt;
    }

    public void setMirUlInitPremAmt(String mirUlInitPremAmt) {
        this.mirUlInitPremAmt = mirUlInitPremAmt;
    }

    public String getMirUnmtchMailInd() {
        return mirUnmtchMailInd;
    }

    public void setMirUnmtchMailInd(String mirUnmtchMailInd) {
        this.mirUnmtchMailInd = mirUnmtchMailInd;
    }

    public String getMirUserId() {
        return mirUserId;
    }

    public void setMirUserId(String mirUserId) {
        this.mirUserId = mirUserId;
    }

    public String getMirPolBusClasCd() {
        return mirPolBusClasCd;
    }

    public void setMirPolBusClasCd(String mirPolBusClasCd) {
        this.mirPolBusClasCd = mirPolBusClasCd;
    }

    public String getMirPolSetlOptCd() {
        return mirPolSetlOptCd;
    }

    public void setMirPolSetlOptCd(String mirPolSetlOptCd) {
        this.mirPolSetlOptCd = mirPolSetlOptCd;
    }

    public String getMirPolSetlOptDur() {
        return mirPolSetlOptDur;
    }

    public void setMirPolSetlOptDur(String mirPolSetlOptDur) {
        this.mirPolSetlOptDur = mirPolSetlOptDur;
    }

    public String getMirPolDivXcesCd() {
        return mirPolDivXcesCd;
    }

    public void setMirPolDivXcesCd(String mirPolDivXcesCd) {
        this.mirPolDivXcesCd = mirPolDivXcesCd;
    }

    public String getMirPolInvHistDt() {
        return mirPolInvHistDt;
    }

    public void setMirPolInvHistDt(String mirPolInvHistDt) {
        this.mirPolInvHistDt = mirPolInvHistDt;
    }

    public String getMirServFeeOpt() {
        return mirServFeeOpt;
    }

    public void setMirServFeeOpt(String mirServFeeOpt) {
        this.mirServFeeOpt = mirServFeeOpt;
    }

    public String getMirOrigApremAmt() {
        return mirOrigApremAmt;
    }

    public void setMirOrigApremAmt(String mirOrigApremAmt) {
        this.mirOrigApremAmt = mirOrigApremAmt;
    }

    public String getMirAppStatCd() {
        return mirAppStatCd;
    }

    public void setMirAppStatCd(String mirAppStatCd) {
        this.mirAppStatCd = mirAppStatCd;
    }

    public String getMirNewAppPcDt() {
        return mirNewAppPcDt;
    }

    public void setMirNewAppPcDt(String mirNewAppPcDt) {
        this.mirNewAppPcDt = mirNewAppPcDt;
    }

    public String getMirPolCstatDesc() {
        return mirPolCstatDesc;
    }

    public void setMirPolCstatDesc(String mirPolCstatDesc) {
        this.mirPolCstatDesc = mirPolCstatDesc;
    }

    public String getMirPolBillMdeDesc() {
        return mirPolBillMdeDesc;
    }

    public void setMirPolBillMdeDesc(String mirPolBillMdeDesc) {
        this.mirPolBillMdeDesc = mirPolBillMdeDesc;
    }

    public String getMirPolBillTypDesc() {
        return mirPolBillTypDesc;
    }

    public void setMirPolBillTypDesc(String mirPolBillTypDesc) {
        this.mirPolBillTypDesc = mirPolBillTypDesc;
    }

    public String getMirPolTpremAmt() {
        return mirPolTpremAmt;
    }

    public void setMirPolTpremAmt(String mirPolTpremAmt) {
        this.mirPolTpremAmt = mirPolTpremAmt;
    }

    public String getMirOwnCliId() {
        return mirOwnCliId;
    }

    public void setMirOwnCliId(String mirOwnCliId) {
        this.mirOwnCliId = mirOwnCliId;
    }

    public String getMirServAgtCliId() {
        return mirServAgtCliId;
    }

    public void setMirServAgtCliId(String mirServAgtCliId) {
        this.mirServAgtCliId = mirServAgtCliId;
    }

    public String getMirServAgtBrNm() {
        return mirServAgtBrNm;
    }

    public void setMirServAgtBrNm(String mirServAgtBrNm) {
        this.mirServAgtBrNm = mirServAgtBrNm;
    }

    public String getMirPolMailAddrInd() {
        return mirPolMailAddrInd;
    }

    public void setMirPolMailAddrInd(String mirPolMailAddrInd) {
        this.mirPolMailAddrInd = mirPolMailAddrInd;
    }

    public String getMirPolMailOptInd() {
        return mirPolMailOptInd;
    }

    public void setMirPolMailOptInd(String mirPolMailOptInd) {
        this.mirPolMailOptInd = mirPolMailOptInd;
    }

    public String getMirPolXtraRatInd() {
        return mirPolXtraRatInd;
    }

    public void setMirPolXtraRatInd(String mirPolXtraRatInd) {
        this.mirPolXtraRatInd = mirPolXtraRatInd;
    }

    public String getMirOneTimePmtAmt() {
        return mirOneTimePmtAmt;
    }

    public void setMirOneTimePmtAmt(String mirOneTimePmtAmt) {
        this.mirOneTimePmtAmt = mirOneTimePmtAmt;
    }

    public String getMirAppPdByLoanInd() {
        return mirAppPdByLoanInd;
    }

    public void setMirAppPdByLoanInd(String mirAppPdByLoanInd) {
        this.mirAppPdByLoanInd = mirAppPdByLoanInd;
    }

    public String getMirAppPdByLoanAgtInd() {
        return mirAppPdByLoanAgtInd;
    }

    public void setMirAppPdByLoanAgtInd(String mirAppPdByLoanAgtInd) {
        this.mirAppPdByLoanAgtInd = mirAppPdByLoanAgtInd;
    }

    public String getMirPolAppSgnLocCd() {
        return mirPolAppSgnLocCd;
    }

    public void setMirPolAppSgnLocCd(String mirPolAppSgnLocCd) {
        this.mirPolAppSgnLocCd = mirPolAppSgnLocCd;
    }

    public String getMirPolSaleSrcCd() {
        return mirPolSaleSrcCd;
    }

    public void setMirPolSaleSrcCd(String mirPolSaleSrcCd) {
        this.mirPolSaleSrcCd = mirPolSaleSrcCd;
    }

    public String getMirPolPrvsnRcptNum() {
        return mirPolPrvsnRcptNum;
    }

    public void setMirPolPrvsnRcptNum(String mirPolPrvsnRcptNum) {
        this.mirPolPrvsnRcptNum = mirPolPrvsnRcptNum;
    }

    public String getMirPolPdAmt() {
        return mirPolPdAmt;
    }

    public void setMirPolPdAmt(String mirPolPdAmt) {
        this.mirPolPdAmt = mirPolPdAmt;
    }

    public String getMirPolPdfInd() {
        return mirPolPdfInd;
    }

    public void setMirPolPdfInd(String mirPolPdfInd) {
        this.mirPolPdfInd = mirPolPdfInd;
    }

    public String getMirPolAgtSignDt() {
        return mirPolAgtSignDt;
    }

    public void setMirPolAgtSignDt(String mirPolAgtSignDt) {
        this.mirPolAgtSignDt = mirPolAgtSignDt;
    }

    public String getMirPolEndBnftCd() {
        return mirPolEndBnftCd;
    }

    public void setMirPolEndBnftCd(String mirPolEndBnftCd) {
        this.mirPolEndBnftCd = mirPolEndBnftCd;
    }

    public String getMirLifeAppFormTypCd() {
        return mirLifeAppFormTypCd;
    }

    public void setMirLifeAppFormTypCd(String mirLifeAppFormTypCd) {
        this.mirLifeAppFormTypCd = mirLifeAppFormTypCd;
    }

    public String getMirXcesPmtVldtyInd() {
        return mirXcesPmtVldtyInd;
    }

    public void setMirXcesPmtVldtyInd(String mirXcesPmtVldtyInd) {
        this.mirXcesPmtVldtyInd = mirXcesPmtVldtyInd;
    }

    public String getMirPolMedexInsrdInd() {
        return mirPolMedexInsrdInd;
    }

    public void setMirPolMedexInsrdInd(String mirPolMedexInsrdInd) {
        this.mirPolMedexInsrdInd = mirPolMedexInsrdInd;
    }

    public String getMirPolMedexOwnerInd() {
        return mirPolMedexOwnerInd;
    }

    public void setMirPolMedexOwnerInd(String mirPolMedexOwnerInd) {
        this.mirPolMedexOwnerInd = mirPolMedexOwnerInd;
    }

    public String getMirStaffAsstCoShrInd() {
        return mirStaffAsstCoShrInd;
    }

    public void setMirStaffAsstCoShrInd(String mirStaffAsstCoShrInd) {
        this.mirStaffAsstCoShrInd = mirStaffAsstCoShrInd;
    }

    public String getMirPolSpclPdupBonOptCd() {
        return mirPolSpclPdupBonOptCd;
    }

    public void setMirPolSpclPdupBonOptCd(String mirPolSpclPdupBonOptCd) {
        this.mirPolSpclPdupBonOptCd = mirPolSpclPdupBonOptCd;
    }

    public String getMirPolOwnerCd() {
        return mirPolOwnerCd;
    }

    public void setMirPolOwnerCd(String mirPolOwnerCd) {
        this.mirPolOwnerCd = mirPolOwnerCd;
    }

    public String getMirThirdPartyInd() {
        return mirThirdPartyInd;
    }

    public void setMirThirdPartyInd(String mirThirdPartyInd) {
        this.mirThirdPartyInd = mirThirdPartyInd;
    }

    public String getMirBenfOwnerInd() {
        return mirBenfOwnerInd;
    }

    public void setMirBenfOwnerInd(String mirBenfOwnerInd) {
        this.mirBenfOwnerInd = mirBenfOwnerInd;
    }

    public String getMirPolFirstDueDt() {
        return mirPolFirstDueDt;
    }

    public void setMirPolFirstDueDt(String mirPolFirstDueDt) {
        this.mirPolFirstDueDt = mirPolFirstDueDt;
    }

    public String getMirPolInitPymntInd() {
        return mirPolInitPymntInd;
    }

    public void setMirPolInitPymntInd(String mirPolInitPymntInd) {
        this.mirPolInitPymntInd = mirPolInitPymntInd;
    }

    public String getMirPolPrvsnRcptDt() {
        return mirPolPrvsnRcptDt;
    }

    public void setMirPolPrvsnRcptDt(String mirPolPrvsnRcptDt) {
        this.mirPolPrvsnRcptDt = mirPolPrvsnRcptDt;
    }

    public String getMirPolPaymTypCsh() {
        return mirPolPaymTypCsh;
    }

    public void setMirPolPaymTypCsh(String mirPolPaymTypCsh) {
        this.mirPolPaymTypCsh = mirPolPaymTypCsh;
    }

    public String getMirPolPaymTypChk() {
        return mirPolPaymTypChk;
    }

    public void setMirPolPaymTypChk(String mirPolPaymTypChk) {
        this.mirPolPaymTypChk = mirPolPaymTypChk;
    }

    public String getMirPolPaymTypBkt() {
        return mirPolPaymTypBkt;
    }

    public void setMirPolPaymTypBkt(String mirPolPaymTypBkt) {
        this.mirPolPaymTypBkt = mirPolPaymTypBkt;
    }

    public String getMirPolPaymTypCrd() {
        return mirPolPaymTypCrd;
    }

    public void setMirPolPaymTypCrd(String mirPolPaymTypCrd) {
        this.mirPolPaymTypCrd = mirPolPaymTypCrd;
    }

    public String getMirPolApptypInd() {
        return mirPolApptypInd;
    }

    public void setMirPolApptypInd(String mirPolApptypInd) {
        this.mirPolApptypInd = mirPolApptypInd;
    }

    public String getMirKnowInsrdCd1() {
        return mirKnowInsrdCd1;
    }

    public void setMirKnowInsrdCd1(String mirKnowInsrdCd1) {
        this.mirKnowInsrdCd1 = mirKnowInsrdCd1;
    }

    public String getMirKnowInsrdCd2() {
        return mirKnowInsrdCd2;
    }

    public void setMirKnowInsrdCd2(String mirKnowInsrdCd2) {
        this.mirKnowInsrdCd2 = mirKnowInsrdCd2;
    }

    public String getMirPolMaxGioInd() {
        return mirPolMaxGioInd;
    }

    public void setMirPolMaxGioInd(String mirPolMaxGioInd) {
        this.mirPolMaxGioInd = mirPolMaxGioInd;
    }

    public String getMirPolRetenInd() {
        return mirPolRetenInd;
    }

    public void setMirPolRetenInd(String mirPolRetenInd) {
        this.mirPolRetenInd = mirPolRetenInd;
    }

    public String getMirPolSmsInd() {
        return mirPolSmsInd;
    }

    public void setMirPolSmsInd(String mirPolSmsInd) {
        this.mirPolSmsInd = mirPolSmsInd;
    }

    public String getMirPolEnoticeInd() {
        return mirPolEnoticeInd;
    }

    public void setMirPolEnoticeInd(String mirPolEnoticeInd) {
        this.mirPolEnoticeInd = mirPolEnoticeInd;
    }

    public String getMirServBnkBrNm() {
        return mirServBnkBrNm;
    }

    public void setMirServBnkBrNm(String mirServBnkBrNm) {
        this.mirServBnkBrNm = mirServBnkBrNm;
    }

    public String getMirPolPaymTypOth() {
        return mirPolPaymTypOth;
    }

    public void setMirPolPaymTypOth(String mirPolPaymTypOth) {
        this.mirPolPaymTypOth = mirPolPaymTypOth;
    }

    public String getMirPtpdBthDt() {
        return mirPtpdBthDt;
    }

    public void setMirPtpdBthDt(String mirPtpdBthDt) {
        this.mirPtpdBthDt = mirPtpdBthDt;
    }

    public String getMirEntityIncorpDt() {
        return mirEntityIncorpDt;
    }

    public void setMirEntityIncorpDt(String mirEntityIncorpDt) {
        this.mirEntityIncorpDt = mirEntityIncorpDt;
    }

    public String getMirPolSmsOrNotifInd() {
        return mirPolSmsOrNotifInd;
    }

    public void setMirPolSmsOrNotifInd(String mirPolSmsOrNotifInd) {
        this.mirPolSmsOrNotifInd = mirPolSmsOrNotifInd;
    }

    public String getMirPolEorNotifInd() {
        return mirPolEorNotifInd;
    }

    public void setMirPolEorNotifInd(String mirPolEorNotifInd) {
        this.mirPolEorNotifInd = mirPolEorNotifInd;
    }

    public String getMirPolPrsrcAlwncInd() {
        return mirPolPrsrcAlwncInd;
    }

    public void setMirPolPrsrcAlwncInd(String mirPolPrsrcAlwncInd) {
        this.mirPolPrsrcAlwncInd = mirPolPrsrcAlwncInd;
    }

    public String getMirPolPrsrcBusnsInd() {
        return mirPolPrsrcBusnsInd;
    }

    public void setMirPolPrsrcBusnsInd(String mirPolPrsrcBusnsInd) {
        this.mirPolPrsrcBusnsInd = mirPolPrsrcBusnsInd;
    }

    public String getMirPolPrsrcPrfeeInd() {
        return mirPolPrsrcPrfeeInd;
    }

    public void setMirPolPrsrcPrfeeInd(String mirPolPrsrcPrfeeInd) {
        this.mirPolPrsrcPrfeeInd = mirPolPrsrcPrfeeInd;
    }

    public String getMirPolPrsrcDonatInd() {
        return mirPolPrsrcDonatInd;
    }

    public void setMirPolPrsrcDonatInd(String mirPolPrsrcDonatInd) {
        this.mirPolPrsrcDonatInd = mirPolPrsrcDonatInd;
    }

    public String getMirPolPrsrcInhrtInd() {
        return mirPolPrsrcInhrtInd;
    }

    public void setMirPolPrsrcInhrtInd(String mirPolPrsrcInhrtInd) {
        this.mirPolPrsrcInhrtInd = mirPolPrsrcInhrtInd;
    }

    public String getMirPolPrsrcRremInd() {
        return mirPolPrsrcRremInd;
    }

    public void setMirPolPrsrcRremInd(String mirPolPrsrcRremInd) {
        this.mirPolPrsrcRremInd = mirPolPrsrcRremInd;
    }

    public String getMirPolPrsrcRentInd() {
        return mirPolPrsrcRentInd;
    }

    public void setMirPolPrsrcRentInd(String mirPolPrsrcRentInd) {
        this.mirPolPrsrcRentInd = mirPolPrsrcRentInd;
    }

    public String getMirPolPrsrcRetireInd() {
        return mirPolPrsrcRetireInd;
    }

    public void setMirPolPrsrcRetireInd(String mirPolPrsrcRetireInd) {
        this.mirPolPrsrcRetireInd = mirPolPrsrcRetireInd;
    }

    public String getMirPolPrsrcSalaryInd() {
        return mirPolPrsrcSalaryInd;
    }

    public void setMirPolPrsrcSalaryInd(String mirPolPrsrcSalaryInd) {
        this.mirPolPrsrcSalaryInd = mirPolPrsrcSalaryInd;
    }

    public String getMirPolPrsrcSoaInd() {
        return mirPolPrsrcSoaInd;
    }

    public void setMirPolPrsrcSoaInd(String mirPolPrsrcSoaInd) {
        this.mirPolPrsrcSoaInd = mirPolPrsrcSoaInd;
    }

    public String getMirPolPrsrcSavingsInd() {
        return mirPolPrsrcSavingsInd;
    }

    public void setMirPolPrsrcSavingsInd(String mirPolPrsrcSavingsInd) {
        this.mirPolPrsrcSavingsInd = mirPolPrsrcSavingsInd;
    }

    public String getMirPolPrsrcOtherInd() {
        return mirPolPrsrcOtherInd;
    }

    public void setMirPolPrsrcOtherInd(String mirPolPrsrcOtherInd) {
        this.mirPolPrsrcOtherInd = mirPolPrsrcOtherInd;
    }

    public String getMirPolInsAppSgnDt() {
        return mirPolInsAppSgnDt;
    }

    public void setMirPolInsAppSgnDt(String mirPolInsAppSgnDt) {
        this.mirPolInsAppSgnDt = mirPolInsAppSgnDt;
    }

    public String getMirPolPrtAppSgnDt() {
        return mirPolPrtAppSgnDt;
    }

    public void setMirPolPrtAppSgnDt(String mirPolPrtAppSgnDt) {
        this.mirPolPrtAppSgnDt = mirPolPrtAppSgnDt;
    }

    public String getMirPolPaymTypDep() {
        return mirPolPaymTypDep;
    }

    public void setMirPolPaymTypDep(String mirPolPaymTypDep) {
        this.mirPolPaymTypDep = mirPolPaymTypDep;
    }

    public String getMirBehlfTrxnInd() {
        return mirBehlfTrxnInd;
    }

    public void setMirBehlfTrxnInd(String mirBehlfTrxnInd) {
        this.mirBehlfTrxnInd = mirBehlfTrxnInd;
    }

    public String getMirPolCntrctTypCd() {
        return mirPolCntrctTypCd;
    }

    public void setMirPolCntrctTypCd(String mirPolCntrctTypCd) {
        this.mirPolCntrctTypCd = mirPolCntrctTypCd;
    }

    public String getMirPolPaymTypDepCsh() {
        return mirPolPaymTypDepCsh;
    }

    public void setMirPolPaymTypDepCsh(String mirPolPaymTypDepCsh) {
        this.mirPolPaymTypDepCsh = mirPolPaymTypDepCsh;
    }

    public String getMirPolPaymTypDepChk() {
        return mirPolPaymTypDepChk;
    }

    public void setMirPolPaymTypDepChk(String mirPolPaymTypDepChk) {
        this.mirPolPaymTypDepChk = mirPolPaymTypDepChk;
    }

    public String getMirPepDclrInd() {
        return mirPepDclrInd;
    }

    public void setMirPepDclrInd(String mirPepDclrInd) {
        this.mirPepDclrInd = mirPepDclrInd;
    }
}
