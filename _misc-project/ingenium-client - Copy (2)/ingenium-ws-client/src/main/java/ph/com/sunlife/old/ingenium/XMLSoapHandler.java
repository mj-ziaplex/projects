package ph.com.sunlife.old.ingenium;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class XMLSoapHandler {

  private String webRequest;

  public XMLSoapHandler() { }

  public void setRequestFile(String fileName){
    StringBuffer xmlData = new StringBuffer();
    try{
      BufferedReader reader = new BufferedReader(new FileReader(fileName));
      String line = reader.readLine();
      while (line != null){
        xmlData.append(line.trim());
        line = reader.readLine();
      }
    }
    catch (IOException ioe){
      ioe.printStackTrace();
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    webRequest = xmlData.toString();
  }

  public String getRequestString(){
    return webRequest;
  }

  public void setRequestString(String webRequest){
    this.webRequest = webRequest;
  }
}
