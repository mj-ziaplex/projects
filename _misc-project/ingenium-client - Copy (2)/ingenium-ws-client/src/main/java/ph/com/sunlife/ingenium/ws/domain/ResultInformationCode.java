package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ResultInfoCode")
public class ResultInformationCode {

    @XStreamAlias("Code")
    private int code;
    @XStreamAlias("Value")
    private String value;

    public ResultInformationCode() { }

    public ResultInformationCode(final int code, final String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
