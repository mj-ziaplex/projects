package ph.com.sunlife.ingenium.ws.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserPswd")
public class EncryptedPassword extends Password {

  public EncryptedPassword(final String algorithm, final String text) {
    super(algorithm, text);
  }
}
