package ph.com.sunlife.ingenium.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCpreqTstDtG")
public class MirCpreqTstDtG {

	@XStreamImplicit(itemFieldName = "MirCpreqTstDtT")
	protected List<String> mirCpreqTstDtT;

	public List<String> getMirCpreqTstDtT() {
		if (mirCpreqTstDtT == null) {
			mirCpreqTstDtT = new ArrayList<String>();
		}
		return this.mirCpreqTstDtT;
	}

}
