package ph.com.sunlife.ingenium;

import org.junit.Before;
import org.junit.Test;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.InquiryConsolidatedInformationRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.ListRequirementsRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.PolicyInquiryRequestBuilder;

import java.io.IOException;

public class RequestTests {
  @Before
  public void setUp() {

  }

  @Test
  public void testClientInquiryRequest() {
    System.out.println(new ClientInquiryRequestBuilder("3080008662").create());
  }

  @Test
  public void testPolicyInquiryRequest() {
    System.out.println(new PolicyInquiryRequestBuilder("0880059770").create());
  }

  @Test
  public void testListRequirementRequest() {
    System.out.println(new ListRequirementsRequestBuilder("0880059770").create());
  }

  @Test
  public void testInquiryConsolidatedInformationRequest() {
    System.out.println(new InquiryConsolidatedInformationRequestBuilder("0880059770")
            .setEffectivityDate("2019-01-15")
            .create());
  }
}
