package ph.com.sunlife.ingenium;

import org.junit.Test;
import ph.com.sunlife.wms.ingenium.domain.Requirement;
import ph.com.sunlife.wms.ingenium.service.PolicyService;
import ph.com.sunlife.wms.ingenium.service.PolicyServiceImpl;

import java.util.List;

public class WebServiceClientTest {

  @Test
  public void testClientService() {
    PolicyService service = new PolicyServiceImpl();
    List<Requirement> requirements = service.getRequirement("0880059770");

    for (Requirement req : requirements) {
      System.out.println(req.getRequirementCode() + " " + req.getIndex());
    }
  }
}

