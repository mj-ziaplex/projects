package ph.com.sunlife.wms.ingenium.data.dbutils;

import com.sunlife.ascp.configuration.data.dbutils.DbUtilsRepository;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import ph.com.sunlife.wms.data.IngeniumSunLifePlansRepository;
import ph.com.sunlife.wms.data.transfer.IngeniumSunLifePlan;
import ph.com.sunlife.wms.ingenium.data.dbutils.configuration.IngeniumSunLifePlansSQL;
import ph.com.sunlife.wms.ingenium.data.dbutils.mapper.IngeniumSunLifePlanMapper;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public final class IngeniumSunLifePlansRepositoryImpl
    extends DbUtilsRepository
    implements IngeniumSunLifePlansRepository {

  public IngeniumSunLifePlansRepositoryImpl(DataSource dataSource) {
    super(dataSource);
  }

  public IngeniumSunLifePlansRepositoryImpl(Properties properties) {
    super(properties);
  }

  @Override
  public String getPlanDescription(final String id) throws SQLException {
    return this.query(IngeniumSunLifePlansSQL.SELECT_DESCRIPTION_BY_ID,
                      new ColumnListHandler<String>(), id).get(0);
  }

  @Override
  public List<IngeniumSunLifePlan> fetchAll() throws SQLException {
    return this.query(IngeniumSunLifePlansSQL.SELECT_ALL,
                      new IngeniumSunLifePlanMapper());
  }
}
