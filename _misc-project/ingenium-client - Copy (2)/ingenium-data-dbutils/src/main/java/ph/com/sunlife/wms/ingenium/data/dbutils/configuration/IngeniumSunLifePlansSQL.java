package ph.com.sunlife.wms.ingenium.data.dbutils.configuration;

public class IngeniumSunLifePlansSQL {

  public static final String SELECT_ALL =
      "SELECT [Id], [Description] FROM [dbo].[IngeniumSunLifePlans] ORDER BY [Description];";

  public static final String SELECT_DESCRIPTION_BY_ID =
      "SELECT [Description] FROM [dbo].[IngeniumSunLifePlans] WHERE [Id] = ? ORDER BY [Description];";

  private static IngeniumSunLifePlansSQL instance = new IngeniumSunLifePlansSQL();
  private IngeniumSunLifePlansSQL() { }
  public static IngeniumSunLifePlansSQL getInstance() { return instance; }
}
