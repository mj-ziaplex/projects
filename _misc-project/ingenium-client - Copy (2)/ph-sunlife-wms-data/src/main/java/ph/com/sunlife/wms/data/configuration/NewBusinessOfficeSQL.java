package ph.com.sunlife.wms.data.configuration;

public class NewBusinessOfficeSQL {

  public static final String SELECT_NAME_BY_ID =
      "SELECT [NBO_Name] FROM [dbo].[NBOs] WHERE [NBO_ID] = ?;";

  private static NewBusinessOfficeSQL instance = new NewBusinessOfficeSQL();
  private NewBusinessOfficeSQL() { }
  public static NewBusinessOfficeSQL getInstance() { return instance; }
}
