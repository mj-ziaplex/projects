package ph.com.sunlife.wms.data;

import ph.com.sunlife.wms.data.transfer.IngeniumSunLifePlan;

import java.sql.SQLException;
import java.util.List;

public interface IngeniumSunLifePlansRepository {

  String getPlanDescription(final String id) throws SQLException;

  List<IngeniumSunLifePlan> fetchAll() throws SQLException;
}
