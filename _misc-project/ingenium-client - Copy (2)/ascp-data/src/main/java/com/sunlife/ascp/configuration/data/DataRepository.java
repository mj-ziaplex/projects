/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 08
 */

package com.sunlife.ascp.configuration.data;

import java.util.Properties;
import javax.sql.DataSource;

import com.sunlife.ascp.connectivity.database.DatabaseConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public abstract class DataRepository {

  protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

  protected final DataSource DATA_SOURCE;

  public DataRepository(final DataSource dataSource) {
    DATA_SOURCE = dataSource;
  }

  public DataRepository(final Properties properties) {
    this(new PropertiesDataSource(properties).getDataSource());
  }

  public DataRepository(final DatabaseConnector connector) {
    this(new DatabaseConnectorDataSource(connector).getDataSource());
  }
}