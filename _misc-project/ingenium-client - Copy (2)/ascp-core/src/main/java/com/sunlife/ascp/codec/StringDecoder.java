package com.sunlife.ascp.codec;

public interface StringDecoder extends Decoder<String> {

  @Override
  String decode(String source);
}
