/*
 * Copyright 2010 Marlon Janssen Arao
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */
package com.sunlife.ascp.connectivity.database;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public final class MsSqlServerConnector extends DatabaseConnector<MsSqlServerConnector> {

    public MsSqlServerConnector() {
        setPort(getDefaultPort());
    }

    public MsSqlServerConnector(final String server,
                                final String databaseName) {
        this();
        setHost(server);
        setSchema(databaseName);
    }

    public MsSqlServerConnector(final String server,
                                final String databaseName,
                                final String username,
                                final String password) {
        this();
        setHost(server);
        setSchema(databaseName);
        setUsername(username);
        setPassword(password);
    }

    public MsSqlServerConnector(final String server,
                                final int port,
                                final String databaseName,
                                final String username,
                                final String password) {
        setHost(server);
        setPort(port);
        setSchema(databaseName);
        setUsername(username);
        setPassword(password);
    }

    @Override
    public int getDefaultPort() {
        return 1433;
    }

    @Override
    public String getDefaultUsername() {
        return "sa";
    }



    @Override
    public String getDriver() {
        return JdbcDriver.MICROSOFT_SQL_SERVER;
    }

    @Override
    public String getUrl() {
        return String.format("jdbc:sqlserver://%s:%d;DatabaseName=%s",
                getHost(), getPort(), getSchema());
    }

    @Override
    public String getDataSourceName() {
        return DataSourceClassName.MICROSOFT_SQL_SERVER;
    }

    @Override
    public String getServerName() {
        return "Microsoft SQL Server";
    }

    @Override
    public String getDefaultDatabase() {
        return "Pubs";
    }

    public List<String> getSampleDatabases() {
        List<String> sampleDatabases = new ArrayList<String>();
        sampleDatabases.add("Northwind");
        sampleDatabases.add("Pubs");
        return sampleDatabases;
    }

    public String getServer() {
        return getHost();
    }

    public void setServer(final String server) {
        setHost(server);
    }

    public String getDatabaseName() {
        return getSchema();
    }

    public void setDatabaseName(final String name) {
        setSchema(name);
    }
}
