package com.sunlife.ascp.codec;

public interface BinaryEncoder extends Encoder<byte[]> {

  @Override
  byte[] encode(byte[] source);
}
