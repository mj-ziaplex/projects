package com.sunlife.ascp.codec;

public interface StringEncoder extends Encoder<String> {

  @Override
  String encode(String source);
}
