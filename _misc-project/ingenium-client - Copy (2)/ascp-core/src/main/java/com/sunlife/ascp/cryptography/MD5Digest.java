package com.sunlife.ascp.cryptography;

import com.sunlife.ascp.codec.binary.HexaDecimal;

import java.io.Serializable;
import java.nio.ByteBuffer;

public class MD5Digest extends MessageDigest implements Serializable {

  private HexaDecimal hex;

  protected MD5Digest(byte[] hashCode) {
    super(hashCode);
    hex = new HexaDecimal();
  }

  @Override
  public String getAsString() {
    return new String(hex.encode(get()));
  }

  @Override
  public int length() {
    return 128;
  }

  public String getAsUpperCaseString() {
    return new String(hex.encodeAsUpperCase(get()));
  }

  @Override
  public String toString() {
    return getAsString();
  }

  @Override
  public int hashCode() {
    return ByteBuffer.wrap(get()).getInt();
  }

//  @Override
//  public boolean equals(Object object) {
//
//  }
}
