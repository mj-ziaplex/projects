/*
 * Copyright 2010 Marlon Janssen Arao
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2011 November 14
 */

package com.sunlife.ascp.connectivity.database;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class JdbcDataSource {

    public static final String DB2 = "com.ibm.db2.jcc.DB2SimpleDataSource";
    public static final String DB2_POOLED = "com.ibm.db2.jcc.DB2ConnectionPoolDataSource";
    public static final String DERBY = "org.apache.derby.jdbc.ClientDataSource";
    public static final String H2 = "org.h2.jdbcx.JdbcDataSource";
    public static final String HSQLDB = "org.hsqldb.jdbc.JDBCDataSource";
    public static final String INFORMIX = "com.informix.jdbcx.IfxDataSource";
    public static final String INFORMIX_XA = "com.informix.jdbcx.IfxDataSource";
    public static final String INFORMIX_POOLED = "com.informix.jdbcx.IfxConnectionPoolDataSource";
    public static final String MARIADB = "org.mariadb.jdbc.MariaDbDataSource";
    public static final String MICROSOFT_SQL_SERVER = "com.microsoft.sqlserver.jdbc.SQLServerDataSource";
    public static final String MYSQL = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource";
    public static final String ORACLE = "oracle.jdbc.pool.OracleDataSource";
    public static final String POSTGRESQL = "org.postgresql.ds.PGSimpleDataSource";
    public static final String SAP_HANA = "";



    private static JdbcDataSource instance = new JdbcDataSource();
    private JdbcDataSource() { }
    public static JdbcDataSource getInstance() { return instance; }
 }
