package com.sunlife.ascp.entity.mapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ColumnFieldMapper<T> {
  /**
   * Gets the mapped column fields of POJO.
   * @param type - POJO 
   * @return {@link Map}
   */
  public final Map<String, String> get(Class<? extends T> type) {
    Map<String, String> map = new HashMap<String, String>();
    try {
      for (Field field : type.getDeclaredFields()) {
        Annotation[] annotations =  field.getAnnotations();
        for (Annotation annotation :  annotations) {
          if (annotation instanceof ColumnField) {
            ColumnField column = (ColumnField) annotation;
            map.put(column.name(), field.getName());
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return map;
  }
}
