/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 * 
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 * 
 * 
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 13
 */
package com.sunlife.ascp.configuration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public abstract class JNDIConfiguration extends Configuration {

  private Context namingContext;
  private String name;
  protected Object resource;

  public JNDIConfiguration() throws NamingException {
    namingContext = new InitialContext();
  }

  public JNDIConfiguration(final String name) throws NamingException {
    namingContext = new InitialContext();
    resource = namingContext.lookup(name);
  }

  public String getName() {
    return this.name;
  }

  public JNDIConfiguration setName(final String name) throws NamingException {
    this.name = name;
    this.resource = namingContext.lookup(this.name);
    return this;
  }
}
