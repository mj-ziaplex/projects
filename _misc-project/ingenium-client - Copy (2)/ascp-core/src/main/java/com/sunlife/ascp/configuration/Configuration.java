package com.sunlife.ascp.configuration;

public abstract class Configuration {

  public abstract <T> T get();
}
