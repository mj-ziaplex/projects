/*
 * Copyright 2010 Marlon Janssen Arao
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */
package com.sunlife.ascp.connectivity.database;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class DataSourceClassName {

    public static final String DB2_EXPRESS_C = "";
    public static final String DERBY = "";
    public static final String H2 = "";
    public static final String HSQLDB = "";
    public static final String INFORMIX = "";
    public static final String MARIADB = "";
    public static final String MICROSOFT_SQL_SERVER = "com.microsoft.sqlserver.jdbc.SQLServerDataSource";
    public static final String MYSQL = "";
    public static final String ORACLE = "";
    public static final String POSTGRESQL = "";
    public static final String SAP_HANA = "";
    



    private static DataSourceClassName instance = new DataSourceClassName();
    private DataSourceClassName() { }
    public static DataSourceClassName getInstance() { return instance; }
 }
