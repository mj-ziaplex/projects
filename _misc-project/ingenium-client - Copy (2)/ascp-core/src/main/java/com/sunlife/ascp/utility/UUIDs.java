package com.sunlife.ascp.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

public class UUIDs {

  private static final char[] HEX_CHARACTERS = "0123456789ABCDEF".toCharArray();

  public static UUID forgeSimple() {
    return UUID.fromString(forgeSimpleString());
  }

  public static String forgeSimpleString() {
    MessageDigest salt = null;
    try {
      salt = MessageDigest.getInstance("SHA-256");
      salt.update(UUID.randomUUID()
                      .toString().getBytes("UTF-8"));
    } catch (NoSuchAlgorithmException nsae) {
      throw new IllegalStateException(nsae.getMessage());
    } catch (UnsupportedEncodingException uee) {
      throw new IllegalStateException(uee.getMessage());
    }

    byte[] digestInBytes = salt.digest();
    String digest = "";

    char[] hexCharacters = new char[digestInBytes.length * 2];
    for (int j = 0; j < digestInBytes.length; j++) {
      int v = digestInBytes[j] & 0xFF;
      hexCharacters[j * 2] = HEX_CHARACTERS[v >>> 4];
      hexCharacters[j * 2 + 1] = HEX_CHARACTERS[v & 0x0F];
    }

    return new String(hexCharacters);
  }

  public static UUID forge(final String namespace, final String name) {
    String source = namespace + name;
    if (source == null || source.length() == 0) {
      return new UUID(0L, 0L);
    }
    byte[] bytes = new byte[0];
    try {
      bytes = source.getBytes("UTF-8");
    } catch (UnsupportedEncodingException uee) {
      throw new IllegalStateException(uee.getMessage());
    }
    return forge(bytes);
  }

  public static UUID forge(final byte[] data) {
    return forge(data, UUIDSecurityLevel.HIGH);
  }

  public static UUID forge(final byte[] data,
                           final UUIDSecurityLevel securityLevel) {
    if (data.length < 1) {
      return new UUID(0L, 0L);
    }
    MessageDigest messageDigest;
    try {
      messageDigest =
          MessageDigest.getInstance(securityLevel.getMessageDigestAlgorithm());
    } catch (NoSuchAlgorithmException nsae) {
      throw new IllegalStateException(nsae.getMessage());
    }
    byte[] bytes = Arrays.copyOfRange(messageDigest.digest(data), 0, 16);
    bytes[6] &= 0x0f;
    bytes[6] |= 0x50;
    bytes[8] &= 0x3f;
    bytes[8] |= 0x80;
    return construct(bytes);
  }

  public static String forgeString(final String namespace, final String name) {
    return forge(namespace, name).toString();
  }

  public static String forgeString(final byte[] data) {
    return forge(data).toString();
  }

  public static String forgeString(final byte[] data,
                                   final UUIDSecurityLevel securityLevel) {
    return forge(data, securityLevel).toString();
  }

  private static UUID construct(byte[] data) {
    if (data.length != 16) {
      throw new IllegalArgumentException("The data must be in 16 bytes in length.");
    }

    long mostSignificantBits = 0;
    long leastSignificantBits = 0;

    for (int i = 0; i < 8; i++)
      mostSignificantBits = (mostSignificantBits << 8) | (data[i] & 0xff);

    for (int i = 8; i < 16; i++)
      leastSignificantBits = (leastSignificantBits << 8) | (data[i] & 0xff);
    return new UUID(mostSignificantBits, leastSignificantBits);
  }


  public enum UUIDSecurityLevel {

    LOW("SHA-256"), MEDIUM("SHA-384"), HIGH("SHA-512");

    UUIDSecurityLevel(final String algorithm) {
      this.algorithm = algorithm;
    }

    private String algorithm;

    String getMessageDigestAlgorithm() {
      return algorithm;
    }
  }



  private static UUIDs instance = new UUIDs();
  private UUIDs() {}
  public static UUIDs getInstance() { return instance; }
}
