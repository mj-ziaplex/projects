package com.sunlife.ascp.cryptography;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Hash implements CryptographicHash<MD5Hash> {

  private final MessageDigest hashCode;

  public MD5Hash() {
    try {
      hashCode = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException nsae) {
      throw new IllegalStateException("This exception should have not " +
          "occured as the MD5 hashfunction should exists. " +
          nsae.getCause().getMessage(), nsae);
    }
  }

  @Override
  public MD5Hash update(final byte data) {
    hashCode.update(data);
    return this;
  }

  @Override
  public MD5Hash update(final byte[] data) {
    hashCode.update(data);
    return this;
  }

  @Override
  public MD5Hash update(final ByteBuffer data) {
    hashCode.update(data);
    return this;
  }

  @Override
  public MD5Hash update(final CharSequence data) {
    hashCode.update(data.toString().getBytes());
    return this;
  }

  @Override
  public MD5Hash update(final File data) throws IOException {
    if (data != null && data.exists() &&
        (!data.isDirectory() && data.isFile())) {
      InputStream fileInputStream = new FileInputStream(data);
      byte[] fileByteArray = new byte[(int) data.length()];
      try {
        fileInputStream.read(fileByteArray);
      } catch (IOException ioe) {
        throw ioe;
      } finally {
        if (fileInputStream != null) {
          try {
            fileInputStream.close();
          } catch (IOException ioe) {
            throw ioe;
          }
        }
      }
      update(fileByteArray);
    }
    return this;
  }

  @Override
  public MD5Digest digest() {
    return new MD5Digest(hashCode.digest());
  }
}
