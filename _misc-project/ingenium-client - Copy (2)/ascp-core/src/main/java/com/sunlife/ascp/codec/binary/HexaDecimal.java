package com.sunlife.ascp.codec.binary;

import com.sunlife.ascp.codec.DecoderException;

public class HexaDecimal {

  private static final char[] DIGITS_LOWER
      = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  private static final char[] DIGITS_UPPER
      = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

  public HexaDecimal() { }

  public char[] encode(final byte[] data) {
    return encode(data, DIGITS_LOWER);
  }

  public char[] encode(final byte[] data, final char[] hexadecimalDigits) {
    final int l = data.length;
    final char[] out = new char[l << 1];
    for (int i = 0, j = 0; i < l; i++) {
      out[j++] = hexadecimalDigits[(0xF0 & data[i]) >>> 4];
      out[j++] = hexadecimalDigits[0x0F & data[i]];
    }
    return out;
  }

  public char[] encodeAsLowerCase(final byte[] data) {
    return encode(data, DIGITS_LOWER);
  }

  public char[] encodeAsUpperCase(final byte[] data) {
    return encode(data, DIGITS_UPPER);
  }

  public byte[] decode(final char[] data) throws DecoderException {
    final int len = data.length;
    if ((len & 0x01) != 0) {
      throw new DecoderException("Odd number of characters.");
    }
    final byte[] out = new byte[len >> 1];

    for (int i = 0, j = 0; j < len; i++) {
      int f = toDigit(data[j], j) << 4;
      j++;
      f = f | toDigit(data[j], j);
      j++;
      out[i] = (byte) (f & 0xFF);
    }
    return out;
  }

  protected static int toDigit(final char ch, final int index) throws DecoderException {
    final int digit = Character.digit(ch, 16);
    if (digit == -1) {
      throw new DecoderException("Illegal hexadecimal character " + ch + " at index " + index);
    }
    return digit;
  }
}
