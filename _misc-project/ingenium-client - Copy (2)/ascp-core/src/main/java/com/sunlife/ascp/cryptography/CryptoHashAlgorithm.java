package com.sunlife.ascp.cryptography;

public enum CryptoHashAlgorithm {

  MD5("MD5"), SHA1("SHA-2");

  private final String algorithmName;

  CryptoHashAlgorithm(final String algorithm) {
    algorithmName = algorithm;
  }

  public final String getName() {
    return algorithmName;
  }
}
