/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 08
 */

package com.sunlife.ascp.configuration.data.dbutils;

import java.sql.SQLException;
import java.util.Properties;
import javax.sql.DataSource;

import com.sunlife.ascp.connectivity.database.DatabaseConnector;
import com.sunlife.ascp.configuration.data.DataRepository;
import com.sunlife.ascp.configuration.data.DatabaseConnectorDataSource;
import com.sunlife.ascp.configuration.data.PropertiesDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public abstract class DbUtilsRepository extends DataRepository {

  private QueryRunner runner;

  public DbUtilsRepository(final DataSource dataSource) {
    super(dataSource);
    runner = new QueryRunner(DATA_SOURCE);
  }

  public DbUtilsRepository(final Properties properties) {
    this(new PropertiesDataSource(properties).getDataSource());
  }

  public DbUtilsRepository(final DatabaseConnector connector) {
    this(new DatabaseConnectorDataSource(connector).getDataSource());
  }

  protected final int insert(final String sql,
                             final Object... parameters) throws SQLException {
    int result = 0;
    try {
      result = runner.execute(sql, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }

  protected <T> T query(final String query,
                        final ResultSetHandler<T> handler,
                        final Object... parameters) throws SQLException {
    T result = null;
    try {
      result = runner.query(query, handler, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }

  protected final int update(final String sql,
                             final Object... parameters) throws SQLException {
    int result = 0;
    try {
      result = runner.update(sql, parameters);
    } catch (SQLException sqle) {
      LOGGER.debug(sqle.getMessage(), sqle.getCause());
      throw sqle;
    }
    return result;
  }
}
