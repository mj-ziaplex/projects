package com.sunlife.ascp.configuration.data.dbutils.handler;

import java.io.Serializable;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.sunlife.ascp.entity.mapper.ColumnFieldMapper;

public class EntityBeanHandler<T extends Serializable> extends BeanHandler<T> {

  public EntityBeanHandler(Class<? extends T> type) {
    super(type, new BasicRowProcessor(new BeanProcessor(new ColumnFieldMapper<T>().get(type))) );
  }
}
