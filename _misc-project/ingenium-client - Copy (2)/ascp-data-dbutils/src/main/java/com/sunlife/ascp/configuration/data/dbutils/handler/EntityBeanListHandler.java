package com.sunlife.ascp.configuration.data.dbutils.handler;

import java.io.Serializable;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.sunlife.ascp.entity.mapper.ColumnFieldMapper;

public class EntityBeanListHandler<T extends Serializable> extends BeanListHandler<T> {

  public EntityBeanListHandler(Class<? extends T> type) {
    super(type, new BasicRowProcessor(new BeanProcessor(new ColumnFieldMapper<T>().get(type))));
  }

  public EntityBeanListHandler(Class<? extends T> type, BasicRowProcessor basicRowProcessor) {
    super(type, basicRowProcessor);
  }
}
