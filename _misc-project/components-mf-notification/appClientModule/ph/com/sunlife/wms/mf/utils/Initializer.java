package ph.com.sunlife.wms.mf.utils;

import java.util.ResourceBundle;

import ph.com.sunlife.wms.common.EncryptedProperties;
import ph.com.sunlife.wms.common.util.RBUtil;
import ph.com.sunlife.wms.jdbc.SQLServerTemplate;
import ph.com.sunlife.wms.jdbc.builder.SQLServerTemplateBuilder;
import ph.com.sunlife.wms.mf.constants.MFNotificationConstants;

public class Initializer {

	private static SQLServerTemplate db;
	private static ResourceBundle constants;
	private static EncryptedProperties dbProps;

	static {
		boolean isBatchConstants = getBatchConstants();
		boolean isAppConstants = false;
		
		if (!isBatchConstants) {
			isAppConstants = getAppConstants();
		}
		
		if (!isBatchConstants && !isAppConstants) {
			try {
//				COMPONENT
				constants = RBUtil.fromFile("D:\\WMS\\WorkFlowComponents\\Properties\\Constants.properties");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			dbProps = new EncryptedProperties(constants.getString(MFNotificationConstants.PROPERTIES_FILE_DB));
			db = new SQLServerTemplateBuilder()
					.connectionUrl(dbProps.getProperty(MFNotificationConstants.JDBC_CONNECTION_URL))
					.user(dbProps.getProperty(MFNotificationConstants.JDBC_USER))
					.password(dbProps.getProperty(MFNotificationConstants.JDBC_PASSWORD))
					.databaseName(dbProps.getProperty(MFNotificationConstants.JDBC_DBNAME))
				.build();
		} catch (Exception e) {
			
		}
	}
	
	public static SQLServerTemplate getDb() {
		return db;
	}
	
	public static ResourceBundle getConstants() {
		return constants;
	}
	
	private static boolean getBatchConstants() {
		try {
			constants = RBUtil.fromFile(System.getenv("BATCH_CONSTANTS"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static boolean getAppConstants() {
		try {
			constants = RBUtil.fromFile("D:\\WMS\\Properties\\Constants.properties");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}