package ph.com.sunlife.wms.mf.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import ph.com.sunlife.wms.mf.dto.WMSEmailTemplateDTO;

public interface WMSEmailTemplateDAO {

	List<WMSEmailTemplateDTO> getAllEmailTemplates() throws SQLException, IOException;
}
