package ph.com.sunlife.wms.mf.dao;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import ph.com.sunlife.wms.mf.dto.MFLookupAgentDTO;
import ph.com.sunlife.wms.mf.utils.DBConnect;

public class MFLookupAgentDAO {
	private ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
	private BufferedWriter out = null;
	private boolean logging = true;

	public void initializeLog() {
		try {
			if (this.logging) {
				this.out = new BufferedWriter(
						new FileWriter(this.rb.getString("WMS_LOG_PATH") + "\\MFLookupAgentDAOLog.log", true));
			}
		} catch (IOException localIOException) {
		}
	}

	public void log(String aString) {
		try {
			if (this.logging) {
				this.out.write(new Date() + " : " + aString + "\n");
			} else {
				System.out.println(aString);
			}
		} catch (IOException localIOException) {
		}
	}

	public void closeLog() {
		try {
			if (this.logging) {
				this.out.close();
			}
		} catch (IOException localIOException) {
		}
	}

	public List<MFLookupAgentDTO> getMFLookupAgentList(String whereClause) {
		List<MFLookupAgentDTO> ls = new ArrayList<MFLookupAgentDTO>();
		MFLookupAgentDTO dto = null;
		DBConnect dbc = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			dbc = new DBConnect();
			con = dbc.connect();
			stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(
					"SELECT CardNo, LastName, FirstName, MiddleName, MobileNo, Email, Branch, BranchId, AgreementTypeCode, CompanyCode FROM MF_LOOKUP_AGENT ")

					.append(whereClause);

			System.out.println("Query :" + query.toString());

			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				dto = new MFLookupAgentDTO();
				dto.setCardNo(rs.getString("CardNo"));
				dto.setLastName(rs.getString("LastName"));
				dto.setFirstName(rs.getString("FirstName"));
				dto.setMiddleName(rs.getString("MiddleName"));
				dto.setMobileNo(rs.getString("MobileNo"));
				dto.setEmail(rs.getString("Email"));
				dto.setBranch(rs.getString("Branch"));
				dto.setBranchId(rs.getString("BranchId"));
				dto.setAgreementTypeCode(rs.getString("AgreementTypeCode"));
				dto.setCompanyCode(rs.getString("CompanyCode"));
				ls.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqe1) {
					sqe1.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqe2) {
					sqe2.printStackTrace();
				}
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ls;
	}
}
