package ph.com.sunlife.wms.mf.dto;

import java.util.List;
import java.util.Vector;

public class MFLookupAgentDTO {
	private String CardNo;
	private String LastName; 
	private String FirstName;
	private String MiddleName;
	private String MobileNo;
	private String Email;
	private String Branch;
	private String BranchId;
	private String AgreementTypeCode;
	private String CompanyCode;
	private List ObjectList = new Vector();
	
	public String getBranch() {
		return Branch;
	}

	public void setBranch(String branch) {
		Branch = branch;
	}

	public String getBranchId() {
		return BranchId;
	}

	public void setBranchId(String branchId) {
		BranchId = branchId;
	}

	public String getCardNo() {
		return CardNo;
	}

	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getMiddleName() {
		return MiddleName;
	}

	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getAgreementTypeCode() {
		return AgreementTypeCode;
	}

	public void setAgreementTypeCode(String agreementTypeCode) {
		AgreementTypeCode = agreementTypeCode;
	}

	public String getCompanyCode() {
		return CompanyCode;
	}

	public void setCompanyCode(String companyCode) {
		CompanyCode = companyCode;
	}
	
	public void add(MFLookupAgentDTO dto) {
		ObjectList.add(dto);
	}
	
	public List getObjectList() {
		return ObjectList;
	}

	public void setObjectList(List objectList) {
		ObjectList = objectList;
	}

}
