package ph.com.sunlife.wms.mf.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;

public class LoggingUtil {

	private static ResourceBundle rb = ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES) == null
			? ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES_OUTSIDE)
			: ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES);
	
	private static BufferedWriter bufferedWriter = null;
	
	private static void initializeLog() {
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(rb.getString("WMS_LOG_PATH")+"\\MFNotificationLog.log", true));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void log(String aString) {
		try {
			if (bufferedWriter == null) {
				initializeLog();
			}
			
			bufferedWriter.write(new Date() + " : "+ aString + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void closeLog() {
		try {
			if (bufferedWriter == null) {
				initializeLog();
			}
			
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
