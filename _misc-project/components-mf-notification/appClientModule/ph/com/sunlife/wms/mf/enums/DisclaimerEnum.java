package ph.com.sunlife.wms.mf.enums;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import ph.com.sunlife.wms.mf.dto.WMSEmailTemplateDTO;
import ph.com.sunlife.wms.mf.service.EmailTemplateService;
import ph.com.sunlife.wms.mf.utils.LoggingUtil;

public enum DisclaimerEnum {

	INTERNAL_CP(DisclaimerType.INTERNAL, Company.SLOCPI) {
		@Override
		public WMSEmailTemplateDTO getDisclaimerDTO() {
			return getWmsNotificationDtoByCodeId(DisclaimerType.INTERNAL, Company.SLOCPI);
		}
	},
	INTERNAL_GF(DisclaimerType.INTERNAL, Company.SLGFI) {
		@Override
		public WMSEmailTemplateDTO getDisclaimerDTO() {
			return getWmsNotificationDtoByCodeId(DisclaimerType.INTERNAL, Company.SLGFI);
		}
	},
	EXTERNAL_CP(DisclaimerType.EXTERNAL, Company.SLOCPI) {
		@Override
		public WMSEmailTemplateDTO getDisclaimerDTO() {
			return getWmsNotificationDtoByCodeId(DisclaimerType.EXTERNAL, Company.SLOCPI);
		}
	},
	EXTERNAL_GF(DisclaimerType.EXTERNAL, Company.SLGFI) {
		@Override
		public WMSEmailTemplateDTO getDisclaimerDTO() {
			return getWmsNotificationDtoByCodeId(DisclaimerType.EXTERNAL, Company.SLGFI);
		}
	};
	
	private DisclaimerEnum(DisclaimerType disclaimerType, Company company) {
		this.disclaimerType = disclaimerType;
		this.company = company;
	}
	
	private DisclaimerType disclaimerType;
	private Company company;
	
	public DisclaimerType getDisclaimerType() {
		return disclaimerType;
	}
	
	public Company getCompany() {
		return company;
	}
	
	public static enum Company {
		SLOCPI("CP"),
		SLGFI("GF");
		
		private final String companyCode;
		
		Company(String companyCode) {
			this.companyCode = companyCode;
		}
		
		
	}
	
	public static enum DisclaimerType {
		INTERNAL,
		EXTERNAL;
	}
	
	private static WMSEmailTemplateDTO getWmsNotificationDtoByCodeId(final DisclaimerType disclaimerType, final Company company) {
		WMSEmailTemplateDTO emailTemplateDTO = (WMSEmailTemplateDTO) CollectionUtils.find(EmailTemplateService.getAllEmailTemplates(), new Predicate() {
			
			@Override
			public boolean evaluate(Object obj) {
				WMSEmailTemplateDTO dto = (WMSEmailTemplateDTO) obj;
				return disclaimerType.toString().equalsIgnoreCase(dto.getCodeId())
						&& company.companyCode.equalsIgnoreCase(dto.getCompanyCode());
			}
		});
		
		return emailTemplateDTO;
	}
	
	public static DisclaimerEnum getDisclaimer(DisclaimerType disclaimerType, Company company) {
		if (disclaimerType == null) {
			LoggingUtil.log("disclaimerType must not be null");
			throw new IllegalArgumentException("disclaimerType must not be null");
		}
		
		if (company == null) {
			LoggingUtil.log("company must not be null");
			throw new IllegalArgumentException("company must not be null");
		}
		
		for (DisclaimerEnum disclaimerEnum : DisclaimerEnum.values()) {
			if (disclaimerEnum.getCompany().equals(company) && disclaimerEnum.getDisclaimerType().equals(disclaimerType)) {
				return disclaimerEnum;
			}
		}
		
		return DisclaimerEnum.INTERNAL_CP;
	}
	
	public abstract WMSEmailTemplateDTO getDisclaimerDTO();
}