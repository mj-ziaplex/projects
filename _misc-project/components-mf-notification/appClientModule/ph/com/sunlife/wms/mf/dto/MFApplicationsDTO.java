package ph.com.sunlife.wms.mf.dto;

import java.util.List;
import java.util.Vector;

public class MFApplicationsDTO {
	private String MFP_OrderTicketNumber;
	private String MFP_AgentID; 
	private String MFP_ClientNo;
	private String MFP_ClientLName;
	private String MFP_ClientFName;
	private String MFP_ClientMName;
	private String MFP_PolicyNo;
	private List ObjectList = new Vector();
	
	public void add(MFApplicationsDTO dto) {
		ObjectList.add(dto);
	}

	public List getObjectList() {
		return ObjectList;
	}

	public String getMFP_AgentID() {
		return MFP_AgentID;
	}

	public void setMFP_AgentID(String agentID) {
		MFP_AgentID = agentID;
	}

	public String getMFP_ClientFName() {
		return MFP_ClientFName;
	}

	public void setMFP_ClientFName(String clientFName) {
		MFP_ClientFName = clientFName;
	}

	public String getMFP_ClientLName() {
		return MFP_ClientLName;
	}

	public void setMFP_ClientLName(String clientLName) {
		MFP_ClientLName = clientLName;
	}

	public String getMFP_ClientMName() {
		return MFP_ClientMName;
	}

	public void setMFP_ClientMName(String clientMName) {
		MFP_ClientMName = clientMName;
	}

	public String getMFP_ClientNo() {
		return MFP_ClientNo;
	}

	public void setMFP_ClientNo(String clientNo) {
		MFP_ClientNo = clientNo;
	}

	public String getMFP_OrderTicketNumber() {
		return MFP_OrderTicketNumber;
	}

	public void setMFP_OrderTicketNumber(String orderTicketNumber) {
		MFP_OrderTicketNumber = orderTicketNumber;
	}

	public String getMFP_PolicyNo() {
		return MFP_PolicyNo;
	}

	public void setMFP_PolicyNo(String policyNo) {
		MFP_PolicyNo = policyNo;
	}
}
