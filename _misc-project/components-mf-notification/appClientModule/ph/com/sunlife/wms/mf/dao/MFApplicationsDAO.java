package ph.com.sunlife.wms.mf.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;

import ph.com.sunlife.wms.mf.dto.MFApplicationsDTO;
import ph.com.sunlife.wms.mf.utils.DBConnect;
import sunlife.wms.util.db.DBManager;

public class MFApplicationsDAO {
	public MFApplicationsDTO getMFApplicationsDTO(String whereClause) {
		MFApplicationsDTO dto = null;
		DBConnect dbc = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			dbc = new DBConnect();
			con = dbc.connect();
			stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(
					"SELECT MFP_OrderTicketNumber, MFP_AgentID, MFP_ClientNo, MFP_ClientLName, MFP_ClientFName, MFP_ClientMName, MFP_PolicyNo FROM MFApplications ")
					.append(whereClause);
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				dto = new MFApplicationsDTO();
				dto.setMFP_OrderTicketNumber(rs.getString("MFP_OrderTicketNumber"));
				dto.setMFP_AgentID(rs.getString("MFP_AgentID"));
				dto.setMFP_ClientNo(rs.getString("MFP_ClientNo"));
				dto.setMFP_ClientLName(rs.getString("MFP_ClientLName"));
				dto.setMFP_ClientFName(rs.getString("MFP_ClientFName"));
				dto.setMFP_ClientMName(rs.getString("MFP_ClientMName"));
				dto.setMFP_PolicyNo(rs.getString("MFP_PolicyNo"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqe1) {
					sqe1.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqe2) {
					sqe2.printStackTrace();
				}
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return dto;
	}

	public String getMFAccountType(String orderTicket) {
		String mfAccountType = null;
		DBConnect dbc = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			dbc = new DBConnect();
			con = dbc.connect();
			stmt = con.createStatement();
			StringBuffer query = new StringBuffer();

			query.append("SELECT MF_AccountType ").append("FROM MFApplications ")
					.append("WHERE MFP_OrderTicketNumber = '").append(orderTicket).append("'");

			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				mfAccountType = rs.getString("MF_AccountType");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqe1) {
					sqe1.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqe2) {
					sqe2.printStackTrace();
				}
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return mfAccountType;
	}

	public boolean updateMFApplications(String orderTicketNum, HashMap hsCriteria) throws Exception {
		System.out.println("MFApplicationsDAO - updateMFApplications - start");

		DBManager dbmgr = new DBManager();

		boolean ret = true;

		String table = null;
		String criteria = null;
		try {
			criteria = "MFP_OrderTicketNumber = '" + orderTicketNum + "'";

			System.out.println(table);
			System.out.println(criteria);
			if ((hsCriteria != null) && (hsCriteria.size() > 0)) {
				StringBuffer sql = new StringBuffer();
				sql.append("UPDATE ").append("MFApplications ").append("SET ");
				int ctr = 0;
				Iterator it = hsCriteria.keySet().iterator();
				while (it.hasNext()) {
					String field = (String) it.next();
					String value = (String) hsCriteria.get(field);

					sql.append(field).append(" = '").append(value).append("' ");

					ctr++;
					if ((ctr > 0) && (ctr != hsCriteria.size())) {
						sql.append(",");
					}
				}
				sql.append("WHERE ").append(criteria);

				System.out.println("sql " + sql.toString());

				dbmgr.ExecQuery(sql.toString());
			} else {
				System.out.println("Incomplete or Malformed update statement.");
				ret = false;
			}
		} catch (Exception e) {
			System.out.println("Exception e " + e.getMessage() + " " + e.getCause());
			ret = false;
			e.printStackTrace();
		}
		System.out.println("MFApplicationsDAO - updateMFApplications - end");

		return ret;
	}
}
