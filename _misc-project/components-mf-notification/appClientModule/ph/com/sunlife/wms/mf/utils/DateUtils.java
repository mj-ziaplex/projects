package ph.com.sunlife.wms.mf.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat();
	public static final String MONTH_DAY_WITH_TIME_FORMAT = "MMM d, yyyy hh:mm:ss a";

	public static String getFormattedDate(String format, Date date){
		DATE_FORMAT.applyPattern(format);
		return DATE_FORMAT.format(date);
	}
}
