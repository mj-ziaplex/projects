package ph.com.sunlife.wms.mf.dao;

public interface WmsParamDAO {

	String getWmsParamValue(String param);
}
