package ph.com.sunlife.wms.mf.helper;

import java.io.BufferedWriter;
import java.util.Date;

public class MFNotifcationDecisionHelper {
	private static BufferedWriter bufferWriter = null;

	public static void initializeLogs(BufferedWriter writer) {
		bufferWriter = writer;
	}

	public static void log(String aString) {
		try {
			bufferWriter.write(new Date() + " : " + aString + "\n");
		} catch (Exception e) {
			System.out.println("MFNotifcationDecisionHelper log Exception : "
					+ PrintExceptionHelper.exceptionStacktraceToString(e));
		}
	}

	public static boolean getNotificationDecisionDependingOnAccountType(String accountType, String notifDecision) {
		log("getNotificationDecisionDependingOnAccountType --START--");
		log("Account Type : " + accountType);
		log("Notification Decision : " + notifDecision);
		if (("PROMO".equalsIgnoreCase(accountType)) && ("N".equalsIgnoreCase(notifDecision))) {
			log("Disabling Notification. Reason : Account Type = " + accountType
					+ " and Account Type Notification Decision is : " + notifDecision);
			return false;
		}
		log("Enabling Notification. Reason : Account Type = " + accountType
				+ " and Promo Account Type Notification Decision is : " + notifDecision);
		return true;
	}

	public static boolean getNotificationDecision(String notifDecision) {
		if ("Y".equalsIgnoreCase(notifDecision)) {
			return true;
		}
		if ("N".equalsIgnoreCase(notifDecision)) {
			return false;
		}
		return false;
	}
}
