package ph.com.sunlife.wms.mf.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ph.com.sunlife.wms.mf.dto.MFLookupCodeDTO;
import ph.com.sunlife.wms.mf.utils.DBConnect;

public class MFLookupCodeDAO {
	public List<MFLookupCodeDTO> getMFLookupCodeList(String whereClause) {
		List<MFLookupCodeDTO> ls = new ArrayList<MFLookupCodeDTO>();
		MFLookupCodeDTO dto = null;
		DBConnect dbc = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			dbc = new DBConnect();
			con = dbc.connect();
			stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("SELECT CODE_ID, CODE_TYPE, CODE_VALUE, CODE_DESC FROM MF_LOOKUP_CODE ").append(whereClause);
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				dto = new MFLookupCodeDTO();
				dto.setCODE_ID(rs.getString("CODE_ID"));
				dto.setCODE_TYPE(rs.getString("CODE_TYPE"));
				dto.setCODE_VALUE(rs.getString("CODE_VALUE"));
				dto.setCODE_DESC(rs.getString("CODE_DESC"));
				ls.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqe1) {
					sqe1.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqe2) {
					sqe2.printStackTrace();
				}
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ls;
	}
}
