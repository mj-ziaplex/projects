

public class MFNotificationConstants {

	public static final String MF_NOTIFICATION_EMAIL_DECISION = "MFEmailNotificationDecision";
	public static final String MF_NOTIFICATION_SMS_DECISION = "MFSMSNotificationDecision";
	public static final String MF_PROMO_NOTIFICATION_DECISION = "MFPromoNotificationDecision";
	public static final String MF_NOTIFICATION_ENABLED = "Y";
	public static final String MF_NOTIFICATION_DISABLED = "N";
	public static final String MF_ACCOUNT_TYPE_FOR_PROMO = "PROMO";
	public static final String EXTERNAL = "EXTERNAL";
	public static final String COMPANY_CODE_CP = "CP";
	public static final String WMS_EMAIL_TEMPLATE_TABLE = "WMS_Email_Template";
	
	public static final String MF_BRANCH_PREFIX = "mailing.group";
	public static final String MAIL_SMTP_HOST_VAR = "mail.smtp.host";
	public static final String MAIL_SMTP_HOST_VAL = "ln_apd_g10.ph.sunlife";
	
	public static final String JDBC_CONNECTION_URL = "JDBC_CONNECTION_URL";
	public static final String JDBC_USER = "JDBC_USER";
	public static final String JDBC_PASSWORD = "JDBC_PASSWORD";
	public static final String JDBC_DBNAME = "JDBC_DBNAME";
	public static final String PROPERTIES_FILE_DB = "PROPERTIES_FILE_DB";
}
