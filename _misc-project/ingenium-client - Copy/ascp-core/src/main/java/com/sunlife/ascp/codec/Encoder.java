package com.sunlife.ascp.codec;

public interface Encoder<T> {

  T encode(T source);
}
