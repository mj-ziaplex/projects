/*
 * Copyright 2010 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 *
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */

package com.sunlife.ascp.connectivity;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public interface Connector<T extends Connector<T>> {

  T setHost(String host);

  String getHost();

  T setPort(int port);

  int getPort();

  T setUsername(String username);

  String getUsername();

  T setPassword(String password);

  String getPassword();


  String getDefaultHost();

  int getDefaultPort();

  String getDefaultUsername();
}

