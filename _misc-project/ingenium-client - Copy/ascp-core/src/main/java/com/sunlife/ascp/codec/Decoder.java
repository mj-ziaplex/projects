package com.sunlife.ascp.codec;

public interface Decoder<T> {

  T decode(T source);
}
