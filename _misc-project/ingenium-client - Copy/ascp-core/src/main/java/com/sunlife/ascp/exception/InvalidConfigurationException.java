package com.sunlife.ascp.exception;

public class InvalidConfigurationException extends Exception {

  public static final long serialVersionUID = 156285246764321333L;
  private static final String DEFAULT_MESSAGE =
      "A configuration was found and .";

  public InvalidConfigurationException() {
    super(DEFAULT_MESSAGE);
  }

  public InvalidConfigurationException(String message) {
    super(message);
  }

  public InvalidConfigurationException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidConfigurationException(Throwable cause) {
    super(cause);
  }
}
