/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 * 
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 * 
 * 
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 13
 */
package com.sunlife.ascp.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class PropertiesConfiguration {

  private final Properties properties;
  
  public PropertiesConfiguration(final String propertiesName) throws IOException {
    properties = new Properties();
    properties.load(getClass().getResourceAsStream("/" + propertiesName + ".properties"));
  }

  public PropertiesConfiguration(final File file) throws IOException {
    properties = new Properties();
    copyProperties(new PropertyResourceBundle(new FileInputStream(file.getAbsoluteFile())));
  }

  public PropertiesConfiguration(final InputStream input) throws IOException {
    properties = new Properties();
    properties.load(input);
  }

  public PropertiesConfiguration(final ResourceBundle bundle) throws IOException {
    properties = new Properties();
    copyProperties(bundle);
  }

  public Properties getProperties() {
    return properties;
  }
  
  public Map<String, String> getPropertiesMap() {
    Map<String, String> props = new HashMap<String, String>();
    Enumeration<String> enums = (Enumeration<String>) properties.propertyNames();
    while (enums.hasMoreElements()) {
      String key = enums.nextElement();
      String value = properties.getProperty(key);
      props.put(key, value);
    }
    return props;
  }

  public String getProperty(final String key) {
    return properties.getProperty(key);
  }

  public String getProperty(final String key, final String defaultValue) {
    return properties.getProperty(key, defaultValue);
  }

  
  private void copyProperties(final ResourceBundle bundle) throws IOException {
    for (Enumeration<String> keys = bundle.getKeys(); keys.hasMoreElements();) {
      String key = keys.nextElement();
      properties.put(key, bundle.getString(key));
    }
  }
}
