/*
 * Copyright 2010 Marlon Janssen Arao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 *
 * Written by Marlon Janssen Arao <mj.arao@techie.com>, 2010 November 22
 */

package com.sunlife.ascp.connectivity.database;

import com.sunlife.ascp.connectivity.Connector;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 * @param <M>
 */
public abstract class DatabaseConnector<T extends DatabaseConnector<T>> implements Connector<T> {

  private String host;
  private int port;
  private String username;
  private String password;
  public String schema;



  @Override
  public final T setHost(final String host) {
    this.host = host;
    return (T) this;
  }

  @Override
  public final String getHost() {
    return host;
  }

  public String getSchema() {
    return schema;
  }

  public void setSchema(final String schema) {
    this.schema = schema;
  }

  @Override
  public final T setPort(final int port) {
    this.port = port;
    return (T) this;
  }

  @Override
  public final int getPort() {
    return port;
  }

  @Override
  public final T setUsername(final String username) {
    this.username = username;
    return (T) this;
  }

  @Override
  public final String getUsername() {
    return username;
  }

  @Override
  public final T setPassword(final String password) {
    this.password = password;
    return (T) this;
  }

  @Override
  public final String getPassword() {
    return password;
  }


  @Override
  public final String getDefaultHost() {
    return "localhost";
  }


  @Override
  public abstract int getDefaultPort();

  @Override
  public abstract String getDefaultUsername();

  public abstract String getDefaultDatabase();


  public abstract String getDriver();
  public abstract String getUrl();
  public abstract String getDataSourceName();
  public abstract String getServerName();
}
