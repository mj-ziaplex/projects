/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 * 
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 * 
 * 
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 15
 */

package com.sunlife.ascp.connectivity;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public enum Protocol {

    INTERNET_INTER_ORB_PROTOCOL("iiop"),
    JNDI_JBOSS("jnp"),
    RMI("rmi"),
    RMI_WEBLOGIC("t3"), SECURE_RMI_WEBLOGIC("t3s"),
    WEB("http"), SECURE_WEB("https");



    public static Protocol detect(final String protocol) {
        for (Protocol prot : Protocol.values()) {
            if (prot.code.equalsIgnoreCase(protocol)) {
                return prot;
            }
        }
        return WEB;
    }
    
    public String asURLPrefix() {
        return code + "://";
    }



    private String code;
    Protocol(String code) { this.code = code; }
    public String acquire() { return this.code; }
 }
