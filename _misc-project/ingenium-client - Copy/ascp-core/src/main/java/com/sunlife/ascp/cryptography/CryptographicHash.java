package com.sunlife.ascp.cryptography;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public interface CryptographicHash<T> {

  T update(byte data);

  T update(byte[] data);

  T update(ByteBuffer data);

  T update(CharSequence data);

  T update(File data) throws IOException;

  MD5Digest digest();
}
