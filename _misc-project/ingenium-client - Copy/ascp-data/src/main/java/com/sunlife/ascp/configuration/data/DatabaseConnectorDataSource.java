/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 13
 */

package com.sunlife.ascp.configuration.data;

import com.sunlife.ascp.connectivity.database.DatabaseConnector;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public final class DatabaseConnectorDataSource extends DataSourceRadix {

  public DatabaseConnectorDataSource(final DatabaseConnector connector) {
    super(prepare(connector));
  }

  private static DataSource prepare(final DatabaseConnector connector) {
    HikariConfig config = new HikariConfig();
    config.setDriverClassName(connector.getDriver());
    config.setJdbcUrl(connector.getUrl());
    config.setUsername(connector.getUsername());
    config.setPassword(connector.getPassword());
    config.addDataSourceProperty(CACHE_PREPARED_STATEMENTS_CONFIG,
        CACHE_PREPARED_STATEMENTS_DEFAULT);
    config.addDataSourceProperty(PREPARED_STATEMENTS_CACHE_SIZE_CONFIG,
        PREPARED_STATEMENTS_CACHE_SIZE_DEFAULT);
    config.addDataSourceProperty(PREPARED_STATEMENTS_CACHE_SQL_LIMIT_CONFIG,
        PREPARED_STATEMENTS_CACHE_SQL_LIMIT_DEFAULT);
    config.setAutoCommit(true);
    return new HikariDataSource(config);
  }

  private static final String CACHE_PREPARED_STATEMENTS_CONFIG = "cachePrepStmts";
  private static final String PREPARED_STATEMENTS_CACHE_SIZE_CONFIG = "prepStmtCacheSize";
  private static final String PREPARED_STATEMENTS_CACHE_SQL_LIMIT_CONFIG = "prepStmtCacheSqlLimit";

  private static final String CACHE_PREPARED_STATEMENTS_DEFAULT = "true";
  private static final String PREPARED_STATEMENTS_CACHE_SIZE_DEFAULT = "250";
  private static final String PREPARED_STATEMENTS_CACHE_SQL_LIMIT_DEFAULT = "2048";
}
