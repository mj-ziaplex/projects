/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 13
 */

package com.sunlife.ascp.configuration.data;

import java.util.Properties;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public final class PropertiesDataSource extends DataSourceRadix {

  public PropertiesDataSource(final Properties properties) {
    super(new HikariDataSource(new HikariConfig(properties)));
  }
}
