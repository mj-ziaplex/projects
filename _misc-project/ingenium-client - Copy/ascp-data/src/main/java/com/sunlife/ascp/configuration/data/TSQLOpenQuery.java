/*
 * Copyright (C) 2019 Sun Life Financial. All Rights Reserved.
 *
 * This software is confidential and proprietary information of Sun Life Financial
 * ("Confidential Information"). You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions you entered
 * with Sun Life Financial.
 *
 *
 * Written by Marlon Janssen Arao <marlon.arao@sunlife.com>, 2019 November 27
 */

package com.sunlife.ascp.configuration.data;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Marlon Janssen Arao <mj.arao@techie.com>
 */
public class TSQLOpenQuery {

  private String linkedServer = StringUtils.EMPTY;
  private String sqlStatement = StringUtils.EMPTY;

  public TSQLOpenQuery(final String linkedServer, final String sqlStatement) {
    this.linkedServer = linkedServer;
    this.sqlStatement = sqlStatement;
  }

  public final TSQLOpenQuery addParameter(final String key, final String parameter) {
    sqlStatement = StringUtils.replace(sqlStatement, key, parameter);
    return this;
  }

  public final String buildSelect() {
    return StringUtils.replace(StringUtils.replace(SELECT_QUERY,
                                                   LINKED_SERVER_KEY, linkedServer),
                               SQL_STATEMENT_KEY, sqlStatement);
  }

  public final String buildInsert() {
    return StringUtils.replace(StringUtils.replace(INSERT_QUERY,
                                                   LINKED_SERVER_KEY, linkedServer),
                               SQL_STATEMENT_KEY, sqlStatement);
  }

  public final String buildUpdate() {
    return StringUtils.replace(StringUtils.replace(UPDATE_QUERY,
                                                   LINKED_SERVER_KEY, linkedServer),
                               SQL_STATEMENT_KEY, sqlStatement);
  }

  public final String buildDelete() {
    return StringUtils.replace(StringUtils.replace(DELETE_QUERY,
                                                   LINKED_SERVER_KEY, linkedServer),
                               SQL_STATEMENT_KEY, sqlStatement);
  }

  private static final String SELECT_QUERY = "SELECT * OPENQUERY(:LinkedServer:, :SQLStatement:)";
  private static final String INSERT_QUERY = "INSERT * OPENQUERY(:LinkedServer:, :SQLStatement:)";
  private static final String UPDATE_QUERY = "UPDATE * OPENQUERY(:LinkedServer:, :SQLStatement:)";
  private static final String DELETE_QUERY = "DELETE * OPENQUERY(:LinkedServer:, :SQLStatement:)";
  private static final String LINKED_SERVER_KEY = ":LinkedServer:";
  private static final String SQL_STATEMENT_KEY = ":SQLStatement:";
}
