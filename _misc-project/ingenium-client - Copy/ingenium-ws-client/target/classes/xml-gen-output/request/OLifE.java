//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.08 at 03:59:50 PM AWST 
//


package ph.com.sunlife.ingenium.domain.reqst;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}ClientInquiryData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientInquiryData"
})
@XmlRootElement(name = "OLifE")
public class OLifE {

    @XmlElement(name = "ClientInquiryData", required = true)
    protected ClientInquiryData clientInquiryData;

    /**
     * Gets the value of the clientInquiryData property.
     * 
     * @return
     *     possible object is
     *     {@link ClientInquiryData }
     *     
     */
    public ClientInquiryData getClientInquiryData() {
        return clientInquiryData;
    }

    /**
     * Sets the value of the clientInquiryData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientInquiryData }
     *     
     */
    public void setClientInquiryData(ClientInquiryData value) {
        this.clientInquiryData = value;
    }

}
