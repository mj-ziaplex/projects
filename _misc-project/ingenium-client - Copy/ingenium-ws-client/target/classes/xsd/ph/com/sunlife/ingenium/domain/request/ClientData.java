//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.08 at 04:47:28 PM AWST 
//


package ph.com.sunlife.ingenium.domain.reqst;

import ph.com.sunlife.ingenium.domain.reqst.client.inquiry.MirCliInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirPrcesInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirCliInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirNmInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirPrevNmInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirAddrInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirEmplrInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirPrevEmplrInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirListInfo"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirClftInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mirPrcesInfo",
    "mirCliInfo",
    "mirNmInfo",
    "mirPrevNmInfo",
    "mirAddrInfo",
    "mirEmplrInfo",
    "mirPrevEmplrInfo",
    "mirListInfo",
    "mirClftInfo"
})
@XmlRootElement(name = "ClientData")
public class ClientData {

    @XmlElement(name = "MirPrcesInfo", required = true)
    protected String mirPrcesInfo;
    @XmlElement(name = "MirCliInfo", required = true)
    protected MirCliInfo mirCliInfo;
    @XmlElement(name = "MirNmInfo", required = true)
    protected String mirNmInfo;
    @XmlElement(name = "MirPrevNmInfo", required = true)
    protected String mirPrevNmInfo;
    @XmlElement(name = "MirAddrInfo", required = true)
    protected MirAddrInfo mirAddrInfo;
    @XmlElement(name = "MirEmplrInfo", required = true)
    protected MirEmplrInfo mirEmplrInfo;
    @XmlElement(name = "MirPrevEmplrInfo", required = true)
    protected String mirPrevEmplrInfo;
    @XmlElement(name = "MirListInfo", required = true)
    protected String mirListInfo;
    @XmlElement(name = "MirClftInfo", required = true)
    protected String mirClftInfo;

    /**
     * Gets the value of the mirPrcesInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirPrcesInfo() {
        return mirPrcesInfo;
    }

    /**
     * Sets the value of the mirPrcesInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirPrcesInfo(String value) {
        this.mirPrcesInfo = value;
    }

    /**
     * Gets the value of the mirCliInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MirCliInfo }
     *     
     */
    public MirCliInfo getMirCliInfo() {
        return mirCliInfo;
    }

    /**
     * Sets the value of the mirCliInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MirCliInfo }
     *     
     */
    public void setMirCliInfo(MirCliInfo value) {
        this.mirCliInfo = value;
    }

    /**
     * Gets the value of the mirNmInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirNmInfo() {
        return mirNmInfo;
    }

    /**
     * Sets the value of the mirNmInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirNmInfo(String value) {
        this.mirNmInfo = value;
    }

    /**
     * Gets the value of the mirPrevNmInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirPrevNmInfo() {
        return mirPrevNmInfo;
    }

    /**
     * Sets the value of the mirPrevNmInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirPrevNmInfo(String value) {
        this.mirPrevNmInfo = value;
    }

    /**
     * Gets the value of the mirAddrInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MirAddrInfo }
     *     
     */
    public MirAddrInfo getMirAddrInfo() {
        return mirAddrInfo;
    }

    /**
     * Sets the value of the mirAddrInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MirAddrInfo }
     *     
     */
    public void setMirAddrInfo(MirAddrInfo value) {
        this.mirAddrInfo = value;
    }

    /**
     * Gets the value of the mirEmplrInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MirEmplrInfo }
     *     
     */
    public MirEmplrInfo getMirEmplrInfo() {
        return mirEmplrInfo;
    }

    /**
     * Sets the value of the mirEmplrInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MirEmplrInfo }
     *     
     */
    public void setMirEmplrInfo(MirEmplrInfo value) {
        this.mirEmplrInfo = value;
    }

    /**
     * Gets the value of the mirPrevEmplrInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirPrevEmplrInfo() {
        return mirPrevEmplrInfo;
    }

    /**
     * Sets the value of the mirPrevEmplrInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirPrevEmplrInfo(String value) {
        this.mirPrevEmplrInfo = value;
    }

    /**
     * Gets the value of the mirListInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirListInfo() {
        return mirListInfo;
    }

    /**
     * Sets the value of the mirListInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirListInfo(String value) {
        this.mirListInfo = value;
    }

    /**
     * Gets the value of the mirClftInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirClftInfo() {
        return mirClftInfo;
    }

    /**
     * Sets the value of the mirClftInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirClftInfo(String value) {
        this.mirClftInfo = value;
    }

}
