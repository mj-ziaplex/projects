//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.08 at 04:47:28 PM AWST 
//


package ph.com.sunlife.ingenium.domain.reqst;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirCliCtryCdG"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirCliPstlCdG"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirCliCrntLocCdG"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mirCliCtryCdG",
    "mirCliPstlCdG",
    "mirCliCrntLocCdG"
})
@XmlRootElement(name = "MirAddrInfo")
public class MirAddrInfo {

    @XmlElement(name = "MirCliCtryCdG", required = true)
    protected String mirCliCtryCdG;
    @XmlElement(name = "MirCliPstlCdG", required = true)
    protected String mirCliPstlCdG;
    @XmlElement(name = "MirCliCrntLocCdG", required = true)
    protected String mirCliCrntLocCdG;

    /**
     * Gets the value of the mirCliCtryCdG property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirCliCtryCdG() {
        return mirCliCtryCdG;
    }

    /**
     * Sets the value of the mirCliCtryCdG property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirCliCtryCdG(String value) {
        this.mirCliCtryCdG = value;
    }

    /**
     * Gets the value of the mirCliPstlCdG property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirCliPstlCdG() {
        return mirCliPstlCdG;
    }

    /**
     * Sets the value of the mirCliPstlCdG property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirCliPstlCdG(String value) {
        this.mirCliPstlCdG = value;
    }

    /**
     * Gets the value of the mirCliCrntLocCdG property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMirCliCrntLocCdG() {
        return mirCliCrntLocCdG;
    }

    /**
     * Sets the value of the mirCliCrntLocCdG property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMirCliCrntLocCdG(String value) {
        this.mirCliCrntLocCdG = value;
    }

}
