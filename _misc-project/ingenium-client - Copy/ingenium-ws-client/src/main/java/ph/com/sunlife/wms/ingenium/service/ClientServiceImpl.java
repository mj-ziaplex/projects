package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.ingenium.IngeniumWebServiceClient;
import ph.com.sunlife.wms.ingenium.domain.Client;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryResponseAdaptor;
import ph.com.sunlife.wms.ingenium.ws.client.IngeniumResponseAdaptor;

import javax.xml.bind.JAXBException;

public class ClientServiceImpl implements ClientService {

    @Override
    public Client getClientDetails(final String clientId) throws JAXBException {
        String request = new ClientInquiryRequestBuilder(clientId)
                .createRequest();
        String response = new IngeniumWebServiceClient().getResponse(request);
        return new ClientInquiryResponseAdaptor(response).transmute();
    }

    @Override
    public Client getClientDetails(final String clientId, final String guid)
            throws JAXBException {
        String request = new ClientInquiryRequestBuilder(clientId)
                .setTransactionReferenceGUID(guid)
                .createRequest();
        String response = new IngeniumWebServiceClient().getResponse(request);
        IngeniumResponseAdaptor adaptor = new ClientInquiryResponseAdaptor(response);
        Client client = adaptor.transmute();
        return client;
    }
}
