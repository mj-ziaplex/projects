package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliIndvTitlTxtG")
public class MirDvClientIndividualTitleTextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliIndvTitlTxtT")
    private List<String> mirDvClientIndividualTitleTextTypes;

    public List<String> getMirDvClientIndividualTitleTextTypes() {
        return mirDvClientIndividualTitleTextTypes;
    }

    public void setMirDvClientIndividualTitleTextTypes(final List<String> titles) {
        this.mirDvClientIndividualTitleTextTypes = titles;
    }
}
