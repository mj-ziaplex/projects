package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.PolicyID;

@XStreamAlias("PolicyData")
public class PolicyData {

  @XStreamAlias("MirPolId")
  private PolicyID policyId;
  @XStreamAlias("SrcSystem")
  private String sourceSystem;

  public PolicyID getPolicyID() {
    return policyId;
  }

  public void setPolicyID(final PolicyID policyId) {
    this.policyId = policyId;
  }

  public String getSourceSystem() {
    return sourceSystem;
  }

  public void setSourceSystem(final String sourceSystem) {
    this.sourceSystem = sourceSystem;
  }
}
