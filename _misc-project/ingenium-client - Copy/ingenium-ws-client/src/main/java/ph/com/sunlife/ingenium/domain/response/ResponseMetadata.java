package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserAuthResponse")
public class ResponseMetadata {

    @XStreamAlias("TransResult")
    private TransactionResult transactionResult;
    @XStreamAlias("SvrDate")
    private String date;
    @XStreamAlias("SvrTime")
    private String time;

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(final TransactionResult transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
