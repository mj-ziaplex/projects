package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ResultCode {

    private int code;
    private String value;

    public ResultCode() { }

    public ResultCode(final int code, final String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
