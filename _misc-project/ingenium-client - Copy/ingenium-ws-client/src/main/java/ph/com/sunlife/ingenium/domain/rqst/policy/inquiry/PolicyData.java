//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.10 at 03:35:47 PM CST 
//


package ph.com.sunlife.ingenium.domain.reqst.policy.inquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}MirPolId"/>
 *         &lt;element ref="{http://ACORD.org/Standards/Life/2}SrcSystem"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mirPolId",
    "srcSystem"
})
@XmlRootElement(name = "PolicyData")
public class PolicyData {

    @XmlElement(name = "MirPolId", required = true)
    protected MirPolId mirPolId;
    @XmlElement(name = "SrcSystem", required = true)
    protected String srcSystem;

    /**
     * Gets the value of the mirPolId property.
     * 
     * @return
     *     possible object is
     *     {@link MirPolId }
     *     
     */
    public MirPolId getMirPolId() {
        return mirPolId;
    }

    /**
     * Sets the value of the mirPolId property.
     * 
     * @param value
     *     allowed object is
     *     {@link MirPolId }
     *     
     */
    public void setMirPolId(MirPolId value) {
        this.mirPolId = value;
    }

    /**
     * Gets the value of the srcSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcSystem() {
        return srcSystem;
    }

    /**
     * Sets the value of the srcSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcSystem(String value) {
        this.srcSystem = value;
    }

}
