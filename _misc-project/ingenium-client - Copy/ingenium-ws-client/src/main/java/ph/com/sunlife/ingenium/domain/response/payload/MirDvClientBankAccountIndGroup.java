package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("MirDvCliBnkAcctIndG")
public class MirDvClientBankAccountIndGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliBnkAcctIndT")
    protected List<String> mirDvClientBankAccountIndTypes;

    public List<String> getMirDvClientBankAccountIndTypes() {
        if (mirDvClientBankAccountIndTypes == null) {
            mirDvClientBankAccountIndTypes = new ArrayList<String>();
        }
        return this.mirDvClientBankAccountIndTypes;
    }

    public void setMirDvClientBankAccountIndTypes(final List<String> types) {
        this.mirDvClientBankAccountIndTypes = types;
    }
}
