package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserPswd")
public class PlainPassword extends Password {

  public PlainPassword() {
    this("");
  }

  public PlainPassword(final String password) {
    super("NONE", password);
  }
}
