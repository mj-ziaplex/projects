package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirEntrGivNmG")
public class MirEntryGivenNameGroup {

    @XStreamImplicit(itemFieldName = "MirEntrGivNmT")
    private List<String> mirEntryGivenNameTypes;

    public List<String> getMirEntryGivenNameTypes() {
        return mirEntryGivenNameTypes;
    }

    public void setMirEntryGivenNameTypes(List<String> mirEntryGivenNameTypes) {
        this.mirEntryGivenNameTypes = mirEntryGivenNameTypes;
    }
}
