package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrCntyCdG")
public class MirDvClientAddressCountryCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrCntyCdT")
    private List<String> mirDvClientAddressCountryCodeTexts;

    public List<String> getMirDvClientAddressCountryCodeTexts() {
        return mirDvClientAddressCountryCodeTexts;
    }

    public void setMirDvClientAddressCountryCodeTexts(final List<String> countryCodes) {
        mirDvClientAddressCountryCodeTexts = countryCodes;
    }
}
