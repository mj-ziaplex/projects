package ph.com.sunlife.wms.ingenium.ws.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.ingenium.domain.reqst.xml.*;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.text.DateFormat;

public class ClientInquiryRequestBuilder extends IngeniumRequestBuilder {

    private static final String TRANSACTION_TYPE = "ClientInquiry";
    private static final Class[] CLASSES_TO_BIND = new Class[] { TXLife.class,
            UserAuthRequest.class, TXLifeRequest.class, UserPassword.class,
            TransactionType.class, OLifE.class, ClientData.class,
            MirClientInformation.class };

    private final String clientId;
    private String transactionReferenceGUID;

    public ClientInquiryRequestBuilder(final String clientId) {
        this(clientId, "");
    }

    public ClientInquiryRequestBuilder(final String clientId,
                                       final String transactionReferenceGUID) {
        this.setClassToBind(CLASSES_TO_BIND);
        this.clientId = clientId;
        this.transactionReferenceGUID = transactionReferenceGUID;
    }

    public String getTransactionReferenceGUID() {
        return transactionReferenceGUID;
    }

    public ClientInquiryRequestBuilder setTransactionReferenceGUID(String transactionReferenceGUID) {
        this.transactionReferenceGUID = transactionReferenceGUID;
        return this;
    }

    @Override
    TXLifeRequest getRequest() {
        TXLifeRequest request = new TXLifeRequest();
        request.setTransactionReferenceGUID(transactionReferenceGUID);

        TransactionType transactionType = new TransactionType();
        transactionType.setTC(TRANSACTION_TYPE);
        transactionType.setTransactionType(TRANSACTION_TYPE);
        request.setTransactionType(transactionType);

        ApplicationContext context =
                new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);

        DateFormat formatter = (DateFormat) context.getBean("dateFormatter");
        request.setTransactionExecutionDate(formatter.format(executionTimestamp));
        formatter = (DateFormat) context.getBean("timeFormatter");
        request.setTransactionExecutionTime(formatter.format(executionTimestamp));

        MirClientInformation cliInfo = new MirClientInformation();
        cliInfo.setClientId(Long.valueOf(clientId));
        ClientData clientData = new ClientData();
        clientData.setSourceSystem("WMS");
        clientData.setMirClientInformation(cliInfo);
        OLifE olife = new OLifE();
        olife.setClientData(clientData);
        request.setOLifE(olife);

        return request;
    }
}
