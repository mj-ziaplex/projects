package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCliPstlCdG")
public class MirClientPostalCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliPstlCdT")
    private short clientPostalCodes;

    public short getClientPostalCodes() {
        return clientPostalCodes;
    }

    public void setClientPostalCodes(final short postalCodes) {
        clientPostalCodes = postalCodes;
    }
}
