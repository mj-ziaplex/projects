package ph.com.sunlife.wms.ingenium.configuration;

import com.sunlife.ascp.configuration.PropertiesConfiguration;
import com.sunlife.ascp.exception.MissingConfigurationException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class IngeniumPropertiesConfigurationReader {

    public static final String DEFAULT_CONFIGURATION_FILE = "D:/WMS/WorkflowComponents/Properties/ingenium.properties";

    private Map<String, String> configuration = new HashMap<String, String>();

    public IngeniumPropertiesConfigurationReader()
            throws MissingConfigurationException {
        this(new File(DEFAULT_CONFIGURATION_FILE));
    }

    public IngeniumPropertiesConfigurationReader(final File configurationPath)
            throws MissingConfigurationException {
        try {
            PropertiesConfiguration propertiesConfiguration =
                    new PropertiesConfiguration(configurationPath);
            this.configuration = propertiesConfiguration.getPropertiesMap();
        } catch (IOException ioe) {
            throw new MissingConfigurationException(ioe.getMessage(),
                    ioe.getCause());
        }
    }

    public Map<String, String> getAll() {
        return configuration;
    }

    public String getServerHost() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.SERVER_HOST);
    }

    public String getUsername() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.AUTHENTICATION_USERNAME);
    }

    public String getPasswordSalt() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.AUTHENTICATION_PASSWORD_SALT);
    }

    public String getPasswordPrefix() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.AUTHENTICATION_PASSWORD_PREFIX);
    }

    public int getPasswordOffset() {
        return Integer.parseInt(configuration.get(IngeniumPropertiesConfigurationKeys.AUTHENTICATION_PASSWORD_OFFSET));
    }

    public String getDatabaseServerHost() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_SERVER_HOST);
    }

    public String getDatabaseLinkedServer() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_LINKED_SERVER);
    }

    public String getDatabaseName() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_NAME);
    }

    public int getDatabasePort() {
        return Integer.parseInt(configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_PORT));
    }

    public String getDatabaseUsername() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_USERNAME);
    }

    public String getDatabasePassword() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATABASE_PASSWORD);
    }

    public String getDateFormat() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.DATE_FORMAT);
    }

    public String getTimeFormat() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.TIME_FORMAT);
    }

    public boolean isRequestResponseLoggingEnabled() {
        return Boolean.parseBoolean(configuration.get(IngeniumPropertiesConfigurationKeys.ENABLE_REQUEST_RESPONSE_LOGGING));
    }

    public String getRequestResponseLogLocation() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.REQUEST_RESPONSE_LOG_LOCATION);
    }

    public String getApplicationLogLocation() {
        return configuration.get(IngeniumPropertiesConfigurationKeys.APPLICATION_LOG_LOCATION);
    }
}
