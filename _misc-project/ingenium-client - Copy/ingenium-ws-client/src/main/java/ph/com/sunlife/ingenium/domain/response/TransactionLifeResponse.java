package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.Container;
import ph.com.sunlife.ingenium.domain.TransactionType;

@XStreamAlias("TXLifeResponse")
public class TransactionLifeResponse {

    @XStreamAlias("TransType")
    private TransactionType transactionType;
    @XStreamAlias("TransResult")
    private TransactionResult transactionResult;
    @XStreamAlias("OLifE")
    private Container container;

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(TransactionResult transactionResult) {
        this.transactionResult = transactionResult;
    }

    public Container getContainer() {
        return container;
    }

    public void setContainer(Container container) {
        this.container = container;
    }
}
