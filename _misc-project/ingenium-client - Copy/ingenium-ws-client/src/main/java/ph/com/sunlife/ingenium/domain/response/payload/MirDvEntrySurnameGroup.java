package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEntrSurNmG")
public class MirDvEntrySurnameGroup {

    @XStreamImplicit(itemFieldName = "MirDvEntrSurNmT")
    private List<String> mirDvEntrySurnameTypes;

    public List<String> getMirDvEntrySurnameTypes() {
        return mirDvEntrySurnameTypes;
    }

    public void setMirDvEntrySurnameTypes(List<String> surnames) {
        this.mirDvEntrySurnameTypes = surnames;
    }
}
