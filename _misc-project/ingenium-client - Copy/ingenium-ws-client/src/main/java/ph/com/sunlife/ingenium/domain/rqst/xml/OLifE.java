package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "OLifE")
public class OLifE {

    @XmlElement(name = "ClientData", required = true)
    protected ClientData clientData;

    public ClientData getClientData() {
        return clientData;
    }

    public void setClientData(ClientData value) {
        this.clientData = value;
    }
}
