package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrLn1TxtG")
public class MirClientAddressLine1TextGroup {

    @XStreamImplicit(itemFieldName = "MirCliAddrLn1TxtT")
    private List<String> mirClientAddressLine1Texts;

    public List<String> getMirClientAddressLine1Texts() {
        return mirClientAddressLine1Texts;
    }

    public void setMirClientAddressLine1Texts(final List<String> texts) {
        mirClientAddressLine1Texts = texts;
    }
}
