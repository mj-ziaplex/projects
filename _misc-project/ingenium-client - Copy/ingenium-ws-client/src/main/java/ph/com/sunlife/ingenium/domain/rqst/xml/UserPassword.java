package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cryptographicType", "password" })
@XmlRootElement(name = "UserPswd")
public class UserPassword {

    @XmlElement(name = "CryptType", required = true)
    protected String cryptographicType;
    @XmlElement(name = "Pswd", required = true)
    protected String password;

    public String getCryptographicType() {
        return cryptographicType;
    }

    public void setCryptographicType(final String type) {
        cryptographicType = type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String value) {
        this.password = value;
    }
}
