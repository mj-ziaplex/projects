package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirAddrStatChngDtG")
public class MirAddressStatusChangeDateGroup {

    @XStreamImplicit(itemFieldName = "MirAddrStatChngDtT")
    private List<String> mirAddressStatusChangeDateTexts;

    public List<String> getMirAddressStatusChangeDateTexts() {
        return mirAddressStatusChangeDateTexts;
    }

    public void setMirAddressStatusChangeDateTexts(final List<String> changeDates) {
        mirAddressStatusChangeDateTexts = changeDates;
    }
}
