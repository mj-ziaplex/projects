package ph.com.sunlife.ingenium.domain.response;

import ph.com.sunlife.ingenium.domain.response.payload.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ClientData")
public class ClientData {

    @XmlElement(name = "MirPrcesInfo", required = true)
    protected MirProcessInformation mirPrcesInfo;
    @XmlElement(name = "MirCliInfo", required = true)
    protected MirClientInformation mirCliInfo;
    @XmlElement(name = "MirBnkInfo", required = true)
    protected MirBankInformation mirBnkInfo;
    @XmlElement(name = "MirNmInfo", required = true)
    protected MirNameInformation mirNmInfo;
    @XmlElement(name = "MirPrevNmInfo", required = true)
    protected MirPreviousNameInformation mirPrevNmInfo;
    @XmlElement(name = "MirAddrInfo", required = true)
    protected MirAddressInformation mirAddrInfo;
    @XmlElement(name = "MirPrevAddrInfo", required = true)
    protected MirPreviousAddressInformation mirPreviousAddressInformation;
    @XmlElement(name = "MirEmplrInfo", required = true)
    protected MirEmployerInformation mirEmployerInformation;
    @XmlElement(name = "MirPrevEmplrInfo", required = true)
    protected MirPreviousEmployerInformation mirPreviousEmployerInformation;
    @XmlElement(name = "MirCrcInfo", required = true)
    protected MirCrcInformation mirCrcInformation;
    @XmlElement(name = "MirListInfo", required = true)
    protected String mirListInfo;
    @XmlElement(name = "MirCliEnoticeInd", required = true)
    protected String mirCliEnoticeInd;
    @XmlElement(name = "MirCliSmsInd", required = true)
    protected String mirCliSmsInd;
    @XmlElement(name = "MirClftInfo", required = true)
    protected MirClftInfo mirClftInfo;
    @XmlElement(name = "MirDvFutDtTrxnTypCd")
    protected byte mirDvFutDtTrxnTypCd;
    @XmlElement(name = "MirCliSmsOrNotifInd", required = true)
    protected String mirCliSmsOrNotifInd;
    @XmlElement(name = "MirCliEorNotifInd", required = true)
    protected String mirCliEorNotifInd;
}
