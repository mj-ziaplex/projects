package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrYrDurG")
public class MirClientAddressYearDurationCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrYrDurT")
    private List<String> clientAddressYearDurations;

    public List<String> getClientAddressYearDurations() {
        return clientAddressYearDurations;
    }

    public void setClientAddressYearDurations(final List<String> durations) {
        this.clientAddressYearDurations = durations;
    }
}
