package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrAddrEffDtG")
public class MirDvEmployerAddressEffectivityDateGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrAddrEffDtT")
    private List<String> mirDvEmployerAddressEffectivityDateTexts;

    public List<String> getMirDvEmployerAddressEffectivityDateTexts() {
        return mirDvEmployerAddressEffectivityDateTexts;
    }

    public void setMirDvEmployerAddressEffectivityDateTexts(final List<String> effectivityDateTexts) {
        mirDvEmployerAddressEffectivityDateTexts = effectivityDateTexts;
    }
}
