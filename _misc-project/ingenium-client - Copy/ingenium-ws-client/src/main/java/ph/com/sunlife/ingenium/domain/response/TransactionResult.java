package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("TransResult")
public class TransactionResult {

    @XStreamAlias("ResultCode")
    private ResultCode resultCode;
    @XStreamImplicit
    private List<ResultInformation> informations;

    public TransactionResult() { }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public List<ResultInformation> getInformations() {
        return informations;
    }

    public void setInformations(List<ResultInformation> informations) {
        this.informations = informations;
    }
}
