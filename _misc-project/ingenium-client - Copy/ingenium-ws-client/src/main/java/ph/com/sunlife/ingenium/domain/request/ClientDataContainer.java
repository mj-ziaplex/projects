package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.Container;

@XStreamAlias("OLife")
public class ClientDataContainer implements Container {

  @XStreamAlias("ClientData")
  private ClientData data;

  public ClientDataContainer() { }

  @Override
  public ClientData getPayload() {
    return data;
  }

  public ClientDataContainer setPayload(ClientData clientData) {
    this.data = clientData;
    return this;
  }
}
