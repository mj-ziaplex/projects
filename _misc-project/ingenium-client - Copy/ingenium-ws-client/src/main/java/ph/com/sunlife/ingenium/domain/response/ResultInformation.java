package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ResultInfo")
public class ResultInformation {

    @XStreamAlias("ResultInfoCode")
    private ResultInformationCode code;
    @XStreamAlias("ResultInfoDesc")
    private String description;

    public ResultInformation() { }

    public ResultInformationCode getCode() {
        return code;
    }

    public void setCode(ResultInformationCode code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
