package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliCityNmTxtG")
public class MirClientCityNameTextCluster {

    @XStreamImplicit(itemFieldName = "MirCliCityNmTxtT")
    private List<String> clientCityNameTexts;

    public List<String> getClientCityNameTexts() {
        return clientCityNameTexts;
    }

    public void setClientCityNameTexts(List<String> texts) {
        this.clientCityNameTexts = texts;
    }
}
