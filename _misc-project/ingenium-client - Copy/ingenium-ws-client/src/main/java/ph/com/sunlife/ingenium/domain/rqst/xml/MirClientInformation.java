package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "MirCliInfo")
public class MirClientInformation {

    @XmlElement(name = "MirCliId")
    protected long mirClientId;

    public long getClientId() {
        return mirClientId;
    }

    public void setClientId(long value) {
        this.mirClientId = value;
    }

}
