package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ReturnCodes {

    @XStreamAlias("LsirReturnCd")
    private String lsirReturnCode;
    @XStreamAlias("MirRetrnCd")
    private String MirReturnCode;
    @XStreamAlias("LsirMoreDataInd")
    private String lsirMoreDataIndicator;

    public ReturnCodes() { }

    public String getLsirReturnCode() {
        return lsirReturnCode;
    }

    public void setLsirReturnCode(String lsirReturnCode) {
        this.lsirReturnCode = lsirReturnCode;
    }

    public String getMirReturnCode() {
        return MirReturnCode;
    }

    public void setMirReturnCode(String mirReturnCode) {
        MirReturnCode = mirReturnCode;
    }

    public String getLsirMoreDataIndicator() {
        return lsirMoreDataIndicator;
    }

    public void setLsirMoreDataIndicator(String lsirMoreDataIndicator) {
        this.lsirMoreDataIndicator = lsirMoreDataIndicator;
    }
}
