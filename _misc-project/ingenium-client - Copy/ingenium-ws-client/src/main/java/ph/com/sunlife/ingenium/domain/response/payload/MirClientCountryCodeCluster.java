package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MirCliCtryCdG")
public class MirClientCountryCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliCtryCdT")
    private String mirClientCountryCodes;

    public String getMirClientCountryCodes() {
        return mirClientCountryCodes;
    }

    public void setMirClientCountryCodes(String mirClientCountryCodes) {
        this.mirClientCountryCodes = mirClientCountryCodes;
    }
}
