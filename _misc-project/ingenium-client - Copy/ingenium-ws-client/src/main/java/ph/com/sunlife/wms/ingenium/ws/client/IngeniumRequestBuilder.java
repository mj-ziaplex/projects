package ph.com.sunlife.wms.ingenium.ws.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.ingenium.domain.reqst.xml.TXLife;
import ph.com.sunlife.ingenium.domain.reqst.xml.TXLifeRequest;
import ph.com.sunlife.ingenium.domain.reqst.xml.UserAuthRequest;
import ph.com.sunlife.ingenium.domain.reqst.xml.UserPassword;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.Date;

public abstract class IngeniumRequestBuilder {

    private Class[] classToBind;
    protected Date executionTimestamp;
    protected TXLife txLife;

    public IngeniumRequestBuilder() {
        executionTimestamp = new Date();

        UserAuthRequest auth = new UserAuthRequest();
        UserPassword password = new UserPassword();

        password.setCryptographicType("NONE");

        ApplicationContext context =
                new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);

        auth.setUserLoginName((String) context.getBean("username"));
        password.setPassword((String) context.getBean("password"));
        auth.setUserPswd(password);

        txLife = new TXLife();
        txLife.setUserAuthRequest(auth);
    }

    public String createRequest() throws JAXBException {
        txLife.setTXLifeRequest(getRequest());

        StringWriter output = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(classToBind);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(txLife, output);
        return output.toString();
    }

    Class[] getClassToBind() {
        return classToBind;
    }

    void setClassToBind(Class[] classToBind) {
        this.classToBind = classToBind;
    }

    abstract TXLifeRequest getRequest();
}
