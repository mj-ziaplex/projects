package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.response.payload.MirUserMessageGroup;

@XStreamAlias("OLifE")
public class ClientInquiryContainer {

    @XStreamAlias("ReturnCds")
    private ReturnCodes returnCodes;
    @XStreamAlias("ClientData")
    private ClientData clientData;
    @XStreamAlias("ClientInquiryData")
    private ClientInquiryData clientInquiryData;
    @XStreamAlias("MirUserMsgG")
    private MirUserMessageGroup mMirUserMessageGroup;
}
