package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvMidNmG")
public class MirClientIndividualMiddlenameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvMidNmT")
    private List<String> mirClientIndividualMiddlenameTypes;

    public List<String> getMirClientIndividualMiddlenameTypes() {
        return mirClientIndividualMiddlenameTypes;
    }

    public void setMirClientIndividualMiddlenameTypes(final List<String> types) {
        mirClientIndividualMiddlenameTypes = types;
    }
}
