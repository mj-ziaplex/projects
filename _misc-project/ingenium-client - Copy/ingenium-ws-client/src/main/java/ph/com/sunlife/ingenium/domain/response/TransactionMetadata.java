package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

@XStreamAlias("UserAuthResponse")
public class TransactionMetadata {

    @XStreamAlias("TransResult")
    private TransactionResult transactionResult;
    @XStreamAlias("SvrDate")
    private String date;
    @XStreamAlias("SvrTime")
    private String time;

    public TransactionMetadata() { }

    public TransactionResult getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(TransactionResult transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
