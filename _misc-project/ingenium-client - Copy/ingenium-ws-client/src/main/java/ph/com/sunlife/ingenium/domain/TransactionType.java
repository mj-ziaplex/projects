package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("TransType")
public class TransactionType {

  @XStreamAsAttribute
  private String tc;
  private String value;

  public TransactionType(final String type) {
    this.setValue(type);
    this.setTc(getValue());
  }

  public TransactionType(final String tc, final String type) {
    this.setTc(tc);
    this.setValue(type);
  }

  public String getTc() {
    return tc;
  }

  public void setTc(String tc) {
    this.tc = tc;
  }

  public String getValue() {
    return value;
  }

  public void setValue(final String value) {
    this.value = value;
  }
}
