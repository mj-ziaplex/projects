package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.wms.ingenium.domain.Policy;

@XStreamAlias("MirPolId")
public class PolicyID {

    @XStreamAlias("MirDvEffDt")
    private String effectivityDate;
    @XStreamAlias("MirPolIdBase")
    private String base;
    @XStreamAlias("MirPolIdSfx")
    private String suffix;

    public PolicyID() { }

    public PolicyID(final String policyId) {
        base = policyId.substring(0, 9);
        if (policyId.length() > 9) {
            suffix = policyId.substring(9, policyId.length());
        } else {
            suffix = "";
        }
    }

    public PolicyID(final String policyIdBase, final String policyIdSuffix) {
        base = policyIdBase;
        suffix = policyIdSuffix;
    }

    public String getEffectivityDate() {
        return effectivityDate;
    }

    public void setEffectivityDate(String effectivityDate) {
        this.effectivityDate = effectivityDate;
    }

    public String getBase() {
        return base;
    }

    public PolicyID setBase(String base) {
        this.base = base;
        return this;
    }

    public String getSuffix() {
        return suffix;
    }

    public PolicyID setSuffix(String suffix) {
        this.suffix = suffix;
        return  this;
    }
}
