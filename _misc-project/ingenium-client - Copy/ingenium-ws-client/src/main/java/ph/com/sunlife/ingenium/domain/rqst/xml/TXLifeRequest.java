package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "transactionReferenceGUID",
        "transactionType",
        "transactionExecutionDate",
        "transactionExecutionTime",
        "oLifE"
})
@XmlRootElement(name = "TXLifeRequest")
public class TXLifeRequest {

    @XmlElement(name = "TransRefGUID", required = true)
    protected String transactionReferenceGUID;
    @XmlElement(name = "TransType", required = true)
    protected TransactionType transactionType;
    @XmlElement(name = "TransExeDate", required = true)
    protected String transactionExecutionDate;
    @XmlElement(name = "TransExeTime", required = true)
    protected String transactionExecutionTime;
    @XmlElement(name = "OLife", required = true)
    protected OLifE oLifE;

    public String getTransactionReferenceGUID() {
        return transactionReferenceGUID;
    }

    public void setTransactionReferenceGUID(String value) {
        this.transactionReferenceGUID = value;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType value) {
        this.transactionType = value;
    }

    public String getTransactionExecutionDate() {
        return transactionExecutionDate;
    }

    public void setTransactionExecutionDate(String value) {
        this.transactionExecutionDate = value;
    }

    public String getTransactionExecutionTime() {
        return transactionExecutionTime;
    }

    public void setTransactionExecutionTime(String value) {
        this.transactionExecutionTime = value;
    }

    public OLifE getOLifE() {
        return oLifE;
    }

    public void setOLifE(OLifE value) {
        this.oLifE = value;
    }
}
