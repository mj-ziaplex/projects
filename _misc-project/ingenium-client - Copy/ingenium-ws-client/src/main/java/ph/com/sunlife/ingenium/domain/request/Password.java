package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("UserPswd")
public abstract class Password {

  @XStreamAlias("CryptType")
  private String cryptographicAlgorithm;
  @XStreamAlias("Pswd")
  private String passwordText;

  public Password(final String algorithm, final String text) {
    cryptographicAlgorithm = algorithm;
    passwordText = text;
  }

  public String getCryptographicAlgorithm() {
    return cryptographicAlgorithm;
  }

  public Password setCryptographicAlgorithm(final String algorithm) {
    cryptographicAlgorithm = algorithm;
    return this;
  }

  public String getPasswordText() {
    return passwordText;
  }

  public Password setPasswordText(final String text) {
    this.passwordText = text;
    return this;
  }
}
