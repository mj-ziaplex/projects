package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.wms.ingenium.domain.ConsolidatedInformation;
import ph.com.sunlife.wms.ingenium.domain.Coverage;
import ph.com.sunlife.wms.ingenium.domain.Requirement;

import java.util.ArrayList;
import java.util.List;

public class PolicyServiceImpl implements PolicyService {

  @Override
  public Coverage getCoverageInformation(final String policyId) {
    return new Coverage();
  }

  @Override
  public ConsolidatedInformation getPolicyClientDetails(final String policyId) {
    return new ConsolidatedInformation();
  }

  @Override
  public List<Requirement> getRequirement(final String policyId) {
    return new ArrayList<Requirement>();
  }

  @Override
  public String getPlanId(final String policyId) {
    return "";
  }

  @Override
  public boolean createRequirement(String mirPolicyIdBase, String requirementId, String status) {
    return true;
  }
}
