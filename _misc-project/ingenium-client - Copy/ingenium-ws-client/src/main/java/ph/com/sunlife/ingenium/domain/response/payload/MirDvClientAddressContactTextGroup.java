package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrCntctTxtG")
public class MirDvClientAddressContactTextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrCntctTxtT")
    private List<String> mirDvClientAddressContactTexts;

    public List<String> getMirDvClientAddressContactTexts() {
        return mirDvClientAddressContactTexts;
    }

    public void setMirDvClientAddressContactTexts(final List<String> contacts) {
        mirDvClientAddressContactTexts = contacts;
    }
}
