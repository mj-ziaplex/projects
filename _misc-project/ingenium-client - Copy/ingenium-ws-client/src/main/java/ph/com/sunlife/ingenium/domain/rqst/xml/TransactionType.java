package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "transactionType" })
@XmlRootElement(name = "TransType")
public class TransactionType {

    @XmlValue
    protected String transactionType;
    @XmlAttribute(name = "tc")
    protected String tc;

    public String getTransactionType() {
        return transactionType;
    }

    public TransactionType setTransactionType(final String type) {
        this.transactionType = type;
        return this;
    }

    public String getTC() {
        return tc;
    }

    public TransactionType setTC(final String transactionCode) {
        this.tc = transactionCode;
        return this;
    }
}
