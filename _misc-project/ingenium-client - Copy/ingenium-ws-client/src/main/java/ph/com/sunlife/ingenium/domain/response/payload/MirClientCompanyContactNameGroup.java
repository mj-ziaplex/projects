package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@XStreamAlias("MirCliCompCntctNmG")
public class MirClientCompanyContactNameGroup {

    @XmlElement(name = "MirCliEmplrLocCdT")
    private List<String> mirClientEmployerLocationCodeTexts;

    public List<String> getMirClientEmployerLocationCodeTexts() {
        return mirClientEmployerLocationCodeTexts;
    }

    public void setMirClientEmployerLocationCodeTexts(List<String> locationCodes) {
        mirClientEmployerLocationCodeTexts = locationCodes;
    }
}
