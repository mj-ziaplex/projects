package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.MirCommonFields;

@XStreamAlias("RequirementData")
public class RequirementData {

    @XStreamAlias("MirCommonFields")
     private MirCommonFields commonFields;

    public RequirementData() { }

    public RequirementData(MirCommonFields commonFields) {
        this.commonFields = commonFields;
    }

    public MirCommonFields getCommonFields() {
        return commonFields;
    }

    public RequirementData setCommonFields(MirCommonFields commonFields) {
        this.commonFields = commonFields;
        return this;
    }
}
