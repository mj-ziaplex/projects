package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("OLifE")
public interface Container<Box extends Container> {

  <Payload> Payload getPayload();
}
