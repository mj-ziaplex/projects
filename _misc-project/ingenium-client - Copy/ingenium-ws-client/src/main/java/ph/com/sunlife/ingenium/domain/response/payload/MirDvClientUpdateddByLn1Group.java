package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliUpdByLn1G")
public class MirDvClientUpdateddByLn1Group {

    @XStreamImplicit(itemFieldName = "MirDvCliUpdByLnT")
    private List<String> mirDvClientUpdatedByLnTypes;

    public List<String> getMirDvClientUpdatedByLnTypes() {
        return mirDvClientUpdatedByLnTypes;
    }

    public void setMirDvClientUpdatedByLnTypes(List<String> updatedBys) {
        this.mirDvClientUpdatedByLnTypes = updatedBys;
    }
}
