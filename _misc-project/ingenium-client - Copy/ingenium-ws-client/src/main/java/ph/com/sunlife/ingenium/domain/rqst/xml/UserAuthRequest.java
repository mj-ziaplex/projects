package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userLoginName", "userPswd" })
public class UserAuthRequest {

    @XmlElement(name = "UserLoginName", required = true)
    protected String userLoginName;
    @XmlElement(name = "UserPswd", required = true)
    protected UserPassword userPswd;

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }

    public UserPassword getUserPswd() {
        return userPswd;
    }

    public void setUserPswd(UserPassword value) {
        this.userPswd = value;
    }
}
