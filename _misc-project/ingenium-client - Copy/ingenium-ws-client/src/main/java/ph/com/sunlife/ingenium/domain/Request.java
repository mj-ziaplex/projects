package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.request.BasicCredentials;
import ph.com.sunlife.ingenium.domain.request.Credentials;
import ph.com.sunlife.ingenium.domain.request.TransactionLifeRequest;

@XStreamAlias("TXLife")
public class Request extends RequestResponse {

  @XStreamAlias("UserAuthRequest")
  private Credentials credentials;
  @XStreamAlias("TXLifeRequest")
  private TransactionLifeRequest transaction;

  public Request() {
    credentials = new BasicCredentials();
  }

  public Request(final Credentials credentials) {
    this.credentials = credentials;
  }

  public Request(final Credentials credentials, final TransactionLifeRequest transaction) {
    this.credentials = credentials;
    this.transaction = transaction;
  }

  public Credentials getCredentials() {
    return credentials;
  }

  public Request setCredentials(final Credentials credentials) {
    this.credentials = credentials;
    return this;
  }

  public TransactionLifeRequest getTransaction() {
    return transaction;
  }

  public void setTransaction(final TransactionLifeRequest transaction) {
    this.transaction = transaction;
  }
}
