package ph.com.sunlife.wms.ingenium.domain;

import java.util.Date;

public class Client {

    private String id;
    private String givenName;
    private String middlename;
    private String familyName;
    private Date birthdate;
    private String otherLegalGivenName;
    private String otherLegalMiddleName;
    private String otherLegalFamilyName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(final Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getOtherLegalGivenName() {
        return otherLegalGivenName;
    }

    public void setOtherLegalGivenName(final String name) {
        this.otherLegalGivenName = name;
    }

    public String getOtherLegalMiddleName() {
        return otherLegalMiddleName;
    }

    public void setOtherLegalMiddleName(final String name) {
        this.otherLegalMiddleName = name;
    }

    public String getOtherLegalFamilyName() {
        return otherLegalFamilyName;
    }

    public void setOtherLegalFamilyName(final String name) {
        this.otherLegalFamilyName = name;
    }
}
