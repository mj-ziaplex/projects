package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XStreamAlias("MirDvEmplrPstlCdG")
public class MirDvEmployerPostalCodeGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrPstlCdT")
    private List<Short> mirDvEmployerPostalCodeTexts;

    public List<Short> getMirDvEmployerPostalCodeTexts() {
        return mirDvEmployerPostalCodeTexts;
    }

    public void setMirDvEmployerPostalCodeTexts(final List<Short> postalCodes) {
        this.mirDvEmployerPostalCodeTexts = postalCodes;
    }
}
