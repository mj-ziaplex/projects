package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.response.xml.TXLife;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;

public abstract class IngeniumResponseAdaptor {

    private Class[] classToBind;
    protected TXLife response;

    public IngeniumResponseAdaptor(final String reply, final Class[] classToBind) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(classToBind);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        response = (TXLife) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(reply.getBytes()));
    }

    protected Class[] getClassToBind() {
        return classToBind;
    }

    protected void setClassToBind(Class[] classToBind) {
        this.classToBind = classToBind;
    }

    public abstract <T> T transmute();
}
