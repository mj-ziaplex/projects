package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sourceSystem",
    "mirClientInformation"
})
@XmlRootElement(name = "ClientData")
public class ClientData {

    @XmlElement(name = "SrcSystem", required = true)
    protected String sourceSystem;
    @XmlElement(name = "MirCliInfo", required = true)
    protected MirClientInformation mirClientInformation;

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String value) {
        this.sourceSystem = value;
    }

    public MirClientInformation getMirClientInformation() {
        return mirClientInformation;
    }

    public void setMirClientInformation(MirClientInformation value) {
        this.mirClientInformation = value;
    }

}
