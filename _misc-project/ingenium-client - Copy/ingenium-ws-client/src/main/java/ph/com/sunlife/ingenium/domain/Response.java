package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.response.ResponseMetadata;
import ph.com.sunlife.ingenium.domain.response.TransactionLifeResponse;

@XStreamAlias("TXLife")
public class Response extends RequestResponse {

    @XStreamAlias("UserAuthResponse")
    private ResponseMetadata metadata;
    @XStreamAlias("TXLifeResponse")
    private TransactionLifeResponse transaction;

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public TransactionLifeResponse getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionLifeResponse transaction) {
        this.transaction = transaction;
    }
}
