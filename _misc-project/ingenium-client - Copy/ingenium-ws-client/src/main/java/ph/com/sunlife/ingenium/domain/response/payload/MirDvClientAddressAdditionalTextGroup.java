package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrAddlTxtG")
public class MirDvClientAddressAdditionalTextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrAddlTxtT")
    private List<String> mirDvClientAddressAdditionalTexts;

    public List<String> getMirDvClientAddressAdditionalTexts() {
        return mirDvClientAddressAdditionalTexts;
    }

    public void setMirDvClientAddressAdditionalTexts(final List<String> additionalTexts) {
        mirDvClientAddressAdditionalTexts = additionalTexts;
    }
}
