package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.Container;
import ph.com.sunlife.ingenium.domain.TransactionType;

@XStreamAlias("TXLifeRequest")
public class TransactionLifeRequest {

  @XStreamAlias("TransRefGUID")
  private String referenceGUID;
  @XStreamAlias("TransType")
  private TransactionType type;
  @XStreamAlias("TransExeDate")
  private String executionDate;
  @XStreamAlias("TransExeTime")
  private String executionTime;
  @XStreamAlias("OLifE")
  private Container container;

  public TransactionLifeRequest() { }

  public TransactionLifeRequest(final TransactionType type) {
    this.type = type;
  }

  public TransactionLifeRequest(final String type) {
    this.type = new TransactionType(type);
  }

  public TransactionLifeRequest(final TransactionType type, final Container container) {
    this.type = type;
    this.container = container;
  }

  public TransactionLifeRequest(final String type, final Container container) {
    this.type = new TransactionType(type);
    this.container = container;
  }

  public String getReferenceGUID() {
    return referenceGUID;
  }

  public TransactionLifeRequest setReferenceGUID(final String id) {
    this.referenceGUID = id;
    return this;
  }

  public String getType() {
    return type.getValue();
  }

  public TransactionLifeRequest setType(final TransactionType type) {
    this.type = type;
    return this;
  }

  public TransactionLifeRequest setType(final String type) {
    this.type = new TransactionType(type);
    return this;
  }

  public String getExecutionDate() {
    return executionDate;
  }

  public TransactionLifeRequest setExecutionDate(final String executionDate) {
    this.executionDate = executionDate;
    return this;
  }

  public String getExecutionTime() {
    return executionTime;
  }

  public TransactionLifeRequest setExecutionTime(final String executionTime) {
    this.executionTime = executionTime;
    return this;
  }

  public Container getContainer() {
    return container;
  }

  public void setContainer(final Container container) {
    this.container = container;
  }
}
