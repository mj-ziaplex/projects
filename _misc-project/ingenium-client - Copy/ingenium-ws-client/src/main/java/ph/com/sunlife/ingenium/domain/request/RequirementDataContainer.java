package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.Container;

@XStreamAlias("OLifE")
public class RequirementDataContainer implements Container {

    @XStreamAlias("RequirementData")
    private RequirementData data;

    public RequirementDataContainer() { }

    public RequirementDataContainer(final RequirementData data) {
        this.data = data;
    }

    @Override
    public RequirementData getPayload() {
        return data;
    }

    public RequirementDataContainer setPayload(final RequirementData data) {
        this.data = data;
        return this;
    }
}
