package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrChngDtG")
public class MirClientAddressChangeDateCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrChngDtT")
    private List<String> clientAddressChangeDates;

    public List<String> getClientAddressChangeDates() {
        return clientAddressChangeDates;
    }

    public void setClientAddressChangeDates(List<String> changeDates) {
        clientAddressChangeDates = changeDates;
    }
}
