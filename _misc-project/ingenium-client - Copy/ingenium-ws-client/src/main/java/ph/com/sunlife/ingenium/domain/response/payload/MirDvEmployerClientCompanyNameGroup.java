package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvEmplrCliCoNmG")
public class MirDvEmployerClientCompanyNameGroup {

    @XStreamImplicit(itemFieldName = "MirDvEmplrCliCoNmT")
    private List<String> mirDvEmployerClientCompanyNameTexts;

    public List<String> getMirDvEmployerClientCompanyNameTexts() {
        return mirDvEmployerClientCompanyNameTexts;
    }

    public void setMirDvEmployerClientCompanyNameTexts(final List<String> companyNames) {
        mirDvEmployerClientCompanyNameTexts = companyNames;
    }
}
