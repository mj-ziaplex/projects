package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirBnkAcctIdG")
public class MirBankAccountIDGroup {

    @XStreamImplicit(itemFieldName = "MirBnkAcctIdT")
    private List<String> mirBankAccountIdTypes;

    public List<String> getMirBankAccountIdTypes() {
        return mirBankAccountIdTypes;
    }

    public MirBankAccountIDGroup setMirBankAccountIdTypes(final List<String> types) {
        this.mirBankAccountIdTypes = types;
        return this;
    }
}
