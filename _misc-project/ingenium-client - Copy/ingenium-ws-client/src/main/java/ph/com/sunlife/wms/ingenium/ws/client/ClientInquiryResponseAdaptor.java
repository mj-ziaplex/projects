package ph.com.sunlife.wms.ingenium.ws.client;

import ph.com.sunlife.ingenium.domain.response.*;
import ph.com.sunlife.ingenium.domain.response.payload.*;
import ph.com.sunlife.ingenium.domain.response.xml.*;
import ph.com.sunlife.ingenium.domain.response.xml.TransactionType;
import ph.com.sunlife.wms.ingenium.domain.Client;

import javax.xml.bind.JAXBException;

public class ClientInquiryResponseAdaptor extends IngeniumResponseAdaptor {

    private static final Class[] CLASSES_TO_BIND =
            new Class[] { TXLife.class, UserAuthResponse.class,
                          TXLifeResponse.class, TransactionResult.class,
                          TransactionType.class, ResultCode.class,
                          ResultInformation.class, OLifE.class,
                          ReturnCodes.class, ClientInquiryData.class,
                          MirAddrInfo.class, MirBnkInfo.class,
                          MirBusAppInfo.class, MirClftInfo.class,
                          MirCliInfo.class, MirCrcInformation.class,
                          MirEmployerInformation.class, MirNmInfo.class,
                          MirPrcesInfo.class, MirPreviousAddressInformation.class,
                          MirPreviousEmployerInformation.class, MirPrevNmInfo.class,
                          MirUserMsgG.class, MirAddressStatusChangeDateGroup.class,
                          MirBnkAcctHldrNmG.class, MirBnkAcctIdG.class,
                          MirBnkAcctMicrIndG.class, MirBnkAcctTypCdG.class,
                          MirBnkBrIdG.class, MirBnkIdG.class, MirBnkNmG.class,
                          MirBusAppInfo.class, MirClientAddressAdditionalTextGroup.class,
                          MirClientAddressChangeDateCluster.class, MirClientAddressContactTextCluster.class,
                          MirClientAddressCountryCodeCluster.class, MirCliAddrEffDtG.class,
                          MirClientAddressLine1TextGroup.class, MirClientAddressLine2TextGroup.class,
                          MirClientAddressLine3TextGroup.class, MirClientAddressMuninicipalityCodeCluster.class,
                          MirClientAddressStatusCodeCluster.class, MirClientAddressYearDurationCluster.class,
                          MirClientAlternateAddressCodeCluster.class, MirCliBnkAcctDtG.class,
                          MirCliBnkAcctNumG.class, MirClientCityNameTextCluster.class,
                          MirCliCoNmG.class, MirClientCrcNumGroup.class,
                          MirClientCrntLocationCodeCluster.class,
            MirClientCountryCodeCluster.class, MirClientEmployerLocationCodeGroup.class,
            MirClientEmployeeYearDurationGroup.class, MirCliIndvEffDtG.class,
            MirCliIndvGivNmG.class, MirCliIndvMidNmG.class,
            MirCliIndvSfxNmG.class, MirCliIndvSurNmG.class,
            MirCliIndvTitlTxtG.class,
            MirCliOtherFirstNmG.class, MirCliOtherLastNmG.class,
            MirCliOtherMidNmG.class, MirClientPostalCodeCluster.class,
            MirClientResidenceNumberCluster.class, MirClientResidenceTypeCodeCluster.class,
            MirCrcBankIdGroup.class, MirCrcCvv2NumberGroup.class,
            MirCrcHolderNameGroup.class, MirCrcIdGroup.class,
            MirCrcTypeCodeGroup.class, MirCrcExpiryMoGroup.class,
            MirCrcExpiryYearGroup.class, MirDvClientAddressAdditionalTextGroup.class,
            MirDvClientAddressContactTextGroup.class, MirDvClientAddressCountryCodeGroup.class,
            MirDvClientAddressEffectivityDateGroup.class, MirDvClientAddressLine1TextGroup.class,
            MirDvClientAddressLine2TextGroup.class, MirDvClientAddressLine3TextGroup.class,
            MirDvClientAddressLastUpdatedLineGroup.class, MirDvClientAddressMuninicipalityCodeGroup.class,
            MirDvClientAddressUpdatedByLineGroup.class, MirDvClientAddressYearDurationGroup.class,
            MirDvClientAlternateAddressCodeGroup.class, MirDvClientBankAccountIndGroup.class,
            MirDvClientCityNameTextGroup.class, MirDvClientCrntLocationCodeGroup.class,
            MirDvClientIndividualEffectivityDateGroup.class,
            MirDvCliIndvMidNmG.class, MirDvCliIndvSfxNmG.class,
            MirDvCliIndvTitlTxtG.class, MirDvClientPostalCodeGroup.class,
            MirDvClientResidenceNumberGroup.class, MirDvClientResidenceTypeCodeGroup.class,
            MirDvCliUpdByLn1G.class, MirDvCliUpdDtLn1G.class,
            MirDvEmployerAddress1TextGroup.class, MirDvEmployerAddress2TextGroup.class,
            MirDvEmployerAddress3TextGroup.class, MirDvEmployerAddressEffectivityDateGroup.class,
            MirDvEmployerCityCodeGroup.class, MirDvEmployerClientCompanyNameGroup.class,
            MirDvEmployerCountryCodeGroup.class,
            MirDvEmployerPostalCodeGroup.class, MirDvEntrGivNmG.class,
            MirDvEntrSurNmG.class, MirDvPreviousAddressIndGroup.class,
            MirDvPreviousEmployerrNameGroup.class, MirEmployerInformation.class,
            MirEntrGivNmG.class, MirEntrSurNmG.class,
            MirPreviousEmployerYearDurationGroup.class,
            MirUserMessageTextGroup.class };

    public ClientInquiryResponseAdaptor(final String responseString) throws JAXBException {
        super(responseString, CLASSES_TO_BIND);
    }

    @Override
    public Client transmute() {
        Client client = new Client();
        client.setId(response.getTXLifeResponse()
                             .getOLifE()
                             .getClientData()
                             .getMirCliInfo()
                             .getMirCliId());
        client.setGivenName(response.getTXLifeResponse()
                                    .getOLifE()
                                    .getClientData()
                                    .getMirNmInfo()
                                    .getMirCliIndvGivNmG()
                                    .getMirCliIndvGivNmT().get(0));
        client.setMiddlename(response.getTXLifeResponse()
                                     .getOLifE()
                                     .getClientData()
                                     .getMirNmInfo()
                                     .getMirCliIndvMidNmG()
                                     .getMirCliIndvMidNmT().get(0));
        client.setFamilyName(response.getTXLifeResponse()
                                     .getOLifE()
                                     .getClientData()
                                     .getMirNmInfo()
                                     .getMirCliIndvSurNmG()
                                     .getMirCliIndvSurNmT().get(0));
        return client;
    }
}
