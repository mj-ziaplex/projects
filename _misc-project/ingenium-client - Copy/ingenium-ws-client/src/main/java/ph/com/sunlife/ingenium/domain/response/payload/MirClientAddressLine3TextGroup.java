package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrLn3TxtG")
public class MirClientAddressLine3TextGroup {

    @XStreamImplicit(itemFieldName = "MirCliAddrLn3TxtT")
    private List<String> clientAddressLine3Texts;

    public List<String> getClientAddressLine3Texts() {
        return clientAddressLine3Texts;
    }

    public void setClientAddressLine3Texts(final List<String> addressLine3s) {
        clientAddressLine3Texts = addressLine3s;
    }
}
