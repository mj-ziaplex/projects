package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCliInfo")
public class MirClientInformation {

    @XStreamAlias("MirCliId")
    private String id;
    @XStreamAlias("MirDvCliNm")
    private String mirDvCliNm;
    @XStreamAlias("MirCliLangCd")
    private String languageCode;
    @XStreamAlias("MirCliCnfdInd")
    private String mirCliCnfdInd;
    @XStreamAlias("MirCliChrtyInd")
    private String mirCliChrtyInd;
    @XStreamAlias("MirCliBthDt")
    private String birthdate;
    @XStreamAlias("MirCliBthLocCd")
    private String mirCliBthLocCd;
    @XStreamAlias("MirCliAgeProofInd")
    private String mirCliAgeProofInd;
    @XStreamAlias("MirUnmtchMailInd")
    private String mirUnmtchMailInd;
    @XStreamAlias("MirUwUser1Id")
    private String mirUwUser1Id;
    @XStreamAlias("MirUwgWrkshtNum")
    private Byte mirUwgWrkshtNum;
    @XStreamAlias("MirCliTxempCd")
    private String mirCliTxempCd;
    @XStreamAlias("MirCliCmpltCcasInd")
    protected String mirCliCmpltCcasInd;
    @XStreamAlias("MirCliSuspAmt")
    protected Float mirCliSuspAmt;
    @XStreamAlias("MirCliPrevDclnInd")
    protected String mirCliPrevDclnInd;
    @XStreamAlias("MirCliLegitDupInd")
    protected String mirCliLegitDupInd;
    @XStreamAlias("MirCliWrkQty")
    protected Float mirCliWrkQty;
    @XStreamAlias("MirCliMaritStatCd")
    protected String mirCliMaritStatCd;
    @XStreamAlias("MirCliMibIndCd")
    protected String mirCliMibIndCd;
    @XStreamAlias("MirDvPrevUpdtDt")
    protected String  mirDvPrevUpdtDt;
    @XStreamAlias("MirOccpId")
    protected String mirOccpId;
    @XStreamAlias("MirCliOccpClasCd")
    protected String mirCliOccpClasCd;
    @XStreamAlias("MirCliPrstRt")
    protected Byte mirCliPrstRt;
    @XStreamAlias("MirCliSexCd")
    protected String sexCode;
    @XStreamAlias("MirCliTaxId")
    protected Byte mirCliTaxId;
    @XStreamAlias("MirCliSmkrCd")
    protected String mirCliSmkrCd;
    @XStreamAlias("MirCliIndvGrCd")
    protected String mirCliIndvGrCd;
    @XStreamAlias("MirCliNatureOfBus")
    protected String mirCliNatureOfBus;
    @XStreamAlias("MirCliCtznshipCd")
    protected String mirCliCtznshipCd;
    @XStreamAlias("MirGovtPosInd")
    protected String mirGovtPosInd;
    @XStreamAlias("MirUSCtznInd")
    protected String mirUSCtznInd;
    @XStreamAlias("MirUSTaxResPRInd")
    protected String mirUSTaxResPRInd;
    @XStreamAlias("MirUSTaxResSPInd")
    protected String mirUSTaxResSPInd;
    @XStreamAlias("MirCliDutyDetlTxt")
    protected String mirCliDutyDetlTxt;
    @XStreamAlias("MirRcvMktngInfoInd")
    protected String mirRcvMktngInfoInd;

    public MirClientInformation() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMirDvCliNm() {
        return mirDvCliNm;
    }

    public void setMirDvCliNm(String mirDvCliNm) {
        this.mirDvCliNm = mirDvCliNm;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMirCliCnfdInd() {
        return mirCliCnfdInd;
    }

    public void setMirCliCnfdInd(String mirCliCnfdInd) {
        this.mirCliCnfdInd = mirCliCnfdInd;
    }

    public String getMirCliChrtyInd() {
        return mirCliChrtyInd;
    }

    public void setMirCliChrtyInd(String mirCliChrtyInd) {
        this.mirCliChrtyInd = mirCliChrtyInd;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(final String birthdate) {
        this.birthdate = birthdate;
    }

    public String getMirCliBthLocCd() {
        return mirCliBthLocCd;
    }

    public void setMirCliBthLocCd(String mirCliBthLocCd) {
        this.mirCliBthLocCd = mirCliBthLocCd;
    }

    public String getMirCliAgeProofInd() {
        return mirCliAgeProofInd;
    }

    public void setMirCliAgeProofInd(String mirCliAgeProofInd) {
        this.mirCliAgeProofInd = mirCliAgeProofInd;
    }

    public String getMirUnmtchMailInd() {
        return mirUnmtchMailInd;
    }

    public void setMirUnmtchMailInd(String mirUnmtchMailInd) {
        this.mirUnmtchMailInd = mirUnmtchMailInd;
    }

    public String getMirUwUser1Id() {
        return mirUwUser1Id;
    }

    public void setMirUwUser1Id(String mirUwUser1Id) {
        this.mirUwUser1Id = mirUwUser1Id;
    }

    public byte getMirUwgWrkshtNum() {
        return mirUwgWrkshtNum;
    }

    public void setMirUwgWrkshtNum(byte mirUwgWrkshtNum) {
        this.mirUwgWrkshtNum = mirUwgWrkshtNum;
    }

    public String getMirCliTxempCd() {
        return mirCliTxempCd;
    }

    public void setMirCliTxempCd(String mirCliTxempCd) {
        this.mirCliTxempCd = mirCliTxempCd;
    }
}
