package ph.com.sunlife.wms.ingenium.domain;

public class Requirement {

  private String index;
  private String requirementCode;

  public String getIndex() {
    return index;
  }

  public void setIndex(final String index) {
    this.index = index;
  }

  public String getRequirementCode() {
    return requirementCode;
  }

  public void setRequirementCode(final String requirementCode) {
    this.requirementCode = requirementCode;
  }
}
