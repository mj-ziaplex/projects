package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrLn1TxtG")
public class MirDvClientAddressLine1TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrLn1TxtT")
    private List<String> mirDvClientAddressLine1Texts;

    public List<String> getMirDvClientAddressLine1Texts() {
        return mirDvClientAddressLine1Texts;
    }

    public void setMirDvClientAddressLine1Texts(final List<String> line1Texts) {
        mirDvClientAddressLine1Texts = line1Texts;
    }
}
