package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliAddrStatCdG")
public class MirClientAddressStatusCodeCluster {

    @XStreamImplicit(itemFieldName = "MirCliAddrStatCdT")
    private List<String> clientAddressStatusCodes;

    public List<String> getClientAddressStatusCodes() {
        return clientAddressStatusCodes;
    }

    public void setClientAddressStatusCodes(final List<String> statusCodes) {
        clientAddressStatusCodes = statusCodes;
    }
}
