package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliResNumG")
public class MirClientResidenceNumberCluster {

    @XStreamImplicit(itemFieldName = "MirCliResNumT")
    private List<String> clientResidenceNumbers;

    public List<String> getClientResidenceNumbers() {
        return clientResidenceNumbers;
    }

    public void setClientResidenceNumbers(final List<String> residenceNumbers) {
        clientResidenceNumbers = residenceNumbers;
    }
}
