package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirPrevEmplrInfo")
public class MirPreviousEmployerInformation {

    @XStreamAlias("MirDvPrevEmplrNmG")
    private MirDvPreviousEmployerrNameGroup mirDvPreviousEmployerrNameGroup;
    @XStreamAlias("MirPrevEmplYrDurG")
    private MirPreviousEmployerYearDurationGroup mirPreviousEmployerYearDurationGroup;
    @XStreamAlias("MirCliCoGrCd1")
    private String mirClientCompanyGrCode1;

    public MirDvPreviousEmployerrNameGroup getMirDvPreviousEmployerrNameGroup() {
        return mirDvPreviousEmployerrNameGroup;
    }

    public void setMirDvPreviousEmployerrNameGroup(MirDvPreviousEmployerrNameGroup mirDvPreviousEmployerrNameGroup) {
        this.mirDvPreviousEmployerrNameGroup = mirDvPreviousEmployerrNameGroup;
    }

    public MirPreviousEmployerYearDurationGroup getMirPreviousEmployerYearDurationGroup() {
        return mirPreviousEmployerYearDurationGroup;
    }

    public void setMirPreviousEmployerYearDurationGroup(MirPreviousEmployerYearDurationGroup mirPreviousEmployerYearDurationGroup) {
        this.mirPreviousEmployerYearDurationGroup = mirPreviousEmployerYearDurationGroup;
    }

    public String getMirClientCompanyGrCode1() {
        return mirClientCompanyGrCode1;
    }

    public void setMirClientCompanyGrCode1(String mirClientCompanyGrCode1) {
        this.mirClientCompanyGrCode1 = mirClientCompanyGrCode1;
    }
}
