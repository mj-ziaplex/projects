package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirNmInfo")
public class MirNameInformation {

    @XStreamAlias("MirCliNmChngDt")
    private String clientNameChangeDate;
    @XStreamAlias("MirCliIndvEffDtG")
    private MirClientIndvEffectivityDateGroup effectivityDates;
    @XStreamAlias("MirCliCoNmG")
    private MirClientCompanyNameGroup companyNames;
    @XStreamAlias("MirEntrGivNmG")
    private MirEntryGivenNameGroup entryGivenNames;
    @XStreamAlias("MirCliIndvSurNmG")
    private MirClientIndividualSurnameGroup individualSurnames;
    @XStreamAlias("MirEntrSurNmG")
    private MirEntrySurnameGroup entrySurnames;
    @XStreamAlias("MirCliIndvMidNmG")
    private MirClientIndividualMiddlenameGroup individualMiddlenames;
    @XStreamAlias("MirCliOtherFirstNmG")
    private MirClientOtherFirstNameGroup otherFirstNames;
    @XStreamAlias("MirCliOtherMidNmG")
    private MirClientOtherMiddlenameGroup otherMiddlenames;
    @XStreamAlias("MirCliOtherLastNmG")
    private MirClientOtherLastNameGroup otherLastNames;
    @XStreamAlias("MirCliIndvTitlTxtG")
    private MirClientIndividualTitleTextGroup titles;
    @XStreamAlias("MirCliIndvSfxNmG")
    private MirClientIndividualSuffixNameGroup suffixes;

    public String getClientNameChangeDate() {
        return clientNameChangeDate;
    }

    public void setClientNameChangeDate(String clientNameChangeDate) {
        this.clientNameChangeDate = clientNameChangeDate;
    }

    public MirClientIndvEffectivityDateGroup getEffectivityDates() {
        return effectivityDates;
    }

    public void setEffectivityDates(MirClientIndvEffectivityDateGroup effectivityDates) {
        this.effectivityDates = effectivityDates;
    }

    public MirClientCompanyNameGroup getCompanyNames() {
        return companyNames;
    }

    public void setCompanyNames(MirClientCompanyNameGroup companyNames) {
        this.companyNames = companyNames;
    }

    public MirEntryGivenNameGroup getEntryGivenNames() {
        return entryGivenNames;
    }

    public void setEntryGivenNames(MirEntryGivenNameGroup entryGivenNames) {
        this.entryGivenNames = entryGivenNames;
    }

    public MirClientIndividualSurnameGroup getIndividualSurnames() {
        return individualSurnames;
    }

    public void setIndividualSurnames(MirClientIndividualSurnameGroup individualSurnames) {
        this.individualSurnames = individualSurnames;
    }

    public MirEntrySurnameGroup getEntrySurnames() {
        return entrySurnames;
    }

    public void setEntrySurnames(MirEntrySurnameGroup entrySurnames) {
        this.entrySurnames = entrySurnames;
    }

    public MirClientIndividualMiddlenameGroup getIndividualMiddlenames() {
        return individualMiddlenames;
    }

    public void setIndividualMiddlenames(MirClientIndividualMiddlenameGroup individualMiddlenames) {
        this.individualMiddlenames = individualMiddlenames;
    }

    public MirClientOtherFirstNameGroup getOtherFirstNames() {
        return otherFirstNames;
    }

    public void setOtherFirstNames(MirClientOtherFirstNameGroup otherFirstNames) {
        this.otherFirstNames = otherFirstNames;
    }

    public MirClientOtherMiddlenameGroup getOtherMiddlenames() {
        return otherMiddlenames;
    }

    public void setOtherMiddlenames(MirClientOtherMiddlenameGroup otherMiddlenames) {
        this.otherMiddlenames = otherMiddlenames;
    }

    public MirClientOtherLastNameGroup getOtherLastNames() {
        return otherLastNames;
    }

    public void setOtherLastNames(MirClientOtherLastNameGroup otherLastNames) {
        this.otherLastNames = otherLastNames;
    }

    public MirClientIndividualTitleTextGroup getTitles() {
        return titles;
    }

    public void setTitles(MirClientIndividualTitleTextGroup titles) {
        this.titles = titles;
    }

    public MirClientIndividualSuffixNameGroup getSuffixes() {
        return suffixes;
    }

    public void setSuffixes(MirClientIndividualSuffixNameGroup suffixes) {
        this.suffixes = suffixes;
    }
}
