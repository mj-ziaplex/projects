package ph.com.sunlife.wms.ingenium.archive;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public abstract class XMLArchiver {

    protected static final Logger LOGGER = LoggerFactory.getLogger(XMLArchiver.class);

    private static final String DEFAULT_FILE_EXTENSION = ".xml";

    private long creationDateInstance;
    private File filename;

    public XMLArchiver(final String prefix) {
        creationDateInstance = (new Date()).getTime();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("test");
        }

        ApplicationContext context =
                new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);
        File location = ((File) context.getBean("xmlLogLocation")).getAbsoluteFile();
        filename =
                new File(location.getAbsolutePath() + File.separator + prefix +
                         String.valueOf(creationDateInstance) + DEFAULT_FILE_EXTENSION);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("filename: " + filename.getAbsolutePath());
        }
    }

    public File register(final String xmlData) throws IOException {
        try {
            if (StringUtils.isNotBlank(xmlData)) {
                FileUtils.write(filename, xmlData, "UTF8");
                return filename;
            }
        } catch (IOException ioe) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(ioe.getMessage(), ioe);
            }
            throw ioe;
        }
        return null;
    }
}
