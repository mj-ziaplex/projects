package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvSurNmG")
public class MirClientIndividualSurnameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvSurNmT")
    private List<String> mirClientIndividualSurnameTypes;

    public List<String> getMirClientIndividualSurnameTypes() {
        return mirClientIndividualSurnameTypes;
    }

    public void setMirClientIndividualSurnameTypes(final List<String> types) {
        this.mirClientIndividualSurnameTypes = types;
    }
}
