package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliCityNmTxtG")
public class MirDvClientCityNameTextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliCityNmTxtT")
    private List<String> mirDvClientCityNameTexts;

    public List<String> getMirDvClientCityNameTexts() {
        return mirDvClientCityNameTexts;
    }

    public void setMirDvClientCityNameTexts(List<String> cityNames) {
        mirDvClientCityNameTexts = cityNames;
    }
}
