package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCrcTypCdG")
public class MirCrcTypeCodeGroup {

    @XStreamImplicit(itemFieldName = "MirCrcTypCdT")
    private List<String> mirCrcTypeCodeTexts;

    public List<String> getMirCrcTypeCodeTexts() {
        return mirCrcTypeCodeTexts;
    }

    public void setMirCrcTypeCodeTexts(List<String> mirCrcTypeCodeTexts) {
        this.mirCrcTypeCodeTexts = mirCrcTypeCodeTexts;
    }
}
