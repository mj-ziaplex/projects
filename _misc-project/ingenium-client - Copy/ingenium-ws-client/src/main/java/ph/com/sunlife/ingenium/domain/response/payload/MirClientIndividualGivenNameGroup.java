package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliIndvGivNmG")
public class MirClientIndividualGivenNameGroup {

    @XStreamImplicit(itemFieldName = "MirCliIndvGivNmT")
    private List<String> mirClientIndividualGivenNAmeTypes;

    public List<String> getMirClientIndividualGivenNAmeTypes() {
        return mirClientIndividualGivenNAmeTypes;
    }

    public MirClientIndividualGivenNameGroup setMirClientIndividualGivenNAmeTypes(List<String> types) {
        mirClientIndividualGivenNAmeTypes = types;
        return this;
    }
}
