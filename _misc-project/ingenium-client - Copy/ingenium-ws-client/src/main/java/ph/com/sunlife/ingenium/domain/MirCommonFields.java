package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCommonFields")
public class MirCommonFields {

    @XStreamAlias("MirDvInsrdCliNm")
    private String insuredClientNumber;
    @XStreamAlias("MirPolId")
    private PolicyID policyId;
    @XStreamAlias("MirCpreqCreatDt")
    private String mirCpreqCreatDt;

    public MirCommonFields() { }

    public String getInsuredClientNumber() {
        return insuredClientNumber;
    }

    public void setInsuredClientNumber(final String insuredClientNumber) {
        this.insuredClientNumber = insuredClientNumber;
    }

    public PolicyID getPolicyId() {
        return policyId;
    }

    public void setPolicyId(final PolicyID policyId) {
        this.policyId = policyId;
    }

    public String getMirCpreqCreatDt() {
        return mirCpreqCreatDt;
    }

    public void setMirCpreqCreatDt(final String mirCpreqCreatDt) {
        this.mirCpreqCreatDt = mirCpreqCreatDt;
    }
}
