package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.PolicyID;

@XStreamAlias("InquiryConsolidatedInformationData")
public class ConsolidatedInformationData {

    @XStreamAlias("SrcSystem")
    private String sourceSystem;
    @XStreamAlias("MirPolId")
    private PolicyID policyId;

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public PolicyID getPolicyId() {
        return policyId;
    }

    public void setPolicyId(PolicyID policyId) {
        this.policyId = policyId;
    }
}
