package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirCliBnkAcctDtG")
public class MirBankAccountDateGroup {

    @XStreamImplicit(itemFieldName = "MirCliBnkAcctDtT")
    private List<String> mirClientBankAccountDateTypes;

    public List<String> getMirClientBankAccountDateTypes() {
        return mirClientBankAccountDateTypes;
    }

    public MirBankAccountDateGroup setMirClientBankAccountDateTypes(List<String> mirClientBankAccountDateTypes) {
        this.mirClientBankAccountDateTypes = mirClientBankAccountDateTypes;
        return this;
    }
}
