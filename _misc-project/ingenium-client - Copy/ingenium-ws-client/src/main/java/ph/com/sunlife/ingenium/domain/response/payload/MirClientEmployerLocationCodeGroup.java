package ph.com.sunlife.ingenium.domain.response.payload;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "MirCliEmplrLocCdG")
public class MirClientEmployerLocationCodeGroup {

    @XmlElement(name = "MirCliEmplrLocCdT")
    private List<String> mirClientEmployerLocationCodeTexts;

    public List<String> getMirClientEmployerLocationCodeTexts() {
        return mirClientEmployerLocationCodeTexts;
    }

    public void setMirClientEmployerLocationCodeTexts(List<String> locationCodes) {
        mirClientEmployerLocationCodeTexts = locationCodes;
    }
}
