package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrEffDtG")
public class MirDvClientAddressEffectivityDateGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrEffDtT")
    private List<String> mirDvClientAddressEffectivityDateTexts;

    public List<String> getMirDvClientAddressEffectivityDateTexts() {
        return mirDvClientAddressEffectivityDateTexts;
    }

    public void setMirDvClientAddressEffectivityDateTexts(List<String> effectivityDates) {
        mirDvClientAddressEffectivityDateTexts = effectivityDates;
    }
}
