package ph.com.sunlife.ingenium.domain.reqst.xml;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userAuthRequest", "txLifeRequest" })
@XmlRootElement(name = "TXLife")
public class TXLife {

    @XmlElement(name = "UserAuthRequest", required = true)
    protected UserAuthRequest userAuthRequest;
    @XmlElement(name = "TXLifeRequest", required = true)
    protected TXLifeRequest txLifeRequest;

    public TXLife() { }

    public UserAuthRequest getUserAuthRequest() {
        return userAuthRequest;
    }

    public void setUserAuthRequest(UserAuthRequest value) {
        this.userAuthRequest = value;
    }

    public TXLifeRequest getTXLifeRequest() {
        return txLifeRequest;
    }

    public void setTXLifeRequest(TXLifeRequest value) {
        this.txLifeRequest = value;
    }

}
