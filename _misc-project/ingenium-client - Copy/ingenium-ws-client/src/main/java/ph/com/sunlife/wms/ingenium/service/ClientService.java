package ph.com.sunlife.wms.ingenium.service;

import ph.com.sunlife.wms.ingenium.domain.Client;

import javax.xml.bind.JAXBException;

public interface ClientService {

    Client getClientDetails(String clientId) throws JAXBException;

    Client getClientDetails(final String clientId, final String guid) throws JAXBException;
}
