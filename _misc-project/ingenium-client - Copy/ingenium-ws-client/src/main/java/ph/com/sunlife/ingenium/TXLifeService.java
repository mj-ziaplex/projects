package ph.com.sunlife.ingenium;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.MalformedURLException;
import java.net.URL;

@WebServiceClient(name = "TXLifeService",
                  targetNamespace = "http://webservices.elink.solcorp.com")
class TXLifeService extends Service {

  private static final QName SERVICE_QNAME = new QName("http://webservices.elink.solcorp.com", "TXLifeService");

  public TXLifeService(final URL wsdlDocumentLocation) {
    super(wsdlDocumentLocation, SERVICE_QNAME);
  }

  public TXLifeService(final String wsdlDocumentUrl)
      throws MalformedURLException {
    this(new URL(wsdlDocumentUrl));
  }

  @WebEndpoint(name = "TXLifeService")
  public TXLifeServicePort getTXLifeService() {
    return super.getPort(SERVICE_QNAME, TXLifeServicePort.class);
  }

  @WebEndpoint(name = "TXLifeService")
  public TXLifeServicePort getTXLifeService(WebServiceFeature... features) {
    return super.getPort(SERVICE_QNAME, TXLifeServicePort.class, features);
  }
}
