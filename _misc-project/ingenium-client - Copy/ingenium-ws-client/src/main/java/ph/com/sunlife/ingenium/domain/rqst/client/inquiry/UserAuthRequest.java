package ph.com.sunlife.ingenium.domain.reqst.client.inquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userLoginName",
    "userPswd"
})
@XmlRootElement(name = "UserAuthRequest")
public class UserAuthRequest {

    @XmlElement(name = "UserLoginName", required = true)
    protected String userLoginName;
    @XmlElement(name = "UserPswd", required = true)
    protected UserPswd userPswd;

    /**
     * Gets the value of the userLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserLoginName() {
        return userLoginName;
    }

    /**
     * Sets the value of the userLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }

    /**
     * Gets the value of the userPswd property.
     * 
     * @return
     *     possible object is
     *     {@link UserPswd }
     *     
     */
    public UserPswd getUserPswd() {
        return userPswd;
    }

    /**
     * Sets the value of the userPswd property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPswd }
     *     
     */
    public void setUserPswd(UserPswd value) {
        this.userPswd = value;
    }

}
