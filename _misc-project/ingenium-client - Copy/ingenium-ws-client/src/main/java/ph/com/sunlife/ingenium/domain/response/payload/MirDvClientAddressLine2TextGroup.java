package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("MirDvCliAddrLn2TxtG")
public class MirDvClientAddressLine2TextGroup {

    @XStreamImplicit(itemFieldName = "MirDvCliAddrLn2TxtT")
    private List<String> mirDvClientAddressLine2Texts;

    public List<String> getMirDvClientAddressLine2Texts() {
        return mirDvClientAddressLine2Texts;
    }

    public void setMirDvClientAddressLine2Texts(final List<String> line2Texts) {
        mirDvClientAddressLine2Texts = line2Texts;
    }
}
