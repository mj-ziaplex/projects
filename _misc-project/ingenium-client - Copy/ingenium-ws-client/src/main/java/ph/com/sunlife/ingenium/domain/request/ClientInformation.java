package ph.com.sunlife.ingenium.domain.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirCliInfo")
public class ClientInformation {

  @XStreamAlias("MirCliId")
  private String clientId;

  public ClientInformation() { }

  public String getClientID() {
    return clientId;
  }

  public void setClientID(String clientId) {
    this.clientId = clientId;
  }
}
