package ph.com.sunlife.ingenium.domain.response.payload;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MirPrcesInfo")
public class MirProcessInformation {

    @XStreamAlias("MirDvLangGrDisplayCd1")
    private String mirDvLangGrDisplayCd1;
    @XStreamAlias("MirDvLangGrDisplayCd2")
    private String mirDvLangGrDisplayCd2;
    @XStreamAlias("MirDvLangGrDisplayCd3")
    private String mirDvLangGrDisplayCd3;
    @XStreamAlias("MirDvLangGrDisplayCd4")
    private String mirDvLangGrDisplayCd4;
    @XStreamAlias("MirDvLangGrDisplayCd5")
    private String mirDvLangGrDisplayCd5;

    public MirProcessInformation() { }

    public String getMirDvLangGrDisplayCd1() {
        return mirDvLangGrDisplayCd1;
    }

    public void setMirDvLangGrDisplayCd1(String mirDvLangGrDisplayCd1) {
        this.mirDvLangGrDisplayCd1 = mirDvLangGrDisplayCd1;
    }

    public String getMirDvLangGrDisplayCd2() {
        return mirDvLangGrDisplayCd2;
    }

    public void setMirDvLangGrDisplayCd2(String mirDvLangGrDisplayCd2) {
        this.mirDvLangGrDisplayCd2 = mirDvLangGrDisplayCd2;
    }

    public String getMirDvLangGrDisplayCd3() {
        return mirDvLangGrDisplayCd3;
    }

    public void setMirDvLangGrDisplayCd3(String mirDvLangGrDisplayCd3) {
        this.mirDvLangGrDisplayCd3 = mirDvLangGrDisplayCd3;
    }

    public String getMirDvLangGrDisplayCd4() {
        return mirDvLangGrDisplayCd4;
    }

    public void setMirDvLangGrDisplayCd4(String mirDvLangGrDisplayCd4) {
        this.mirDvLangGrDisplayCd4 = mirDvLangGrDisplayCd4;
    }

    public String getMirDvLangGrDisplayCd5() {
        return mirDvLangGrDisplayCd5;
    }

    public void setMirDvLangGrDisplayCd5(String mirDvLangGrDisplayCd5) {
        this.mirDvLangGrDisplayCd5 = mirDvLangGrDisplayCd5;
    }
}
