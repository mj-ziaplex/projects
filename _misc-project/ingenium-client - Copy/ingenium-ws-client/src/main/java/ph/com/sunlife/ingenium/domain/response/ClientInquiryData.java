package ph.com.sunlife.ingenium.domain.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ph.com.sunlife.ingenium.domain.response.payload.MirBusAppInfo;
import ph.com.sunlife.ingenium.domain.response.payload.MirClftInfo;

@XStreamAlias("ClientInquiryData")
public class ClientInquiryData {

    @XStreamAlias("MirClftInfo")
    private MirClftInfo mirClftInfo;
    @XStreamAlias("MirBusAppInfo")
    private MirBusAppInfo mirBusAppInfo;

    public MirClftInfo getMirClftInfo() {
        return mirClftInfo;
    }

    public void setMirClftInfo(final MirClftInfo mirClftInfo) {
        this.mirClftInfo = mirClftInfo;
    }

    public MirBusAppInfo getMirBusAppInfo() {
        return mirBusAppInfo;
    }

    public void setMirBusAppInfo(MirBusAppInfo mirBusAppInfo) {
        this.mirBusAppInfo = mirBusAppInfo;
    }
}
