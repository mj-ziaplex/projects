package ph.com.sunlife.ingenium.domain.reqst.policy.inquiry;


import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;

public class PolicyInquiryRequestsTests {

    @Test
    public void testClientInquiryRequests() throws IOException, JAXBException {
        UserPswd password = new UserPswd();
        password.setCryptType("NONE");
        password.setPswd("a0bf6b61");

        UserAuthRequest userAuth =
            new ph.com.sunlife.ingenium.domain.reqst.policy.inquiry.UserAuthRequest();
        userAuth.setUserLoginName("WC01W");
        userAuth.setUserPswd(password);

        TransType transactionType = new TransType();
        transactionType.setTc("PolicyInquiry");
        transactionType.setValue("PolicyInquiry");

        MirPolId policyId = new MirPolId();
        policyId.setMirPolIdBase("088005977");
        policyId.setMirPolIdSfx("0");

        PolicyData policyData = new PolicyData();
        policyData.setMirPolId(policyId);
        policyData.setSrcSystem("WMS");

        OLifE oLifE = new OLifE();
        oLifE.setPolicyData(policyData);

        TXLifeRequest txLifeRequest = new TXLifeRequest();
        txLifeRequest.setTransRefGUID("2996b11f-e5b2-d00f-281b-03582683cd49");
        txLifeRequest.setTransType(transactionType);
        txLifeRequest.setTransExeDate("2020-03-10");
        txLifeRequest.setTransExeTime("14:36:08+8000");
        txLifeRequest.setOLifE(oLifE);

        TXLife txLife = new TXLife();
        txLife.setUserAuthRequest(userAuth);
        txLife.setTXLifeRequest(txLifeRequest);

        StringWriter output = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(UserPswd.class,
                                                          UserAuthRequest.class,
                                                          TransType.class, MirPolId.class, PolicyData.class, OLifE.class,
            TXLifeRequest.class, TXLife.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(txLife, output);
        System.out.println(output.toString());
    }
}
