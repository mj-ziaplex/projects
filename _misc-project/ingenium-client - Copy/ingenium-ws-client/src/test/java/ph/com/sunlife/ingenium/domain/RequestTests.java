package ph.com.sunlife.ingenium.domain;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import ph.com.sunlife.ingenium.domain.request.*;
import ph.com.sunlife.ingenium.domain.xml.converter.TransactionTypeAttribute;

import java.io.*;

public class RequestTests {

  private Credentials credentials;
  private TransactionLifeRequest transactionLifeRequest;

  @Before
  public void setUp() {
    credentials = new BasicCredentials();
    credentials.setUsername("WC01W");
    credentials.setPassword("af59fef8");

    transactionLifeRequest = new TransactionLifeRequest();
    transactionLifeRequest.setReferenceGUID("2996b11f-e5b2-d00f-281b-03582683cd49");
    transactionLifeRequest.setExecutionDate("2020-03-10");
  }

  @Test
  public void testClientInquiryRequest() throws IOException {
    ClientInformation clientInformation = new ClientInformation();
    clientInformation.setClientID("3080008662");

    ClientData clientData = new ClientData();
    clientData.setClientInformation(clientInformation);

    ClientDataContainer box = new ClientDataContainer();
    box.setPayload(clientData);

    transactionLifeRequest.setType("ClientInquiry");
    transactionLifeRequest.setExecutionTime("14:36:08+8000");
    transactionLifeRequest.setContainer(box);

    Request request = new Request();
    request.setCredentials(credentials);
    request.setTransaction(transactionLifeRequest);

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
    writer.write(StringUtils.LF);

    XStream xstream = new XStream();
    xstream.autodetectAnnotations(true);
    xstream.registerConverter(new TransactionTypeAttribute());

    xstream.toXML(request, writer);
    System.out.println(outputStream.toString("UTF-8"));
  }

  @Test
  public void testPolicyInquiryRequest() throws IOException {
    PolicyID policyId = new PolicyID("0880059770");

    PolicyData policyData = new PolicyData();
    policyData.setPolicyID(policyId);
    policyData.setSourceSystem("WMS");

    PolicyDataContainer box = new PolicyDataContainer(policyData);

    transactionLifeRequest.setType("PolicyInquiry");
    transactionLifeRequest.setExecutionTime("09:00:00-05:00");
    transactionLifeRequest.setContainer(box);

    Request request = new Request();
    request.setCredentials(credentials);
    request.setTransaction(transactionLifeRequest);

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
    writer.write(StringUtils.LF);

    XStream xstream = new XStream();
    xstream.autodetectAnnotations(true);
    xstream.registerConverter(new TransactionTypeAttribute());

    xstream.toXML(request, writer);
    System.out.println(outputStream.toString("UTF-8"));
  }

  @Test
  public void testListRequirementRequest() throws IOException {
    PolicyID policyId = new PolicyID("0880059770");

    MirCommonFields commonFields = new MirCommonFields();
    commonFields.setPolicyId(policyId);

    RequirementData clientData = new RequirementData(commonFields);

    RequirementDataContainer box = new RequirementDataContainer(clientData);

    transactionLifeRequest.setType("RequirementsList");
    transactionLifeRequest.setExecutionTime("09:00:00-05:00");
    transactionLifeRequest.setContainer(box);

    Request request = new Request();
    request.setCredentials(credentials);
    request.setTransaction(transactionLifeRequest);

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
    writer.write(StringUtils.LF);

    XStream xstream = new XStream();
    xstream.autodetectAnnotations(true);
    xstream.registerConverter(new TransactionTypeAttribute());

    xstream.toXML(request, writer);
    System.out.println(outputStream.toString("UTF-8"));
  }

  @Test
  public void testInquiryConsolidatedInformationRequest() throws IOException {
    PolicyID policyId = new PolicyID("0880059770");
    policyId.setEffectivityDate("2019-01-15");

    ConsolidatedInformationData consolidatedInformationData =
            new ConsolidatedInformationData();
    consolidatedInformationData.setSourceSystem("WMS");
    consolidatedInformationData.setPolicyId(policyId);

    ConsolidatedInformationDataContainer box =
            new ConsolidatedInformationDataContainer(consolidatedInformationData);

    transactionLifeRequest.setType("InquiryConsolidatedInformation");
    transactionLifeRequest.setExecutionTime("18:32:55");
    transactionLifeRequest.setContainer(box);

    Request request = new Request();
    request.setCredentials(credentials);
    request.setTransaction(transactionLifeRequest);

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
    writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
    writer.write(StringUtils.LF);

    XStream xstream = new XStream();
    xstream.autodetectAnnotations(true);
    xstream.registerConverter(new TransactionTypeAttribute());

    xstream.toXML(request, writer);
    System.out.println(outputStream.toString("UTF-8"));
  }
}
