package ph.com.sunlife.wms.ingenium;

import org.junit.Test;
import ph.com.sunlife.ingenium.domain.reqst.xml.*;
import ph.com.sunlife.wms.ingenium.archive.RequestXMLArchiver;
import ph.com.sunlife.wms.ingenium.archive.XMLArchiver;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryRequestBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Tester {

    @Test
    public void test() throws IOException, JAXBException {
        XMLArchiver archiver = new RequestXMLArchiver("test");
        archiver.register("this is a string");

        UserAuthRequest auth = new UserAuthRequest();
        UserPassword password = new UserPassword();
        auth.setUserLoginName("test");
        password.setCryptographicType("NONE");
        password.setPassword("P@$$w0rd");
        auth.setUserPswd(password);

        TXLifeRequest request = new TXLifeRequest();
        OLifE olife = new OLifE();
        TransactionType transactionType = new TransactionType();
        ClientData clientData = new ClientData();
        MirClientInformation cliInfo = new MirClientInformation();

        cliInfo.setClientId(826457913);
        clientData.setSourceSystem("WMS");
        clientData.setMirClientInformation(cliInfo);
        olife.setClientData(clientData);
        transactionType.setTransactionType("ClientInquiry");
        request.setTransactionReferenceGUID("test123456789");
        request.setTransactionType(transactionType);
        request.setTransactionExecutionDate("2020-02-14");
        request.setTransactionExecutionTime("15:58:50+8000");
        request.setOLifE(olife);

        TXLife life = new TXLife();
        life.setUserAuthRequest(auth);
        life.setTXLifeRequest(request);

        JAXBContext jaxbContext =
                JAXBContext.newInstance(TXLife.class, UserAuthRequest.class,
                                        TXLifeRequest.class, UserPassword.class,
                                        TransactionType.class, OLifE.class,
                                        ClientData.class, MirClientInformation.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(life, new PrintWriter( System.out ) );

        String responseString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><TXLife><UserAuthRequest><UserLoginName>test</UserLoginName><UserPswd><CryptType>NONE</CryptType><Pswd>P@$$w0rd</Pswd></UserPswd></UserAuthRequest><TXLifeRequest><TransRefGUID>test123456789</TransRefGUID><TransType tc=\"ClientInquiry\">ClientInquiry</TransType><TransExeDate>2020-02-14</TransExeDate><TransExeTime>15:58:50+8000</TransExeTime><OLife><ClientData><SrcSystem>WMS</SrcSystem><MirCliInfo><MirCliId>826457913</MirCliId></MirCliInfo></ClientData></OLife></TXLifeRequest></TXLife>";

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        TXLife response = (TXLife) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(responseString.getBytes()));

        System.out.println("");
        System.out.println(response.getUserAuthRequest().getUserPswd().getPassword());

        ClientInquiryRequestBuilder builder = new ClientInquiryRequestBuilder("826457913");
        builder.setTransactionReferenceGUID("blank space");
        String requestString = builder.createRequest();

        System.out.println("");
        System.out.println("REQUEST STRING :: " + requestString);


        System.out.println("");
        System.out.println("REQUEST STRING :: " + requestString);
    }
}
