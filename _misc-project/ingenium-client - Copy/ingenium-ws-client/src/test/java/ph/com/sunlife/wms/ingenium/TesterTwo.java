package ph.com.sunlife.wms.ingenium;


import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ph.com.sunlife.ingenium.IngeniumWebServiceClient;
import ph.com.sunlife.ingenium.domain.response.xml.*;
import ph.com.sunlife.wms.ingenium.configuration.IngeniumClientConfigurator;
import ph.com.sunlife.wms.ingenium.domain.Client;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryRequestBuilder;
import ph.com.sunlife.wms.ingenium.ws.client.ClientInquiryResponseAdaptor;
import ph.com.sunlife.wms.ingenium.ws.client.IngeniumResponseAdaptor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public class TesterTwo {

    @Test
    public void one() throws IOException, JAXBException {
        String request = new ClientInquiryRequestBuilder("3080008502").createRequest();

        System.out.println("REQUEST >>> " + request);

        String response = new IngeniumWebServiceClient().getResponse(request);

        System.out.println("RESPONSE >>> " + response);

        IngeniumResponseAdaptor adaptor = new ClientInquiryResponseAdaptor(response);

        Client client = adaptor.transmute();

        System.out.println("Client ID :: " + client.getId());
    }

    @Test
    public void two() throws IOException, JAXBException {
        ApplicationContext context =
            new AnnotationConfigApplicationContext(IngeniumClientConfigurator.class);

        System.out.println((String) context.getBean("password"));
    }
}
