package ph.com.sunlife.wms.ingenium;

import org.junit.Test;
import ph.com.sunlife.wms.ingenium.domain.Client;
import ph.com.sunlife.wms.ingenium.service.ClientService;
import ph.com.sunlife.wms.ingenium.service.ClientServiceImpl;

import javax.xml.bind.JAXBException;

public class ClientInquiryTest {

  @Test
  public void test() throws JAXBException {
    System.out.println("<<< Test Client Inquiry >>>");
    ClientService service = new ClientServiceImpl();
    Client client = service.getClientDetails("3080008502");
    System.out.println("Client ID :: " + client.getId());
    System.out.println("Given Name :: " + client.getGivenName());
    System.out.println("Middlename :: " + client.getMiddlename());
    System.out.println("Family Name :: " + client.getFamilyName());
  }


}
