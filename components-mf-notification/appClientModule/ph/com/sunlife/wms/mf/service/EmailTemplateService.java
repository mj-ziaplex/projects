package ph.com.sunlife.wms.mf.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import ph.com.sunlife.wms.mf.constants.MFNotificationConstants;
import ph.com.sunlife.wms.mf.dao.WMSEmailTemplateDAO;
import ph.com.sunlife.wms.mf.dao.impl.WMSEmailTemplateDAOImpl;
import ph.com.sunlife.wms.mf.dto.WMSEmailTemplateDTO;

public class EmailTemplateService {

	private static final LoadingCache<String, List<WMSEmailTemplateDTO>> PARAM_CACHE;
	
	private static final WMSEmailTemplateDAO EMAIL_TEMPLATE_DAO = new WMSEmailTemplateDAOImpl();
	
	static {
		PARAM_CACHE = CacheBuilder.newBuilder().maximumSize(100).expireAfterWrite(30L, TimeUnit.MINUTES).build(
				new CacheLoader<String, List<WMSEmailTemplateDTO>>() {

					@Override
					public List<WMSEmailTemplateDTO> load(String type) throws Exception {
						return EMAIL_TEMPLATE_DAO.getAllEmailTemplates();
					}
				}
			);
	}
	
	public static List<WMSEmailTemplateDTO> getAllEmailTemplates() {
		return PARAM_CACHE.getUnchecked(MFNotificationConstants.WMS_EMAIL_TEMPLATE_TABLE);
	}
	
	@SuppressWarnings("unchecked")
	public static List<WMSEmailTemplateDTO> getEmailTemplatesByType(final String type) {
		return (List<WMSEmailTemplateDTO>) CollectionUtils.select(getAllEmailTemplates(), new Predicate() {
			
			@Override
			public boolean evaluate(Object obj) {
				WMSEmailTemplateDTO dto = (WMSEmailTemplateDTO) obj;
				return dto.getCodeType().equalsIgnoreCase(type);
			}
		});
	}
}
