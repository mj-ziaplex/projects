package ph.com.sunlife.wms.mf.helper;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class PrintExceptionHelper {

	public static String exceptionStacktraceToString(Exception e)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		e.printStackTrace(ps);
		ps.close();
		return baos.toString();
	}
}
