package ph.com.sunlife.wms.mf.utils;

public class StringUtils {

	public static boolean isNotEmpty(String param){
		return (param != null && !"".equalsIgnoreCase(param));
	}
	
	public static boolean isEmpty(String param){
		return (param == null || "".equalsIgnoreCase(param));
	}
}
