package ph.com.sunlife.wms.mf.utils;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

public final class ResourceBundleFactory {

	public static final String PROP_JAR_COMPONENT_PROPERTIES = "com.ph.sunlife.component.properties.ComponentProperties";
	public static final String PROP_JAR_COMPONENT_PROPERTIES_OUTSIDE = "ComponentProperties";

	private static ResourceBundleFactory resourceBundleFactory;
	private Map<String, ResourceBundle> resourceBundleMap;

	private ResourceBundleFactory() {
		this.resourceBundleMap = new ConcurrentHashMap<String, ResourceBundle>();
	}

	public static ResourceBundleFactory getInstance() {
		if (null == resourceBundleFactory) {
			resourceBundleFactory = new ResourceBundleFactory();
		}
		return resourceBundleFactory;
	}

	public ResourceBundle getBundle(String resourceBundleName) {

		ResourceBundle resourceBundle = null;
		try {

			if (StringUtils.isBlank(resourceBundleName)) {
				return null;
			}

			resourceBundle = resourceBundleMap.get(resourceBundleName);

			if (null == resourceBundle) {
				resourceBundle = ResourceBundle.getBundle(resourceBundleName);
				resourceBundleMap.put(resourceBundleName, resourceBundle);
			}

		} catch (MissingResourceException mre) {

		}
		return resourceBundle;
	}

}
