package ph.com.sunlife.wms.mf.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class DBConnect {
	PrintStream printStream;
	PreparedStatement ps;	
	Connection con = this.connect();
	ResultSet rs ;
	Statement stmt;

	public Connection connect() {
			
		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
		Connection conn = null;
		String url = "";
		
		try {
		String DatabaseServer = rb.getString("DatabaseServer");
		String port = rb.getString("DatabasePort");
		String databaseName = rb.getString("DatabaseName");
		String userName = rb.getString("DatabaseUserName");
		String password = rb.getString("DatabasePassword");
		String urlStart = rb.getString("DatabaseUrlStart");
		
		Class.forName(rb.getString("DatabaseDriverClass"));
		url = (urlStart + DatabaseServer + ":"	+ port + ";User=" + userName + ";Password=" + password + ";DatabaseName=" + databaseName);
		} catch (ClassNotFoundException e) {
			
		} catch(Exception e) {
			
		}
		
		
		try {
			conn = DriverManager.getConnection(url);
			System.out.println("DBQuery : connected ");
		} catch (SQLException e) {
			
		}
		return conn;
	}
	
	private void initializeLog(){
		
		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
		String fileName = (rb.getString("WMS_LOG_PATH")+ "\\" + "MFEmailModule.log");
		File file = new File(fileName);
		
		if(file.length() == 0L){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			printStream = new PrintStream(new FileOutputStream(file, true));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
