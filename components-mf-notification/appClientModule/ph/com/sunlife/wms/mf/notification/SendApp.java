package ph.com.sunlife.wms.mf.notification;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import ph.com.sunlife.wms.mf.constants.MFNotificationConstants;
import ph.com.sunlife.wms.mf.dao.MFApplicationsDAO;
import ph.com.sunlife.wms.mf.dao.MFLookupAgentDAO;
import ph.com.sunlife.wms.mf.dao.MFLookupCodeDAO;
import ph.com.sunlife.wms.mf.dao.WmsParamDAO;
import ph.com.sunlife.wms.mf.dao.impl.WmsParamDAOImpl;
import ph.com.sunlife.wms.mf.dto.MFApplicationsDTO;
import ph.com.sunlife.wms.mf.dto.MFLookupAgentDTO;
import ph.com.sunlife.wms.mf.dto.MFLookupCodeDTO;
import ph.com.sunlife.wms.mf.enums.DisclaimerEnum;
import ph.com.sunlife.wms.mf.enums.DisclaimerEnum.Company;
import ph.com.sunlife.wms.mf.enums.DisclaimerEnum.DisclaimerType;
import ph.com.sunlife.wms.mf.helper.MFNotifcationDecisionHelper;
import ph.com.sunlife.wms.mf.helper.PrintExceptionHelper;
import ph.com.sunlife.wms.mf.utils.DBConnect;
import ph.com.sunlife.wms.mf.utils.DateUtils;
import ph.com.sunlife.wms.mf.utils.ResourceBundleFactory;
import sunlife.wms.nb.bl.NotificationHandler;

public class SendApp {

	private PrintStream ps = null;
	private String fileName = null;
	private long maxLength = 10485760L;
	private int type1ParLen = 5;
	private int type2ParLen = 5;
	private int type3ParLen = 5;
	private int type4ParLen = 2;
	private int type5ParLen = 2;
	private int type6ParLen = 2;
	private int type7ParLen = 2;
	private int type8ParLen = 2;
	private int type9ParLen = 2;
	private int type10ParLen = 2;
	private int type11ParLen = 2;
	private int type21ParLen = 2;
	private int type22ParLen = 2;
	private int type23ParLen = 2;
	private int type24ParLen = 1;
	private int type25ParLen = 1;
	private int type26ParLen = 1;
	private int type27ParLen = 1;
	private int type28ParLen = 1;
	private int typeParLen;
	
	ResourceBundle rb = ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES) == null
			? ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES_OUTSIDE)
			: ResourceBundleFactory.getInstance().getBundle(ResourceBundleFactory.PROP_JAR_COMPONENT_PROPERTIES);
			
	private final String mfMailInDB = "MFSMSMIDB";
	private final String mfCompCode = "MC";
	private final String mfSMSDtFormat = "MMM d, yyyy hh:mm:ss a";
	private final String smsSubjectFormat = "<<SMS_STATUS>> to <<AGENT_NAME>> (<<AGENT_ID>>) <<AGENT_MOBILE_NO>> for <<ORDER_TICKET_NO>> - <<CLIENT_NAME>> on <<SMS_DATE>>";
	private final String smsStatusFormatSuccess = "SMS record saved in sms gateway";
	private final String smsSubjectFormatFailed = "SMS Failure Notification";//"Failed to save in sms gateway";
	private final String smsPHNotBrnchPrefix = StringUtils.trimToEmpty(rb.getString(MFNotificationConstants.MF_BRANCH_PREFIX));

	private BufferedWriter out = null;
	private boolean logging = true;

	public void initializeLog(){
		try {if(logging){
				out = new BufferedWriter(new FileWriter(rb.getString("WMS_LOG_PATH")+"\\MFNotificationLog.log", true));
			}
		} catch (IOException e) {
		}
	}

	public void log(String aString){
		try {
			if(logging){
				out.write(new Date() + " : "+ aString + "\n");
			}else{
				System.out.println(aString);
			}
		} catch (IOException e) {
		}
	}

	public void closeLog(){
		try {
			if(logging){
				out.close();
			}
		} catch (IOException e) {
		}
	}
	
	public boolean send(
			String from, String to, String cc,
			String subject, int mailType, String orderTicketNo,
			HashMap map)
	{
		Properties props = new Properties();
		props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);
		
		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		try {
			if (from != null && !from.trim().equals(""))
				msg.setFrom(new InternetAddress(from));
			if (to != null && !to.trim().equals("")) {
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals(""))
				msg.setSubject(subject);


			if (mailType < 20) {
				String dueTime = (map.get("dueTime") != null) ? (String) map.get("dueTime") : "";
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				String dueTime = (map.get("dueTime") != null) ? (String) map.get("dueTime") : "";
				collectSMS(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}
			
			if (map.get("invoker") != null && map.get("attachments") != null && map.get("remarks") != null) {
				String invoker = (String) map.get("invoker");
				String [] attachments = (String []) map.get("attachments"); 
				String remarks = (String) map.get("remarks");
				if (invoker.equals("emailDocs")) { 
					onEmailDocs(msg, attachments, remarks);		
				}
			} else {
			}

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean send(
			String from, String to, String cc,
			String subject, int mailType, String orderTicketNo)
	{

		Properties props = new Properties();
		props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);
		
		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		try {
			if (from != null && !from.trim().equals(""))
				msg.setFrom(new InternetAddress(from));
			if (to != null && !to.trim().equals("")) {
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals(""))
				msg.setSubject(subject);


			if (mailType < 20) {
				String dueTime = getDueTime(orderTicketNo); // Used by the component
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				String dueTime = getDueTime(orderTicketNo); // Used by the component
				collectSMS(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean sendDueTime(
			String from, String to, String cc,
			String subject, int mailType, String orderTicketNo) 
	{

		initializeLog();
		log("sendDueTime 6 params");
		log("");
		log("before from "+from);
		log("before to "+to);
		log("before cc "+cc);
		log("before subject "+subject);
		log("before mailType "+mailType);
		log("before orderTicketNo "+orderTicketNo);

		Properties props = new Properties();
		props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);

		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);

		String ccFailed = null;
		String ccPassed = null;

		String agentId = null;
		String agentMobile = null;
		String agentEmail = null;
		String agentFName = null;
		String agentLName = null;
		String agentBranchId = null;
		String SMSDate = null;
		String clientLName = null;
		String clientFName = null;
		String body = null;

		String Ccc = "";

		if(mailType==1 || mailType==2 || mailType==3){
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			List mfLUCodeList = new ArrayList();
			MFLookupCodeDTO mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");
			Iterator it = mfLUCodeList.iterator();
			int i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					Ccc = mfLUCodeDto.getCODE_VALUE();
				}else{
					Ccc = Ccc + ","+ mfLUCodeDto.getCODE_VALUE();
				}
				i = i++;
			}
			if(cc!=null && !cc.equals("") && Ccc!=null && !Ccc.equals("")){
				cc = cc+", "+Ccc;
				log("cc and mailInDB "+cc);
			}
		}

		if(mailType==21 || mailType==22 || mailType==23){

			try
			{
				log("");
				log("Putting to sleep so that MFApplications will be updated with correct agent. "+orderTicketNo);
				log("This is for SMS only.");
				Thread.sleep(20000);        
				log("");
			} catch (InterruptedException ie) {
				log("Putting to sleep failed."+ orderTicketNo);
				System.out.println(ie.getMessage());
			}
			
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			List mfLUCodeList = new ArrayList();
			MFLookupCodeDTO mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");
			Iterator it = mfLUCodeList.iterator();
			int i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					to = mfLUCodeDto.getCODE_VALUE();
				}else{
					to = to + ","+ mfLUCodeDto.getCODE_VALUE();
				}
				i = i++;
			}

			MFApplicationsDAO mfAppDao = new MFApplicationsDAO();
			MFApplicationsDTO mfAppDto = new MFApplicationsDTO();

			mfAppDto = mfAppDao.getMFApplicationsDTO("where MFP_OrderTicketNumber='"+orderTicketNo+"'");
			agentId = mfAppDto.getMFP_AgentID();
			clientLName = mfAppDto.getMFP_ClientLName();
			clientFName = mfAppDto.getMFP_ClientFName();

			log("agentId from MFApplications : "+agentId);
			log("clientLName from MFApplications : "+clientLName);
			log("clientFName from MFApplications : "+clientFName);

			MFLookupAgentDAO mfLUAgentDao = new MFLookupAgentDAO();
			List mfLUAgentList = new ArrayList();
			MFLookupAgentDTO mfLUAgentDto = null;
			//mfLUAgentList = mfLUAgentDao.getMFLookupAgentList("where CardNo='"+agentId+"' and CompanyCode='"+mfCompCode+"'");
			mfLUAgentList = mfLUAgentDao.getMFLookupAgentList("where CardNo='"+agentId+"'");
			it = mfLUAgentList.iterator();
			while(it.hasNext()){
				mfLUAgentDto = new MFLookupAgentDTO();
				mfLUAgentDto = (MFLookupAgentDTO)it.next();
				agentMobile = mfLUAgentDto.getMobileNo();
				agentEmail = mfLUAgentDto.getEmail();
				agentFName = mfLUAgentDto.getFirstName();
				agentLName = mfLUAgentDto.getLastName();
				agentBranchId = mfLUAgentDto.getBranchId();
			}

			DateFormat formatter = new SimpleDateFormat(mfSMSDtFormat);
			try {
				SMSDate = formatter.format(new Date());
			} catch (Exception e) {
			}
			log("");
			log("agentId : "+agentId);
			log("agentFName : "+agentFName);
			log("agentLName : "+agentLName);
			log("agentBranchId : "+agentBranchId);
			log("agentMobile : "+agentMobile);
			log("agentEmail : "+agentEmail);
			log("SMSDate : "+SMSDate);
			log("clientLName : "+clientLName);
			log("clientFName : "+clientFName);
			log("");

			//cc
			//'ISM02', #PHIL-Notification-Brch-


			mfLUCodeDao = new MFLookupCodeDAO();
			mfLUCodeList = new ArrayList();
			mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = 'ISM02'");
			it = mfLUCodeList.iterator();
			i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					cc = mfLUCodeDto.getCODE_VALUE();
					ccFailed = mfLUCodeDto.getCODE_VALUE();
				}else{
					cc = cc + ","+ mfLUCodeDto.getCODE_VALUE();
					ccFailed = ccFailed + ","+ mfLUCodeDto.getCODE_VALUE();
				}
			}

			if(ccFailed!=null && !ccFailed.equals("") && agentBranchId!=null && !agentBranchId.equals("")){
				ccFailed = ccFailed +","+smsPHNotBrnchPrefix+agentBranchId;
			}else{
				ccFailed = smsPHNotBrnchPrefix+agentBranchId;
			}

			if(agentBranchId!=null && !agentBranchId.equals("")){
				cc = cc + ","+smsPHNotBrnchPrefix+agentBranchId;
			}


			if(agentBranchId!=null && !agentBranchId.equals("")){
				ccPassed = smsPHNotBrnchPrefix+agentBranchId;
			}

			log("mailType==21 || mailType==22 || mailType==23 to "+to);
			log("mailType==21 || mailType==22 || mailType==23 cc "+cc);
		}

		try {
			if (from != null && !from.trim().equals("")){
				msg.setFrom(new InternetAddress(from));
			}
			if (to != null && !to.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals("")){
				msg.setSubject(subject);
			}

			log("");
			log("from "+from);
			log("to "+to);
			log("cc "+cc);
			log("subject "+subject);
			log("mailType "+mailType);
			log("orderTicketNo "+orderTicketNo);
			log("");

			if (mailType < 20) {
				//String dueTime = extractTimePart(entryDate);
				String dueTime = getDueTime(orderTicketNo);
				log("dueTime "+dueTime);
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				//String dueTime = extractTimePart(entryDate);
				String dueTime = getDueTime(orderTicketNo);
				log("dueTime "+dueTime);
				body = collectSMSText(msg, mailType, orderTicketNo, dueTime);
				log("body "+body);
				body=body.trim();
				body = body.replaceAll(", : ", ": ");
				body = body.replaceAll(", . ", ". ");
				body = body.replaceAll("for ,", "for ");
				body = body.replaceAll("'", "''");
				//collectSMS(msg, mailType, orderTicketNo, dueTime);
				// Gateway SMS Notification - Andre Ceasar Dacanay - start
				boolean a = false;
				try{
					NotificationHandler s = new NotificationHandler();
					if(agentMobile!=null && !agentMobile.equals("")){
						//a = s.sendSMS(from,agentMobile, "CLI", body);
						a = s.sendSMS_loc(from,agentMobile, "CLI", body, mfCompCode);
					}
					log("agentMobile : "+agentMobile);
					body = body.replaceAll("''", "'");
					log("body after "+body);
					//a = s.sendSMS("wms@sunlife.com",agentMobile , "CLI", body);
					log("sending of sms thru gateway "+a);
				}catch(Exception e){
					log("Exception s.sendSMS e "+e.getCause());
					log("Exception s.sendSMS e "+e.getMessage());
					log("Exception s.sendSMS e "+e.getStackTrace());
					log("");
					log("");
				}

				agentFName = agentFName!=null ? agentFName : "";
				agentLName = agentLName!=null ? agentLName : "";
				agentId = agentId!=null ? agentId : "";
				agentMobile = agentMobile!=null ? agentMobile : "";
				orderTicketNo = orderTicketNo!=null ? orderTicketNo : "";
				clientFName = clientFName!=null ? clientFName : "";
				SMSDate = SMSDate!=null ? SMSDate : "";

				if(mailType==21 || mailType==22 || mailType==23){
					subject = smsSubjectFormat;
					subject = subject.replaceAll("<<AGENT_NAME>>", agentFName+" "+agentLName);
					subject = subject.replaceAll("<<AGENT_ID>>", agentId);
					subject = subject.replaceAll("<<AGENT_MOBILE_NO>>", agentMobile);
					subject = subject.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
					subject = subject.replaceAll("<<CLIENT_NAME>>", clientFName+" "+clientLName);
					subject = subject.replaceAll("<<SMS_DATE>>", SMSDate);
					subject = subject.replaceAll("  ", " ");
					subject = subject.trim();
				}

				if(a){
					subject = subject.replaceAll("<<SMS_STATUS>>", smsStatusFormatSuccess);
					if (ccPassed != null && !ccPassed.trim().equals("")) {
						//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
						String []ccRecipients = ccPassed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}else{
					subject = subject.replaceAll("<<SMS_STATUS>>", smsSubjectFormatFailed);
					if (ccFailed != null && !ccFailed.trim().equals("")) {
						//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
						String []ccRecipients = ccFailed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}

				if(mailType==21 || mailType==22 || mailType==23){

					if(agentMobile!=null && agentMobile.equals("") &&
							agentEmail!=null && agentEmail.equals("")){
						subject = subject + " - no agent email address and agent mobile number";
					}else if((agentMobile!=null && agentMobile.equals("")) && 
							(agentEmail!=null && !agentEmail.equals(""))){
						subject = subject + " - no agent mobile number";
					}else if((agentMobile!=null && !agentMobile.equals("")) &&
							(agentEmail!=null && agentEmail.equals(""))){
						subject = subject + " - no agent email address";
					}

					msg.setSubject(subject);
					msg.setText(body);
				}
				log("Audit Email subject after "+subject);
				// Gateway SMS Notification - Andre Ceasar Dacanay - end

			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {

			log("AddressException e "+e.getCause());
			log("AddressException e "+e.getMessage());
			log("AddressException e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		} catch (MessagingException e) {

			log("MessagingException e "+e.getCause());
			log("MessagingException e "+e.getMessage());
			log("MessagingException e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		} catch (Exception e) {

			log("Exception e "+e.getCause());
			log("Exception e "+e.getMessage());
			log("Exception e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		}finally{
			closeLog();
		}


		return true;
	}

	public boolean send(String from, String to, String cc, String subject, String message)
	{

		Properties props = new Properties();
		props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);

		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		try {
			if (from != null && !from.trim().equals(""))
				msg.setFrom(new InternetAddress(from));
			if (to != null && !to.trim().equals("")) {
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals(""))
				msg.setSubject(subject);

			message += "\n\n" + StringEscapeUtils.unescapeJava(DisclaimerEnum.getDisclaimer(DisclaimerType.EXTERNAL, Company.SLOCPI).getDisclaimerDTO().getData());
			collectEmail(msg, message);

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean sendDueTime(
			String from, String to, String cc,
			String subject, int mailType, String orderTicketNo, String dueTime) 
	{

		initializeLog();
		log("sendDueTime 7 params");
		log("");
		log("before from "+from);
		log("before to "+to);
		log("before cc "+cc);
		log("before subject "+subject);
		log("before mailType "+mailType);
		log("before orderTicketNo "+orderTicketNo);
		log("before dueTime "+dueTime);

		Properties props = new Properties();
		props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);

		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);

		String ccFailed = null;
		String ccPassed = null;

		String agentId = null;
		String agentMobile = null;
		String agentEmail = null;
		String agentFName = null;
		String agentLName = null;
		String agentBranchId = null;
		String SMSDate = null;
		String clientLName = null;
		String clientFName = null;
		String body = null;

		String Ccc = "";

		if(mailType==1 || mailType==2 || mailType==3){
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			List mfLUCodeList = new ArrayList();
			MFLookupCodeDTO mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");
			Iterator it = mfLUCodeList.iterator();
			int i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					Ccc = mfLUCodeDto.getCODE_VALUE();
				}else{
					Ccc = Ccc + ","+ mfLUCodeDto.getCODE_VALUE();
				}
				i = i++;
			}
			if(cc!=null && !cc.equals("") && Ccc!=null && !Ccc.equals("")){
				cc = cc+", "+Ccc;
				log("cc and mailInDB "+cc);
			}
		}

		if(mailType==21 || mailType==22 || mailType==23){

			try
			{
				log("");
				log("Putting to sleep so that MFApplications will be updated with correct agent. "+orderTicketNo);
				log("This is for SMS only.");
				Thread.sleep(20000);        
				log("");
			}catch (InterruptedException ie)
			{
				log("Putting to sleep failed."+ orderTicketNo);
				System.out.println(ie.getMessage());
			}
			
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			List mfLUCodeList = new ArrayList();
			MFLookupCodeDTO mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");
			Iterator it = mfLUCodeList.iterator();
			int i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					to = mfLUCodeDto.getCODE_VALUE();
				}else{
					to = to + ","+ mfLUCodeDto.getCODE_VALUE();
				}
				i = i++;
			}

			MFApplicationsDAO mfAppDao = new MFApplicationsDAO();
			MFApplicationsDTO mfAppDto = new MFApplicationsDTO();

			mfAppDto = mfAppDao.getMFApplicationsDTO("where MFP_OrderTicketNumber='"+orderTicketNo+"'");
			agentId = mfAppDto.getMFP_AgentID();
			clientLName = mfAppDto.getMFP_ClientLName();
			clientFName = mfAppDto.getMFP_ClientFName();

			log("agentId from MFApplications : "+agentId);
			log("clientLName from MFApplications : "+clientLName);
			log("clientFName from MFApplications : "+clientFName);

			MFLookupAgentDAO mfLUAgentDao = new MFLookupAgentDAO();
			List mfLUAgentList = new ArrayList();
			MFLookupAgentDTO mfLUAgentDto = null;
			mfLUAgentList = mfLUAgentDao.getMFLookupAgentList("where CardNo='"+agentId+"'");
			it = mfLUAgentList.iterator();
			while(it.hasNext()){
				mfLUAgentDto = new MFLookupAgentDTO();
				mfLUAgentDto = (MFLookupAgentDTO)it.next();
				agentMobile = mfLUAgentDto.getMobileNo();
				agentEmail = mfLUAgentDto.getEmail();
				agentFName = mfLUAgentDto.getFirstName();
				agentLName = mfLUAgentDto.getLastName();
				agentBranchId = mfLUAgentDto.getBranchId();
			}

			DateFormat formatter = new SimpleDateFormat(mfSMSDtFormat);
			try {
				SMSDate = formatter.format(new Date());
			} catch (Exception e) {
			}
			log("");
			log("agentId : "+agentId);
			log("agentFName : "+agentFName);
			log("agentLName : "+agentLName);
			log("agentBranchId : "+agentBranchId);
			log("agentMobile : "+agentMobile);
			log("agentEmail : "+agentEmail);
			log("SMSDate : "+SMSDate);
			log("clientLName : "+clientLName);
			log("clientFName : "+clientFName);
			log("");

			mfLUCodeDao = new MFLookupCodeDAO();
			mfLUCodeList = new ArrayList();
			mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = 'ISM02'");
			it = mfLUCodeList.iterator();
			i = 1;
			while(it.hasNext()){
				mfLUCodeDto = new MFLookupCodeDTO();
				mfLUCodeDto = (MFLookupCodeDTO)it.next();

				if(i==1){
					cc = mfLUCodeDto.getCODE_VALUE();
					ccFailed = mfLUCodeDto.getCODE_VALUE();
				}else{
					cc = cc + ","+ mfLUCodeDto.getCODE_VALUE();
					ccFailed = ccFailed + ","+ mfLUCodeDto.getCODE_VALUE();
				}
			}

			if(ccFailed!=null && !ccFailed.equals("") && agentBranchId!=null && !agentBranchId.equals("")){
				ccFailed = ccFailed +","+smsPHNotBrnchPrefix+agentBranchId;
			}else{
				ccFailed = smsPHNotBrnchPrefix+agentBranchId;
			}

			if(agentBranchId!=null && !agentBranchId.equals("")){
				cc = cc + ","+smsPHNotBrnchPrefix+agentBranchId;
			}


			if(agentBranchId!=null && !agentBranchId.equals("")){
				ccPassed = smsPHNotBrnchPrefix+agentBranchId;
			}

			log("mailType==21 || mailType==22 || mailType==23 to "+to);
			log("mailType==21 || mailType==22 || mailType==23 cc "+cc);
		}

		try {
			if (from != null && !from.trim().equals("")){
				msg.setFrom(new InternetAddress(from));
			}
			if (to != null && !to.trim().equals("")) {
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals("")){
				msg.setSubject(subject);
			}

			log("");
			log("from "+from);
			log("to "+to);
			log("cc "+cc);
			log("subject "+subject);
			log("mailType "+mailType);
			log("orderTicketNo "+orderTicketNo);
			log("entryDate "+dueTime);
			log("");

			if (mailType < 20) {
				//String dueTime = extractTimePart(entryDate);
				log("dueTime "+dueTime);
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				//String dueTime = extractTimePart(entryDate);
				log("dueTime "+dueTime);
				body = collectSMSText(msg, mailType, orderTicketNo, dueTime);
				log("body "+body);
				body=body.trim();
				body = body.replaceAll(", : ", ": ");
				body = body.replaceAll(", . ", ". ");
				body = body.replaceAll("for ,", "for ");
				body = body.replaceAll("'", "''");
				boolean a = false;
				try{
					NotificationHandler s = new NotificationHandler();
					if(agentMobile!=null && !agentMobile.equals("")){
						a = s.sendSMS_loc(from,agentMobile, "CLI", body, mfCompCode);
					}
					log("agentMobile : "+agentMobile);
					body = body.replaceAll("''", "'");
					log("body after "+body);
					log("sending of sms thru gateway "+a);
				}catch(Exception e){
					log("Exception s.sendSMS e "+e.getCause());
					log("Exception s.sendSMS e "+e.getMessage());
					log("Exception s.sendSMS e "+e.getStackTrace());
					log("");
					log("");
				}

				body += "\n\n" + StringEscapeUtils.unescapeJava(DisclaimerEnum.getDisclaimer(DisclaimerType.EXTERNAL, Company.SLOCPI).getDisclaimerDTO().getData());
				agentFName = agentFName!=null ? agentFName : "";
				agentLName = agentLName!=null ? agentLName : "";
				agentId = agentId!=null ? agentId : "";
				agentMobile = agentMobile!=null ? agentMobile : "";
				orderTicketNo = orderTicketNo!=null ? orderTicketNo : "";
				clientFName = clientFName!=null ? clientFName : "";
				SMSDate = SMSDate!=null ? SMSDate : "";

				if(mailType==21 || mailType==22 || mailType==23){
					subject = smsSubjectFormat;
					subject = subject.replaceAll("<<AGENT_NAME>>", agentFName+" "+agentLName);
					subject = subject.replaceAll("<<AGENT_ID>>", agentId);
					subject = subject.replaceAll("<<AGENT_MOBILE_NO>>", agentMobile);
					subject = subject.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
					subject = subject.replaceAll("<<CLIENT_NAME>>", clientFName+" "+clientLName);
					subject = subject.replaceAll("<<SMS_DATE>>", SMSDate);
					subject = subject.replaceAll("  ", " ");
					subject = subject.trim();
				}

				if(a){
					subject = subject.replaceAll("<<SMS_STATUS>>", smsStatusFormatSuccess);
					if (ccPassed != null && !ccPassed.trim().equals("")) {
						String []ccRecipients = ccPassed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}else{
					subject = subject.replaceAll("<<SMS_STATUS>>", smsSubjectFormatFailed);
					if (ccFailed != null && !ccFailed.trim().equals("")) {
						String []ccRecipients = ccFailed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}

				if(mailType==21 || mailType==22 || mailType==23){

					if(agentMobile!=null && agentMobile.equals("") &&
							agentEmail!=null && agentEmail.equals("")){
						subject = subject + " - no agent email address and agent mobile number";
					}else if((agentMobile!=null && agentMobile.equals("")) && 
							(agentEmail!=null && !agentEmail.equals(""))){
						subject = subject + " - no agent mobile number";
					}else if((agentMobile!=null && !agentMobile.equals("")) &&
							(agentEmail!=null && agentEmail.equals(""))){
						subject = subject + " - no agent email address";
					}

					msg.setSubject(subject);
					msg.setText(body);
				}
				log("Audit Email subject after "+subject);

			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {

			log("AddressException e "+e.getCause());
			log("AddressException e "+e.getMessage());
			log("AddressException e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		} catch (MessagingException e) {

			log("MessagingException e "+e.getCause());
			log("MessagingException e "+e.getMessage());
			log("MessagingException e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		} catch (Exception e) {

			log("Exception e "+e.getCause());
			log("Exception e "+e.getMessage());
			log("Exception e "+e.getStackTrace());

			e.printStackTrace();
			//closeLog();
		}finally{
			closeLog();
		}


		return true;
	}

	// Used by component
	public boolean send(String from, String to, String cc,
			String subject, int mailType, String orderTicketNo, String entryDate) {
		try {
			initializeLog();
			log("send 7 params");log("");
			log("before from "+from); log("before to "+to);
			log("before cc "+cc); log("before subject "+subject);
			log("before mailType "+mailType); log("before orderTicketNo "+orderTicketNo);
			log("before entryDate "+entryDate);
			log("SAMMD --MFNOTIFICATION START--");
			Properties props = new Properties();
			props.put(MFNotificationConstants.MAIL_SMTP_HOST_VAR, MFNotificationConstants.MAIL_SMTP_HOST_VAL);
			
			Session session = Session.getDefaultInstance(props, null);
			Message msg = new MimeMessage(session);
			
			WmsParamDAO wmsParam = new WmsParamDAOImpl();
			MFApplicationsDAO mfAppDAO = new MFApplicationsDAO();
			log("MFNotificationDecisionHelper init log --START--");
			MFNotifcationDecisionHelper.initializeLogs(out);
			log("MFNotificationDecisionHelper init log --END--");
			
			if(mailType > 0 && mailType < 20){
				log("Getting MF Email Notif Decision in WMS Param");
				if(MFNotifcationDecisionHelper.getNotificationDecision(wmsParam.getWmsParamValue(MFNotificationConstants.MF_NOTIFICATION_EMAIL_DECISION))){
					log("Getting MF Email Notif Decision Depending on Account Type in WMS Param");
					if(MFNotifcationDecisionHelper.getNotificationDecisionDependingOnAccountType(mfAppDAO.getMFAccountType(orderTicketNo), wmsParam.getWmsParamValue(MFNotificationConstants.MF_PROMO_NOTIFICATION_DECISION))){
						sendEmailNotification(msg, from, to, cc, subject, mailType, orderTicketNo, entryDate);
					}
				}
			} else if (mailType > 20 && mailType < 30){
				log("Getting MF SMS Notif Decision in WMS Param");
				if(MFNotifcationDecisionHelper.getNotificationDecision(wmsParam.getWmsParamValue(MFNotificationConstants.MF_NOTIFICATION_SMS_DECISION)))
					log("Getting MF SMS Notif Decision Depending on Account Type in WMS Param");
					if(MFNotifcationDecisionHelper.getNotificationDecisionDependingOnAccountType(mfAppDAO.getMFAccountType(orderTicketNo), wmsParam.getWmsParamValue(MFNotificationConstants.MF_PROMO_NOTIFICATION_DECISION))){
						sendSMSNotification(msg, from, to, cc, subject, mailType, orderTicketNo, entryDate);
					}
			} else if(mailType < 200){
				collectSMSOption(msg, subject, orderTicketNo);
			}else {
				customEmail(msg, subject); 
			}
		} catch (Exception e) {
			log("MFNotifcation send Exception : "+PrintExceptionHelper.exceptionStacktraceToString(e));
			closeLog();
		}finally{
			closeLog();
		}
		return true;
	}

	private String getCurrentDate(String format) {
		Date date = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	private String extractTimePart(String entryDate) {
		DateFormat formatter = new SimpleDateFormat("MMM d, yyyy hh:mm:ss a");
		String timePartEntryDate = "";
		String dueTime = "";
		try {
			Date date = (Date) formatter.parse(entryDate);
			Format format = new SimpleDateFormat("HH:mm:ss");
			timePartEntryDate = format.format(date);
			dueTime = determineCutOff(timePartEntryDate);
		} catch (ParseException e) {
			//e.printStackTrace();
		}
		return dueTime;

	}

	private String determineCutOff(String timePartEntryDate) {
		String dueTime = "";
		Time timeValueEntryDate = Time.valueOf(timePartEntryDate);
		Time timeSet1 = Time.valueOf("8:00:00");
		Time timeSet2 = Time.valueOf("11:01:00");
		Time timeSet3 = Time.valueOf("14:01:00");
		Time timeSet4 = Time.valueOf("15:00:00");
		if (!timeValueEntryDate.before(timeSet1) 
				&& timeValueEntryDate.before(timeSet2)) {
			dueTime = "12:00pm ";
		} else if (!timeValueEntryDate.before(timeSet2) 
				&& timeValueEntryDate.before(timeSet3)) {
			dueTime = "3:00pm ";
		} else if (!timeValueEntryDate.before(timeSet3) 
				&& timeValueEntryDate.before(timeSet4)) {
			dueTime = "4:30pm ";
		} else {

		}
		return dueTime;
	}

	// Same method used in Requirement Abeyance Process class
	private String getDueTime(String orderTicketNo) {
		Timestamp entryDateTime = null;
		try {
			DBConnect dbc = new DBConnect();
			Connection con = dbc.connect();
			Statement stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("SELECT ENTRY_DATETIME FROM MF_NOTIFY_SCHED WHERE ORDER_TICKET_NO='")
			.append(orderTicketNo)
			.append("'");
			ResultSet rs = stmt.executeQuery(query.toString());

			String remarks = "";
			while (rs.next()) {
				entryDateTime = rs.getTimestamp("ENTRY_DATETIME");
			}
			rs.close();
			stmt.close();
			con.close();
		} catch(Exception e) {

		}
		System.out.println("getDueTime() : Entry");
		System.out.println("getDueTime() : entryDateTime=" + entryDateTime);
		String dueTime = "";
		Format formatter = new SimpleDateFormat("HH:mm:ss");
		String timeOfEntryDate = formatter.format(entryDateTime);
		Time timeValueEntryDate = Time.valueOf(timeOfEntryDate);
		Time timeSet1 = Time.valueOf("8:00:00");
		Time timeSet2 = Time.valueOf("11:01:00");
		Time timeSet3 = Time.valueOf("14:01:00");
		Time timeSet4 = Time.valueOf("15:00:00");
		if (!timeValueEntryDate.before(timeSet1) 
				&& timeValueEntryDate.before(timeSet2)) {
			dueTime = "12:00pm";
		} else if (!timeValueEntryDate.before(timeSet2) 
				&& timeValueEntryDate.before(timeSet3)) {
			dueTime = "3:00pm";
		} else if (!timeValueEntryDate.before(timeSet3) 
				&& timeValueEntryDate.before(timeSet4)) {
			dueTime = "4:30pm";
		} else {
			System.out.println("getDueTime() : Did not qualify to conditions");
		}
		System.out.println("getDueTime() : dueTime="+dueTime);
		System.out.println("getDueTime() : Exit");
		return dueTime;
	}

	public void onEmailDocs(Message msg, String attachments[], String remarks) {
		try {
			BodyPart mbp[] = new BodyPart[attachments.length];
			Multipart multipart = new MimeMultipart();
			String filename = "";
			DataSource source = null;

			for (int i = 0; i < attachments.length; i++) {
				mbp[i] = new MimeBodyPart();
				filename = attachments[i];
				source = new FileDataSource(attachments[i]);
				mbp[i].setDataHandler(new DataHandler(source));
				mbp[i].setFileName(attachments[i]);
				multipart.addBodyPart(mbp[i]);
			}

			// Add text 
			BodyPart bp = new MimeBodyPart();
			bp.setText("\n" + remarks + "\n\n" + StringEscapeUtils.unescapeJava(DisclaimerEnum.getDisclaimer(DisclaimerType.EXTERNAL, Company.SLOCPI).getDisclaimerDTO().getData()));
			multipart.addBodyPart(bp);
			msg.setContent(multipart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void collectSMS(Message msg, int mailType, String orderTicketNo, String dueTime) 
			throws MessagingException {

		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");

		String []arrClientDetails = getClientDetails(orderTicketNo);
		String clientLastName = arrClientDetails[0];
		String clientFirstName = arrClientDetails[1];
		String mfTxnType = arrClientDetails[2];
		StringBuffer sb = new StringBuffer();
		String body = null;	
		String mType = null;
		switch(mailType) {
		case 21:
			String reqts[] = getRequirements(orderTicketNo);
			mType = "SMSReq"; // SMS Requirement Notification
			typeParLen = type21ParLen;
			sb.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(": ");
			for(int i=0; i<reqts.length; i++) {
				if (i>0) {
					sb.append(", ");
				}
				if (reqts[i].indexOf("-") != -1) {
					sb.append(reqts[i].substring(0, reqts[i].indexOf("-")-1));
				} else {
					sb.append(reqts[i]);
				}
			}
			sb.append(rb.getString(mType+"Par2").replaceAll("12 nn ", dueTime));
			break;
		case 22:
			mType = "SMSCan"; // SMS Cancellation Notification
			typeParLen = type22ParLen;
			sb.append(mfTxnType)
			.append(" ")
			.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(" ")
			.append(rb.getString(mType+"Par2"));
			break;
		case 23:
			mType = "SMSRef"; // SMS Notice of Refund
			typeParLen = type23ParLen;
			sb.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(" ")
			.append(rb.getString(mType+"Par2"));
			break;
		case 24:
			mType = "SMSValComp"; // SMS prior to processing on MFTransactionValidation and MFFinanceProcessing queue
			sb.append(rb.getString(mType+"Par1"));
			break;
		case 25:
			mType = "SMSReqtRcvd"; // SMS prior to processing on MFRequirementAbeyance and MFForNPW queue
			sb.append(rb.getString(mType+"Par1"));			
			break;
		case 26:
			mType = "RDRcvd"; // SMS prior to processing on RDIFT queue - Redemption
			sb.append(rb.getString(mType+"Par1"));			
			break;
		case 27:
			mType = "IFTRcvd"; // SMS prior to processing on RDIFT queue - Interfund
			sb.append(rb.getString(mType+"Par1"));									
			break;
		case 28:
			mType = "SMSValCompIS"; // SMS prior to processing on MFTransactionValidation and MFFinanceProcessing queue - IS
			sb.append(rb.getString(mType+"Par1"));
			break;			
		default: break;
		}
		body = sb.toString();
		body = body.replaceAll("<<CLIENT_NAME>>", clientFirstName + " " + clientLastName);
		body = body.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
		body = body.replaceAll("<<CURRENT_TIME>>", getCurrentDate("ddMMMyyyy"));
		msg.setText(body);
	}

	public String collectSMSText(Message msg, int mailType, String orderTicketNo, String dueTime) 
			throws MessagingException {

		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");

		String []arrClientDetails = getClientDetails(orderTicketNo);
		String clientLastName = arrClientDetails[0];
		String clientFirstName = arrClientDetails[1];
		String mfTxnType = arrClientDetails[2];
		StringBuffer sb = new StringBuffer();
		String body = null;
		String mType = null;
		switch(mailType) {
		case 21:
			String reqts[] = getRequirements(orderTicketNo);
			mType = "SMSReq"; // SMS Requirement Notification
			typeParLen = type21ParLen;
			sb.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(": ");
			for(int i=0; i<reqts.length; i++) {
				if (i>0) {
					sb.append(", ");
				}
				if (reqts[i].indexOf("-") != -1) {
					sb.append(reqts[i].substring(0, reqts[i].indexOf("-")-1));
				} else {
					sb.append(reqts[i]);
				}
			}
			sb.append(rb.getString(mType+"Par2").replaceAll("12 nn ", dueTime));
			break;
		case 22:
			mType = "SMSCan"; // SMS Cancellation Notification
			typeParLen = type22ParLen;
			sb.append(mfTxnType)
			.append(" ")
			.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(" ")
			.append(rb.getString(mType+"Par2"));
			break;
		case 23:
			mType = "SMSRef"; // SMS Notice of Refund
			typeParLen = type23ParLen;
			sb.append(rb.getString(mType+"Par1"))
			.append(" ")
			.append(clientLastName)
			.append(", ")
			.append(clientFirstName)
			.append(" ")
			.append(rb.getString(mType+"Par2"));
			break;
		case 24:
			mType = "SMSValComp"; // SMS prior to processing on MFTransactionValidation and MFFinanceProcessing queue
			sb.append(rb.getString(mType+"Par1"));			
			break;
		case 25:
			mType = "SMSReqtRcvd"; // SMS prior to processing on MFRequirementAbeyance and MFForNPW queue
			sb.append(rb.getString(mType+"Par1"));						
			break;
		case 26:
			mType = "RDRcvd"; // SMS prior to processing on RDIFT queue - Redemption
			sb.append(rb.getString(mType+"Par1"));			
			break;
		case 27:
			mType = "IFTRcvd"; // SMS prior to processing on RDIFT queue - Interfund
			sb.append(rb.getString(mType+"Par1"));												
			break;
		case 28:
			mType = "SMSValCompIS"; // SMS prior to processing on MFTransactionValidation and MFFinanceProcessing queue
			sb.append(rb.getString(mType+"Par1"));			
			break;			
		default: break;
		}
		body = sb.toString();
		body = body.replaceAll("<<CLIENT_NAME>>", clientFirstName + " " + clientLastName);
		body = body.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
		body = body.replaceAll("<<CURRENT_TIME>>", getCurrentDate("ddMMMyyyy"));
		return body;
	}

	public void collectSMSOption(Message msg, String subject, String orderTicketNo) 
			throws MessagingException {
		typeParLen = type21ParLen;
		msg.setText(subject);
	}

	public void collectEmail(Message msg, String message) 
			throws MessagingException {
		typeParLen = type21ParLen;
		msg.setText(message);
	}

	public void customEmail(Message msg, String message) 
			throws MessagingException{

		msg.setText(message);
	}

	public void collect(Message msg, int mailType, String OrderTicketNo, String dueTime) 
			throws MessagingException{
		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");

		String []arrClientDetails = getClientDetails(OrderTicketNo);
		String clientLastName = arrClientDetails[0];
		String clientFirstName = arrClientDetails[1];
		String mfTxnType = arrClientDetails[2];
		String body = null;

		StringBuffer sb = new StringBuffer();

		String mType = null;
		switch(mailType) {
		case 1:
			mType = "Req"; // Order Requirements
			typeParLen = type1ParLen;
			break;
		case 2:
			mType = "Can"; // Notification for Cancellation
			typeParLen = type2ParLen;
			break;
		case 3:
			mType = "Pay"; // Payment Ready
			typeParLen = type3ParLen;
			break;
		case 4: 
			mType = "Cor"; // For Payment Correction
			typeParLen = type4ParLen;
			break;
		case 5:
			mType = "Fin"; // Cancellation of MF Transactions to Finance
			typeParLen = type5ParLen;
			break;
		case 6:
			mType = "Ref"; // For Refund
			typeParLen = type6ParLen;
			break;
		case 7:
			mType = "ValComp"; // Validation Complete on MFTransactionValidation and MFFinanceProcessing queues
			typeParLen = type7ParLen;
			break;
		case 8:
			mType = "ReqtRcvd"; // Requirements Received
			typeParLen = type8ParLen;
			break;
		case 9:
			mType = "RDRcvd"; // Redemption Received
			typeParLen = type9ParLen;
			break;				
		case 10:
			mType = "IFTRcvd"; // Interfund Received
			typeParLen = type10ParLen;
			break;				
		case 11:
			mType = "ValCompIS"; // Validation Complete on MFTransactionValidation and MFFinanceProcessing queues - IS
			typeParLen = type11ParLen;
			break;
		case 12:
			mType = "SIF-EF-Comp"; // Sun Interfund Enrollment Complete
			typeParLen = type7ParLen;
			break;
		case 13:
			mType = "SIF-RD-Can"; // Sun Interfund Enrolled Cancel
			typeParLen = type2ParLen;
			break;	
		default: break;
		}

		sb.append(rb.getString("Salutation"));
		sb.append("\n\n");

		try {
			for (int i = 1; i <= typeParLen; i++) {
				sb.append(rb.getString(mType + "Par" + i).replaceAll("12noon ", dueTime));
				sb.append("\n\n");
				//			String OrderTicketNo = "";
				if (mailType == 1 && i == 1) {
					String []arrReqs = getRequirements(OrderTicketNo);
					for (int j = 0; j < arrReqs.length; j++) {
						sb.append("\t\t")
						.append(arrReqs[j]) 
						.append("\n\n");
					}
				} 
			}
		} catch(Exception e) {
		}

		sb.append("\n\n");
		sb.append(rb.getString("Sender"));
		sb.append("\n\n\n\n");
		if (mailType < 7) {
			sb.append(rb.getString("Disclaimer1"));
			sb.append("\n\n");
		}
		sb.append(StringEscapeUtils.unescapeJava(DisclaimerEnum.getDisclaimer(DisclaimerType.EXTERNAL, Company.SLOCPI).getDisclaimerDTO().getData()));
		sb.append("\n\n");

		body = sb.toString();
		body = body.replaceAll("<<CLIENT_NAME>>", clientFirstName + " " + clientLastName);
		body = body.replaceAll("<<ORDER_TICKET_NO>>", OrderTicketNo);
		body = body.replaceAll("<<CURRENT_TIME>>", getCurrentDate("ddMMMyyyy"));
		msg.setText(body);
	}

	private String[] getRequirements(String OrderTicketNo){
		String []arrReqs = null;
		List listRequirements= new ArrayList();
		try {
			DBConnect dbc = new DBConnect();
			Connection con = dbc.connect();
			Statement stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("select DISTINCT mflr.rm_requirement_desc as REQUIREMENTS, REMARKS from MF_LOOKUP_REQUIREMENTS as mflr, mf_requirements as mfr where mfr.rm_requirement_type = mflr.rm_requirement_ID and mfr.ot_ticket_number = '")
			.append(OrderTicketNo)
			.append("'");
			ResultSet rs = stmt.executeQuery(query.toString());
			String reqType = "";
			String remarks = "";
			while (rs.next()) {
				reqType = rs.getString("REQUIREMENTS");
				remarks = rs.getString("REMARKS");
				//	            System.out.println("Requirement Type: " + reqType);
				if (remarks != null) {
					if (remarks.toString().equals(""))
						listRequirements.add(reqType);
					else if(reqType.indexOf("Other")!=-1){
						listRequirements.add(remarks);
					}
					else{
						listRequirements.add(reqType+" - "+remarks);
					}
				} else {
					listRequirements.add(reqType);
				}
			}
			rs.close();
			stmt.close();
			arrReqs = (String[]) listRequirements.toArray(new String [listRequirements.size()]);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return arrReqs;
	}

	private String[] getClientDetails(String OrderTicketNo) {

		ResultSet rs = null;
		Statement stmt = null;
		Connection con = null;

		String []arrClientDetails = new String[3];
		try {
			DBConnect dbc = new DBConnect();
			con = dbc.connect();
			stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("select MFP_ClientLName, MFP_ClientFName, MFP_MFTransactionType from mfapplications where MFP_OrderTicketNumber = '")
			.append(OrderTicketNo)
			.append("'");
			rs = stmt.executeQuery(query.toString());
			String clientLastName = "";
			String clientFirstName = "";
			String mfTxnType = "";
			while (rs.next()) {
				clientLastName = rs.getString("MFP_ClientLName");
				clientFirstName = rs.getString("MFP_ClientFName");
				mfTxnType = rs.getString("MFP_MFTransactionType");
				arrClientDetails[0] = clientLastName;
				arrClientDetails[1] = clientFirstName;
				arrClientDetails[2] = mfTxnType;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				try {
					if (stmt != null) {
						stmt.close();
					}
				} catch (SQLException g) {
					g.printStackTrace();
				}
			} catch (SQLException f) {
				f.printStackTrace();
			}
		}
		return arrClientDetails;
	}

	public StringBuffer collectTest(int mailType, String OrderTicketNo, String dueTime) 
			throws MessagingException{

		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");

		StringBuffer sb = new StringBuffer();

		String mType = null;
		switch(mailType) {
		case 1:
			mType = "Req"; // Order Requirements
			typeParLen = type1ParLen;
			break;
		case 2:
			mType = "Can"; // Notification for Cancellation
			typeParLen = type2ParLen;
			break;
		case 3:
			mType = "Pay"; // Payment Ready
			typeParLen = type3ParLen;
			break;
		case 4: 
			mType = "Cor"; // For Payment Correction
			typeParLen = type4ParLen;
			break;
		case 5:
			mType = "Fin"; // Cancellation of MF Transactions to Finance
			typeParLen = type5ParLen;
			break;
		case 6:
			mType = "Ref"; // For Refund
			typeParLen = type6ParLen;
			break;
		case 21:
			mType = "SMSReq"; // SMS Requirement Notification
			typeParLen = type21ParLen;
			break;
		case 22:
			mType = "SMSCan"; // SMS Cancellation Notification
			typeParLen = type22ParLen;
			break;
		case 23:
			mType = "SMSRef"; // SMS Notice of Refund
			typeParLen = type23ParLen;
			break;
		default: break;
		}

		sb.append(rb.getString("Salutation"));
		sb.append("\n\n");

		try {
			for (int i = 1; i <= typeParLen; i++) {
				sb.append(rb.getString(mType + "Par" + i).replaceAll("12noon", dueTime));
				sb.append("\n\n");
				if (mailType == 1 && i == 1) {
					String []arrReqs = getRequirements(OrderTicketNo);
					for (int j = 0; j < arrReqs.length; j++) {
						sb.append("\t\t")
						.append(arrReqs[j]) 
						.append("\n\n");
					}
				} 
			}
		} catch (Exception e) {
			//			ps.println("Mike: " + e.);
			//			e.printStackTrace(ps);

		}

		sb.append("\n\n");
		sb.append(rb.getString("Sender"));
		sb.append("\n\n\n\n");
		sb.append(rb.getString("Disclaimer1"));
		sb.append("\n\n");
		sb.append(rb.getString("Disclaimer2"));
		sb.append("\n\n");

		return sb;
	}

	public StringBuffer collectSMSTest(int mailType, String OrderTicketNo) 
			throws MessagingException{

		//ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.EmailContent");

		String []arrClientDetails = getClientDetails(OrderTicketNo);
		String clientLastName = arrClientDetails[0];
		String clientFirstName = arrClientDetails[1];
		String mfTxnType = arrClientDetails[2];
		for (int i=0; i < arrClientDetails.length; i++) {
			System.out.println("arrClientDetails[" + i + "]: " + arrClientDetails[i]);
		}

		StringBuffer sb = new StringBuffer();
		return sb;

	}

	public void sendEmailNotification(Message msg, String from, String to, String cc, String subject, int mailType, 
			String orderTicketNo, String entryDate){

		StringBuffer ccRecipient = new StringBuffer(cc);
		try {
			
			log("Email Notification --START--");
			
			List<MFLookupCodeDTO> mfLUCodeList = new ArrayList<MFLookupCodeDTO>();
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");

			for(MFLookupCodeDTO mfLUCodeDto : mfLUCodeList){
				if(StringUtils.isNotEmpty(ccRecipient.toString())){
					ccRecipient.append(",").append(mfLUCodeDto.getCODE_VALUE());
				} else {
					ccRecipient.append(mfLUCodeDto.getCODE_VALUE());
				}
			}

			if(StringUtils.isNotEmpty(cc)){
				if(StringUtils.isNotEmpty(ccRecipient.toString())){
					ccRecipient.append(",").append(cc);
				} else {
					ccRecipient.append(cc);
				}
			}

			log("Cc Recipient : "+ccRecipient.toString());

			setEmailRecipients(msg, from, to, ccRecipient.toString(), subject);

			collect(msg, mailType, orderTicketNo, extractTimePart(entryDate));
			
			if (mailType < 20 || mailType > 99) {
				Transport.send(msg);
			}
			
			log("Email Notification --END--");

		} catch (Exception e) {
			log("sendEmailNotification Exception : "+PrintExceptionHelper.exceptionStacktraceToString(e));
		}
	}

	public void sendSMSNotification(Message msg, String from, String to, String cc, String subject, int mailType, 
			String orderTicketNo, String entryDate){
		
		log("sendSMSNotification --START--");
		System.out.println("sendSMSNotification --START--");
		
		String body = null;
		StringBuffer toRecipient = new StringBuffer();
		StringBuffer ccRecipient = new StringBuffer();

		try {
			
			log("");
			log("Putting to sleep so that MFApplications will be updated with correct agent. "+orderTicketNo);
			log("This is for SMS only.");
			//System.out.println("This is for SMS only.");
			Thread.sleep(20000);  
			
			List<MFLookupCodeDTO> mfLUCodeList = new ArrayList<MFLookupCodeDTO>();
			List<MFLookupCodeDTO> mfCCRecipientList = new ArrayList<MFLookupCodeDTO>();

			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");

			for(MFLookupCodeDTO mfLUCodeDto : mfLUCodeList){
				if(StringUtils.isNotEmpty(toRecipient.toString())){
					toRecipient.append(",").append(mfLUCodeDto.getCODE_VALUE().trim());
				} else {
					toRecipient.append(mfLUCodeDto.getCODE_VALUE().trim());
				}
			}

			log("SMS to recipient :"+toRecipient.toString());

			mfCCRecipientList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = 'ISM02'");

			for(MFLookupCodeDTO mfCCRecipientDTO : mfCCRecipientList){
				if(StringUtils.isNotEmpty(ccRecipient.toString())){
					ccRecipient.append(",").append(mfCCRecipientDTO.getCODE_VALUE().trim());
				} else {
					ccRecipient.append(mfCCRecipientDTO.getCODE_VALUE().trim());
				}
			}

			MFApplicationsDAO mfAppDao = new MFApplicationsDAO();
			MFApplicationsDTO mfAppDto = new MFApplicationsDTO();

			mfAppDto = mfAppDao.getMFApplicationsDTO("where MFP_OrderTicketNumber='"+orderTicketNo+"'");

			MFLookupAgentDAO mfLUAgentDao = new MFLookupAgentDAO();
			List<MFLookupAgentDTO> mfLUAgentList = new ArrayList<MFLookupAgentDTO>();

			mfLUAgentList = mfLUAgentDao.getMFLookupAgentList("where CardNo='"+mfAppDto.getMFP_AgentID()+"' and (companycode is null or companycode <> 'GF') AND (MobileNo IS NOT NULL AND MobileNo != '')");
			
			if(mfLUAgentList.isEmpty() || mfLUAgentList == null){
				throw new Exception ("Agent Information is empty.");
			}
			MFLookupAgentDTO agentDTO = mfLUAgentList.get(0);

			if(agentDTO.getBranchId() != null){
				ccRecipient.append(",").append(smsPHNotBrnchPrefix + agentDTO.getBranchId());
			}

			log("SMS CC recipient :"+toRecipient.toString());

			log("");
			log("agentId : "+mfAppDto.getMFP_AgentID()); log("agentFName : "+agentDTO.getFirstName());
			log("agentLName : "+agentDTO.getLastName()); log("agentBranchId : "+agentDTO.getBranchId());
			log("agentMobile : "+agentDTO.getMobileNo()); log("agentEmail : "+agentDTO.getEmail());
			log("clientLName : "+mfAppDto.getMFP_ClientLName()); log("clientFName : "+mfAppDto.getMFP_ClientFName());
			log("");

			setEmailRecipients(msg, from, to, ccRecipient.toString(), subject);

			body = collectSMSText(msg, mailType, orderTicketNo, extractTimePart(entryDate));
			log("body "+body);
			
			body=body.trim();
			body = body.replaceAll(", : ", ": ");
			body = body.replaceAll(", . ", ". ");
			body = body.replaceAll("for ,", "for ");
			body = body.replaceAll("mtoday", "m today");
			body = body.replaceAll("'", "''");
			body = body.replaceAll("''", "'");
			body += "\n\n" + StringEscapeUtils.unescapeJava(DisclaimerEnum.getDisclaimer(DisclaimerType.EXTERNAL, Company.SLOCPI).getDisclaimerDTO().getData());
			
			boolean isSMSSend = false;
			try{
				NotificationHandler s = new NotificationHandler();
				if(StringUtils.isNotEmpty(agentDTO.getMobileNo())){
					isSMSSend = s.sendSMS_loc(from,agentDTO.getMobileNo(), "CLI", body, mfCompCode);
				} else {
					log("Skipping SMS Notification....");
					log("Cause : Agent Mobile number is null or empty");
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			if(mailType > 20 && mailType < 30){
				subject = smsSubjectFormat;
				subject = subject.replaceAll("<<AGENT_NAME>>", agentDTO.getFirstName()+" "+agentDTO.getLastName());
				subject = subject.replaceAll("<<AGENT_ID>>", mfAppDto.getMFP_AgentID());
				subject = subject.replaceAll("<<AGENT_MOBILE_NO>>", agentDTO.getMobileNo());
				subject = subject.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
				subject = subject.replaceAll("<<CLIENT_NAME>>", mfAppDto.getMFP_ClientFName()+" "+mfAppDto.getMFP_ClientLName());
				subject = subject.replaceAll("<<SMS_DATE>>", DateUtils.getFormattedDate(DateUtils.MONTH_DAY_WITH_TIME_FORMAT, new Date()));
				subject = subject.replaceAll("  ", " ");
				subject = subject.trim();
			}

			if(isSMSSend){
				subject = subject.replaceAll("<<SMS_STATUS>>", smsStatusFormatSuccess);
			}else{
				subject = subject.replaceAll("<<SMS_STATUS>>", smsSubjectFormatFailed);
			}

			if(agentDTO.getMobileNo() !=null && "".equals(agentDTO.getMobileNo()) &&
					agentDTO.getEmail()!=null && "".equals(agentDTO.getEmail())){
				subject = subject + " - no agent email address and agent mobile number";
			}else if((agentDTO.getMobileNo()!=null && "".equals(agentDTO.getMobileNo())) && 
					(agentDTO.getEmail() !=null && !"".equals(agentDTO.getEmail()))){
				subject = subject + " - no agent mobile number";
			}else if((agentDTO.getMobileNo() !=null && !"".equals(agentDTO.getMobileNo())) &&
					(agentDTO.getEmail() !=null && "".equals(agentDTO.getEmail()))){
				subject = subject + " - no agent email address";
			}

			msg.setSubject(subject);
			msg.setText(body);
			
			if (mailType < 20 || mailType > 99) {
				Transport.send(msg);
			}
			
			log("sendSMSNotification --END--");
		} catch (Exception e) {
			log("sendSMSNotification Exception : "+PrintExceptionHelper.exceptionStacktraceToString(e));
		}
	}

	public void setEmailRecipients(Message msg, String from, String to, String cc, String subject) throws MessagingException{

		if (StringUtils.isNotEmpty(from.trim())){
			msg.setFrom(new InternetAddress(from));
		}
		if (StringUtils.isNotEmpty(to.trim())) {
			InternetAddress []iaToRecipients = InternetAddress.parse(to);
			msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
		}
		if (StringUtils.isNotEmpty(cc.trim())) {
			InternetAddress []iaCcRecipients = InternetAddress.parse(cc);
			msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
		}
		if (StringUtils.isNotEmpty(subject)){
			msg.setSubject(subject);
		}
	}

	public static void main(String[] args) throws Exception {
		SendApp sa = new SendApp();
		//ORT0915001 PROMO
		//ORT0517009 NOT PROMO
		sa.send("Michael.Samson@sunlife.com","Michael.Samson@sunlife.com","","subject",23,"CP00003204","08/23/2012");
		System.out.println("wooooot");
	}
}
