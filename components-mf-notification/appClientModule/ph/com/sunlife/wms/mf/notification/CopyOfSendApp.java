package ph.com.sunlife.wms.mf.notification;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.activation.*;

import ph.com.sunlife.wms.mf.dao.MFApplicationsDAO;
import ph.com.sunlife.wms.mf.dao.MFLookupAgentDAO;
import ph.com.sunlife.wms.mf.dao.MFLookupCodeDAO;
import ph.com.sunlife.wms.mf.dto.MFApplicationsDTO;
import ph.com.sunlife.wms.mf.dto.MFLookupAgentDTO;
import ph.com.sunlife.wms.mf.dto.MFLookupCodeDTO;
import ph.com.sunlife.wms.mf.utils.DBConnect;
import sunlife.wms.nb.bl.NotificationHandler;

public class CopyOfSendApp {
	
	private PrintStream ps = null;
	private String fileName = null;
	private long maxLength = 10485760L;
	private int type1ParLen = 5;
	private int type2ParLen = 5;
	private int type3ParLen = 5;
	private int type4ParLen = 2;
	private int type5ParLen = 2;
	private int type6ParLen = 2;
	private int type21ParLen = 2;
	private int type22ParLen = 2;
	private int type23ParLen = 2;
	private int typeParLen;
	
	
	private final String mfMailInDB = "MFSMSMIDB";
	private final String mfCompCode = "MC";
	private final String mfSMSDtFormat = "MMM d, yyyy hh:mm:ss a";
	private final String smsSubjectFormat = "<<SMS_STATUS>> to <<AGENT_NAME>> (<<AGENT_ID>>) <<AGENT_MOBILE_NO>> on the account of <<ORDER_TICKET_NO>> - <<CLIENT_NAME>> on <<SMS_DATE>>";
	private final String smsStatusFormatSuccess = "SMS record saved in sms gateway";
	private final String smsSubjectFormatFailed = "Failed to save in sms gateway";
	private final String smsPHNotBrnchPrefix = "#PHIL-Notification-Brch-";
	
	
	
	//ResourceBundle rb2 = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
	private ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.ComponentProperties");
	//Andre Ceasar Dacanay - logging - start
	
	private BufferedWriter out = null;
	private boolean logging = true;
	
	public void initializeLog(){
	    try {
	    	
	    	//logging = rbLB.getString("simultaneousapp.logging").equalsIgnoreCase("ON");
	    	
	    	if(logging){
	    		out = new BufferedWriter(new FileWriter(rb.getString("WMS_LOG_PATH")+"\\MFNotificationLog.log", true));
	    	}

	        //out.write(aString);
	        //out.close();
	    } catch (IOException e) {
	    }
	}
	
	public void log(String aString){
	    try {
	    	if(logging){
	    		out.write(new Date() + " : "+ aString + "\n");
	    	}else{
	    		System.out.println(aString);
	    	}
	    } catch (IOException e) {
	    }
	}

	public void closeLog(){
	    try {
	    	if(logging){
	        out.close();
	    	}
	    } catch (IOException e) {
	    }
	}
	
	//Andre Ceasar Dacanay - logging - end
	
//	public boolean send(String smtpHost, int smtpPort,
	public boolean send(
            String from, String to, String cc,
            String subject, int mailType, String orderTicketNo,
            HashMap map)
		{
		
	// mailType: 1-Requirements, 2- Cancellation 
	
	// Create a mail session
//	initializeLog();	
	
	Properties props = new Properties();
//	if (mailType>20) { // For email, tried using dev SMS server. No go.
//		props.put("mail.smtp.host", "ln_apd_d01.ph.sunlife");
//	} else {
		props.put("mail.smtp.host", "ln_apd_g10.ph.sunlife");
//	}
	
//	props.put("mail.smtp.port", "25");
	Session session = Session.getDefaultInstance(props, null);
	// Construct the message
	Message msg = new MimeMessage(session);
	try {
		if (from != null && !from.trim().equals(""))
			msg.setFrom(new InternetAddress(from));
		if (to != null && !to.trim().equals("")) {
			//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			String []toRecipients = to.split(",");
			InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
			for (int i = 0; i < toRecipients.length; i++) {
				iaToRecipients[i] = new InternetAddress(toRecipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
		}
		if (cc != null && !cc.trim().equals("")) {
			//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
			String []ccRecipients = cc.split(",");
			InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
			for (int i = 0; i < ccRecipients.length; i++) {
				iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
			}
			msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
		}
		if (subject != null && !subject.trim().equals(""))
			msg.setSubject(subject);
		
		
		if (mailType < 20) {
			String dueTime = (map.get("dueTime") != null) ? (String) map.get("dueTime") : "";
			collect(msg, mailType, orderTicketNo, dueTime);
		} else if(mailType < 100){
			String dueTime = (map.get("dueTime") != null) ? (String) map.get("dueTime") : "";
			collectSMS(msg, mailType, orderTicketNo, dueTime);
		} else if(mailType < 200)
		{
			collectSMSOption(msg, subject, orderTicketNo);
		}
		else
		{
			customEmail(msg, subject); 
		}
		
		//msg.setText(content);
		
		if (map.get("invoker") != null && map.get("attachments") != null && map.get("remarks") != null) {
			String invoker = (String) map.get("invoker");
			String [] attachments = (String []) map.get("attachments"); 
			String remarks = (String) map.get("remarks");
			if (invoker.equals("emailDocs")) { 
				onEmailDocs(msg, attachments, remarks);		
			}
		} else {
//			System.out.println("ELSE");
//			String []attachments = null;
//			String remarks = "No document was selected";
//			onEmailDocs(msg, attachments, remarks);
		}
				
		// Send the message
		Transport.send(msg);
	} catch (AddressException e) {
		e.printStackTrace();
	} catch (MessagingException e) {
		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	return true;
	}
	
	public boolean send(
            String from, String to, String cc,
            String subject, int mailType, String orderTicketNo)
		{
		
		Properties props = new Properties();
	//	if (mailType>20) { // For email, tried using dev SMS server. No go.
	//		props.put("mail.smtp.host", "ln_apd_d01.ph.sunlife");
	//	} else {
			props.put("mail.smtp.host", "ln_apd_g10.ph.sunlife");
	//	}
		
		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		try {
			if (from != null && !from.trim().equals(""))
				msg.setFrom(new InternetAddress(from));
			if (to != null && !to.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals(""))
				msg.setSubject(subject);
			
			
			if (mailType < 20) {
				String dueTime = getDueTime(orderTicketNo); // Used by the component
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				String dueTime = getDueTime(orderTicketNo); // Used by the component
				collectSMS(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}
			
			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean send(
            String from, String to, String cc,
            String subject, String message)
		{
		
		Properties props = new Properties();
	//	if (mailType>20) { // For email, tried using dev SMS server. No go.
	//		props.put("mail.smtp.host", "ln_apd_d01.ph.sunlife");
	//	} else {
			props.put("mail.smtp.host", "ln_apd_g10.ph.sunlife");
	//	}
		
		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		try {
			if (from != null && !from.trim().equals(""))
				msg.setFrom(new InternetAddress(from));
			if (to != null && !to.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals(""))
				msg.setSubject(subject);

			collectEmail(msg, message);

			// Send the message
			Transport.send(msg);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	// Used by component
	public boolean send(
            String from, String to, String cc,
            String subject, int mailType, String orderTicketNo, String entryDate) 
		{
		
		initializeLog();
		log("");
		log("");
		log("before from "+from);
		log("before to "+to);
		log("before cc "+cc);
		log("before subject "+subject);
		log("before mailType "+mailType);
		log("before orderTicketNo "+orderTicketNo);
		log("before entryDate "+entryDate);
		
		Properties props = new Properties();
	//	if (mailType>20) { // For email, tried using dev SMS server. No go.
	//		props.put("mail.smtp.host", "ln_apd_d01.ph.sunlife");
	//	} else {
			props.put("mail.smtp.host", "ln_apd_g10.ph.sunlife");
	//	}
		
		Session session = Session.getDefaultInstance(props, null);
		// Construct the message
		Message msg = new MimeMessage(session);
		
		String ccFailed = null;
		String ccPassed = null;
		
		String agentId = null;
		String agentMobile = null;
		String agentEmail = null;
		String agentFName = null;
		String agentLName = null;
		String agentBranchId = null;
		String SMSDate = null;
		String clientLName = null;
		String clientFName = null;
		String body = null;
		if(mailType==21 || mailType==22 || mailType==23){

			//to
			//to = "adaca@sunlife.com,aposa@sunlife.com";
			MFLookupCodeDAO mfLUCodeDao = new MFLookupCodeDAO();
			List mfLUCodeList = new ArrayList();
			MFLookupCodeDTO mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = '"+mfMailInDB+"'");
			Iterator it = mfLUCodeList.iterator();
			int i = 1;
			while(it.hasNext()){
				mfLUCodeDto = (MFLookupCodeDTO)it.next();
				
				if(i==1){
					to = mfLUCodeDto.getCODE_VALUE();
				}else{
					to = to + ","+ mfLUCodeDto.getCODE_VALUE();
				}
			}
			
			MFApplicationsDAO mfAppDao = new MFApplicationsDAO();
			MFApplicationsDTO mfAppDto = new MFApplicationsDTO();
			
			mfAppDto = mfAppDao.getMFApplicationsDTO("where MFP_OrderTicketNumber='"+orderTicketNo+"'");
			agentId = mfAppDto.getMFP_AgentID();
			clientLName = mfAppDto.getMFP_ClientLName();
			clientFName = mfAppDto.getMFP_ClientFName();
			
			MFLookupAgentDAO mfLUAgentDao = new MFLookupAgentDAO();
			List mfLUAgentList = new ArrayList();
			MFLookupAgentDTO mfLUAgentDto = null;
			mfLUAgentList = mfLUAgentDao.getMFLookupAgentList("where CardNo='"+agentId+"' and CompanyCode='"+mfCompCode+"'");
			it = mfLUAgentList.iterator();
			while(it.hasNext()){
				mfLUAgentDto = (MFLookupAgentDTO)it.next();
				agentMobile = mfLUAgentDto.getMobileNo();
				agentEmail = mfLUAgentDto.getEmail();
				agentFName = mfLUAgentDto.getFirstName();
				agentLName = mfLUAgentDto.getLastName();
				agentBranchId = mfLUAgentDto.getBranchId();
			}

			DateFormat formatter = new SimpleDateFormat(mfSMSDtFormat);
			try {
				SMSDate = formatter.format(new Date());
			} catch (Exception e) {
			}
			log("");
			log("agentId : "+agentId);
			log("agentFName : "+agentFName);
			log("agentLName : "+agentLName);
			log("agentBranchId : "+agentBranchId);
			log("agentMobile : "+agentMobile);
			log("agentEmail : "+agentEmail);
			log("SMSDate : "+SMSDate);
			log("clientLName : "+clientLName);
			log("clientFName : "+clientFName);
			log("");
			
			//cc
			//'ISM02', #PHIL-Notification-Brch-
			
			
			mfLUCodeDao = new MFLookupCodeDAO();
			mfLUCodeList = new ArrayList();
			mfLUCodeDto = null;
			mfLUCodeList = mfLUCodeDao.getMFLookupCodeList("where CODE_ID = 'ISM02'");
			it = mfLUCodeList.iterator();
			i = 1;
			while(it.hasNext()){
				mfLUCodeDto = (MFLookupCodeDTO)it.next();
				
				if(i==1){
					cc = mfLUCodeDto.getCODE_VALUE();
					ccFailed = mfLUCodeDto.getCODE_VALUE();
				}else{
					cc = cc + ","+ mfLUCodeDto.getCODE_VALUE();
					ccFailed = ccFailed + ","+ mfLUCodeDto.getCODE_VALUE();
				}
			}
			
			
			if(agentBranchId!=null && !agentBranchId.equals("")){
				cc = cc + ","+smsPHNotBrnchPrefix+agentBranchId;
			}
			
			
			if(agentBranchId!=null && !agentBranchId.equals("")){
				ccPassed = smsPHNotBrnchPrefix+agentBranchId;
			}
			
			log("mailType==21 || mailType==22 || mailType==23 to "+to);
			log("mailType==21 || mailType==22 || mailType==23 cc "+cc);
		}
		
		try {
			if (from != null && !from.trim().equals("")){
				msg.setFrom(new InternetAddress(from));
			}
			if (to != null && !to.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
				String []toRecipients = to.split(",");
				InternetAddress []iaToRecipients= new InternetAddress[toRecipients.length];
				for (int i = 0; i < toRecipients.length; i++) {
					iaToRecipients[i] = new InternetAddress(toRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, iaToRecipients);
			}
			if (cc != null && !cc.trim().equals("")) {
				//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
				String []ccRecipients = cc.split(",");
				InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
				}
				msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
			}
			if (subject != null && !subject.trim().equals("")){
				msg.setSubject(subject);
			}

			log("");
			log("from "+from);
			log("to "+to);
			log("cc "+cc);
			log("subject "+subject);
			log("mailType "+mailType);
			log("orderTicketNo "+orderTicketNo);
			log("entryDate "+entryDate);
			log("");
			
			if (mailType < 20) {
				String dueTime = extractTimePart(entryDate);
				log("dueTime "+dueTime);
				collect(msg, mailType, orderTicketNo, dueTime);
			} else if(mailType < 100){
				String dueTime = extractTimePart(entryDate);
				log("dueTime "+dueTime);
				body = collectSMSText(msg, mailType, orderTicketNo, dueTime);
				log("body "+body);
				body=body.trim();
				body = body.replaceAll(", . ", ". ");
				body = body.replaceAll("for ,", "for ");
				body = body.replaceAll("'", "''");
				//collectSMS(msg, mailType, orderTicketNo, dueTime);
				// Gateway SMS Notification - Andre Ceasar Dacanay - start
				boolean a = false;
				try{
					NotificationHandler s = new NotificationHandler();
					//a = s.sendSMS(from,agentMobile, "CLI", body);
					log("agentMobile : "+agentMobile);
					log("body after "+body);
					//a = s.sendSMS("wms@sunlife.com",agentMobile , "CLI", body);
					log("sending of sms thru gateway "+a);
				}catch(Exception e){
					log("Exception s.sendSMS e "+e.getCause());
					log("Exception s.sendSMS e "+e.getMessage());
					log("Exception s.sendSMS e "+e.getStackTrace());
					log("");
					log("");
				}
				
				agentFName = agentFName!=null ? agentFName : "";
				agentLName = agentLName!=null ? agentLName : "";
				agentId = agentId!=null ? agentId : "";
				agentMobile = agentMobile!=null ? agentMobile : "";
				orderTicketNo = orderTicketNo!=null ? orderTicketNo : "";
				clientFName = clientFName!=null ? clientFName : "";
				SMSDate = SMSDate!=null ? SMSDate : "";
				
				if(mailType==21 || mailType==22 || mailType==23){
					subject = smsSubjectFormat;
					subject = subject.replaceAll("<<AGENT_NAME>>", agentFName+" "+agentLName);
					subject = subject.replaceAll("<<AGENT_ID>>", agentId);
					subject = subject.replaceAll("<<AGENT_MOBILE_NO>>", agentMobile);
					subject = subject.replaceAll("<<ORDER_TICKET_NO>>", orderTicketNo);
					subject = subject.replaceAll("<<CLIENT_NAME>>", clientFName+" "+clientLName);
					subject = subject.replaceAll("<<SMS_DATE>>", SMSDate);
					subject = subject.replaceAll("  ", " ");
					subject = subject.trim();
				}

				if(a){
					subject = subject.replaceAll("<<SMS_STATUS>>", smsStatusFormatSuccess);
					if (ccPassed != null && !ccPassed.trim().equals("")) {
						//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
						String []ccRecipients = ccPassed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}else{
					subject = subject.replaceAll("<<SMS_STATUS>>", smsSubjectFormatFailed);
					if (ccFailed != null && !ccFailed.trim().equals("")) {
						//msg.setRecipient(Message.RecipientType.CC, new InternetAddress(cc));
						String []ccRecipients = ccFailed.split(",");
						InternetAddress []iaCcRecipients = new InternetAddress[ccRecipients.length];
						for (int i = 0; i < ccRecipients.length; i++) {
							iaCcRecipients[i] = new InternetAddress(ccRecipients[i]);
						}
						msg.setRecipients(Message.RecipientType.CC, iaCcRecipients);
					}
				}
				
				if(mailType==21 || mailType==22 || mailType==23){
					msg.setSubject(subject);
					msg.setDataHandler(new DataHandler(new ByteArrayDataSource(body, "text/rtf")));
				}
				log("Audit Email subject after "+subject);
				// Gateway SMS Notification - Andre Ceasar Dacanay - end
				
			} else if(mailType < 200)
			{
				collectSMSOption(msg, subject, orderTicketNo);
			}
			else
			{
				customEmail(msg, subject); 
			}
			
			// Send the message
			//Transport.send(msg);
		} catch (AddressException e) {
			
			log("AddressException e "+e.getCause());
			log("AddressException e "+e.getMessage());
			log("AddressException e "+e.getStackTrace());
			
			e.printStackTrace();
			//closeLog();
		} catch (MessagingException e) {
			
			log("MessagingException e "+e.getCause());
			log("MessagingException e "+e.getMessage());
			log("MessagingException e "+e.getStackTrace());
			
			e.printStackTrace();
			//closeLog();
		} catch (Exception e) {
			
			log("Exception e "+e.getCause());
			log("Exception e "+e.getMessage());
			log("Exception e "+e.getStackTrace());
			
			e.printStackTrace();
			//closeLog();
		}finally{
			closeLog();
		}
		
		
		return true;
	}
	
	private String extractTimePart(String entryDate) {
		DateFormat formatter = new SimpleDateFormat("MMM d, yyyy hh:mm:ss a");
		String timePartEntryDate = "";
		String dueTime = "";
		try {
			Date date = (Date) formatter.parse(entryDate);
			Format format = new SimpleDateFormat("HH:mm:ss");
			timePartEntryDate = format.format(date);
			dueTime = determineCutOff(timePartEntryDate);
		} catch (ParseException e) {
			//e.printStackTrace();
		}
		return dueTime;
		
	}
	
	private String determineCutOff(String timePartEntryDate) {
		String dueTime = "";
		Time timeValueEntryDate = Time.valueOf(timePartEntryDate);
		Time timeSet1 = Time.valueOf("8:00:00");
		Time timeSet2 = Time.valueOf("11:01:00");
		Time timeSet3 = Time.valueOf("14:01:00");
		Time timeSet4 = Time.valueOf("15:00:00");
		if (!timeValueEntryDate.before(timeSet1) 
				&& timeValueEntryDate.before(timeSet2)) {
			dueTime = "12:00pm ";
		} else if (!timeValueEntryDate.before(timeSet2) 
				&& timeValueEntryDate.before(timeSet3)) {
			dueTime = "3:00pm ";
		} else if (!timeValueEntryDate.before(timeSet3) 
				&& timeValueEntryDate.before(timeSet4)) {
			dueTime = "4:30pm ";
		} else {
			
		}
		return dueTime;
	}

	// Same method used in Requirement Abeyance Process class
	private String getDueTime(String orderTicketNo) {
		Timestamp entryDateTime = null;
		try {
			DBConnect dbc = new DBConnect();
			Connection con = dbc.connect();
			Statement stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("SELECT ENTRY_DATETIME FROM MF_NOTIFY_SCHED WHERE ORDER_TICKET_NO='")
					.append(orderTicketNo)
					.append("'");
			ResultSet rs = stmt.executeQuery(query.toString());
			
			String remarks = "";
			while (rs.next()) {
	            entryDateTime = rs.getTimestamp("ENTRY_DATETIME");
	        }
			rs.close();
			stmt.close();
			con.close();
		} catch(Exception e) {
			
		}
		System.out.println("getDueTime() : Entry");
		System.out.println("getDueTime() : entryDateTime=" + entryDateTime);
		String dueTime = "";
		Format formatter = new SimpleDateFormat("HH:mm:ss");
		String timeOfEntryDate = formatter.format(entryDateTime);
		Time timeValueEntryDate = Time.valueOf(timeOfEntryDate);
		Time timeSet1 = Time.valueOf("8:00:00");
		Time timeSet2 = Time.valueOf("11:01:00");
		Time timeSet3 = Time.valueOf("14:01:00");
		Time timeSet4 = Time.valueOf("15:00:00");
		if (!timeValueEntryDate.before(timeSet1) 
				&& timeValueEntryDate.before(timeSet2)) {
			dueTime = "12:00pm";
		} else if (!timeValueEntryDate.before(timeSet2) 
				&& timeValueEntryDate.before(timeSet3)) {
			dueTime = "3:00pm";
		} else if (!timeValueEntryDate.before(timeSet3) 
				&& timeValueEntryDate.before(timeSet4)) {
			dueTime = "4:30pm";
		} else {
			System.out.println("getDueTime() : Did not qualify to conditions");
		}
		System.out.println("getDueTime() : dueTime="+dueTime);
		System.out.println("getDueTime() : Exit");
		return dueTime;
	}
	
	public void onEmailDocs(Message msg, String attachments[], String remarks) {
		try {
			BodyPart mbp[] = new BodyPart[attachments.length];
			Multipart multipart = new MimeMultipart();
			String filename = "";
			DataSource source = null;
			
			for (int i = 0; i < attachments.length; i++) {
				mbp[i] = new MimeBodyPart();
				filename = attachments[i];
				source = new FileDataSource(attachments[i]);
				mbp[i].setDataHandler(new DataHandler(source));
				mbp[i].setFileName(attachments[i]);
				multipart.addBodyPart(mbp[i]);
			}
			
			// Add text 
			BodyPart bp = new MimeBodyPart();
			bp.setText("\n" + remarks);
			multipart.addBodyPart(bp);
			
//			BodyPart messageBodyPart1 = new MimeBodyPart();
//			messageBodyPart1.setText(" ");
//			Multipart multipart = new MimeMultipart();
//			multipart.addBodyPart(messageBodyPart1);
//			messageBodyPart1 = new MimeBodyPart();
//			String filename = "d:\\mike\\a.txt";
//			DataSource source = new FileDataSource(filename);
//			//DataSource source = new URLDataSource(url);
//			messageBodyPart1.setDataHandler(new DataHandler(source));
//			messageBodyPart1.setFileName(filename);
//			multipart.addBodyPart(messageBodyPart1);
//			filename = "d:\\mike\\b.txt";
//			BodyPart msbp2 = new MimeBodyPart();
//			source = new FileDataSource(filename);
//			msbp2.setDataHandler(new DataHandler(source));
//			msbp2.setFileName(filename);
//			multipart.addBodyPart(msbp2);
			
			msg.setContent(multipart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void collectSMS(Message msg, int mailType, String orderTicketNo, String dueTime) 
	throws MessagingException {
	
	ResourceBundle rb = ResourceBundle.getBundle("EmailLines");
	
	String []arrClientDetails = getClientDetails(orderTicketNo);
	String clientLastName = arrClientDetails[0];
	String clientFirstName = arrClientDetails[1];
	String mfTxnType = arrClientDetails[2];
	StringBuffer sb = new StringBuffer();
	
	String mType = null;
	switch(mailType) {
		case 21:
			String reqts[] = getRequirements(orderTicketNo);
			mType = "SMSReq"; // SMS Requirement Notification
			typeParLen = type21ParLen;
			sb.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(". ");
					for(int i=0; i<reqts.length; i++) {
						if (i>0) {
							sb.append(", ");
						}
						if (reqts[i].indexOf("-") != -1) {
							sb.append(reqts[i].substring(0, reqts[i].indexOf("-")-1));
						} else {
							sb.append(reqts[i]);
						}
					}
					sb.append(rb.getString(mType+"Par2").replaceAll("12 nn ", dueTime));
			break;
		case 22:
			mType = "SMSCan"; // SMS Cancellation Notification
			typeParLen = type22ParLen;
			sb.append(mfTxnType)
				.append(" ")
				.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(" ")
				.append(rb.getString(mType+"Par2"));
			break;
		case 23:
			mType = "SMSRef"; // SMS Notice of Refund
			typeParLen = type23ParLen;
			sb.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(" ")
				.append(rb.getString(mType+"Par2"));
			break;
		default: break;
	}
	msg.setText(sb.toString());
}
	
	public String collectSMSText(Message msg, int mailType, String orderTicketNo, String dueTime) 
	throws MessagingException {
	
	ResourceBundle rb = ResourceBundle.getBundle("EmailLines");
	
	String []arrClientDetails = getClientDetails(orderTicketNo);
	String clientLastName = arrClientDetails[0];
	String clientFirstName = arrClientDetails[1];
	String mfTxnType = arrClientDetails[2];
	StringBuffer sb = new StringBuffer();
	
	String mType = null;
	switch(mailType) {
		case 21:
			String reqts[] = getRequirements(orderTicketNo);
			mType = "SMSReq"; // SMS Requirement Notification
			typeParLen = type21ParLen;
			sb.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(". ");
					for(int i=0; i<reqts.length; i++) {
						if (i>0) {
							sb.append(", ");
						}
						if (reqts[i].indexOf("-") != -1) {
							sb.append(reqts[i].substring(0, reqts[i].indexOf("-")-1));
						} else {
							sb.append(reqts[i]);
						}
					}
					sb.append(rb.getString(mType+"Par2").replaceAll("12 nn ", dueTime));
			break;
		case 22:
			mType = "SMSCan"; // SMS Cancellation Notification
			typeParLen = type22ParLen;
			sb.append(mfTxnType)
				.append(" ")
				.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(" ")
				.append(rb.getString(mType+"Par2"));
			break;
		case 23:
			mType = "SMSRef"; // SMS Notice of Refund
			typeParLen = type23ParLen;
			sb.append(rb.getString(mType+"Par1"))
				.append(" ")
				.append(clientLastName)
				.append(", ")
				.append(clientFirstName)
				.append(" ")
				.append(rb.getString(mType+"Par2"));
			break;
		default: break;
	}
	return sb.toString();
}

	public void collectSMSOption(Message msg, String subject, String orderTicketNo) 
		throws MessagingException {
		typeParLen = type21ParLen;
		msg.setText(subject);
	}
	
	public void collectEmail(Message msg, String message) 
	throws MessagingException {
	typeParLen = type21ParLen;
	msg.setText(message);
}

	public void customEmail(Message msg, String message) 
	throws MessagingException{
	
	
	msg.setDataHandler(new DataHandler(new ByteArrayDataSource(message, "text/rtf")));
}
	
	public void collect(Message msg, int mailType, String OrderTicketNo, String dueTime) 
		throws MessagingException{
		
//		ResourceBundle rb = ResourceBundle.getBundle("ph.com.sunlife.wms.mf.notification.file");
//		ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.EmailContent");
		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");
		
		
		StringBuffer sb = new StringBuffer();
		
		String mType = null;
		switch(mailType) {
			case 1:
				mType = "Req"; // Order Requirements
				typeParLen = type1ParLen;
				break;
			case 2:
				mType = "Can"; // Notification for Cancellation
				typeParLen = type2ParLen;
				break;
			case 3:
				mType = "Pay"; // Payment Ready
				typeParLen = type3ParLen;
				break;
			case 4: 
				mType = "Cor"; // For Payment Correction
				typeParLen = type4ParLen;
				break;
			case 5:
				mType = "Fin"; // Cancellation of MF Transactions to Finance
				typeParLen = type5ParLen;
				break;
			case 6:
				mType = "Ref"; // For Refund
				typeParLen = type6ParLen;
				break;
			default: break;
		}
		
		sb.append(rb.getString("Salutation"));
		sb.append("\n\n");
		
		try {
			for (int i = 1; i <= typeParLen; i++) {
				sb.append(rb.getString(mType + "Par" + i).replaceAll("12noon ", dueTime));
				sb.append("\n\n");
	//			String OrderTicketNo = "";
				if (mailType == 1 && i == 1) {
					String []arrReqs = getRequirements(OrderTicketNo);
					for (int j = 0; j < arrReqs.length; j++) {
						sb.append("\t\t")
								.append(arrReqs[j]) 
								.append("\n\n");
					}
	//				sb.append("\t\t<Itemize requirements>\n\t\tOthers: Additional valid Id");
	//				sb.append("\n\n");
				} 
			}
		} catch(Exception e) {
//			e.printStackTrace(ps);
		}
						
		sb.append("\n\n");
		sb.append(rb.getString("Sender"));
		sb.append("\n\n\n\n");
		sb.append(rb.getString("Disclaimer1"));
		sb.append("\n\n");
		sb.append(rb.getString("Disclaimer2"));
		sb.append("\n\n");
		
		msg.setDataHandler(new DataHandler(new ByteArrayDataSource(sb.toString(), "text/rtf")));
	}
	
	private String[] getRequirements(String OrderTicketNo){
		String []arrReqs = null;
		List listRequirements= new ArrayList();
		try {
			DBConnect dbc = new DBConnect();
			Connection con = dbc.connect();
			Statement stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("select DISTINCT mflr.rm_requirement_desc as REQUIREMENTS, REMARKS from MF_LOOKUP_REQUIREMENTS as mflr, mf_requirements as mfr where mfr.rm_requirement_type = mflr.rm_requirement_ID and mfr.ot_ticket_number = '")
					.append(OrderTicketNo)
					.append("'");
			ResultSet rs = stmt.executeQuery(query.toString());
			String reqType = "";
			String remarks = "";
			while (rs.next()) {
	            reqType = rs.getString("REQUIREMENTS");
	            remarks = rs.getString("REMARKS");
//	            System.out.println("Requirement Type: " + reqType);
	            if (remarks != null) {
		            if (remarks.toString().equals(""))
		            	listRequirements.add(reqType);
		            else if(reqType.indexOf("Other")!=-1){
		            	listRequirements.add(remarks);
		            }
		            else{
		            	listRequirements.add(reqType+" - "+remarks);
		            }
	            } else {
	            	listRequirements.add(reqType);
	            }
	        }
			rs.close();
			stmt.close();
			con.close();
			arrReqs = (String[]) listRequirements.toArray(new String [listRequirements.size()]);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return arrReqs;
	}
	
	private String[] getClientDetails(String OrderTicketNo) {
//		String []clientDetails = null;
		String []arrClientDetails = new String[3];
		try {
			DBConnect dbc = new DBConnect();
			Connection con = dbc.connect();
			Statement stmt = con.createStatement();
			StringBuffer query = new StringBuffer();
			query.append("select MFP_ClientLName, MFP_ClientFName, MFP_MFTransactionType from mfapplications where MFP_OrderTicketNumber = '")
					.append(OrderTicketNo)
					.append("'");
			ResultSet rs = stmt.executeQuery(query.toString());
			String clientLastName = "";
			String clientFirstName = "";
			String mfTxnType = "";
			while (rs.next()) {
				clientLastName = rs.getString("MFP_ClientLName");
				clientFirstName = rs.getString("MFP_ClientFName");
				mfTxnType = rs.getString("MFP_MFTransactionType");
				arrClientDetails[0] = clientLastName;
				arrClientDetails[1] = clientFirstName;
				arrClientDetails[2] = mfTxnType;
	        }
		} catch(Exception e) {
			e.printStackTrace();
		}
		return arrClientDetails;
	}
	
	private void initializeLog1(){
		/*
		if(fileName == null) 
			fileName = (rb2.getString("WMS_LOG_PATH")+ "\\" + "MFEmailModule.log");
		File file = new File(fileName);
		
		if(file.length() == 0L){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			ps = new PrintStream(new FileOutputStream(file, true));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		*/
	}
	
	public StringBuffer collectTest(int mailType, String OrderTicketNo, String dueTime) 
	throws MessagingException{
	
		ResourceBundle rb = ResourceBundle.getBundle("EmailLines");
		
		StringBuffer sb = new StringBuffer();
		
		String mType = null;
		switch(mailType) {
			case 1:
				mType = "Req"; // Order Requirements
				typeParLen = type1ParLen;
				break;
			case 2:
				mType = "Can"; // Notification for Cancellation
				typeParLen = type2ParLen;
				break;
			case 3:
				mType = "Pay"; // Payment Ready
				typeParLen = type3ParLen;
				break;
			case 4: 
				mType = "Cor"; // For Payment Correction
				typeParLen = type4ParLen;
				break;
			case 5:
				mType = "Fin"; // Cancellation of MF Transactions to Finance
				typeParLen = type5ParLen;
				break;
			case 6:
				mType = "Ref"; // For Refund
				typeParLen = type6ParLen;
				break;
			case 21:
				mType = "SMSReq"; // SMS Requirement Notification
				typeParLen = type21ParLen;
				break;
			case 22:
				mType = "SMSCan"; // SMS Cancellation Notification
				typeParLen = type22ParLen;
				break;
			case 23:
				mType = "SMSRef"; // SMS Notice of Refund
				typeParLen = type23ParLen;
				break;
			default: break;
		}
		
		sb.append(rb.getString("Salutation"));
		sb.append("\n\n");
		
		try {
			for (int i = 1; i <= typeParLen; i++) {
				sb.append(rb.getString(mType + "Par" + i).replaceAll("12noon", dueTime));
				sb.append("\n\n");
				if (mailType == 1 && i == 1) {
					String []arrReqs = getRequirements(OrderTicketNo);
					for (int j = 0; j < arrReqs.length; j++) {
						sb.append("\t\t")
								.append(arrReqs[j]) 
								.append("\n\n");
					}
				} 
			}
		} catch (Exception e) {
//			ps.println("Mike: " + e.);
//			e.printStackTrace(ps);
			
		}
						
		sb.append("\n\n");
		sb.append(rb.getString("Sender"));
		sb.append("\n\n\n\n");
		sb.append(rb.getString("Disclaimer1"));
		sb.append("\n\n");
		sb.append(rb.getString("Disclaimer2"));
		sb.append("\n\n");
		
		return sb;
	}
	
	public StringBuffer collectSMSTest(int mailType, String OrderTicketNo) 
	throws MessagingException{
	
		//ResourceBundle rb = ResourceBundle.getBundle("com.ph.sunlife.component.properties.EmailContent");
		
		String []arrClientDetails = getClientDetails(OrderTicketNo);
		String clientLastName = arrClientDetails[0];
		String clientFirstName = arrClientDetails[1];
		String mfTxnType = arrClientDetails[2];
		for (int i=0; i < arrClientDetails.length; i++) {
			System.out.println("arrClientDetails[" + i + "]: " + arrClientDetails[i]);
		}
		
		StringBuffer sb = new StringBuffer();
		/*
		String mType = null;
		switch(mailType) {
			case 21:
				String reqts[] = getRequirements(OrderTicketNo);
				mType = "SMSReq"; // SMS Requirement Notification
				typeParLen = type21ParLen;
				sb.append(rb.getString(mType+"Par1"))
					.append(" ")
					.append(clientLastName)
					.append(", ")
					.append(clientFirstName)
					.append(". ");
					for(int i=0; i<reqts.length; i++) {
						if (i>0) {
							sb.append(", ");
						}
						if (reqts[i].indexOf("-") != -1) {
							sb.append(reqts[i].substring(0, reqts[i].indexOf("-")-1));
						} else {
							sb.append(reqts[i]);
						}
					}
					sb.append(rb.getString(mType+"Par2"));
				break;
			case 22:
				mType = "SMSCan"; // SMS Cancellation Notification
				typeParLen = type22ParLen;
				sb.append(mfTxnType)
					.append(" ")
					.append(rb.getString(mType+"Par1"))
					.append(" ")
					.append(clientLastName)
					.append(", ")
					.append(clientFirstName)
					.append(" ")
					.append(rb.getString(mType+"Par2"));
				break;
			case 23:
				mType = "SMSRef"; // SMS Notice of Refund
				typeParLen = type23ParLen;
				sb.append(rb.getString(mType+"Par1"))
					.append(" ")
					.append(clientLastName)
					.append(", ")
					.append(clientFirstName)
					.append(" ")
					.append(rb.getString(mType+"Par2"));
				break;
			default: break;
		}
						
//		sb.append("\n\n");
//		sb.append(rb.getString("Sender"));
//		sb.append("\n\n\n\n");
//		sb.append(rb.getString("Disclaimer1"));
//		sb.append("\n\n");
//		sb.append(rb.getString("Disclaimer2"));
//		sb.append("\n\n");
		*/
		return sb;
		
	}
	
	public static void main(String[] args) throws Exception {
	// Send a test message
	//SendApp sa = new SendApp();
//	sa.initializeLog();
//	sa.send("ln_apd_g10.ph.sunlife", 25, "pf32@sunlife.com", "pf32@sunlife.com",
//			"mmang@sunlife.com", "SubjectText", 5);
//	sa.send("pf32@sunlife.com", "pf32@sunlife.com",
//	sa.send("mmang@sunlife.com", "mmang@sunlife.com",
//			"mmang@sunlife.com", "SubjectText", 5, "Dummy OTN");
	
//	String [] reqs = sa.getRequirements("MM03142115");
//		for (int i= 0; i < reqs.length; i++) {
//			System.out.println("reqs[" + i + "]: " + reqs[i]);
//		}
	//StringBuffer sb = sa.collectTest(22, "MM03142115");
//	StringBuffer sb = sa.collectTest(1, "N200822015");
//	StringBuffer sb = sa.collectTest(1, "MM04081315");
//	StringBuffer sb = sa.collectTest(1, "MM04031010");
//	StringBuffer sb = sa.collectSMSTest(21, "S200827061");
//	StringBuffer sb = sa.collectSMSTest(21, "MM04031010");
//	System.out.println(sb);

//	sa.send("mmang@sunlife.com", "+639157123383@SMSuat.PH.Sunlife",
//			"", "", 22, "S200827061");
	//String [] attachments = {"d:\\mike\\a.txt","d:\\mike\\b.txt"};
	//String remarks = "";
	
	//HashMap map = new HashMap();
//	map.put("invoker", "emailDocs");
//	map.put("attachments", attachments);
//	map.put("remarks", remarks);
	//System.out.println(sa.collectTest(1, "MM07221150", "3:00pm"));
	//sa.getDueTime("MM07221150");
	//sa.send("me", "po17@sunlife.com", "", "Test", 21, "MM07221150");
//	sa.send("me", "po17@sunlife.com", "", "Test", 1, "MM07221150");
//	sa.send("mmang@sunlife.com", "mmang@sunlife.com", "", " ", 1, "TEST000002", map);
//	sa.send("mmang@sunlife.com", "mmang@sunlife.com", "", "		", 22, "S200827061");
//	sa.send("mmang@sunlife.com", "mmang@sunlife.com", "", "", 23, "S200827061");
//	sa.send("wms@sunlife.com","+639157123383@SMSuat.PH.Sunlife","","",21, "S200827061");
//	sa.send("wms@sunlife.com","+639157123383@SMSuat.PH.Sunlife","","",22, "S200827061");
//	sa.send("wms@sunlife.com","aabro@sunlife.com","","TEST SMS CUSTOM",200, "S200827061");
	NotificationHandler s = new NotificationHandler();
	s.sendSMS("wms@sunlife.com","+639273145965", "CLI", "Pls submit pending requirement for Michael Scofield. Others, Request Form/letter. We'll await by 3:00pm today. Thank you.");
	}
	

}
