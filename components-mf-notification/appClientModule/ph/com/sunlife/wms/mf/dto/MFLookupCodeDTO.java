package ph.com.sunlife.wms.mf.dto;

import java.util.List;
import java.util.Vector;

public class MFLookupCodeDTO {

	private String CODE_ID;
	private String CODE_TYPE; 
	private String CODE_VALUE;
	private String CODE_DESC;
	private List ObjectList = new Vector();
	
	public void add(MFLookupCodeDTO dto) {
		ObjectList.add(dto);
	}

	public List getObjectList() {
		return ObjectList;
	}

	public String getCODE_DESC() {
		return CODE_DESC;
	}

	public void setCODE_DESC(String code_desc) {
		CODE_DESC = code_desc;
	}

	public String getCODE_ID() {
		return CODE_ID;
	}

	public void setCODE_ID(String code_id) {
		CODE_ID = code_id;
	}

	public String getCODE_TYPE() {
		return CODE_TYPE;
	}

	public void setCODE_TYPE(String code_type) {
		CODE_TYPE = code_type;
	}

	public String getCODE_VALUE() {
		return CODE_VALUE;
	}

	public void setCODE_VALUE(String code_value) {
		CODE_VALUE = code_value;
	}

}
