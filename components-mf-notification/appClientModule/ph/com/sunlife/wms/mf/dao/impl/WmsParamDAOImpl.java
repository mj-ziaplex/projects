package ph.com.sunlife.wms.mf.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ph.com.sunlife.wms.mf.dao.WmsParamDAO;
import ph.com.sunlife.wms.mf.utils.DBConnect;

public class WmsParamDAOImpl implements WmsParamDAO {
	public String getWmsParamValue(String param) {
		DBConnect db = new DBConnect();
		String notfiDecision = null;
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		try {
			StringBuffer query = new StringBuffer();

			connection = db.connect();
			statement = connection.createStatement();

			query.append("SELECT WMSP_Value ").append("FROM WMSParam ").append("WHERE WMSP_ParamField = '")
					.append(param).append("'");

			rs = statement.executeQuery(query.toString());
			while (rs.next()) {
				notfiDecision = rs.getString("WMSP_Value");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqe1) {
					sqe1.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException sqe2) {
					sqe2.printStackTrace();
				}
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return notfiDecision;
	}
}
