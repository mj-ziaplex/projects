package ph.com.sunlife.wms.mf.dao.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ph.com.sunlife.wms.jdbc.DataRow;
import ph.com.sunlife.wms.jdbc.DataRowCollection;
import ph.com.sunlife.wms.jdbc.SQLServerTemplate;
import ph.com.sunlife.wms.mf.dao.WMSEmailTemplateDAO;
import ph.com.sunlife.wms.mf.dto.WMSEmailTemplateDTO;
import ph.com.sunlife.wms.mf.utils.Initializer;

public class WMSEmailTemplateDAOImpl implements WMSEmailTemplateDAO {

	private static SQLServerTemplate sqlServerTemplate = null;
	
	static {
		try {
			sqlServerTemplate = Initializer.getDb();
		} catch (Exception e) {
			System.out.println("Cannot initialize sql server template");
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public List<WMSEmailTemplateDTO> getAllEmailTemplates() throws SQLException, IOException {
		List<WMSEmailTemplateDTO> emailTemplateDTOList = new ArrayList<WMSEmailTemplateDTO>();
		WMSEmailTemplateDTO emailTemplateDTO = null;
		
		String sql = "SELECT ID, EN_CODE_ID, EN_CODE_TYPE, EN_DESCRIPTION, EN_COMPANY_CODE, EN_DATA FROM WMS_Email_Template";
		
		DataRowCollection rows = sqlServerTemplate.findAll(sql);
		
		for (DataRow row : rows) {
			emailTemplateDTO = new WMSEmailTemplateDTO();
			emailTemplateDTO.setId(row.getString("ID"));
			emailTemplateDTO.setCodeId(row.getString("EN_CODE_ID"));
			emailTemplateDTO.setCodeType(row.getString("EN_CODE_TYPE"));
			emailTemplateDTO.setCompanyCode(row.getString("EN_COMPANY_CODE"));
			emailTemplateDTO.setData(row.getString("EN_DATA"));
			emailTemplateDTO.setDescription(row.getString("EN_DESCRIPTION"));
			
			emailTemplateDTOList.add(emailTemplateDTO);
		}
		
		return emailTemplateDTOList;
	}

}
